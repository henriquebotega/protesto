'use strict';

var serveStatic = require('serve-static');

module.exports = function (grunt) {

    require('load-grunt-tasks')(grunt);
    require('time-grunt')(grunt);

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        // Alimenta o contador de versão de build
        bumpup: {
            options: {
                updateProps: {
                    pkg: 'package.json'
                }
            },
            file: 'package.json'
        },

        // Limpa os diretorios para iniciar novo build
        clean: ["dist", '.tmp'],

        // Sobe o serviço web
        connect: {
            options: {
                port: 9000,
                base: 'app',
                open: true,
                livereload: 35729,
                hostname: '0.0.0.0'
            },
            livereload: {
                options: {
                    middleware: function (connect) {
                        return [
                            serveStatic('.tmp'),
                            connect().use('/node_modules', serveStatic('./node_modules')),
                            serveStatic('app')
                        ];
                    }
                }
            }
        },

        jshint: {
            options: {
                jshintrc: '.jshintrc',
                reporter: require('jshint-stylish')
            }
        },

        // escuta alterações
        watch: {
            js: {
                files: [
                    'app/*.js',
                    'app/**/*.js'
                ],
                tasks: ['jshint']
            },
            livereload: {
                options: {
                    livereload: '<%= connect.options.livereload %>'
                },
                files: [
                    'Gruntfile.js',
                    'app/*.html',
                    'app/**/*.html',
                    'app/app.js',
                    'app/scripts/**/*.js',
                    'app/styles/*.css',
                    'app/images/*.{png,jpg,jpeg,gif,webp,svg}'
                ]
            },
            options: {
                livereload: true
            }
        },

        // copia arquivos
        copy: {
            main: {
                expand: true,
                cwd: 'app',
                src: ['**', '!scripts/**', '!app.js', '!**/*.css'],
                dest: 'dist'
            },
            htaccess: {
                expand: true,
                cwd: 'app',
                src: ['.htaccess'],
                dest: 'dist'
            },
            fontsBootstrap: {
                expand: true,
                cwd: 'node_modules/bootstrap/fonts',
                src: ['**'],
                dest: 'dist/fonts'
            }
        },

        // renomeia arquivos (evitar cache)
        filerev: {
            dist: {
                src: ['dist/**/*.{js,css}', '!dist/index.html']
            }
        },

        // prepara arquivos para iniciar minificação
        useminPrepare: {
            html: 'app/index.html'
        },

        // substitui as chamadas de arquivos externos
        usemin: {
            html: ['dist/index.html']
        },

        // mescla varios arquivos em um unico
        uglify: {
            options: {
                report: 'min',
                mangle: false
            }
        },

        // minifica as imagens JPG,PNG
        imagemin: {
            options: {
                cache: true
            },
            dist: {
                files: [{
                    expand: true,
                    cwd: 'app',
                    src: ['**/*.{png,jpg,gif,jpeg}'],
                    dest: 'dist'
                }]
            }
        },

        // minifica os HTML
        htmlmin: {
            dist: {
                options: {
                    removeComments: true,
                    collapseWhitespace: true,
                    conservativeCollapse: true,
                    collapseBooleanAttributes: true,
                    removeCommentsFromCDATA: true,
                    removeOptionalTags: true
                },
                files: [{
                    expand: true,
                    cwd: 'dist',
                    src: 'views/**/*.html',
                    dest: 'dist'
                }]
            }
        },

        // Teste de tela
        protractor: {
            options: {
                configFile: 'test/protractor.conf.js',
                keepAlive: true,
                browserName: 'chrome'
            },
            chrome: {
                options: {
                    args: {
                        browser: "chrome"
                    }
                }
            },
            run: {}
        }
    });

    grunt.registerTask('default', [
        'clean',          // limpa os diretorios para iniciar novo build
        'jshint',         // valida codigo javascript
        'connect',        // sobe o serviço web
        'watch'           // escuta alterações
    ]);

    grunt.registerTask('test', [
        'clean',          // limpa os diretorios para iniciar novo build
        'jshint',         // valida codigo javascript
        'connect',        // sobe o serviço web
        'protractor:run'  // Executa os testes de telas
    ]);

    grunt.registerTask('dist', [
        'clean',          // limpa os diretorios para iniciar novo build
        'jshint',         // valida codigo javascript
        'bumpup',         // altera numero da versao da APP
        'copy',           // copia arquivos
        'filerev',        // renomeia arquivos (evitar cache)
        'useminPrepare',  // prepara arquivos para iniciar minificação
        'concat',         // mescla varios arquivos em um unico
        'uglify',         // otimiza os arquivos minificados
        'cssmin',         // minifica os CSS
        //'imagemin',       // minifica as imagens JPG,PNG
        'htmlmin',        // minifica os HTML
        'usemin'          // substitui as chamadas de arquivos externos
    ]);
};