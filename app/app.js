"use strict";

var appDebug = false;
var appName = "protesto";
var servidor = window.location.origin;
var configSistema = null;

var dateAtual = new Date();
var diaAtual = dateAtual.getDate() < 10 ? "0" + dateAtual.getDate() : dateAtual.getDate();
var mesAtual = dateAtual.getMonth() + 1 < 10 ? "0" + (dateAtual.getMonth() + 1) : dateAtual.getMonth() + 1;
var dataAtual = diaAtual + "/" + mesAtual + "/" + dateAtual.getFullYear();

if (servidor.indexOf("localhost") > -1) {
	servidor = servidor.replace(":9000", "") + "/php/protesto/php/";
	appDebug = true;
} else {
	if (window.location.href.indexOf("www.") == -1) {
		window.location.href = "https://www." + window.location.hostname + window.location.pathname;
	}

	servidor = "https://www.alvoideal.com.br/uploads/protesto/php/";
	appDebug = false;
}

// Verifica se o dispositivo têm suporte ao HTML 5!
if (typeof Storage == undefined || typeof Storage == null) {
	location.href = "#/error";
}

String.prototype.reverse = function() {
	return this.split("")
		.reverse()
		.join("");
};

var app = angular.module("app", ["ngResource", "ngSanitize", "ngTouch", "ngAnimate", "ngRoute", "ngLocale", "ui.router", "ui.bootstrap", "dx", "btorfs.multiselect", "vcRecaptcha", "ngFileSaver"]);

app.config(function($httpProvider, $stateProvider, $urlRouterProvider, $sceProvider) {
	$httpProvider.interceptors.push("InterceptorFactory");
	$sceProvider.enabled(false);

	if (isBlank(window.sessionStorage.getItem("protesto-logado"))) {
		$urlRouterProvider.otherwise("/login");
	} else {
		if (JSON.parse(window.sessionStorage.getItem("protesto-logado")).usuario.id_tipo == 1) {
			$urlRouterProvider.otherwise("/inicio");
		} else {
			$urlRouterProvider.otherwise("/principal");
		}
	}

	$stateProvider
		.state("login", {
			url: "/login",
			templateUrl: "views/login/tela.html",
			controller: "LoginController",
			onlyLoggedIn: false,
			onlyAdmin: false
		})
		.state("intranet", {
			abstract: true,
			url: "/",
			templateUrl: "views/menu.html",
			controller: "MenuController",
			onlyLoggedIn: true,
			onlyAdmin: true
		})
		.state("extranet", {
			abstract: true,
			url: "/",
			templateUrl: "views/menu.html",
			controller: "MenuController",
			onlyLoggedIn: true,
			onlyAdmin: false
		})

		.state("intranet.principal", {
			url: "inicio",
			templateUrl: "views/intranet/principal/tela.html",
			controller: "InicioController",
			onlyLoggedIn: true,
			onlyAdmin: true
		})
		.state("intranet.administradores", {
			url: "administradores",
			templateUrl: "views/intranet/administradores/listar.html",
			controller: "AdministradoresController",
			onlyLoggedIn: true,
			onlyAdmin: true
		})
		.state("intranet.detalhes_administradores", {
			url: "administradores/:id",
			templateUrl: "views/intranet/administradores/editar.html",
			controller: "AdministradoresDetalhesController",
			onlyLoggedIn: true,
			onlyAdmin: true
		})
		.state("intranet.clientes", {
			url: "clientes",
			templateUrl: "views/intranet/clientes/listar.html",
			controller: "ClientesController",
			onlyLoggedIn: true,
			onlyAdmin: true
		})
		.state("intranet.detalhes_clientes", {
			url: "clientes/:id",
			templateUrl: "views/intranet/clientes/editar.html",
			controller: "ClientesDetalhesController",
			onlyLoggedIn: true,
			onlyAdmin: true
		})
		.state("intranet.contratos", {
			url: "contratos",
			templateUrl: "views/intranet/contratos/listar.html",
			controller: "ContratosController",
			onlyLoggedIn: true,
			onlyAdmin: true
		})
		.state("intranet.detalhes_contratos", {
			url: "contratos/:id",
			templateUrl: "views/intranet/contratos/editar.html",
			controller: "ContratosDetalhesController",
			onlyLoggedIn: true,
			onlyAdmin: true
		})
		.state("intranet.itens_contratos", {
			url: "contratos/:id_contrato/itens",
			templateUrl: "views/intranet/contratos/itens/listar.html",
			controller: "ContratosItensController",
			onlyLoggedIn: true,
			onlyAdmin: true
		})
		.state("intranet.detalhes_itens_contratos", {
			url: "contratos/:id_contrato/itens/:id",
			templateUrl: "views/intranet/contratos/itens/editar.html",
			controller: "ContratosItensDetalhesController",
			onlyLoggedIn: true,
			onlyAdmin: true
		})
		.state("intranet.mensalidades", {
			url: "mensalidades",
			templateUrl: "views/intranet/mensalidades/listar.html",
			controller: "MensalidadesController",
			onlyLoggedIn: true,
			onlyAdmin: true
		})
		.state("intranet.taxas_mensalidades", {
			url: "mensalidades/:id_mensalidade/taxas",
			templateUrl: "views/intranet/mensalidades/taxas/listar_visualizar.html",
			controller: "MensalidadesTaxasController",
			onlyLoggedIn: true,
			onlyAdmin: true
		})
		.state("intranet.taxas", {
			url: "taxas",
			templateUrl: "views/intranet/taxas/listar.html",
			controller: "TaxasController",
			onlyLoggedIn: true,
			onlyAdmin: true
		})
		.state("intranet.detalhes_taxas", {
			url: "taxas/:id",
			templateUrl: "views/intranet/taxas/editar.html",
			controller: "TaxasDetalhesController",
			onlyLoggedIn: true,
			onlyAdmin: true
		})
		.state("intranet.servicos", {
			url: "servicos",
			templateUrl: "views/intranet/servicos/listar.html",
			controller: "ServicosController",
			onlyLoggedIn: true,
			onlyAdmin: true
		})
		.state("intranet.detalhes_servicos", {
			url: "servicos/:id",
			templateUrl: "views/intranet/servicos/editar.html",
			controller: "ServicosDetalhesController",
			onlyLoggedIn: true,
			onlyAdmin: true
		})
		.state("intranet.config", {
			url: "config",
			templateUrl: "views/intranet/config/tela.html",
			controller: "ConfigController",
			onlyLoggedIn: true,
			onlyAdmin: true
		})
		.state("intranet.dividas_remessa_banco", {
			url: "dividas_remessa_banco",
			templateUrl: "views/intranet/dividas_remessa_banco/listar.html",
			controller: "DividasRemessaBancoController",
			onlyLoggedIn: true,
			onlyAdmin: false
		})

		.state("extranet.principal", {
			url: "principal",
			templateUrl: "views/extranet/principal/tela.html",
			controller: "PrincipalController",
			onlyLoggedIn: true,
			onlyAdmin: false
		})
		.state("extranet.configuracoes", {
			url: "configuracoes",
			templateUrl: "views/extranet/configuracoes/tela.html",
			controller: "ConfiguracoesController",
			onlyLoggedIn: true,
			onlyAdmin: false
		})
		.state("extranet.avisos", {
			url: "avisos",
			templateUrl: "views/extranet/avisos/listar.html",
			controller: "AvisosController",
			onlyLoggedIn: true,
			onlyAdmin: false
		})
		.state("extranet.detalhes_avisos", {
			url: "avisos/:id",
			templateUrl: "views/extranet/avisos/editar.html",
			controller: "AvisosDetalhesController",
			onlyLoggedIn: true,
			onlyAdmin: false
		})
		.state("extranet.usuarios", {
			url: "usuarios",
			templateUrl: "views/extranet/usuarios/listar.html",
			controller: "UsuariosController",
			onlyLoggedIn: true,
			onlyAdmin: false
		})
		.state("extranet.detalhes_usuarios", {
			url: "usuarios/:id",
			templateUrl: "views/extranet/usuarios/editar.html",
			controller: "UsuariosDetalhesController",
			onlyLoggedIn: true,
			onlyAdmin: false
		})
		.state("extranet.devedores", {
			url: "devedores",
			templateUrl: "views/extranet/devedores/listar.html",
			controller: "DevedoresController",
			onlyLoggedIn: true,
			onlyAdmin: false
		})
		.state("extranet.detalhes_devedores", {
			url: "devedores/:id",
			templateUrl: "views/extranet/devedores/editar.html",
			controller: "DevedoresDetalhesController",
			onlyLoggedIn: true,
			onlyAdmin: false
		})
		.state("extranet.layouts", {
			url: "layouts",
			templateUrl: "views/extranet/layouts/listar.html",
			controller: "LayoutsController",
			onlyLoggedIn: true,
			onlyAdmin: false
		})
		.state("extranet.detalhes_layouts", {
			url: "layouts/:id",
			templateUrl: "views/extranet/layouts/editar.html",
			controller: "LayoutsDetalhesController",
			onlyLoggedIn: true,
			onlyAdmin: false
		})
		.state("extranet.layouts_importados", {
			url: "layouts_importados",
			templateUrl: "views/extranet/layouts_importados/listar.html",
			controller: "LayoutsImportadosController",
			onlyLoggedIn: true,
			onlyAdmin: false
		})
		.state("extranet.detalhes_layouts_importados", {
			url: "layouts_importados/:id",
			templateUrl: "views/extranet/layouts_importados/editar.html",
			controller: "LayoutsImportadosDetalhesController",
			onlyLoggedIn: true,
			onlyAdmin: false
		})
		.state("extranet.dividas", {
			url: "dividas",
			templateUrl: "views/extranet/dividas/listar.html",
			controller: "DividasController",
			onlyLoggedIn: true,
			onlyAdmin: false
		})
		.state("extranet.detalhes_dividas", {
			url: "dividas/:id",
			templateUrl: "views/extranet/dividas/editar.html",
			controller: "DividasDetalhesController",
			onlyLoggedIn: true,
			onlyAdmin: false
		})
		.state("extranet.dividas_remessa_cartorio", {
			url: "dividas_remessa_cartorio",
			templateUrl: "views/extranet/dividas_remessa_cartorio/listar.html",
			controller: "DividasRemessaCartorioController",
			onlyLoggedIn: true,
			onlyAdmin: false
		})
		.state("extranet.contatos", {
			url: "contatos",
			templateUrl: "views/extranet/contatos/listar.html",
			controller: "ContatosController",
			onlyLoggedIn: true,
			onlyAdmin: false
		})
		.state("extranet.detalhes_contatos", {
			url: "contatos/:id",
			templateUrl: "views/extranet/contatos/editar.html",
			controller: "ContatosDetalhesController",
			onlyLoggedIn: true,
			onlyAdmin: false
		})
		.state("extranet.interacoes_contatos", {
			url: "contatos/:id_contato/interacoes",
			templateUrl: "views/extranet/contatos/interacoes/listar.html",
			controller: "ContatosInteracoesController",
			onlyLoggedIn: true,
			onlyAdmin: false
		})
		.state("extranet.detalhes_interacoes_contatos", {
			url: "contatos/:id_contato/interacoes/:id",
			templateUrl: "views/extranet/contatos/interacoes/editar.html",
			controller: "ContatosInteracoesDetalhesController",
			onlyLoggedIn: true,
			onlyAdmin: false
		})
		.state("extranet.faturas", {
			url: "faturas",
			templateUrl: "views/extranet/faturas/listar.html",
			controller: "FaturasController",
			onlyLoggedIn: true,
			onlyAdmin: false
		})
		.state("extranet.taxas_faturas", {
			url: "faturas/:id_mensalidade/taxas",
			templateUrl: "views/extranet/faturas/taxas/listar_visualizar.html",
			controller: "FaturasTaxasController",
			onlyLoggedIn: true,
			onlyAdmin: false
		})
		.state("extranet.minha_conta", {
			url: "minha_conta",
			templateUrl: "views/extranet/minha_conta/tela.html",
			controller: "MinhaContaController",
			onlyLoggedIn: true,
			onlyAdmin: false
		})
		.state("extranet.servicos_disponiveis-L0001", {
			url: "servicos_disponiveis/L0001/:id_servico",
			templateUrl: "views/extranet/servicos_disponiveis/L0001.html",
			controller: "ServicosDisponiveisL0001Controller",
			onlyLoggedIn: true,
			onlyAdmin: false
		})
		.state("extranet.servicos_disponiveis-L0002", {
			url: "servicos_disponiveis/L0002/:id_servico",
			templateUrl: "views/extranet/servicos_disponiveis/L0002.html",
			controller: "ServicosDisponiveisL0002Controller",
			onlyLoggedIn: true,
			onlyAdmin: false
		})
		.state("extranet.servicos_disponiveis-L0003", {
			url: "servicos_disponiveis/L0003/:id_servico",
			templateUrl: "views/extranet/servicos_disponiveis/L0003.html",
			controller: "ServicosDisponiveisL0003Controller",
			onlyLoggedIn: true,
			onlyAdmin: false
		})
		.state("extranet.servicos_disponiveis-L0004", {
			url: "servicos_disponiveis/L0004/:id_servico",
			templateUrl: "views/extranet/servicos_disponiveis/L0004.html",
			controller: "ServicosDisponiveisL0004Controller",
			onlyLoggedIn: true,
			onlyAdmin: false
		})
		.state("extranet.servicos_disponiveis-L0006", {
			url: "servicos_disponiveis/L0006/:id_servico",
			templateUrl: "views/extranet/servicos_disponiveis/L0006.html",
			controller: "ServicosDisponiveisL0006Controller",
			onlyLoggedIn: true,
			onlyAdmin: false
		})
		.state("extranet.servicos_disponiveis-L0007", {
			url: "servicos_disponiveis/L0007/:id_servico",
			templateUrl: "views/extranet/servicos_disponiveis/L0007.html",
			controller: "ServicosDisponiveisL0007Controller",
			onlyLoggedIn: true,
			onlyAdmin: false
		})
		.state("extranet.servicos_disponiveis-L0008", {
			url: "servicos_disponiveis/L0008/:id_servico",
			templateUrl: "views/extranet/servicos_disponiveis/L0008.html",
			controller: "ServicosDisponiveisL0008Controller",
			onlyLoggedIn: true,
			onlyAdmin: false
		})
		.state("extranet.servicos_disponiveis-L0011", {
			url: "servicos_disponiveis/L0011/:id_servico",
			templateUrl: "views/extranet/servicos_disponiveis/L0011.html",
			controller: "ServicosDisponiveisL0011Controller",
			onlyLoggedIn: true,
			onlyAdmin: false
		})
		.state("extranet.servicos_disponiveis-L0014", {
			url: "servicos_disponiveis/L0014/:id_servico",
			templateUrl: "views/extranet/servicos_disponiveis/L0014.html",
			controller: "ServicosDisponiveisL0014Controller",
			onlyLoggedIn: true,
			onlyAdmin: false
		})
		.state("extranet.servicos_disponiveis-L0015", {
			url: "servicos_disponiveis/L0015/:id_servico",
			templateUrl: "views/extranet/servicos_disponiveis/L0015.html",
			controller: "ServicosDisponiveisL0015Controller",
			onlyLoggedIn: true,
			onlyAdmin: false
		})
		.state("extranet.servicos_disponiveis-L0022", {
			url: "servicos_disponiveis/L0022/:id_servico",
			templateUrl: "views/extranet/servicos_disponiveis/L0022.html",
			controller: "ServicosDisponiveisL0022Controller",
			onlyLoggedIn: true,
			onlyAdmin: false
		})
		.state("extranet.servicos_disponiveis-L0024", {
			url: "servicos_disponiveis/L0024/:id_servico",
			templateUrl: "views/extranet/servicos_disponiveis/L0024.html",
			controller: "ServicosDisponiveisL0024Controller",
			onlyLoggedIn: true,
			onlyAdmin: false
		})
		.state("extranet.servicos_disponiveis-L0029", {
			url: "servicos_disponiveis/L0029/:id_servico",
			templateUrl: "views/extranet/servicos_disponiveis/L0029.html",
			controller: "ServicosDisponiveisL0029Controller",
			onlyLoggedIn: true,
			onlyAdmin: false
		})
		.state("extranet.servicos_disponiveis-L0030", {
			url: "servicos_disponiveis/L0030/:id_servico",
			templateUrl: "views/extranet/servicos_disponiveis/L0030.html",
			controller: "ServicosDisponiveisL0030Controller",
			onlyLoggedIn: true,
			onlyAdmin: false
		})
		.state("extranet.servicos_disponiveis-L0032", {
			url: "servicos_disponiveis/L0032/:id_servico",
			templateUrl: "views/extranet/servicos_disponiveis/L0032.html",
			controller: "ServicosDisponiveisL0032Controller",
			onlyLoggedIn: true,
			onlyAdmin: false
		})
		.state("extranet.servicos_disponiveis-L0033", {
			url: "servicos_disponiveis/L0033/:id_servico",
			templateUrl: "views/extranet/servicos_disponiveis/L0033.html",
			controller: "ServicosDisponiveisL0033Controller",
			onlyLoggedIn: true,
			onlyAdmin: false
		})
		.state("extranet.servicos_disponiveis-L0035", {
			url: "servicos_disponiveis/L0035/:id_servico",
			templateUrl: "views/extranet/servicos_disponiveis/L0035.html",
			controller: "ServicosDisponiveisL0035Controller",
			onlyLoggedIn: true,
			onlyAdmin: false
		})
		.state("extranet.servicos_disponiveis-L0038", {
			url: "servicos_disponiveis/L0038/:id_servico",
			templateUrl: "views/extranet/servicos_disponiveis/L0038.html",
			controller: "ServicosDisponiveisL0038Controller",
			onlyLoggedIn: true,
			onlyAdmin: false
		})
		.state("extranet.servicos_disponiveis-L0039", {
			url: "servicos_disponiveis/L0039/:id_servico",
			templateUrl: "views/extranet/servicos_disponiveis/L0039.html",
			controller: "ServicosDisponiveisL0039Controller",
			onlyLoggedIn: true,
			onlyAdmin: false
		})
		.state("extranet.servicos_disponiveis-L0041", {
			url: "servicos_disponiveis/L0041/:id_servico",
			templateUrl: "views/extranet/servicos_disponiveis/L0041.html",
			controller: "ServicosDisponiveisL0041Controller",
			onlyLoggedIn: true,
			onlyAdmin: false
		})
		.state("extranet.servicos_disponiveis-L0059", {
			url: "servicos_disponiveis/L0059/:id_servico",
			templateUrl: "views/extranet/servicos_disponiveis/L0059.html",
			controller: "ServicosDisponiveisL0059Controller",
			onlyLoggedIn: true,
			onlyAdmin: false
		})
		.state("extranet.servicos_disponiveis-L0064", {
			url: "servicos_disponiveis/L0064/:id_servico",
			templateUrl: "views/extranet/servicos_disponiveis/L0064.html",
			controller: "ServicosDisponiveisL0064Controller",
			onlyLoggedIn: true,
			onlyAdmin: false
		})
		.state("extranet.servicos_disponiveis-L0066", {
			url: "servicos_disponiveis/L0066/:id_servico",
			templateUrl: "views/extranet/servicos_disponiveis/L0066.html",
			controller: "ServicosDisponiveisL0066Controller",
			onlyLoggedIn: true,
			onlyAdmin: false
		})
		.state("extranet.servicos_disponiveis-L0067", {
			url: "servicos_disponiveis/L0067/:id_servico",
			templateUrl: "views/extranet/servicos_disponiveis/L0067.html",
			controller: "ServicosDisponiveisL0067Controller",
			onlyLoggedIn: true,
			onlyAdmin: false
		})
		.state("extranet.servicos_disponiveis-L0068", {
			url: "servicos_disponiveis/L0068/:id_servico",
			templateUrl: "views/extranet/servicos_disponiveis/L0068.html",
			controller: "ServicosDisponiveisL0068Controller",
			onlyLoggedIn: true,
			onlyAdmin: false
		})
		.state("extranet.servicos_disponiveis-R0001", {
			url: "servicos_disponiveis/R0001/:id_servico",
			templateUrl: "views/extranet/servicos_disponiveis/R0001.html",
			controller: "ServicosDisponiveisR0001Controller",
			onlyLoggedIn: true,
			onlyAdmin: false
		})
		.state("extranet.servicos_disponiveis-R0002", {
			url: "servicos_disponiveis/R0002/:id_servico",
			templateUrl: "views/extranet/servicos_disponiveis/R0002.html",
			controller: "ServicosDisponiveisR0002Controller",
			onlyLoggedIn: true,
			onlyAdmin: false
		})
		.state("extranet.servicos_disponiveis-R0014", {
			url: "servicos_disponiveis/R0014/:id_servico",
			templateUrl: "views/extranet/servicos_disponiveis/R0014.html",
			controller: "ServicosDisponiveisR0014Controller",
			onlyLoggedIn: true,
			onlyAdmin: false
		})
		.state("extranet.servicos_disponiveis-R0016", {
			url: "servicos_disponiveis/R0016/:id_servico",
			templateUrl: "views/extranet/servicos_disponiveis/R0016.html",
			controller: "ServicosDisponiveisR0016Controller",
			onlyLoggedIn: true,
			onlyAdmin: false
		})
		.state("extranet.servicos_disponiveis-R0019", {
			url: "servicos_disponiveis/R0019/:id_servico",
			templateUrl: "views/extranet/servicos_disponiveis/R0019.html",
			controller: "ServicosDisponiveisR0019Controller",
			onlyLoggedIn: true,
			onlyAdmin: false
		});
});

var validarAcessoRestrito = function($state, StorageFactory, toState, event) {
	// Se a pagina atual não for LOGIN, e tentar acessar outra página sem direito, é redirecionado ao login
	if (isBlank(toState.templateUrl.indexOf("login"))) {
		if (isBlank(StorageFactory.get("logado")) || (isNotBlank(StorageFactory.get("logado")) && isBlank(StorageFactory.get("logado").token))) {
			StorageFactory.clearAll();
			$state.go("login");
			jQuery("#body").addClass("body-login");
			event.preventDefault();
		}
	}

	// Se não estiver logado, e a página exige que esteja logado
	if (isBlank(StorageFactory.get("logado")) && toState.onlyLoggedIn) {
		StorageFactory.clearAll();
		$state.go("login");
		jQuery("#body").addClass("body-login");
		event.preventDefault();
	}

	// Se estiver logado, e perder as configurações padrão do sistema
	if (isNotBlank(StorageFactory.get("logado"))) {
		if (isBlank(StorageFactory.get("configSistema"))) {
			StorageFactory.clearAll();
			$state.go("login");
			jQuery("#body").addClass("body-login");
			event.preventDefault();
		}
	}

	// Se já está logado
	if (isNotBlank(StorageFactory.get("logado"))) {
		// Se for Usuário, e a página for permitida apenas para Admin
		if (StorageFactory.get("logado").usuario.id_tipo == 0 && toState.onlyAdmin) {
			$state.go("extranet.principal");
			event.preventDefault();
		}
	}
};

app.run(function($rootScope, $state, StorageFactory) {
	$rootScope.isAdmin = false;

	$rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams) {
		// Ao trocar de tela remove a proteção
		jQuery("#body").removeClass("body-login");

		configSistema = StorageFactory.get("configSistema");
		validarAcessoRestrito($state, StorageFactory, toState, event);
	});

	$rootScope.$on("$stateChangeSuccess", function(event, toState, toParams, fromState, fromParams) {
		validarAcessoRestrito($state, StorageFactory, toState, event);
	});

	if (isNotBlank(StorageFactory.get("logado"))) {
		$rootScope.isAdmin = StorageFactory.get("logado").usuario.id_tipo == 1;
	}
});

function upload_arquivo($q, StorageFactory, url, file, extra) {
	var defer = $q.defer();

	var fd = new FormData();
	fd.append("file", file);

	if (extra) {
		fd.append("extra", JSON.stringify(extra));
	}

	jQuery.ajax({
		url: url,
		data: fd,
		cache: false,
		contentType: false,
		processData: false,
		type: "POST",
		beforeSend: function(xhr) {
			xhr.setRequestHeader("Authorization", StorageFactory.get("logado").token);
			xhr.setRequestHeader("WWW-Authorization", StorageFactory.get("logado").token);
		},
		success: function(data, textStatus, jqXHR) {
			// O retorno do tipo OBJECT deve ser criado manualmente - Não sei o motivo, mas não retorna o objeto jqXHR
			defer.resolve({ status: jqXHR.status, filename: file.name });
		},
		error: function(jqXHR) {
			// O retorno do tipo OBJECT deve ser criado manualmente - Não sei o motivo, mas não retorna o objeto jqXHR
			defer.resolve({ status: jqXHR.status, filename: file.name, responseText: jqXHR.responseText });
		}
	});

	return defer.promise;
}

// this.readFileBase64 = function (file) {
//     return readFileBase64($q, file);
// }
//
// function readFileBase64($q, file) {
//     var deferred = $q.defer();
//
//     var reader = new FileReader();
//
//     reader.onload = function (e) {
//         deferred.resolve(e.target.result);
//     };
//
//     reader.onerror = function (e) {
//         deferred.reject(e);
//     };
//
//     reader.readAsDataURL(file);
//
//     return deferred.promise;
// }

app.directive("fileModel", [
	"$parse",
	function($parse) {
		return {
			restrict: "A",
			link: function(scope, element, attrs) {
				var model = $parse(attrs.fileModel);
				var modelSetter = model.assign;

				element.bind("change", function() {
					scope.$apply(function() {
						modelSetter(scope, element[0].files[0]);
					});
				});
			}
		};
	}
]);

app.directive("ngEnter", function() {
	return function(scope, element, attrs) {
		element.bind("keydown keypress", function(event) {
			if (event.which === 13) {
				scope.$apply(function() {
					scope.$eval(attrs.ngEnter);
				});

				event.preventDefault();
			}
		});
	};
});

function isBlank(vlr) {
	if (vlr == undefined || vlr == null || vlr == "" || vlr == -1) {
		return true;
	}

	return false;
}

function isNotBlank(vlr) {
	return !isBlank(vlr);
}

function validarEmail(email) {
	var filter = /^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/;
	return String(email).search(filter) != -1;
}

function formatar_Valor(vlr) {
	return number_format(vlr, 2, ",", ".");
}

function exibir_Valor(vlr) {
	return "R$ " + formatar_Valor(vlr);
}

function formatar_Data(vlr) {
	if (vlr != "") {
		var prefixo = vlr.slice(2, 3);

		if (prefixo == "/") {
			var d = vlr.slice(0, 2);
			var m = vlr.slice(3, 5);
			var a = vlr.slice(6, 10);

			return a + "-" + m + "-" + d;
		} else {
			var a = vlr.slice(0, 4);
			var m = vlr.slice(5, 7);
			var d = vlr.slice(8, 10);

			return d + "/" + m + "/" + a;
		}
	}

	return "";
}

function inArray(needle, myArray) {
	for (var i = 0; i < myArray.length; i++) {
		if (myArray[i] == needle) return true;
	}

	return false;
}

function number_format(number, decimals, decPoint, thousandsSep) {
	number = (number + "").replace(/[^0-9+\-Ee.]/g, "");
	var n = !isFinite(+number) ? 0 : +number;
	var prec = !isFinite(+decimals) ? 0 : Math.abs(decimals);
	var sep = typeof thousandsSep === "undefined" ? "," : thousandsSep;
	var dec = typeof decPoint === "undefined" ? "." : decPoint;
	var s = "";

	var toFixedFix = function(n, prec) {
		var k = Math.pow(10, prec);
		return "" + (Math.round(n * k) / k).toFixed(prec);
	};

	// @todo: for IE parseFloat(0.55).toFixed(0) = 0;
	s = (prec ? toFixedFix(n, prec) : "" + Math.round(n)).split(".");
	if (s[0].length > 3) {
		s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
	}
	if ((s[1] || "").length < prec) {
		s[1] = s[1] || "";
		s[1] += new Array(prec - s[1].length + 1).join("0");
	}

	return s.join(dec);
}

function primeiraLetraMaiuscula(text) {
	var words = text
		.toString()
		.toLowerCase()
		.replace(/_/g, " ")
		.split(" ");

	for (var a = 0; a < words.length; a++) {
		var w = words[a];
		words[a] = w[0].toUpperCase() + w.slice(1);
	}

	return words.join(" ");
}

function retornaConteudo(conteudo) {
	var colRetorno = [];

	angular.forEach(conteudo, function(filterObj, filterIndex) {
		colRetorno.push(filterObj);
	});

	var colUnico = colRetorno.filter(function(elem, pos, self) {
		return self.indexOf(elem) == pos;
	});

	return colUnico;
}

function formatarValor(conteudo) {
	var ehDataAmericana = conteudo.match(/\b(\d+\-\d+\-\d+)\b/g);

	if (ehDataAmericana) {
		var dataConvertida = conteudo.split("-");
		return dataConvertida[2] + "/" + dataConvertida[1] + "/" + dataConvertida[0];
	} else {
		return conteudo;
	}
}

function mostrarProps(colObj) {
	var resultado = "";
	var existeConteudo = false;
	resultado += "<table width='100%' class='tblListagemWS'>";

	for (var i in colObj) {
		if (colObj.hasOwnProperty(i)) {
			if (Array.isArray(colObj[i])) {
				resultado += mostrarProps(colObj[i]);
			} else {
				if (colObj[i] instanceof Object) {
					resultado += mostrarProps(colObj[i]);
				} else {
					if (colObj[i]) {
						existeConteudo = true;

						resultado += "<tr>";
						resultado += "<th>";
						resultado += primeiraLetraMaiuscula(i);
						resultado += "</th>";
						resultado += "<td>";
						resultado += formatarValor(colObj[i]);
						resultado += "</td>";
						resultado += "</tr>";
					}
				}
			}
		}
	}

	resultado += "</table>";

	if (existeConteudo) {
		resultado += "<hr>";
	}

	return resultado;
}
