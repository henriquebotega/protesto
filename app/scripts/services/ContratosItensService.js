'use strict';

app.service("ContratosItensService", function ($http, $q) {

    this.getAll = function (id_contrato) {
        var deferred = $q.defer();

        var params = JSON.stringify({
            "acao": "read",
            "id_contrato": id_contrato
        });

        $http.post(servidor + "controle/contratos_itens.php", params)
            .success(function (retorno, status, headers, config) {
                deferred.resolve(retorno);
            });

        return deferred.promise;
    }

    this.getByID = function (id_contrato, id) {
        var deferred = $q.defer();

        var params = JSON.stringify({
            "acao": "getByID",
            "id_contrato": id_contrato,
            "id": id
        });

        $http.post(servidor + "controle/contratos_itens.php", params)
            .success(function (retorno, status, headers, config) {
                deferred.resolve(retorno);
            });

        return deferred.promise;
    }

    this.getByCliente = function (id_cliente) {
        var deferred = $q.defer();

        var params = JSON.stringify({
            "acao": "getByCliente",
            "id_cliente": id_cliente
        });

        $http.post(servidor + "controle/contratos_itens.php", params)
            .success(function (retorno, status, headers, config) {
                deferred.resolve(retorno);
            });

        return deferred.promise;
    }
    
    this.salvar = function (registroAtual, id_contrato) {
        var deferred = $q.defer();

        var params = JSON.stringify({
            "acao": "salvar",
            "id_contrato": id_contrato,
            "registroAtual": registroAtual
        });

        $http.post(servidor + "controle/contratos_itens.php", params)
            .success(function (retorno, status, headers, config) {
                deferred.resolve(retorno);
            });

        return deferred.promise;
    }

    this.excluir = function (colRegistroAtual) {
        var deferred = $q.defer();

        var params = JSON.stringify({
            "acao": "excluir",
            "colRegistroAtual": colRegistroAtual
        });

        $http.post(servidor + "controle/contratos_itens.php", params)
            .success(function (retorno, status, headers, config) {
                deferred.resolve(retorno);
            });

        return deferred.promise;
    }

    this.filtrar = function (id_contrato, palavraChave) {
        var deferred = $q.defer();

        var params = JSON.stringify({
            "acao": "filtrar",
            "id_contrato": id_contrato,
            "palavraChave": palavraChave
        });

        $http.post(servidor + "controle/contratos_itens.php", params)
            .success(function (retorno, status, headers, config) {
                deferred.resolve(retorno);
            });

        return deferred.promise;
    }

});