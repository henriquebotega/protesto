'use strict';

app.service("LayoutsImportadosService", function ($http, $q, StorageFactory) {

    this.getAll = function () {
        var deferred = $q.defer();

        var params = JSON.stringify({
            "acao": "read"
        });

        $http.post(servidor + "controle/layouts_importados.php", params)
            .success(function (retorno, status, headers, config) {
                deferred.resolve(retorno);
            });

        return deferred.promise;
    }

    this.salvar = function (registroAtual) {
        var deferred = $q.defer();

        var params = JSON.stringify({
            "acao": "salvar",
            "registroAtual": registroAtual
        });

        $http.post(servidor + "controle/layouts_importados.php", params)
            .success(function (retorno, status, headers, config) {
                deferred.resolve(retorno);
            });

        return deferred.promise;
    }

    this.excluir = function (colRegistroAtual) {
        var deferred = $q.defer();

        var params = JSON.stringify({
            "acao": "excluir",
            "colRegistroAtual": colRegistroAtual
        });

        $http.post(servidor + "controle/layouts_importados.php", params)
            .success(function (retorno, status, headers, config) {
                deferred.resolve(retorno);
            });

        return deferred.promise;
    }

    this.upload = function (file, id_layout_importado, registroAtual) {
        return upload_arquivo($q, StorageFactory, servidor + 'controle/uploadLayoutsImportados.php', file, {'id_layout': registroAtual['id_layout'], 'id_layout_importado': id_layout_importado, 'id_cliente': registroAtual['id_cliente']});
    }
});