'use strict';

app.service("LoginService", function ($http, $q, $location, StorageFactory) {

        this.logar = function (email, senha, tokenRecaptcha, debugTest) {
            var deferred = $q.defer();

            var params = JSON.stringify({
                "acao": "logar",
                "debugTest": debugTest,
                "tokenRecaptcha": tokenRecaptcha,
                "email": email,
                "senha": senha
            });

            $http.post(servidor + "controle/login.php", params)
                .success(function (retorno, status, headers, config) {
                    deferred.resolve(retorno);
                });

            return deferred.promise;
        }

        this.recuperar_senha = function (email) {
            var deferred = $q.defer();

            var params = JSON.stringify({
                "acao": "recuperar_senha",
                "email": email
            });

            $http.post(servidor + "controle/login.php", params)
                .success(function (retorno, status, headers, config) {
                    deferred.resolve(retorno);
                });

            return deferred.promise;
        }

        this.email_ja_existe = function (email) {
            var deferred = $q.defer();

            var params = JSON.stringify({
                "acao": "email_ja_existe",
                "email": email
            });

            $http.post(servidor + "controle/login.php", params)
                .success(function (retorno, status, headers, config) {
                    deferred.resolve(retorno);
                });

            return deferred.promise;
        }

        this.salvar_conta = function (registroAtual) {
            var deferred = $q.defer();

            var params = JSON.stringify({
                "acao": "salvar_conta",
                "registroAtual": registroAtual
            });

            $http.post(servidor + "controle/login.php", params)
                .success(function (retorno, status, headers, config) {
                    deferred.resolve(retorno);
                });

            return deferred.promise;
        }

        this.alterar_senha = function (senhaAtual, senhaNova) {
            var deferred = $q.defer();

            var params = JSON.stringify({
                "acao": "alterar_senha",
                "senhaAtual": senhaAtual,
                "senhaNova": senhaNova
            });

            $http.post(servidor + "controle/login.php", params)
                .success(function (retorno, status, headers, config) {
                    deferred.resolve(retorno);
                });

            return deferred.promise;
        }

    }
);