'use strict';

app.service("ClientesService", function ($http, $q) {

    this.getAll = function () {
        var deferred = $q.defer();

        var params = JSON.stringify({
            "acao": "read"
        });

        $http.post(servidor + "controle/clientes.php", params)
            .success(function (retorno, status, headers, config) {
                deferred.resolve(retorno);
            });

        return deferred.promise;
    }

    this.getByID = function (id) {
        var deferred = $q.defer();

        var params = JSON.stringify({
            "acao": "getByID",
            "id": id
        });

        $http.post(servidor + "controle/clientes.php", params)
            .success(function (retorno, status, headers, config) {
                deferred.resolve(retorno);
            });

        return deferred.promise;
    }

    this.salvar = function (registroAtual) {
        var deferred = $q.defer();

        var params = JSON.stringify({
            "acao": "salvar",
            "registroAtual": registroAtual
        });

        $http.post(servidor + "controle/clientes.php", params)
            .success(function (retorno, status, headers, config) {
                deferred.resolve(retorno);
            });

        return deferred.promise;
    }

    this.excluir = function (registroAtual) {
        var deferred = $q.defer();

        var params = JSON.stringify({
            "acao": "excluir",
            "registroAtual": registroAtual
        });

        $http.post(servidor + "controle/clientes.php", params)
            .success(function (retorno, status, headers, config) {
                deferred.resolve(retorno);
            });

        return deferred.promise;
    }

    this.filtrar = function (palavraChave, filtroSituacao) {
        var deferred = $q.defer();

        var params = JSON.stringify({
            "acao": "filtrar",
            "filtroSituacao": filtroSituacao,
            "palavraChave": palavraChave
        });

        $http.post(servidor + "controle/clientes.php", params)
            .success(function (retorno, status, headers, config) {
                deferred.resolve(retorno);
            });

        return deferred.promise;
    }

    this.getContratoByCliente = function (registroAtual) {
        var deferred = $q.defer();

        var params = JSON.stringify({
            "acao": "getContratoByCliente",
            "registroAtual": registroAtual
        });

        $http.post(servidor + "controle/clientes.php", params)
            .success(function (retorno, status, headers, config) {
                deferred.resolve(retorno);
            });

        return deferred.promise;
    }

});