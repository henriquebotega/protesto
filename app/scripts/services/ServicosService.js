'use strict';

app.service("ServicosService", function ($http, $q) {

    this.getAll = function () {
        var deferred = $q.defer();

        var params = JSON.stringify({
            "acao": "read"
        });

        $http.post(servidor + "controle/servicos.php", params)
            .success(function (retorno, status, headers, config) {
                deferred.resolve(retorno);
            });

        return deferred.promise;
    }
    
    this.getAllObrigatorios = function () {
        var deferred = $q.defer();

        var params = JSON.stringify({
            "acao": "readObrigatorios"
        });

        $http.post(servidor + "controle/servicos.php", params)
            .success(function (retorno, status, headers, config) {
                deferred.resolve(retorno);
            });

        return deferred.promise;
    }

    this.getByID = function (id) {
        var deferred = $q.defer();

        var params = JSON.stringify({
            "acao": "getByID",
            "id": id
        });

        $http.post(servidor + "controle/servicos.php", params)
            .success(function (retorno, status, headers, config) {
                deferred.resolve(retorno);
            });

        return deferred.promise;
    }

    this.salvar = function (registroAtual) {
        var deferred = $q.defer();

        var params = JSON.stringify({
            "acao": "salvar",
            "registroAtual": registroAtual
        });

        $http.post(servidor + "controle/servicos.php", params)
            .success(function (retorno, status, headers, config) {
                deferred.resolve(retorno);
            });

        return deferred.promise;
    }

    this.excluir = function (registroAtual) {
        var deferred = $q.defer();

        var params = JSON.stringify({
            "acao": "excluir",
            "registroAtual": registroAtual
        });

        $http.post(servidor + "controle/servicos.php", params)
            .success(function (retorno, status, headers, config) {
                deferred.resolve(retorno);
            });

        return deferred.promise;
    }

    this.filtrar = function (palavraChave) {
        var deferred = $q.defer();

        var params = JSON.stringify({
            "acao": "filtrar",
            "palavraChave": palavraChave
        });

        $http.post(servidor + "controle/servicos.php", params)
            .success(function (retorno, status, headers, config) {
                deferred.resolve(retorno);
            });

        return deferred.promise;
    }

    this.permiteRemover = function (registroAtual) {
        var deferred = $q.defer();

        var params = JSON.stringify({
            "acao": "permiteRemover",
            "registroAtual": registroAtual
        });

        $http.post(servidor + "controle/servicos.php", params)
            .success(function (retorno, status, headers, config) {
                deferred.resolve(retorno);
            });

        return deferred.promise;
    }

});