'use strict';

app.service("TaxasService", function ($http, $q, StorageFactory) {

    this.getAll = function () {
        var deferred = $q.defer();

        var params = JSON.stringify({
            "acao": "read"
        });

        $http.post(servidor + "controle/taxas.php", params)
            .success(function (retorno, status, headers, config) {
                deferred.resolve(retorno);
            });

        return deferred.promise;
    }

    this.getByID = function (id) {
        var deferred = $q.defer();

        var params = JSON.stringify({
            "acao": "getByID",
            "id": id
        });

        $http.post(servidor + "controle/taxas.php", params)
            .success(function (retorno, status, headers, config) {
                deferred.resolve(retorno);
            });

        return deferred.promise;
    }

    this.excluir = function (colRegistroAtual) {
        var deferred = $q.defer();

        var params = JSON.stringify({
            "acao": "excluir",
            "colRegistroAtual": colRegistroAtual
        });

        $http.post(servidor + "controle/taxas.php", params)
            .success(function (retorno, status, headers, config) {
                deferred.resolve(retorno);
            });

        return deferred.promise;
    }

    this.salvar = function (registroAtual) {
        var deferred = $q.defer();

        var params = JSON.stringify({
            "acao": "salvar",
            "registroAtual": registroAtual
        });

        $http.post(servidor + "controle/taxas.php", params)
            .success(function (retorno, status, headers, config) {
                deferred.resolve(retorno);
            });

        return deferred.promise;
    }

    this.filtrar = function (palavraChave) {
        var deferred = $q.defer();

        var params = JSON.stringify({
            "acao": "filtrar",
            "palavraChave": palavraChave
        });

        $http.post(servidor + "controle/taxas.php", params)
            .success(function (retorno, status, headers, config) {
                deferred.resolve(retorno);
            });

        return deferred.promise;
    }

    this.getAllSituacao = function () {
        var deferred = $q.defer();

        var listagem = StorageFactory.get("listagemTaxasSituacao");

        if (isNotBlank(listagem)) {
            deferred.resolve(listagem);

        } else {

            var params = JSON.stringify({
                "acao": "read"
            });

            $http.post(servidor + "controle/taxas_situacao.php", params)
                .success(function (retorno, status, headers, config) {
                    deferred.resolve(retorno.retorno);
                });
        }

        return deferred.promise;
    }

});