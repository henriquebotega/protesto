'use strict';

app.service("FaturasTaxasService", function ($http, $q) {

    this.getAll = function (id_mensalidade) {
        var deferred = $q.defer();

        var params = JSON.stringify({
            "acao": "read",
            "id_mensalidade": id_mensalidade
        });

        $http.post(servidor + "controle/faturas_taxas.php", params)
            .success(function (retorno, status, headers, config) {
                deferred.resolve(retorno);
            });

        return deferred.promise;
    }

});