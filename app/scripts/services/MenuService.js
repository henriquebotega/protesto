'use strict';

app.service("MenuService", function ($http, $q, $location, StorageFactory) {

        this.getEstados = function () {
            var deferred = $q.defer();

            var listagem = StorageFactory.get("listagemEstados");

            if (isNotBlank(listagem)) {
                deferred.resolve(listagem);

            } else {

                var params = JSON.stringify({
                    "acao": "read"
                });

                $http.post(servidor + "controle/estados.php", params)
                    .success(function (retorno, status, headers, config) {
                        deferred.resolve(retorno.retorno);
                    });
            }

            return deferred.promise;
        }

        this.getMunicipiosPorEstados = function (id_estado) {
            var deferred = $q.defer();

            var params = JSON.stringify({
                "acao": "getMunicipiosPorEstados",
                "id_estado": id_estado
            });

            $http.post(servidor + "controle/municipios.php", params)
                .success(function (retorno, status, headers, config) {
                    deferred.resolve(retorno);
                });

            return deferred.promise;
        }

        this.getCidadesPorEstados = function (id_estado) {
            var deferred = $q.defer();

            var params = JSON.stringify({
                "acao": "read",
                "id_estado": id_estado
            });

            $http.post(servidor + "controle/cidades.php", params)
                .success(function (retorno, status, headers, config) {
                    deferred.resolve(retorno);
                });

            return deferred.promise;
        }

        this.getCidadePorCEP = function (codigoIBGE) {
            var deferred = $q.defer();

            var params = JSON.stringify({
                "acao": "read",
                "codigoIBGE": codigoIBGE
            });

            $http.post(servidor + "controle/municipios.php", params)
                .success(function (retorno, status, headers, config) {
                    deferred.resolve(retorno);
                });

            return deferred.promise;
        }

    }
);