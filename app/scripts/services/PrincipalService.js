'use strict';

app.service("PrincipalService", function ($http, $q) {

    this.getConsumoMensalWS = function () {
        var deferred = $q.defer();

        var params = JSON.stringify({
            "acao": "getConsumoMensalWS"
        });

        $http.post(servidor + "controle/principal.php", params)
            .success(function (retorno, status, headers, config) {
                deferred.resolve(retorno);
            });

        return deferred.promise;
    }
    
    this.getTotalDividas = function () {
        var deferred = $q.defer();

        var params = JSON.stringify({
            "acao": "totalDividas"
        });

        $http.post(servidor + "controle/principal.php", params)
            .success(function (retorno, status, headers, config) {
                deferred.resolve(retorno);
            });

        return deferred.promise;
    }
    
    this.getTotalRemessasCartorio = function () {
        var deferred = $q.defer();

        var params = JSON.stringify({
            "acao": "totalRemessasCartorio"
        });

        $http.post(servidor + "controle/principal.php", params)
            .success(function (retorno, status, headers, config) {
                deferred.resolve(retorno);
            });

        return deferred.promise;
    }

});