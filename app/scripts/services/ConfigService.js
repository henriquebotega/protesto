'use strict';

app.service("ConfigService", function ($http, $q) {

        this.getAll = function () {
            var deferred = $q.defer();

            var params = JSON.stringify({
                "acao": "read"
            });

            $http.post(servidor + "controle/config.php", params)
                .success(function (retorno, status, headers, config) {
                    deferred.resolve(retorno);
                });

            return deferred.promise;
        }

        this.salvar = function (registroAtual) {
            var deferred = $q.defer();

            var params = JSON.stringify({
                "acao": "salvar",
                "registroAtual": registroAtual
            });

            $http.post(servidor + "controle/config.php", params)
                .success(function (retorno, status, headers, config) {
                    deferred.resolve(retorno);
                });

            return deferred.promise;
        }

    }
);