'use strict';

app.service("ContatosInteracoesService", function ($http, $q, StorageFactory) {

        this.getAll = function (id_contato) {
            var deferred = $q.defer();

            var params = JSON.stringify({
                "acao": "read",
                "id_contato": id_contato
            });

            $http.post(servidor + "controle/contatos_interacoes.php", params)
                .success(function (retorno, status, headers, config) {
                    deferred.resolve(retorno);
                });

            return deferred.promise;
        }

        this.getByID = function (id_contato, id) {
            var deferred = $q.defer();

            var params = JSON.stringify({
                "acao": "getByID",
                "id_contato": id_contato,
                "id": id
            });

            $http.post(servidor + "controle/contatos_interacoes.php", params)
                .success(function (retorno, status, headers, config) {
                    deferred.resolve(retorno);
                });

            return deferred.promise;
        }

        this.salvar = function (registroAtual, id_contato) {
            var deferred = $q.defer();

            var params = JSON.stringify({
                "acao": "salvar",
                "id_contato": id_contato,
                "registroAtual": registroAtual
            });

            $http.post(servidor + "controle/contatos_interacoes.php", params)
                .success(function (retorno, status, headers, config) {
                    deferred.resolve(retorno);
                });

            return deferred.promise;
        }

        this.excluir = function (colRegistroAtual) {
            var deferred = $q.defer();

            var params = JSON.stringify({
                "acao": "excluir",
                "colRegistroAtual": colRegistroAtual
            });

            $http.post(servidor + "controle/contatos_interacoes.php", params)
                .success(function (retorno, status, headers, config) {
                    deferred.resolve(retorno);
                });

            return deferred.promise;
        }

        this.filtrar = function (id_contato, palavraChave) {
            var deferred = $q.defer();

            var params = JSON.stringify({
                "acao": "filtrar",
                "id_contato": id_contato,
                "palavraChave": palavraChave
            });

            $http.post(servidor + "controle/contatos_interacoes.php", params)
                .success(function (retorno, status, headers, config) {
                    deferred.resolve(retorno);
                });

            return deferred.promise;
        }
    }
);