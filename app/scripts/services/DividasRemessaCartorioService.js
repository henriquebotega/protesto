'use strict';

app.service("DividasRemessaCartorioService", function ($http, $q, StorageFactory) {

        this.getAll = function () {
            var deferred = $q.defer();

            var params = JSON.stringify({
                "acao": "read"
            });

            $http.post(servidor + "controle/dividas_remessa_cartorio.php", params)
                .success(function (retorno, status, headers, config) {
                    deferred.resolve(retorno);
                });

            return deferred.promise;
        }

        this.filtrar = function (palavraChave, filtroSituacao) {
            var deferred = $q.defer();

            var params = JSON.stringify({
                "acao": "filtrar",
                "filtroSituacao": filtroSituacao,
                "palavraChave": palavraChave
            });

            $http.post(servidor + "controle/dividas_remessa_cartorio.php", params)
                .success(function (retorno, status, headers, config) {
                    deferred.resolve(retorno);
                });

            return deferred.promise;
        }

        this.exportar = function (colRegistroAtual) {
            var deferred = $q.defer();

            var params = JSON.stringify({
                "acao": "exportar",
                "colRegistroAtual": colRegistroAtual
            });

            $http.post(servidor + "controle/exportar_cartorio.php", params)
                .success(function (retorno, status, headers, config) {
                    deferred.resolve(retorno);
                });

            return deferred.promise;
        }

        this.uploadCartorio = function (file) {
            return upload_arquivo($q, StorageFactory, servidor + 'controle/importar_cartorio.php', file, null);
        }

        this.enviarCartorio = function (colRegistroAtual) {
            var deferred = $q.defer();

            var params = JSON.stringify({
                "acao": "enviarCartorio",
                "colRegistroAtual": colRegistroAtual
            });

            $http.post(servidor + "controle/dividas_remessa_cartorio.php", params)
                .success(function (retorno, status, headers, config) {
                    deferred.resolve(retorno);
                });

            return deferred.promise;
        }
    }
);