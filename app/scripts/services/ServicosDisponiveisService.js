'use strict';

app.service("ServicosDisponiveisService", function ($http, $q) {

    this.consultar = function (id_servico, registroAtual) {
        var deferred = $q.defer();

        var params = JSON.stringify({
            "acao": "consultar",
            "id_servico": id_servico,
            "registroAtual": JSON.stringify(registroAtual)
        });

        $http.post(servidor + "controle/servicos_disponiveis.php", params)
            .success(function (retorno, status, headers, config) {
                deferred.resolve(retorno);
            });

        return deferred.promise;
    }

    this.ws_foi_desligado = function (id_servico) {
        var deferred = $q.defer();

        var params = JSON.stringify({
            "acao": "ws_foi_desligado",
            "id_servico": id_servico
        });

        $http.post(servidor + "controle/servicos_disponiveis.php", params)
            .success(function (retorno, status, headers, config) {
                deferred.resolve(retorno);
            });

        return deferred.promise;
    }

    this.historico = function (id_servico) {
        var deferred = $q.defer();

        var params = JSON.stringify({
            "acao": "historico",
            "id_servico": id_servico
        });

        $http.post(servidor + "controle/servicos_disponiveis.php", params)
            .success(function (retorno, status, headers, config) {
                deferred.resolve(retorno);
            });

        return deferred.promise;
    }

});