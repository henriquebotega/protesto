'use strict';

app.service("ContatosService", function ($http, $q, StorageFactory) {

        this.getAll = function () {
            var deferred = $q.defer();

            var params = JSON.stringify({
                "acao": "read"
            });

            $http.post(servidor + "controle/contatos.php", params)
                .success(function (retorno, status, headers, config) {
                    deferred.resolve(retorno);
                });

            return deferred.promise;
        }

        this.getByID = function (id) {
            var deferred = $q.defer();

            var params = JSON.stringify({
                "acao": "getByID",
                "id": id
            });

            $http.post(servidor + "controle/contatos.php", params)
                .success(function (retorno, status, headers, config) {
                    deferred.resolve(retorno);
                });

            return deferred.promise;
        }

        this.salvar = function (registroAtual) {
            var deferred = $q.defer();

            var params = JSON.stringify({
                "acao": "salvar",
                "registroAtual": registroAtual
            });

            $http.post(servidor + "controle/contatos.php", params)
                .success(function (retorno, status, headers, config) {
                    deferred.resolve(retorno);
                });

            return deferred.promise;
        }

        this.excluir = function (colRegistroAtual) {
            var deferred = $q.defer();

            var params = JSON.stringify({
                "acao": "excluir",
                "colRegistroAtual": colRegistroAtual
            });

            $http.post(servidor + "controle/contatos.php", params)
                .success(function (retorno, status, headers, config) {
                    deferred.resolve(retorno);
                });

            return deferred.promise;
        }

        this.filtrar = function (palavraChave, filtroSituacao) {
            var deferred = $q.defer();

            var params = JSON.stringify({
                "acao": "filtrar",
                "palavraChave": palavraChave
            });

            $http.post(servidor + "controle/contatos.php", params)
                .success(function (retorno, status, headers, config) {
                    deferred.resolve(retorno);
                });

            return deferred.promise;
        }
    }
);