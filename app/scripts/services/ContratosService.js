'use strict';

app.service("ContratosService", function ($http, $q, StorageFactory) {

        this.getAll = function () {
            var deferred = $q.defer();

            var params = JSON.stringify({
                "acao": "read"
            });

            $http.post(servidor + "controle/contratos.php", params)
                .success(function (retorno, status, headers, config) {
                    deferred.resolve(retorno);
                });

            return deferred.promise;
        }

        this.getByID = function (id) {
            var deferred = $q.defer();

            var params = JSON.stringify({
                "acao": "getByID",
                "id": id
            });

            $http.post(servidor + "controle/contratos.php", params)
                .success(function (retorno, status, headers, config) {
                    deferred.resolve(retorno);
                });

            return deferred.promise;
        }

        this.salvar = function (registroAtual) {
            var deferred = $q.defer();

            var params = JSON.stringify({
                "acao": "salvar",
                "registroAtual": registroAtual
            });

            $http.post(servidor + "controle/contratos.php", params)
                .success(function (retorno, status, headers, config) {
                    deferred.resolve(retorno);
                });

            return deferred.promise;
        }

        this.excluir = function (registroAtual) {
            var deferred = $q.defer();

            var params = JSON.stringify({
                "acao": "excluir",
                "registroAtual": registroAtual
            });

            $http.post(servidor + "controle/contratos.php", params)
                .success(function (retorno, status, headers, config) {
                    deferred.resolve(retorno);
                });

            return deferred.promise;
        }

        this.filtrar = function (palavraChave, filtroSituacao) {
            var deferred = $q.defer();

            var params = JSON.stringify({
                "acao": "filtrar",
                "filtroSituacao": filtroSituacao,
                "palavraChave": palavraChave
            });

            $http.post(servidor + "controle/contratos.php", params)
                .success(function (retorno, status, headers, config) {
                    deferred.resolve(retorno);
                });

            return deferred.promise;
        }

        this.getAllSituacao = function () {
            var deferred = $q.defer();

            var listagem = StorageFactory.get("listagemContratoSituacao");

            if (isNotBlank(listagem)) {
                deferred.resolve(listagem);

            } else {

                var params = JSON.stringify({
                    "acao": "read"
                });

                $http.post(servidor + "controle/contratos_situacao.php", params)
                    .success(function (retorno, status, headers, config) {
                        deferred.resolve(retorno.retorno);
                    });
            }

            return deferred.promise;
        }

        this.permiteRemover = function (registroAtual) {
            var deferred = $q.defer();

            var params = JSON.stringify({
                "acao": "permiteRemover",
                "registroAtual": registroAtual
            });

            $http.post(servidor + "controle/contratos.php", params)
                .success(function (retorno, status, headers, config) {
                    deferred.resolve(retorno);
                });

            return deferred.promise;
        }

        this.getContratoFinalizado = function (registroAtual) {
            var deferred = $q.defer();

            var params = JSON.stringify({
                "acao": "getContratoFinalizado",
                "registroAtual": registroAtual
            });

            $http.post(servidor + "controle/contratos.php", params)
                .success(function (retorno, status, headers, config) {
                    deferred.resolve(retorno);
                });

            return deferred.promise;
        }

    }
);