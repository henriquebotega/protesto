'use strict';

app.service("LayoutsService", function ($http, $q, StorageFactory) {

    this.getAll = function () {
        var deferred = $q.defer();

        var params = JSON.stringify({
            "acao": "read"
        });

        $http.post(servidor + "controle/layouts.php", params)
            .success(function (retorno, status, headers, config) {
                deferred.resolve(retorno);
            });

        return deferred.promise;
    }

    this.getByID = function (id) {
        var deferred = $q.defer();

        var params = JSON.stringify({
            "acao": "getByID",
            "id": id
        });

        $http.post(servidor + "controle/layouts.php", params)
            .success(function (retorno, status, headers, config) {
                deferred.resolve(retorno);
            });

        return deferred.promise;
    }

    this.salvar = function (registroAtual) {
        var deferred = $q.defer();

        var params = JSON.stringify({
            "acao": "salvar",
            "registroAtual": registroAtual
        });

        $http.post(servidor + "controle/layouts.php", params)
            .success(function (retorno, status, headers, config) {
                deferred.resolve(retorno);
            });

        return deferred.promise;
    }

    this.excluir = function (colRegistroAtual) {
        var deferred = $q.defer();

        var params = JSON.stringify({
            "acao": "excluir",
            "colRegistroAtual": colRegistroAtual
        });

        $http.post(servidor + "controle/layouts.php", params)
            .success(function (retorno, status, headers, config) {
                deferred.resolve(retorno);
            });

        return deferred.promise;
    }

    this.filtrar = function (palavraChave) {
        var deferred = $q.defer();

        var params = JSON.stringify({
            "acao": "filtrar",
            "palavraChave": palavraChave
        });

        $http.post(servidor + "controle/layouts.php", params)
            .success(function (retorno, status, headers, config) {
                deferred.resolve(retorno);
            });

        return deferred.promise;
    }

    this.download = function (layout) {
        var anchor = document.createElement('a');
        anchor.href = servidor + 'uploads/layouts/' + layout;
        anchor.target = '_blank';
        anchor.download = layout;
        anchor.click();
    }

    this.upload = function (file, id_layout) {
        return upload_arquivo($q, StorageFactory, servidor + 'controle/uploadLayouts.php', file, {'id_layout': id_layout});
    }

});