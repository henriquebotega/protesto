'use strict';

app.service("FaturasService", function ($http, $q, StorageFactory) {

        this.getAll = function () {
            var deferred = $q.defer();

            var params = JSON.stringify({
                "acao": "read"
            });

            $http.post(servidor + "controle/faturas.php", params)
                .success(function (retorno, status, headers, config) {
                    deferred.resolve(retorno);
                });

            return deferred.promise;
        }

        this.getAllEmAberto = function () {
            var deferred = $q.defer();

            var params = JSON.stringify({
                "acao": "getAllEmAberto"
            });

            $http.post(servidor + "controle/faturas.php", params)
                .success(function (retorno, status, headers, config) {
                    deferred.resolve(retorno);
                });

            return deferred.promise;
        }

        this.getAllEmAtraso = function () {
            var deferred = $q.defer();

            var params = JSON.stringify({
                "acao": "getAllEmAtraso"
            });

            $http.post(servidor + "controle/faturas.php", params)
                .success(function (retorno, status, headers, config) {
                    deferred.resolve(retorno);
                });

            return deferred.promise;
        }

    }
);