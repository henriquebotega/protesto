'use strict';

app.service("PracasService", function ($http, $q, StorageFactory) {

        this.getAll = function () {
            var deferred = $q.defer();

            var listagem = StorageFactory.get("listagemPracas");

            if (isNotBlank(listagem)) {
                deferred.resolve(listagem);

            } else {

                var params = JSON.stringify({
                    "acao": "read"
                });

                $http.post(servidor + "controle/pracas.php", params)
                    .success(function (retorno, status, headers, config) {
                        deferred.resolve(retorno.retorno);
                    });
            }

            return deferred.promise;
        }

    }
);