'use strict';

app.service("InicioService", function ($http, $q) {

    this.getTotalRegistros = function () {
        var deferred = $q.defer();

        var params = JSON.stringify({
            "acao": "totalRegistros"
        });

        $http.post(servidor + "controle/inicio.php", params)
            .success(function (retorno, status, headers, config) {
                deferred.resolve(retorno);
            });

        return deferred.promise;
    }

    this.getTotalRemessasCartorio = function () {
        var deferred = $q.defer();

        var params = JSON.stringify({
            "acao": "totalRemessasCartorio"
        });

        $http.post(servidor + "controle/inicio.php", params)
            .success(function (retorno, status, headers, config) {
                deferred.resolve(retorno);
            });

        return deferred.promise;
    }

    this.getTotalUsuariosPorCliente = function () {
        var deferred = $q.defer();

        var params = JSON.stringify({
            "acao": "totalUsuariosPorCliente"
        });

        $http.post(servidor + "controle/inicio.php", params)
            .success(function (retorno, status, headers, config) {
                deferred.resolve(retorno);
            });

        return deferred.promise;
    }

    this.getTotalDevedoresPorCliente = function () {
        var deferred = $q.defer();

        var params = JSON.stringify({
            "acao": "totalDevedoresPorCliente"
        });

        $http.post(servidor + "controle/inicio.php", params)
            .success(function (retorno, status, headers, config) {
                deferred.resolve(retorno);
            });

        return deferred.promise;
    }

    this.getConsumoMensalPorCliente = function () {
        var deferred = $q.defer();

        var params = JSON.stringify({
            "acao": "getConsumoMensalPorCliente"
        });

        $http.post(servidor + "controle/inicio.php", params)
            .success(function (retorno, status, headers, config) {
                deferred.resolve(retorno);
            });

        return deferred.promise;
    }

    this.getConsumoMensalPorWS = function () {
        var deferred = $q.defer();

        var params = JSON.stringify({
            "acao": "getConsumoMensalPorWS"
        });

        $http.post(servidor + "controle/inicio.php", params)
            .success(function (retorno, status, headers, config) {
                deferred.resolve(retorno);
            });

        return deferred.promise;
    }

});