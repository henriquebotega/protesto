'use strict';

app.service("DevedoresService", function ($http, $q) {

    this.getAll = function () {
        var deferred = $q.defer();

        var params = JSON.stringify({
            "acao": "read"
        });

        $http.post(servidor + "controle/devedores.php", params)
            .success(function (retorno, status, headers, config) {
                deferred.resolve(retorno);
            });

        return deferred.promise;
    }

    this.getAllByCliente = function (id_cliente) {
        var deferred = $q.defer();

        var params = JSON.stringify({
            "acao": "getAllByCliente",
            "id_cliente": id_cliente
        });

        $http.post(servidor + "controle/devedores.php", params)
            .success(function (retorno, status, headers, config) {
                deferred.resolve(retorno);
            });

        return deferred.promise;
    }

    this.getByID = function (id) {
        var deferred = $q.defer();

        var params = JSON.stringify({
            "acao": "getByID",
            "id": id
        });

        $http.post(servidor + "controle/devedores.php", params)
            .success(function (retorno, status, headers, config) {
                deferred.resolve(retorno);
            });

        return deferred.promise;
    }

    this.salvar = function (registroAtual) {
        var deferred = $q.defer();

        var params = JSON.stringify({
            "acao": "salvar",
            "registroAtual": registroAtual
        });

        $http.post(servidor + "controle/devedores.php", params)
            .success(function (retorno, status, headers, config) {
                deferred.resolve(retorno);
            });

        return deferred.promise;
    }

    this.excluir = function (registroAtual) {
        var deferred = $q.defer();

        var params = JSON.stringify({
            "acao": "excluir",
            "registroAtual": registroAtual
        });

        $http.post(servidor + "controle/devedores.php", params)
            .success(function (retorno, status, headers, config) {
                deferred.resolve(retorno);
            });

        return deferred.promise;
    }

    this.filtrar = function (palavraChave) {
        var deferred = $q.defer();

        var params = JSON.stringify({
            "acao": "filtrar",
            "palavraChave": palavraChave
        });

        $http.post(servidor + "controle/devedores.php", params)
            .success(function (retorno, status, headers, config) {
                deferred.resolve(retorno);
            });

        return deferred.promise;
    }

    this.getDividasByDevedor = function (registroAtual) {
        var deferred = $q.defer();

        var params = JSON.stringify({
            "acao": "getDividasByDevedor",
            "registroAtual": registroAtual
        });

        $http.post(servidor + "controle/devedores.php", params)
            .success(function (retorno, status, headers, config) {
                deferred.resolve(retorno);
            });

        return deferred.promise;
    }

});