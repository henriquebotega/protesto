'use strict';

app.service("DividasRemessaBancoService", function ($http, $q, StorageFactory) {

        this.getAll = function () {
            var deferred = $q.defer();

            var params = JSON.stringify({
                "acao": "read"
            });

            $http.post(servidor + "controle/dividas_remessa_banco.php", params)
                .success(function (retorno, status, headers, config) {
                    deferred.resolve(retorno);
                });

            return deferred.promise;
        }

        this.filtrar = function (palavraChave, filtroSituacao) {
            var deferred = $q.defer();

            var params = JSON.stringify({
                "acao": "filtrar",
                "filtroSituacao": filtroSituacao,
                "palavraChave": palavraChave
            });

            $http.post(servidor + "controle/dividas_remessa_banco.php", params)
                .success(function (retorno, status, headers, config) {
                    deferred.resolve(retorno);
                });

            return deferred.promise;
        }

        this.exportar = function (colRegistroAtual) {
            var deferred = $q.defer();

            var params = JSON.stringify({
                "acao": "exportar",
                "colRegistroAtual": colRegistroAtual
            });

            $http.post(servidor + "controle/exportar_banco.php", params)
                .success(function (retorno, status, headers, config) {
                    deferred.resolve(retorno);
                });

            return deferred.promise;
        }

        this.uploadBanco = function (file) {
            return upload_arquivo($q, StorageFactory, servidor + 'controle/importar_banco.php', file, null);
        }
    }
);