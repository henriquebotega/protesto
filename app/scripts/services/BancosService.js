'use strict';

app.service("BancosService", function ($http, $q) {

    this.getAll = function () {
        var deferred = $q.defer();

        var params = JSON.stringify({
            "acao": "read"
        });

        $http.post(servidor + "controle/bancos.php", params)
            .success(function (retorno, status, headers, config) {
                deferred.resolve(retorno);
            });

        return deferred.promise;
    }

});