'use strict';

app.service("AvisosService", function ($http, $q, StorageFactory) {

        this.getAll = function () {
            var deferred = $q.defer();

            var params = JSON.stringify({
                "acao": "read"
            });

            $http.post(servidor + "controle/avisos.php", params)
                .success(function (retorno, status, headers, config) {
                    deferred.resolve(retorno);
                });

            return deferred.promise;
        }

        this.getAllEmAberto = function () {
            var deferred = $q.defer();

            var params = JSON.stringify({
                "acao": "getAllEmAberto"
            });

            $http.post(servidor + "controle/avisos.php", params)
                .success(function (retorno, status, headers, config) {
                    deferred.resolve(retorno);
                });

            return deferred.promise;
        }

        this.getByID = function (id) {
            var deferred = $q.defer();

            var params = JSON.stringify({
                "acao": "getByID",
                "id": id
            });

            $http.post(servidor + "controle/avisos.php", params)
                .success(function (retorno, status, headers, config) {
                    deferred.resolve(retorno);
                });

            return deferred.promise;
        }

        this.salvar = function (registroAtual) {
            var deferred = $q.defer();

            var params = JSON.stringify({
                "acao": "salvar",
                "registroAtual": registroAtual
            });

            $http.post(servidor + "controle/avisos.php", params)
                .success(function (retorno, status, headers, config) {
                    deferred.resolve(retorno);
                });

            return deferred.promise;
        }

        this.definirComoLido = function (id) {
            var deferred = $q.defer();

            var params = JSON.stringify({
                "acao": "definirComoLido",
                "id": id
            });

            $http.post(servidor + "controle/avisos.php", params)
                .success(function (retorno, status, headers, config) {
                    deferred.resolve(retorno);
                });

            return deferred.promise;
        }

        this.excluir = function (colRegistroAtual) {
            var deferred = $q.defer();

            var params = JSON.stringify({
                "acao": "excluir",
                "colRegistroAtual": colRegistroAtual
            });

            $http.post(servidor + "controle/avisos.php", params)
                .success(function (retorno, status, headers, config) {
                    deferred.resolve(retorno);
                });

            return deferred.promise;
        }

        this.filtrar = function (palavraChave, filtroTipo) {
            var deferred = $q.defer();

            var params = JSON.stringify({
                "acao": "filtrar",
                "filtroTipo": filtroTipo,
                "palavraChave": palavraChave
            });

            $http.post(servidor + "controle/avisos.php", params)
                .success(function (retorno, status, headers, config) {
                    deferred.resolve(retorno);
                });

            return deferred.promise;
        }
    }
);