'use strict';

app.service("MensalidadesService", function ($http, $q, StorageFactory) {

        this.getAll = function () {
            var deferred = $q.defer();

            var params = JSON.stringify({
                "acao": "read"
            });

            $http.post(servidor + "controle/mensalidades.php", params)
                .success(function (retorno, status, headers, config) {
                    deferred.resolve(retorno);
                });

            return deferred.promise;
        }

        this.getByID = function (id) {
            var deferred = $q.defer();

            var params = JSON.stringify({
                "acao": "getByID",
                "id": id
            });

            $http.post(servidor + "controle/mensalidades.php", params)
                .success(function (retorno, status, headers, config) {
                    deferred.resolve(retorno);
                });

            return deferred.promise;
        }

        this.darBaixa = function (registroAtual) {
            var deferred = $q.defer();

            var params = JSON.stringify({
                "acao": "darBaixa",
                "registroAtual": registroAtual
            });

            $http.post(servidor + "controle/mensalidades.php", params)
                .success(function (retorno, status, headers, config) {
                    deferred.resolve(retorno);
                });

            return deferred.promise;
        }

        this.filtrar = function (palavraChave, filtroSituacao) {
            var deferred = $q.defer();

            var params = JSON.stringify({
                "acao": "filtrar",
                "filtroSituacao": filtroSituacao,
                "palavraChave": palavraChave
            });

            $http.post(servidor + "controle/mensalidades.php", params)
                .success(function (retorno, status, headers, config) {
                    deferred.resolve(retorno);
                });

            return deferred.promise;
        }

        this.gerar = function () {
            var deferred = $q.defer();

            var params = JSON.stringify({
                "acao": "gerar"
            });

            $http.post(servidor + "controle/mensalidades.php", params)
                .success(function (retorno, status, headers, config) {
                    deferred.resolve(retorno);
                });

            return deferred.promise;
        }

        this.getAllSituacao = function () {
            var deferred = $q.defer();

            var listagem = StorageFactory.get("listagemMensalidadesSituacao");

            if (isNotBlank(listagem)) {
                deferred.resolve(listagem);

            } else {

                var params = JSON.stringify({
                    "acao": "read"
                });

                $http.post(servidor + "controle/mensalidades_situacao.php", params)
                    .success(function (retorno, status, headers, config) {
                        deferred.resolve(retorno.retorno);
                    });
            }

            return deferred.promise;
        }

    }
);