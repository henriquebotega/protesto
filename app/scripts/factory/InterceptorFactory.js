'use strict';

app.factory('InterceptorFactory', function ($q, $location, StorageFactory) {
    return {
        request: function (config) {
            config.headers = config.headers || {};

            config.headers['Content-Type'] = 'application/json;charset=UTF-8';
            config.headers['Accept'] = 'application/json;charset=UTF-8';

            // Envia o "TOKEN" em todas as requisições, menos Login
            if (isBlank(config.url.indexOf("login")) && isNotBlank(StorageFactory.get('logado'))) {
                // Chrome
                config.headers['Authorization'] = StorageFactory.get('logado').token;
                // Firefox
                config.headers['WWW-Authorization'] = StorageFactory.get('logado').token;
            }

            return config || $q.when(config);
        },
        requestError: function (rejection) {
            return $q.reject(rejection);
        },
        response: function (response) {

            // Se expirar a sessão do servidor, redireciona para o Login
            // Nivel 1 = (livre), 2 = (aviso), 3 = (fatal)
            if(response.data['success'] === false && response.data['nivel'] == 3) {
                StorageFactory.clearAll();
                $location.path('/login');
                event.preventDefault();
            }

            return response || $q.when(response);
        },
        responseError: function (rejection) {
            return $q.reject(rejection);
        }
    };
});