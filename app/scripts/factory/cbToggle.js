app.directive('toggleCheckbox', function($timeout) {

    /**
     * Directive
     */
    return {
        restrict: 'A',
        transclude: true,
        replace: false,
        require: 'ngModel',
        priority: 1500,
        scope: {
            ngModel:'='
        },
        link: function ($scope, $element, $attr, ctrl) {

            // update model from Element
            var updateModelFromElement = function() {
                // If modified
                var checked = $element.prop('checked');
                if (checked != ctrl.$viewValue) {
                    // Update ngModel
                    ctrl.$setViewValue(checked);
                    $scope.$apply();
                }
            };

            // Update input from Model
            var updateElementFromModel = function(newValue) {
                // If modified
                if ($element.prop('checked') != newValue) {
                    // Update button state to match model
                    $element.prop('checked', newValue).change();
                }
            };

            // Observe: Element changes affect Model
            $element.on('change', function() {
                updateModelFromElement();
            });

            // Observe: ngModel for changes
            $scope.$watch('ngModel', function(newValue) {
                updateElementFromModel(newValue);
            });

            // Initialise BootstrapToggle
            // Warning: timeout might not be needed (just my code?)
            $timeout(function() {
                $element.bootstrapToggle();
            });
        }
    };
});