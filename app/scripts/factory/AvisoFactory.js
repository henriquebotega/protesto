app.factory("AvisoFactory", function () {

    function aviso(codigo, mensagem) {

        var avisoFlutuante = {
            codigo: codigo,
            mensagem: mensagem
        }

        if (avisoFlutuante != null) {
            var typeModal = 'success';

            switch (avisoFlutuante.codigo) {
                case 1:
                    typeModal = 'success';
                    break;

                case 2:
                    typeModal = 'error';
                    break;

                case 3:
                    typeModal = 'warning';
                    break;

                default:
                    typeModal = 'info';
            }

            swal({
                title: avisoFlutuante.mensagem,
                type: typeModal,
                confirmButtonText: "OK"
            });
        }
    }

    return {
        success: function (mensagem) {
            aviso(1, mensagem);
        },
        error: function (mensagem) {
            aviso(2, mensagem);
        },
        warning: function (mensagem) {
            aviso(3, mensagem);
        },
        info: function (mensagem) {
            aviso(4, mensagem);
        }
    }
});