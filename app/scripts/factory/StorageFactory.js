'use strict';

app.factory('StorageFactory', function ($window) {
    return {
        get: function (key) {
            var keyName = appName + '-' + key;
            var item = $window.sessionStorage.getItem(keyName);

            if (item != "undefined" && item != null) {
                if (appDebug) {
                    return JSON.parse(item);
                } else {
                    var decrypted = CryptoJS.TripleDES.decrypt(item, appName);
                    return JSON.parse(decrypted.toString(CryptoJS.enc.Utf8));
                }
            }

            return null;
        },

        save: function (key, data) {
            var keyName = appName + '-' + key;

            if (appDebug) {
                $window.sessionStorage.setItem(keyName, angular.toJson(data));
            } else {
                var encrypted = CryptoJS.TripleDES.encrypt(angular.toJson(data), appName);
                $window.sessionStorage.setItem(keyName, encrypted);
            }
        },

        remove: function (key) {
            $window.sessionStorage.removeItem(key);
        },

        clearAll: function () {
            $window.sessionStorage.clear();
        }
    };
});