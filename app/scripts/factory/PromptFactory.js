app.factory("PromptFactory", function ($q) {

    return {
        info: function (titulo, mensagem) {
            var deferred = $q.defer();

            swal({
                    title: titulo,
                    text: mensagem,
                    type: "info",
                    showCancelButton: true,
                    confirmButtonText: "Continuar",
                    cancelButtonText: "Desistir"
                },
                function (ret) {
                    deferred.resolve(ret);
                }
            );

            return deferred.promise;
        }
    }
});