'use strict';

app.controller('ModalInfo', function ($scope, $rootScope, $uibModalInstance, registroModal) {

    $scope.registroAtual = {
        descricao: registroModal.erros
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});