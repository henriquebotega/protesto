'use strict';

app.controller('ModalHistorico', function ($scope, $rootScope, $state, StorageFactory, UsuariosService, AvisoFactory, $uibModalInstance, registroModal, ServicosDisponiveisService) {

    $scope.itensSelecionados = [];
    $scope.retHTML = [];
    $scope.loading = true;
    $scope.exibirDetalhes = false;
    $scope.detalhesRetorno = "";

    $scope.registroAtual = {
        titulo: registroModal.titulo,
        id_servico: registroModal.id
    }

    $scope.init = function () {
        ServicosDisponiveisService.historico($scope.registroAtual.id_servico).then(function (ret) {
            $scope.loading = false;

            var theGrid = jQuery('#gridHistorico').dxDataGrid('instance');
            theGrid.option('dataSource', []);

            if (ret.success) {
                theGrid.option('dataSource', ret.retorno);
                theGrid.refresh();
                theGrid.repaint();
            }
        });
    }

    $scope.dataGridOptions = {
        dataSource: [],
        noDataText: 'Nenhum registro encontrado',
        rowAlternationEnabled: true,
        paging: {
            pageSize: 5
        },
        selection: {
            mode: 'multiple'
        },
        onSelectionChanged: function (data) {
            $scope.itensSelecionados = data.selectedRowsData;
            $scope.detalhesHistorico();
        },
        columns: [
            {dataField: 'id', caption: '#', width: 60, alignment: 'center'},
            {dataField: 'grid_situacao', caption: 'Situação'},
            {dataField: 'grid_valor', width: 150, caption: 'Valor'},
            {dataField: 'grid_data', width: 150, caption: 'Data'}
        ]
    };

    $scope.detalhesHistorico = function () {
        $scope.exibirDetalhes = false;

        if ($scope.itensSelecionados.length == 1) {
            $scope.exibirDetalhes = true;
            $scope.detalhes = $scope.itensSelecionados[0].historico;

            // Formata os dados para exibição
            $scope.formatarRetorno();
        }
    }

    $scope.formatarRetorno = function () {

        var me = this;
        var ret = [];
        var retorno = JSON.parse($scope.detalhes);

        if (retorno.code == '000') {
            var texto = "";

            var info = retornaConteudo(retorno.content);

            angular.forEach(info, function (valueInfo, keyInfo) {

                if(valueInfo['existe_informacao'] == "SIM") {

                    if(Array.isArray(valueInfo['conteudo'])){

                        valueInfo['conteudo'].map(function(objFilho){
                            texto += "<table width='100%' class='tblListagemWS'>";

                            for (var i in objFilho) {
                                if (objFilho.hasOwnProperty(i)) {
                                    texto += "<tr>";
                                    texto += "<th>";
                                    texto += primeiraLetraMaiuscula(i);
                                    texto += "</th>";
                                    texto += "<td>";
                                    texto += formatarValor(objFilho[i]);
                                    texto += "</td>";
                                    texto += "</tr>";
                                }
                            }

                            texto += "</table>";
                            texto += "<hr>";
                        });

                    } else {
                        texto += mostrarProps(valueInfo['conteudo']);
                    }

                }
            });

            $scope.detalhesRetorno = texto;

        } else {
            $scope.detalhesRetorno = retorno.message;
        }
    }

    $scope.formatarValor = function (conteudo) {
        var ehDataAmericana = conteudo.match(/\b(\d+\-\d+\-\d+)\b/g);

        if (ehDataAmericana) {
            var dataConvertida = conteudo.split("-");
            return dataConvertida[2] + "/" + dataConvertida[1] + "/" + dataConvertida[0];
        } else {
            return conteudo;
        }
    }

    $scope.formatarChave = function (conteudo) {
        return primeiraLetraMaiuscula(conteudo);
    }

    $scope.retornaConteudo = function (conteudo) {
        var colRetorno = [];

        angular.forEach(conteudo, function (filterObj, filterIndex) {
            colRetorno.push(filterObj);
        });

        var colUnico = colRetorno.filter(function (elem, pos, self) {
            return self.indexOf(elem) == pos;
        });

        return colUnico;
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.init();
});