'use strict';

app.controller('ModalAlterarSenhaUsuarios', function ($scope, $rootScope, $state, StorageFactory, UsuariosService, AvisoFactory, $uibModalInstance, registroModal) {

    $scope.registroAtual = {
        id: registroModal.id,
        id_tipo: '0',
        senha_atual: '',
        nova_senha: ''
    }
    
    $scope.ok = function () {
        UsuariosService.salvarNovaSenha($scope.registroAtual).then(function (ret) {
            if (ret.success) {
                $uibModalInstance.close(true);
            } else {
                AvisoFactory.warning(ret.mensagem);
            }
        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});