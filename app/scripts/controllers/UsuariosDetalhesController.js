'use strict';

app.controller('UsuariosDetalhesController', function ($scope, $rootScope, $stateParams, $state, MenuService, StorageFactory, ClientesService, UsuariosService, AvisoFactory) {

    $scope.titulo = "Novo Usuário";
    $scope.loading = true;

    $scope.registroAtual = {
        id_situacao: "0",
        email: ""
    }

    $scope.array_clientes = [];

    ClientesService.getAll().then(function (ret) {
        ret.retorno.map(function(o, i, a) {

            var obj = o;

            if(obj.id_tipo == 1){
                obj.cb_nome = o.nome;
            } else {
                obj.cb_nome = o.razao_social;
            }

            $scope.array_clientes.push(obj);
        });
    });

    $scope.array_situacao = [
        {id: "0", titulo: "Desativado"},
        {id: "1", titulo: "Ativado"}
    ]

    if (isNotBlank($stateParams.id)) {
        $scope.titulo = "Editar Usuário";

        UsuariosService.getByID($stateParams.id).then(function (ret) {
            if (ret.success) {
                $scope.loading = false;
                $scope.registroAtual = ret.retorno[0];
            } else {
                AvisoFactory.warning("Este registro não existe!");
                $state.go("extranet.usuarios");
            }
        });
    } else {
        $scope.loading = false;
    }

    $scope.salvar = function () {
        UsuariosService.salvar($scope.registroAtual).then(function (ret) {
            AvisoFactory.success("Registro salvo com sucesso!");
            
            $rootScope.$broadcast('atualizar_intranet_total_usuarios', true);
            $rootScope.$broadcast('atualizar_extranet_total_usuarios', true);
            $state.go("extranet.usuarios");
        });
    }

    $scope.voltar = function () {
        $state.go("extranet.usuarios")
    }
});