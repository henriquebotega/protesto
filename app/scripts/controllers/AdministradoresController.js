'use strict';

app.controller('AdministradoresController', function ($scope, $rootScope, $timeout, $state, AdministradoresService, AvisoFactory, PromptFactory, $uibModal) {

    $scope.itensSelecionados = [];
    $scope.loading = true;

    $scope.array_situacao = [
        {id: "0", titulo: "Desativado"},
        {id: "1", titulo: "Ativado"}
    ]

    $scope.init = function () {
        AdministradoresService.getAll().then(function (ret) {
            $scope.loading = false;

            var theGrid = jQuery('#gridAdministradores').dxDataGrid('instance');
            theGrid.option('dataSource', []);

            if (ret.success) {
                theGrid.option('dataSource', ret.retorno);
                theGrid.refresh();
                theGrid.repaint();
            }
        });
    }

    $scope.dataGridOptions = {
        dataSource: [],
        noDataText: 'Nenhum registro encontrado',
        rowAlternationEnabled: true,
        paging: {
            pageSize: configSistema.qtd_itens_listagem
        },
        export: {
            enabled: true,
            fileName: "tbl-Administradores",
            allowExportSelectedData: true
        },
        selection: {
            mode: 'multiple'
        },
        onSelectionChanged: function (data) {
            $scope.itensSelecionados = data.selectedRowsData;
        },
        columns: [
            {dataField: 'id', caption: '#', width: 60, alignment: 'center'},
            {dataField: 'grid_email', caption: 'E-mail'},
            {dataField: 'grid_situacao', width: 100, caption: 'Situação'},
            {dataField: 'grid_ultimo_acesso', width: 150, caption: 'Último Acesso'}
        ]
    };

    $scope.novo = function () {
        $state.go("intranet.detalhes_administradores", {id: null})
    }

    $scope.limpar_filtro = function () {
        $scope.palavraChave = "";
        $scope.filtroSituacao = [];
        $scope.init();
    }

    $scope.filtrar = function () {
        AdministradoresService.filtrar($scope.palavraChave, $scope.filtroSituacao).then(function (ret) {
            var theGrid = jQuery('#gridAdministradores').dxDataGrid('instance');
            theGrid.option('dataSource', []);

            if (ret.success) {
                theGrid.option('dataSource', ret.retorno);
                theGrid.refresh();
                theGrid.repaint();
            }
        });
    }

    $scope.modalAlterarSenha = function () {

        var modalInstance = $uibModal.open({
            templateUrl: 'views/intranet/administradores/modalAlterarSenha.html',
            controller: 'ModalAlterarSenhaAdmin',
            resolve: {
                registroModal: function () {
                    return $scope.itensSelecionados[0];
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            AvisoFactory.success("Registro salvo com sucesso.");
        });
    }

    $scope.editar = function () {
        if ($scope.itensSelecionados.length == 1) {
            $state.go("intranet.detalhes_administradores", {id: $scope.itensSelecionados[0].id})
        }
    }

    $scope.excluir = function () {
        PromptFactory.info("Deseja realmente remover o(s) registro(s) selecionado(s)?", "").then(function (retPrompt) {
            if (retPrompt) {

                $timeout(function () {
                    AdministradoresService.excluir($scope.itensSelecionados).then(function () {
                        AvisoFactory.success("Registro removido com sucesso.");

                        $rootScope.$broadcast('atualizar_intranet_total_administradores', true);
                        $scope.init();
                    });
                }, 500);

            }
        });
    }

    $scope.init();
});