'use strict';

app.controller('FaturasController', function ($scope, $rootScope, $state, FaturasService) {

    $scope.itensSelecionados = [];
    $scope.loading = true;

    $scope.init = function () {
        FaturasService.getAll().then(function (ret) {
            $scope.loading = false;
            var theGrid = jQuery('#gridFaturas').dxDataGrid('instance');
            theGrid.option('dataSource', []);

            if (ret.success) {
                theGrid.option('dataSource', ret.retorno);
                theGrid.refresh();
                theGrid.repaint();
            }
        });
    }

    $scope.dataGridOptions = {
        dataSource: [],
        noDataText: 'Nenhum registro encontrado',
        rowAlternationEnabled: true,
        paging: {
            pageSize: configSistema.qtd_itens_listagem
        },
        export: {
            enabled: true,
            fileName: "tbl-Faturas",
            allowExportSelectedData: true
        },
        selection: {
            mode: 'multiple'
        },
        onSelectionChanged: function (data) {
            $scope.itensSelecionados = data.selectedRowsData;
        },
        columns: [
            {dataField: 'id', caption: '#', width: 60, alignment: 'center'},
            {dataField: 'grid_id_contrato', width: 80, alignment: 'center', caption: 'Contrato'},
            {dataField: 'grid_situacao', caption: 'Situação'},
            {dataField: 'grid_valor_vcto', width: 150, alignment: 'center', caption: 'Valor de Vencimento'},
            {dataField: 'grid_data_vcto', width: 150, alignment: 'center', caption: 'Data de Vencimento'},
            {dataField: 'grid_valor_pgto', width: 150, alignment: 'center', caption: 'Valor de Pagamento'},
            {dataField: 'grid_data_pgto', width: 150, alignment: 'center', caption: 'Data de Pagamento'}
        ]
    };

    $scope.taxas = function () {
        if ($scope.itensSelecionados.length == 1) {
            $state.go("extranet.taxas_faturas", {id_mensalidade: $scope.itensSelecionados[0].id})
        }
    }

    $scope.init();
});