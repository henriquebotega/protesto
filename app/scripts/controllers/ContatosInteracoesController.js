'use strict';

app.controller('ContatosInteracoesController', function ($scope, $rootScope, $state, $stateParams, AvisoFactory, StorageFactory, ContatosService, ContatosInteracoesService, PromptFactory) {

    $scope.itensSelecionados = [];
    $scope.loading = true;

    $scope.init = function () {
        if (isNotBlank($stateParams.id_contato)) {
            ContatosInteracoesService.getAll($stateParams.id_contato).then(function (ret) {
                $scope.loading = false;

                var theGrid = jQuery('#gridContatosInteracoes').dxDataGrid('instance');
                theGrid.option('dataSource', []);

                if (ret.success) {
                    theGrid.option('dataSource', ret.retorno);
                    theGrid.refresh();
                    theGrid.repaint();
                }
            });
        } else {
            $state.go("extranet.contatos")
        }
    }

    $scope.dataGridOptions = {
        dataSource: [],
        noDataText: 'Nenhum registro encontrado',
        rowAlternationEnabled: true,
        paging: {
            pageSize: configSistema.qtd_itens_listagem
        },
        export: {
            enabled: true,
            fileName: "tbl-ContatosInteracoes",
            allowExportSelectedData: true
        },
        selection: {
            mode: 'multiple'
        },
        onSelectionChanged: function (data) {
            $scope.itensSelecionados = data.selectedRowsData;
        },
        columns: [
            {dataField: 'id', caption: '#', width: 60, alignment: 'center'},
            {dataField: 'grid_resposta', caption: 'Citado por', width: 130, alignment: 'center'},
            {dataField: 'grid_mensagem', caption: 'Mensagem'},
            {dataField: 'grid_data', width: 150, caption: 'Data'}
        ]
    };

    $scope.novo = function () {
        $state.go("extranet.detalhes_interacoes_contatos", {id_contato: $stateParams.id_contato, id: null})
    }

    $scope.limpar_filtro = function () {
        $scope.palavraChave = "";
        $scope.init();
    }

    $scope.filtrar = function () {
        ContatosInteracoesService.filtrar($stateParams.id_contato, $scope.palavraChave).then(function (ret) {
            var theGrid = jQuery('#gridContatosInteracoes').dxDataGrid('instance');
            theGrid.option('dataSource', []);

            if (ret.success) {
                theGrid.option('dataSource', ret.retorno);
                theGrid.refresh();
                theGrid.repaint();
            }
        });
    }

    $scope.editar = function () {
        if ($scope.itensSelecionados.length == 1) {
            $state.go("extranet.detalhes_interacoes_contatos", {id_contato: $stateParams.id_contato, id: $scope.itensSelecionados[0].id})
        }
    }

    $scope.excluir = function () {
        PromptFactory.info("Deseja realmente remover o(s) registro(s) selecionado(s)?", "").then(function (retPrompt) {
            if (retPrompt) {
                ContatosInteracoesService.excluir($scope.itensSelecionados).then(function () {
                    AvisoFactory.success("Registro removido com sucesso.");

                    $rootScope.$broadcast('atualizar_extranet_total_contatos', true);
                    $rootScope.$broadcast('atualizar_intranet_total_contatos', true);
                    $scope.init();
                });
            }
        });
    }

    $scope.voltar = function () {
        $state.go("extranet.contatos")
    }

    $scope.init();
});