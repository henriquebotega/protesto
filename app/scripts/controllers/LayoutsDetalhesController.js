'use strict';

app.controller('LayoutsDetalhesController', function ($scope, $rootScope, $stateParams, $state, MenuService, StorageFactory, LayoutsService, AvisoFactory) {

    $scope.titulo = "Novo Layout";
    $scope.loading = true;

    $scope.registroAtual = {
        titulo: ""
    };

    $scope.arquivoLayout = null;

    if (isNotBlank($stateParams.id)) {
        $scope.titulo = "Editar Layout";

        LayoutsService.getByID($stateParams.id).then(function (ret) {
            if (ret.success) {
                $scope.loading = false;
                $scope.registroAtual = ret.retorno[0];
            } else {
                AvisoFactory.warning("Este registro não existe!");
                $state.go("extranet.layouts");
            }
        });
    } else {
        $scope.loading = false;
    }

    $scope.salvar = function () {
        LayoutsService.salvar($scope.registroAtual).then(function (ret) {

            if ($scope.arquivoLayout) {
                LayoutsService.upload($scope.arquivoLayout, ret['id_layout']).then(function (retUpload) {
                    AvisoFactory.success("Registro salvo com sucesso!");

                    $rootScope.$broadcast('atualizar_extranet_total_layouts', true);
                    $state.go("extranet.layouts");
                });

            } else {
                AvisoFactory.success("Registro salvo com sucesso!");

                $rootScope.$broadcast('atualizar_extranet_total_layouts', true);
                $state.go("extranet.layouts");
            }

        });
    }

    $scope.voltar = function () {
        $state.go("extranet.layouts")
    }
});