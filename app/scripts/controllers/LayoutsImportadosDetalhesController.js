'use strict';

app.controller('LayoutsImportadosDetalhesController', function ($scope, $rootScope, $stateParams, $state, MenuService, StorageFactory, LayoutsImportadosService, LayoutsService, ClientesService, AvisoFactory) {

    $scope.titulo = "Novo Layout Importado";
    $scope.loading = true;

    $scope.registroAtual = {
        id_cliente: "0",
        id_layout: "0"
    };

    $scope.arquivoLayoutImportado = null;

    $scope.array_clientes = [];
    $scope.array_layouts = [];
    $scope.loading = false;

    ClientesService.getAll().then(function (ret) {
        ret.retorno.map(function (o, i, a) {
            var obj = o;

            if (obj.id_tipo == 1) {
                obj.cb_nome = o.nome;
            } else {
                obj.cb_nome = o.razao_social;
            }

            $scope.array_clientes.push(obj);
        });
    });

    LayoutsService.getAll().then(function (ret) {
        $scope.array_layouts = ret.retorno;
    });

    $scope.salvar = function () {
        LayoutsImportadosService.salvar($scope.registroAtual).then(function (ret) {

            if ($scope.arquivoLayoutImportado) {
                LayoutsImportadosService.upload($scope.arquivoLayoutImportado, ret['id_layout_importado'], $scope.registroAtual).then(function (retUpload) {
                    AvisoFactory.success("Registro salvo com sucesso!");

                    $rootScope.$broadcast('atualizar_intranet_total_devedores', true);
                    $rootScope.$broadcast('atualizar_extranet_total_devedores', true);
                    $rootScope.$broadcast('atualizar_intranet_total_dividas', true);
                    $rootScope.$broadcast('atualizar_extranet_total_dividas', true);

                    $rootScope.$broadcast('atualizar_extranet_total_layouts_importados', true);
                    $state.go("extranet.layouts_importados");
                });

            } else {
                AvisoFactory.success("Registro salvo com sucesso!");

                $rootScope.$broadcast('atualizar_extranet_total_layouts_importados', true);
                $state.go("extranet.layouts_importados");
            }

        });
    }

    $scope.voltar = function () {
        $state.go("extranet.layouts_importados")
    }
});