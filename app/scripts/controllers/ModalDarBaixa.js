'use strict';

app.controller('ModalDarBaixa', function ($scope, $rootScope, $state, StorageFactory, UsuariosService, MensalidadesService, AvisoFactory, $uibModalInstance, registroModal) {

    $scope.registroAtual = {
        id: registroModal.id,
        id_situacao: 2,
        valor_pgto: registroModal.valor_vcto,
        data_pgto: dataAtual
    }

    $scope.ok = function () {
        MensalidadesService.darBaixa($scope.registroAtual).then(function (ret) {
            if (ret.success) {
                $uibModalInstance.close(true);
            } else {
                AvisoFactory.warning(ret.mensagem);
            }
        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});