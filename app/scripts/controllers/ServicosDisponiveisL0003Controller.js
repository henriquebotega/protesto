'use strict';

app.controller('ServicosDisponiveisL0003Controller', function ($scope, $rootScope, $state, $stateParams, ServicosDisponiveisService, AvisoFactory, StorageFactory, ServicosService, $uibModal) {

    jQuery(function () {
        jQuery('[data-toggle="popover"]').popover();
    });

    $scope.loading = true;
    $scope.loadingDetalhes = true;
    $scope.exibeDetalhes = false;
    $scope.detalhes = "";
    $scope.detalhesRetorno = "";
    $scope.total_historico = 0;

    $scope.registroAtual = {
        ddd: "",
        telefone: ""
    };

    $scope.servicoAtual = {
        titulo: "",
        descricao: ""
    };

    if (isNotBlank($stateParams.id_servico)) {

        var servDisp = false;
        StorageFactory.get("listagemServicosDisponiveis").map(function (o, i, a) {
            if (o.id_servico == $stateParams.id_servico) {
                servDisp = true;
            }
        });

        if (servDisp) {
            ServicosService.getByID($stateParams.id_servico).then(function (ret) {
                if (ret.success) {
                    $scope.loading = false;
                    $scope.servicoAtual = ret.retorno[0];
                } else {
                    AvisoFactory.warning("Este serviço não existe!");
                    $state.go("extranet.principal");
                }
            });
        } else {
            AvisoFactory.warning("Este serviço não está disponível!");
            $state.go("extranet.principal");
        }

        ServicosDisponiveisService.historico($stateParams.id_servico).then(function (ret) {
            if (ret.success) {
                $scope.total_historico = ret.total;
            }
        });
    }

    $scope.modalHistorico = function () {
        $uibModal.open({
            size: 'lg',
            templateUrl: 'views/extranet/servicos_disponiveis/modalHistorico.html',
            controller: 'ModalHistorico',
            resolve: {
                registroModal: function () {
                    return $scope.servicoAtual;
                }
            }
        });
    }

    $scope.consultar = function () {

        if (isBlank($scope.registroAtual.ddd) || isBlank($scope.registroAtual.telefone)) {
            AvisoFactory.warning("Preencha o campo solicitado.");
            return;
        }

        // Verifica se algum serviço foi desligado pelo Administrador
        ServicosDisponiveisService.ws_foi_desligado($stateParams.id_servico).then(function (retDesligado) {

            if (!retDesligado['success']) {
                AvisoFactory.warning("Este serviço não está disponível no momento, contate seu Administrador!");
                return;
            } else {

                $scope.exibeDetalhes = true;
                $scope.loadingDetalhes = true;
                $scope.detalhes = "";
                $scope.detalhesRetorno = "";

                ServicosDisponiveisService.consultar($stateParams.id_servico, $scope.registroAtual).then(function (ret) {
                    $scope.loadingDetalhes = false;

                    // $scope.detalhes = ret.retorno;
                    $scope.detalhes = JSON.parse(ret.retorno);

                    ServicosDisponiveisService.historico($stateParams.id_servico).then(function (retHistorico) {
                        if (retHistorico.success) {
                            $scope.total_historico = retHistorico.total;
                        }
                    });

                    if ($scope.detalhes.code == '000') {
                        var texto = "";

                        var info = retornaConteudo($scope.detalhes.content);

                        angular.forEach(info, function (valueInfo, keyInfo) {

                            if(valueInfo['existe_informacao'] == "SIM") {

                                if(Array.isArray(valueInfo['conteudo'])){

                                    valueInfo['conteudo'].map(function(objFilho){
                                        texto += "<table width='100%' class='tblListagemWS'>";

                                        for (var i in objFilho) {
                                            if (objFilho.hasOwnProperty(i)) {
                                                texto += "<tr>";
                                                texto += "<th>";
                                                texto += primeiraLetraMaiuscula(i);
                                                texto += "</th>";
                                                texto += "<td>";
                                                texto += formatarValor(objFilho[i]);
                                                texto += "</td>";
                                                texto += "</tr>";
                                            }
                                        }

                                        texto += "</table>";
                                        texto += "<hr>";
                                    });

                                } else {
                                    texto += mostrarProps(valueInfo['conteudo']);
                                }

                            }
                        });

                        $scope.detalhesRetorno = texto;

                    } else {
                        $scope.detalhesRetorno = $scope.detalhes.message;
                    }

                });
            }
        });
    }

});