'use strict';

app.controller('TaxasController', function ($scope, $rootScope, $state, TaxasService, AvisoFactory, StorageFactory, PromptFactory, $uibModal) {

    $scope.itensSelecionados = [];
    $scope.loading = true;
    $scope.exibeBtnExcluir = true;
    $scope.array_situacao = StorageFactory.get("listagemTaxasSituacao");

    $scope.init = function () {
        TaxasService.getAll().then(function (ret) {
            $scope.loading = false;
            var theGrid = jQuery('#gridTaxas').dxDataGrid('instance');
            theGrid.option('dataSource', []);

            if (ret.success) {
                theGrid.option('dataSource', ret.retorno);
                theGrid.refresh();
                theGrid.repaint();
            }
        });
    }

    $scope.dataGridOptions = {
        dataSource: [],
        noDataText: 'Nenhum registro encontrado',
        rowAlternationEnabled: true,
        paging: {
            pageSize: configSistema.qtd_itens_listagem
        },
        export: {
            enabled: true,
            fileName: "tbl-Taxas",
            allowExportSelectedData: true
        },
        selection: {
            mode: 'multiple'
        },
        onSelectionChanged: function (data) {
            $scope.itensSelecionados = data.selectedRowsData;
            $scope.habilitarBotoes();
        },
        columns: [
            {dataField: 'id', caption: '#', width: 60, alignment: 'center'},
            {dataField: 'grid_id_mensalidade', width: 100, alignment: 'center', caption: 'Mensalidade'},
            {dataField: 'grid_cliente', caption: 'Cliente'},
            {dataField: 'grid_servico', caption: 'Serviço'},
            {dataField: 'grid_situacao', width: 100, caption: 'Situação'},
            {dataField: 'grid_valor', width: 150, alignment: 'center', caption: 'Valor'},
            {dataField: 'grid_data', width: 150, alignment: 'center', caption: 'Data'}
        ]
    };

    $scope.habilitarBotoes = function() {
        $scope.exibeBtnExcluir = true;

        $scope.itensSelecionados.map(function (o, i, a) {
            if (o['id_situacao'] != 1) {
                $scope.exibeBtnExcluir = false;
            }
        });
    }

    $scope.limpar_filtro = function () {
        $scope.palavraChave = "";
        $scope.filtroSituacao = [];
        $scope.init();
    }

    $scope.filtrar = function () {
        TaxasService.filtrar($scope.palavraChave, $scope.filtroSituacao).then(function (ret) {
            var theGrid = jQuery('#gridTaxas').dxDataGrid('instance');
            theGrid.option('dataSource', []);

            if (ret.success) {
                theGrid.option('dataSource', ret.retorno);
                theGrid.refresh();
                theGrid.repaint();
            }
        });
    }

    $scope.darBaixa = function () {
        if ($scope.itensSelecionados.length == 1) {
            var modalInstance = $uibModal.open({
                templateUrl: 'views/intranet/mensalidades/modalDarBaixa.html',
                controller: 'ModalDarBaixa',
                resolve: {
                    registroModal: function () {
                        return $scope.itensSelecionados[0];
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                AvisoFactory.success("Registro salvo com sucesso.");
                $scope.init();
            });
        }
    }

    $scope.gerarTaxas = function () {
        TaxasService.gerar().then(function (ret) {
            if (ret.success) {
                if (ret.total == 1) {
                    AvisoFactory.success("Foi gerado " + ret.total + " registro.");
                    $scope.init();
                } else if (ret.total > 1) {
                    AvisoFactory.success("Foram gerados " + ret.total + " registros.");
                    $scope.init();
                } else {
                    AvisoFactory.warning("Nenhum registro gerado.");
                }
            } else {
                AvisoFactory.warning("Nenhum registro gerado.");
            }
        })
    }

    $scope.novo = function () {
        $state.go("intranet.detalhes_taxas", {id: null})
    }

    $scope.editar = function () {
        if ($scope.itensSelecionados.length == 1) {
            $state.go("intranet.detalhes_taxas", {id: $scope.itensSelecionados[0].id})
        }
    }

    $scope.excluir = function () {
        PromptFactory.info("Deseja realmente remover o(s) registro(s) selecionado(s)?", "").then(function (retPrompt) {
            if (retPrompt) {
                TaxasService.excluir($scope.itensSelecionados).then(function () {
                    AvisoFactory.success("Registro removido com sucesso.");

                    $rootScope.$broadcast('atualizar_intranet_total_taxas', true);
                    $scope.init();
                });
            }
        });
    }

    $scope.init();
});