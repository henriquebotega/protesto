'use strict';

app.controller('PrincipalController', function ($scope, $state, StorageFactory, FaturasService, AvisosService, PrincipalService) {

    $scope.extranet_total_faturas_abertas = 0;
    $scope.extranet_total_faturas_atraso = 0;
    $scope.listagem_avisos = [];

    AvisosService.getAllEmAberto().then(function (ret) {
        if (ret.success) {
            $scope.listagem_avisos = ret.retorno;
        }
    });

    $scope.filtragem = function (colecao, tipo) {
        return colecao.filter(function(obj) {
            return obj.tipo == tipo;
        });
    }

    FaturasService.getAllEmAberto().then(function (ret) {
        if (ret.success) {
            $scope.extranet_total_faturas_abertas = ret.total;
        } else {
            $scope.extranet_total_faturas_abertas = 0;
        }
    });

    FaturasService.getAllEmAtraso().then(function (ret) {
        if (ret.success) {
            $scope.extranet_total_faturas_atraso = ret.total;
        } else {
            $scope.extranet_total_faturas_atraso = 0;
        }
    });

    PrincipalService.getConsumoMensalWS().then(function (ret) {
        var chartConsumoMensalWS = jQuery('#chartConsumoMensalWS').dxChart('instance');

        if (chartConsumoMensalWS) {
            chartConsumoMensalWS.showLoadingIndicator();
            chartConsumoMensalWS.option('dataSource', []);

            if (ret.success) {
                chartConsumoMensalWS.option('dataSource', ret.retorno);
            }
        }
    });

    PrincipalService.getTotalDividas().then(function (ret) {
        var chartTotalDividas = jQuery('#chartTotalDividas').dxPieChart('instance');

        if (chartTotalDividas) {
            chartTotalDividas.showLoadingIndicator();
            chartTotalDividas.option('dataSource', []);

            if (ret.success) {
                chartTotalDividas.option('dataSource', ret.retorno);
            }
        }
    });

    PrincipalService.getTotalRemessasCartorio().then(function (ret) {
        var chartTotalRemessasCartorio = jQuery('#chartTotalRemessasCartorio').dxPieChart('instance');

        if (chartTotalRemessasCartorio) {
            chartTotalRemessasCartorio.showLoadingIndicator();
            chartTotalRemessasCartorio.option('dataSource', []);

            if (ret.success) {
                chartTotalRemessasCartorio.option('dataSource', ret.retorno);
            }
        }
    });

    $scope.chart1Options = {
        dataSource: [],
        title: "Total de dívidas",
        legend: {
            orientation: "horizontal",
            itemTextPosition: "right",
            horizontalAlignment: "center",
            verticalAlignment: "bottom",
            columnCount: 4
        },
        series: [{
            type: "doughnut",
            innerRadius: 0.3,
            argumentField: "titulo",
            valueField: "total",
            label: {
                visible: true,
                font: {
                    size: 16
                },
                connector: {
                    visible: true,
                    width: 1
                },
                position: "columns",
                customizeText: function (arg) {
                    return arg.valueText + " (" + arg.percentText + ")";
                }
            }
        }]
    };

    $scope.chart2Options = {
        dataSource: [],
        title: "Dividas em Cartório",
        legend: {
            orientation: "horizontal",
            itemTextPosition: "right",
            horizontalAlignment: "center",
            verticalAlignment: "bottom",
            columnCount: 4
        },
        series: [{
            type: "doughnut",
            innerRadius: 0.3,
            argumentField: "titulo",
            valueField: "total",
            label: {
                visible: true,
                font: {
                    size: 16
                },
                connector: {
                    visible: true,
                    width: 1
                },
                position: "columns",
                customizeText: function (arg) {
                    return arg.valueText + " (" + arg.percentText + ")";
                }
            }
        }]
    };

    $scope.chartConsumoMensalWSOptions = {
        dataSource: [],
        title: "Consumo mensal de WS",
        equalBarWidth: false,
        commonSeriesSettings: {
            argumentField: "mes",
            type: "bar",
            label: {
                visible: true,
                format: {
                    type: "fixedPoint",
                    precision: 0
                }
            }
        },
        tooltip: {
            enabled: true,
            location: "edge",
            customizeTooltip: function (arg) {
                return {
                    text: '<b>WS:</b> ' + arg.seriesName + '<br />' +
                    '<b>Total:</b> ' + arg.valueText
                };
            }
        },
        onSeriesClick: function (e) {
            var series = e.target;
            series.isVisible() ? series.hide() : series.show();
        },
        series: [
            {valueField: "ws2", name: "Pessoa Cheque Plus"},
            {valueField: "ws3", name: "Pessoa Pefin Cheque"},
            {valueField: "ws4", name: "Pessoa Pefin Protesto"},
            {valueField: "ws5", name: "Pessoa Protesto Nacional"},
            {valueField: "ws6", name: "Empresa Cheque Plus"},
            {valueField: "ws7", name: "Empresa Pefin Cheque"},
            {valueField: "ws8", name: "Empresa Pefin Protesto"},
            {valueField: "ws9", name: "Empresa Concentre"},
            {valueField: "ws10", name: "Empresa Serasa"},
            {valueField: "ws11", name: "CPF CNPJ Completo"},
            {valueField: "ws12", name: "CPF CNPJ Nome"},
            {valueField: "ws13", name: "DDD Fone"},
            {valueField: "ws14", name: "Lista Telefonica"},
            {valueField: "ws15", name: "Quadro Societario"}
        ],
        legend: {
            verticalAlignment: "bottom",
            horizontalAlignment: "center"
        },
        onPointClick: function (e) {
            e.target.select();
        }
    };

});