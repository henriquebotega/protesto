'use strict';

app.controller('ModalEnviarEmail', function ($scope, $rootScope, $state, StorageFactory, AvisoFactory, DividasService, ClientesService, $uibModalInstance, $uibModal, registroModal) {

    // Busca o e-mail do cliente
    $scope.emailAtual = "";

    ClientesService.getByID(registroModal[0]['id_cliente']).then(function (ret) {

        if (isNotBlank(ret.retorno[0]['email1'])) {
            $scope.emailAtual += (isBlank($scope.emailAtual) ? "" : ";");
            $scope.emailAtual += ret.retorno[0]['email1'];
        }

        if (isNotBlank(ret.retorno[0]['email12'])) {
            $scope.emailAtual += (isBlank($scope.emailAtual) ? "" : ";");
            $scope.emailAtual += ret.retorno[0]['email2'];
        }

        if (isNotBlank(ret.retorno[0]['email3'])) {
            $scope.emailAtual += (isBlank($scope.emailAtual) ? "" : ";");
            $scope.emailAtual += ret.retorno[0]['email3'];
        }
    });

    $scope.enviar = function () {

        DividasService.boletoEnviarEmail($scope.emailAtual, registroModal).then(function (ret) {
            if (ret.success) {
                AvisoFactory.success("E-mail enviado com sucesso.");
                $uibModalInstance.close(true);
            } else {
                AvisoFactory.warning(ret.mensagem);
            }
        });

    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});