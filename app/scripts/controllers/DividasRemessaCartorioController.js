'use strict';

app.controller('DividasRemessaCartorioController', function ($scope, $rootScope, $state, DividasRemessaCartorioService, AvisoFactory, StorageFactory, $uibModal, FileSaver, DividasService) {

    $scope.loading = true;
    $scope.itensSelecionados = [];
    $scope.array_situacao = [{id: 0, titulo: 'Não'}, {id: 1, titulo: 'Sim'}];
    $scope.iframeURLCartorio = servidor + "controle/download_remessa_cartorio.php";

    $scope.init = function () {
        DividasRemessaCartorioService.getAll().then(function (ret) {
            $scope.loading = false;
            var theGrid = jQuery('#gridDividasRemessaCartorio').dxDataGrid('instance');
            theGrid.option('dataSource', []);

            if (ret.success) {
                theGrid.option('dataSource', ret.retorno);
                theGrid.refresh();
                theGrid.repaint();
            }
        });
    }

    $scope.dataGridOptions = {
        dataSource: [],
        noDataText: 'Nenhum registro encontrado',
        rowAlternationEnabled: true,
        paging: {
            pageSize: configSistema.qtd_itens_listagem
        },
        export: {
            enabled: true,
            fileName: "tbl-DividasRemessaCartorio",
            allowExportSelectedData: true
        },
        selection: {
            mode: 'multiple'
        },
        onSelectionChanged: function (data) {
            $scope.itensSelecionados = data.selectedRowsData;
        },
        columns: [
            {dataField: 'id', caption: '#', width: 60, alignment: 'center'},
            {dataField: 'grid_devedor', caption: 'Cliente'},
            {dataField: 'grid_divida', caption: 'Nº Divida', width: 100, alignment: 'center'},
            {dataField: 'grid_data', caption: 'Data de Entrada'},
            {dataField: 'grid_enviado_cartorio', caption: 'Enviado', width: 100, alignment: 'center'},
            {dataField: 'grid_cartorio_numero_sequencial', caption: 'Nº Sequencial', width: 120, alignment: 'center'},
            {dataField: 'grid_data_saida_cartorio', caption: 'Data de Saída'},
            {dataField: 'grid_data_retorno_cartorio', caption: 'Data de Retorno'},
            {dataField: 'grid_cod_remessa', caption: 'Cod. Remessa', width: 120, alignment: 'center'}
        ]
    };

    $scope.exibirDownloadRemessaCartorio = function () {
        $uibModal.open({
            size: 'lg',
            templateUrl: 'views/extranet/dividas_remessa_cartorio/modalDownloadRemessaCartorio.html',
            controller: 'ModalDownloadRemessaCartorio'
        });
    }

    $scope.exportar = function () {
        DividasRemessaCartorioService.exportar($scope.itensSelecionados).then(function (ret) {
            // var data = new Blob([ret.retorno], { type: 'text/plain;charset=utf-8' });
            // FileSaver.saveAs(data, 'remessa_cartorio.txt');

            $scope.exibirDownloadRemessaCartorio();
            $scope.init();
        });
    }

    $scope.$watch('fileCartorio', function (newFile) {
        if (newFile) {

            // if (newFile.name.reverse().toUpperCase().substr(0, 3) == "TER" && newFile.size > 0) {
            //
            //     // Envia o arquivo para o servidor
            //     DividasRemessaCartorioService.uploadCartorio(newFile).then(function (ret) {
            //         if (ret.status == 200) {
            //             AvisoFactory.success("Importação finalizada.");
            //             $scope.init();
            //         } else {
            //             AvisoFactory.warning("Este arquivo não é válido!");
            //         }
            //     });
            //
            // } else {
            //     AvisoFactory.warning("O arquivo não é válido!");
            // }

        }
    });

    $scope.limpar_filtro = function () {
        $scope.palavraChave = "";
        $scope.filtroSituacao = [];
        $scope.init();
    }

    $scope.filtrar = function () {
        DividasRemessaCartorioService.filtrar($scope.palavraChave, $scope.filtroSituacao).then(function (ret) {
            var theGrid = jQuery('#gridDividasRemessaCartorio').dxDataGrid('instance');
            theGrid.option('dataSource', []);

            if (ret.success) {
                theGrid.option('dataSource', ret.retorno);
                theGrid.refresh();
                theGrid.repaint();
            }
        });
    }

    $scope.init();
});