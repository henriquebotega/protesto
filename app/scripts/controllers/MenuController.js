'use strict';

app.controller('MenuController', function ($scope, $rootScope, $state, $stateParams, StorageFactory, MenuService, DevedoresService, AdministradoresService, AvisosService, ServicosService, ClientesService, PracasService, UsuariosService, DividasService, ConfigService, DividasRemessaCartorioService, DividasRemessaBancoService, TaxasService, ContratosItensService, MensalidadesService, ContratosService, FaturasService, ContatosService, LayoutsService, LayoutsImportadosService) {

    $scope.$state = $state;
    $scope.subMenu = true;
    $scope.subMenuDividas = false;
    $scope.subMenuLayouts = false;
    $scope.exibirTotalRegistroMenu = false;
    $scope.subMenuServicosDisponiveis = false;
    $scope.existeServicosDisponiveis = false;
    $scope.listagemServicosDisponiveis = [];

    $scope.extranet_total_usuarios = 0;
    $scope.extranet_total_layouts_importados = 0;
    $scope.extranet_total_devedores = 0;
    $scope.extranet_total_contatos = 0;
    $scope.extranet_total_layouts = 0;
    $scope.extranet_total_dividas = 0;
    $scope.extranet_total_dividas_remessa_cartorio = 0;
    $scope.extranet_total_faturas_abertas = 0;
    $scope.extranet_total_servicos_disponiveis = 0;
    $scope.extranet_total_avisos = 0;

    $scope.intranet_total_administradores = 0;
    $scope.intranet_total_layouts_importados = 0;
    $scope.intranet_total_servicos = 0;
    $scope.intranet_total_clientes = 0;
    $scope.intranet_total_contratos = 0;
    $scope.intranet_total_mensalidades = 0;
    $scope.intranet_total_taxas = 0;
    $scope.intranet_total_dividas = 0;
    $scope.intranet_total_dividas_remessa_cartorio = 0;
    $scope.intranet_total_dividas_remessa_banco = 0;

    if (StorageFactory.get("subMenu") != null) {
        $scope.subMenu = StorageFactory.get("subMenu");

        if (!$scope.subMenu) {
            jQuery("#wrapper").toggleClass("toggled");
        }
    }

    if (isNotBlank(StorageFactory.get('logado'))) {
        $rootScope.usuarioLogado = StorageFactory.get("logado").usuario;
        $rootScope.isAdmin = (StorageFactory.get("logado").usuario.id_tipo == 1);
    }

    if ($state.current.url.indexOf('dividas') > -1 || $state.current.url.indexOf('enviado_cartorio') > -1) {
        $scope.subMenuDividas = true;
    }

    if ($state.current.url.indexOf('layouts') > -1 || $state.current.url.indexOf('layouts_importados') > -1) {
        $scope.subMenuLayouts = true;
    }

    if ($state.current.url.indexOf('servicos_disponiveis') > -1) {
        $scope.subMenuServicosDisponiveis = true;
    }

    if (StorageFactory.get("configSistema").exibir_total_registro_menu) {
        $scope.exibirTotalRegistroMenu = true;
    }

    $scope.exibirServicosDisponiveis = function (servicoAtual) {
        $state.go("extranet.servicos_disponiveis-" + servicoAtual.rota, {id_servico: servicoAtual.id_servico});
    }

    $scope.toogleDividas = function () {
        $scope.subMenuDividas = !$scope.subMenuDividas;
    }

    $scope.toogleLayouts = function () {
        $scope.subMenuLayouts = !$scope.subMenuLayouts;
    }

    $scope.toogleServicosDisponiveis = function () {
        $scope.subMenuServicosDisponiveis = !$scope.subMenuServicosDisponiveis;
    }

    $scope.toogleMenu = function () {
        $scope.subMenu = !$scope.subMenu;
        StorageFactory.save("subMenu", $scope.subMenu);
    }

    $scope.sair = function () {
        StorageFactory.clearAll();
        $state.go("login");
    }

    MenuService.getEstados().then(function (ret) {
        StorageFactory.save("listagemEstados", ret);
    });

    PracasService.getAll().then(function (ret) {
        StorageFactory.save("listagemPracas", ret);
    });

    MensalidadesService.getAllSituacao().then(function (ret) {
        StorageFactory.save("listagemMensalidadesSituacao", ret);
    });

    ContratosService.getAllSituacao().then(function (ret) {
        StorageFactory.save("listagemContratoSituacao", ret);
    });

    DividasService.getAllSituacao().then(function (ret) {
        StorageFactory.save("listagemDividasSituacao", ret);
    });

    TaxasService.getAllSituacao().then(function (ret) {
        StorageFactory.save("listagemTaxasSituacao", ret);
    });

    if (!$rootScope.isAdmin) {
        ContratosItensService.getByCliente(StorageFactory.get("logado").usuario['id_cliente']).then(function (ret) {
            StorageFactory.remove("listagemServicosDisponiveis");

            if (ret.success && ret.retorno.length > 0) {
                $scope.existeServicosDisponiveis = true;

                StorageFactory.save("listagemServicosDisponiveis", ret.retorno);
                $scope.listagemServicosDisponiveis = ret.retorno;
                $scope.extranet_total_servicos_disponiveis = ret.retorno.length;
            } else {
                $scope.extranet_total_servicos_disponiveis = 0;
            }
        })
    }

    if ($rootScope.isAdmin) {

        $scope.atualizar_intranet_total_administradores = function () {
            AdministradoresService.getAll().then(function (ret) {
                if (ret.success) {
                    $scope.intranet_total_administradores = ret.total;
                } else {
                    $scope.intranet_total_administradores = 0;
                }
            });
        }

        $scope.atualizar_intranet_total_servicos = function () {
            ServicosService.getAll().then(function (ret) {
                if (ret.success) {
                    $scope.intranet_total_servicos = ret.total;
                } else {
                    $scope.intranet_total_servicos = 0;
                }
            });
        }

        $scope.atualizar_intranet_total_clientes = function () {
            ClientesService.getAll().then(function (ret) {
                if (ret.success) {
                    $scope.intranet_total_clientes = ret.total;
                } else {
                    $scope.intranet_total_clientes = 0;
                }
            });
        }

        $scope.atualizar_intranet_total_contratos = function () {
            ContratosService.getAll().then(function (ret) {
                if (ret.success) {
                    $scope.intranet_total_contratos = ret.total;
                } else {
                    $scope.intranet_total_contratos = 0;
                }
            });
        }

        $scope.atualizar_intranet_total_mensalidades = function () {
            MensalidadesService.getAll().then(function (ret) {
                if (ret.success) {
                    $scope.intranet_total_mensalidades = ret.total;
                } else {
                    $scope.intranet_total_mensalidades = 0;
                }
            });
        }

        $scope.atualizar_intranet_total_taxas = function () {
            TaxasService.getAll().then(function (ret) {
                if (ret.success) {
                    $scope.intranet_total_taxas = ret.total;
                } else {
                    $scope.intranet_total_taxas = 0;
                }
            });
        }

        $scope.atualizar_intranet_total_dividas = function () {
            DividasService.getAll().then(function (ret) {
                if (ret.success) {
                    $scope.intranet_total_dividas = ret.total;
                } else {
                    $scope.intranet_total_dividas = 0;
                }
            });

            DividasRemessaCartorioService.getAll().then(function (ret) {
                if (ret.success) {
                    $scope.intranet_total_dividas_remessa_cartorio = ret.total;
                } else {
                    $scope.intranet_total_dividas_remessa_cartorio = 0;
                }
            });

            DividasRemessaBancoService.getAll().then(function (ret) {
                if (ret.success) {
                    $scope.intranet_total_dividas_remessa_banco = ret.total;
                } else {
                    $scope.intranet_total_dividas_remessa_banco = 0;
                }
            });
        }


        $scope.atualizar_intranet_total_layouts_importados = function () {
            LayoutsImportadosService.getAll().then(function (ret) {
                if (ret.success) {
                    $scope.intranet_total_layouts_importados = ret.total;
                } else {
                    $scope.intranet_total_layouts_importados = 0;
                }
            });
        }

        $scope.$on('atualizar_intranet_total_layouts_importados', function (args) {
            if (args) {
                $scope.atualizar_intranet_total_layouts_importados();
            }
        });

        $scope.$on('atualizar_intranet_total_administradores', function (args) {
            if (args) {
                $scope.atualizar_intranet_total_administradores();
            }
        });

        $scope.$on('atualizar_intranet_total_servicos', function (args) {
            if (args) {
                $scope.atualizar_intranet_total_servicos();
            }
        });

        $scope.$on('atualizar_intranet_total_clientes', function (args) {
            if (args) {
                $scope.atualizar_intranet_total_clientes();
            }
        });

        $scope.$on('atualizar_intranet_total_contratos', function (args) {
            if (args) {
                $scope.atualizar_intranet_total_contratos();
            }
        });

        $scope.$on('atualizar_intranet_total_mensalidades', function (args) {
            if (args) {
                $scope.atualizar_intranet_total_mensalidades();
            }
        });

        $scope.$on('atualizar_intranet_total_taxas', function (args) {
            if (args) {
                $scope.atualizar_intranet_total_taxas();
            }
        });

        $scope.$on('atualizar_intranet_total_dividas', function (args) {
            if (args) {
                $scope.atualizar_intranet_total_dividas();
            }
        });

        $scope.atualizar_intranet_total_layouts_importados();
        $scope.atualizar_intranet_total_administradores();
        $scope.atualizar_intranet_total_servicos();
        $scope.atualizar_intranet_total_clientes();
        $scope.atualizar_intranet_total_contratos();
        $scope.atualizar_intranet_total_mensalidades();
        $scope.atualizar_intranet_total_taxas();
        $scope.atualizar_intranet_total_dividas();

    } else {

        $scope.atualizar_extranet_total_avisos = function () {
            AvisosService.getAllEmAberto().then(function (ret) {
                if (ret.success) {
                    $scope.extranet_total_avisos = ret.total;
                } else {
                    $scope.extranet_total_avisos = 0;
                }
            });
        }

        $scope.atualizar_extranet_total_dividas = function () {
            DividasService.getAll().then(function (ret) {
                if (ret.success) {
                    $scope.extranet_total_dividas = ret.total;
                } else {
                    $scope.extranet_total_dividas = 0;
                }
            });

            DividasRemessaCartorioService.getAll().then(function (ret) {
                if (ret.success) {
                    $scope.extranet_total_dividas_remessa_cartorio = ret.total;
                } else {
                    $scope.extranet_total_dividas_remessa_cartorio = 0;
                }
            });
        }

        FaturasService.getAllEmAberto().then(function (ret) {
            if (ret.success) {
                $scope.extranet_total_faturas_abertas = ret.total;
            } else {
                $scope.extranet_total_faturas_abertas = 0;
            }
        });

        $scope.$on('atualizar_extranet_total_avisos', function (args) {
            if (args) {
                $scope.atualizar_extranet_total_avisos();
            }
        });

        $scope.$on('atualizar_extranet_total_dividas', function (args) {
            if (args) {
                $scope.atualizar_extranet_total_dividas();
            }
        });

        $scope.atualizar_extranet_total_avisos();
        $scope.atualizar_extranet_total_dividas();
    }


    $scope.atualizar_extranet_total_contatos = function () {
        ContatosService.getAll().then(function (ret) {
            if (ret.success) {
                $scope.extranet_total_contatos = ret.total;
            } else {
                $scope.extranet_total_contatos = 0;
            }
        });
    }

    $scope.atualizar_extranet_total_devedores = function () {
        DevedoresService.getAll().then(function (ret) {
            if (ret.success) {
                $scope.extranet_total_devedores = ret.total;
            } else {
                $scope.extranet_total_devedores = 0;
            }
        });
    }

    $scope.atualizar_extranet_total_usuarios = function () {
        UsuariosService.getAll().then(function (ret) {
            if (ret.success) {
                $scope.extranet_total_usuarios = ret.total;
            } else {
                $scope.extranet_total_usuarios = 0;
            }
        });
    }

    $scope.atualizar_extranet_total_layouts_importados = function () {
        LayoutsImportadosService.getAll().then(function (ret) {
            if (ret.success) {
                $scope.extranet_total_layouts_importados = ret.total;
            } else {
                $scope.extranet_total_layouts_importados = 0;
            }
        });
    }

    $scope.atualizar_extranet_total_layouts = function () {
        LayoutsService.getAll().then(function (ret) {
            if (ret.success) {
                $scope.extranet_total_layouts = ret.total;
            } else {
                $scope.extranet_total_layouts = 0;
            }
        });
    }

    $scope.$on('atualizar_extranet_total_devedores', function (args) {
        if (args) {
            $scope.atualizar_extranet_total_devedores();
        }
    });

    $scope.$on('atualizar_extranet_total_layouts_importados', function (args) {
        if (args) {
            $scope.atualizar_extranet_total_layouts_importados();
        }
    });

    $scope.$on('atualizar_extranet_total_contatos', function (args) {
        if (args) {
            $scope.atualizar_extranet_total_contatos();
        }
    });

    $scope.$on('atualizar_extranet_total_usuarios', function (args) {
        if (args) {
            $scope.atualizar_extranet_total_usuarios();
        }
    });

    $scope.$on('atualizar_extranet_total_layouts', function (args) {
        if (args) {
            $scope.atualizar_extranet_total_layouts();
        }
    });

    $scope.atualizar_extranet_total_contatos();
    $scope.atualizar_extranet_total_devedores();
    $scope.atualizar_extranet_total_usuarios();
    $scope.atualizar_extranet_total_layouts();
    $scope.atualizar_extranet_total_layouts_importados();
});