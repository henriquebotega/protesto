'use strict';

app.controller('ContratosDetalhesController', function ($scope, $rootScope, $state, $stateParams, StorageFactory, AvisoFactory, ServicosService, ClientesService, ContratosItensService, ContratosService) {

    $scope.titulo = "Novo Contrato";
    $scope.loading = true;
    $scope.id_situacaoAtual = 1;
    $scope.array_situacao = [StorageFactory.get("listagemContratoSituacao")[0]];

    $scope.registroAtual = {
        id_situacao: "0",
        id_cliente: "0",
        dia_vcto: "0"
    }

    $scope.array_clientes = [];

    ClientesService.getAll().then(function (ret) {
        ret.retorno.map(function (o, i, a) {
            var obj = o;

            if (obj.id_tipo == 1) {
                obj.cb_nome = o.nome;
            } else {
                obj.cb_nome = o.razao_social;
            }

            $scope.array_clientes.push(obj);
        });
    });

    if (isNotBlank($stateParams.id)) {
        $scope.titulo = "Editar Contrato";
        $scope.array_situacao = StorageFactory.get("listagemContratoSituacao");

        ContratosService.getByID($stateParams.id).then(function (ret) {
            if (ret.success) {
                $scope.loading = false;
                $scope.registroAtual = ret.retorno[0];
                $scope.id_situacaoAtual = ret.retorno[0].id_situacao;
            } else {
                AvisoFactory.warning("Este registro não existe!");
                $state.go("intranet.contratos");
            }
        });
    } else {
        $scope.loading = false;
    }

    $scope.salvar = function () {

        if (!$scope.validarCamposObrigatorios()) {

            // Antes de salvar, verifica se já existe contrato finalizado para o mesmo cliente.
            ContratosService.getContratoFinalizado($scope.registroAtual).then(function (ret) {
                if (ret.success) {

                    if (ret.retorno.length > 0 && $scope.registroAtual.id_situacao == 3 && $scope.id_situacaoAtual != 3) {
                        AvisoFactory.warning("Este cliente já possui um contrato ativo!");
                    } else {
                        $scope.verificaContratoObrigatorio();
                    }

                } else {
                    $scope.verificaContratoObrigatorio();
                }

            });

        } else {
            AvisoFactory.warning("Preencha os campos corretamente!");
        }

    }

    $scope.salvarContrato = function () {
        ContratosService.salvar($scope.registroAtual).then(function (ret) {
            AvisoFactory.success("Registro salvo com sucesso!");

            $rootScope.$broadcast('atualizar_intranet_total_contratos', true);
            $state.go("intranet.contratos");
        });
    }

    $scope.verificaContratoObrigatorio = function () {

        if (isNotBlank($scope.registroAtual.id)) {

            if ($scope.registroAtual.id_situacao == 3) {

                ContratosItensService.getAll($scope.registroAtual.id).then(function (retItens) {

                    if (!retItens.success) {
                        AvisoFactory.warning("É necessário incluir os serviços obrigatórios: \n - Mensalidade \n - Emissão de boleto \n - Cancelamento de Boleto \n - Envio de divida para protesto");
                    } else {
                        ServicosService.getAllObrigatorios().then(function (retServicos) {

                            var obrigatorios = [];
                            var itemObrigatorio = [];
                            var total = retServicos.total;

                            retItens.retorno.map(function (oItem, iItem, aItem) {
                                itemObrigatorio = retServicos.retorno.filter(function (oServico) {
                                    return oItem['id_servico'] == oServico['id'];
                                });

                                if (itemObrigatorio.length > 0) {
                                    obrigatorios.push(itemObrigatorio);
                                }
                            });

                            if (obrigatorios.length < total) {
                                AvisoFactory.warning("É necessário incluir os serviços obrigatórios: \n - Mensalidade \n - Emissão de boleto \n - Cancelamento de Boleto \n - Envio de divida para protesto");
                            } else {
                                $scope.salvarContrato();
                            }
                        });

                    }

                });

            } else {
                $scope.salvarContrato();
            }


        } else {
            $scope.salvarContrato();
        }
    }

    $scope.voltar = function () {
        $state.go("intranet.contratos")
    }

    $scope.validarCamposObrigatorios = function () {
        var erro = false;

        if (isBlank($scope.registroAtual.id_situacao) || isBlank($scope.registroAtual.id_cliente) || isBlank($scope.registroAtual.dia_vcto)) {
            erro = true;
        }

        return erro;
    }

});