'use strict';

app.controller('AvisosController', function ($scope, $rootScope, $state, AvisoFactory, StorageFactory, AvisosService) {

    $scope.itensSelecionados = [];
    $scope.loading = true;
    $scope.array_tipo = [
        {"id": 0, "titulo": "Informação"},
        {"id": 1, "titulo": "Aviso"},
        {"id": 2, "titulo": "Atenção"}
    ];

    $scope.init = function () {
        AvisosService.getAll().then(function (ret) {
            $scope.loading = false;

            var theGrid = jQuery('#gridAvisos').dxDataGrid('instance');
            theGrid.option('dataSource', []);

            if (ret.success) {
                theGrid.option('dataSource', ret.retorno);
                theGrid.refresh();
                theGrid.repaint();
            }
        });
    }

    $scope.dataGridOptions = {
        dataSource: [],
        noDataText: 'Nenhum registro encontrado',
        rowAlternationEnabled: true,
        paging: {
            pageSize: configSistema.qtd_itens_listagem
        },
        export: {
            enabled: true,
            fileName: "tbl-Avisos",
            allowExportSelectedData: true
        },
        selection: {
            mode: 'multiple'
        },
        onSelectionChanged: function (data) {
            $scope.itensSelecionados = data.selectedRowsData;
        },
        columns: [
            {dataField: 'id', caption: '#', width: 60, alignment: 'center'},
            {dataField: 'grid_lido', width: 100, caption: 'Lido'},
            {dataField: 'grid_tipo', width: 100, caption: 'Tipo'},
            {dataField: 'grid_titulo', caption: 'Titulo'},
            {dataField: 'grid_conteudo', caption: 'Conteudo'}
        ]
    };

    $scope.novo = function () {
        $state.go("extranet.detalhes_avisos", {id: null})
    }

    $scope.limpar_filtro = function () {
        $scope.palavraChave = "";
        $scope.filtroTipo = [];
        $scope.init();
    }

    $scope.filtrar = function () {
        AvisosService.filtrar($scope.palavraChave, $scope.filtroTipo).then(function (ret) {
            var theGrid = jQuery('#gridAvisos').dxDataGrid('instance');
            theGrid.option('dataSource', []);

            if (ret.success) {
                theGrid.option('dataSource', ret.retorno);
                theGrid.refresh();
                theGrid.repaint();
            }
        });
    }

    $scope.servicosContratados = function () {
        if ($scope.itensSelecionados.length == 1) {
            $state.go("extranet.itens_avisos", {id_contrato: $scope.itensSelecionados[0].id})
        }
    }

    $scope.editar = function () {
        if ($scope.itensSelecionados.length == 1) {
            $state.go("extranet.detalhes_avisos", {id: $scope.itensSelecionados[0].id})
        }
    }

    $scope.excluir = function () {
        if ($scope.itensSelecionados.length == 1) {

            AvisosService.permiteRemover($scope.itensSelecionados[0]).then(function (ret) {
                if (!ret.success) {
                    AvisoFactory.warning(ret.mensagem);
                } else {
                    PromptFactory.info("Deseja realmente remover o registro selecionado?", "").then(function (retPrompt) {
                        if (retPrompt) {
                            AvisosService.excluir($scope.itensSelecionados[0]).then(function () {
                                AvisoFactory.success("Registro removido com sucesso.");

                                $rootScope.$broadcast('atualizar_extranet_total_avisos', true);
                                $scope.init();
                            });
                        }
                    });
                }
            });

        }
    }
    $scope.init();
});