'use strict';

app.controller('ServicosDetalhesController', function ($scope, $rootScope, $stateParams, $state, ServicosService, AvisoFactory) {

    $scope.titulo = "Novo Serviço";
    $scope.loading = true;

    $scope.registroAtual = {
        titulo: "",
        descricao: "",
        valor: "0"
    }

    if (isNotBlank($stateParams.id)) {
        $scope.titulo = "Editar Serviço";

        ServicosService.getByID($stateParams.id).then(function (ret) {
            if (ret.success) {
                $scope.loading = false;
                $scope.registroAtual = ret.retorno[0];
            } else {
                AvisoFactory.warning("Este registro não existe!");
                $state.go("intranet.servicos");
            }
        });
    } else {
        $scope.loading = false;
    }

    $scope.salvar = function () {
        ServicosService.salvar($scope.registroAtual).then(function (ret) {
            AvisoFactory.success("Registro salvo com sucesso!");

            $rootScope.$broadcast('atualizar_intranet_total_servicos', true);
            $state.go("intranet.servicos");
        });
    }

    $scope.voltar = function () {
        $state.go("intranet.servicos")
    }
});