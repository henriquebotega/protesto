'use strict';

app.controller('UsuariosController', function ($scope, $rootScope, $state, $timeout, StorageFactory, UsuariosService, AvisoFactory, PromptFactory, $uibModal) {

    $scope.itensSelecionados = [];
    $scope.loading = true;

    $scope.array_situacao = [
        {id: "0", titulo: "Desativado"},
        {id: "1", titulo: "Ativado"}
    ]

    $scope.init = function () {
        UsuariosService.getAll().then(function (ret) {
            $scope.loading = false;
            var theGrid = jQuery('#gridUsuarios').dxDataGrid('instance');
            theGrid.option('dataSource', []);

            if (ret.success) {
                theGrid.option('dataSource', ret.retorno);
                theGrid.refresh();
                theGrid.repaint();
            }
        });
    }

    $scope.dataGridOptions = {
        dataSource: [],
        noDataText: 'Nenhum registro encontrado',
        rowAlternationEnabled: true,
        paging: {
            pageSize: configSistema.qtd_itens_listagem
        },
        export: {
            enabled: true,
            fileName: "tbl-Usuarios",
            allowExportSelectedData: true
        },
        selection: {
            mode: 'multiple'
        },
        onSelectionChanged: function (data) {
            $scope.itensSelecionados = data.selectedRowsData;
        },
        columns: [
            {dataField: 'grid_id', caption: '#', width: 60, alignment: 'center'},
            {dataField: 'grid_cliente', caption: 'Cliente', visible: ($rootScope.isAdmin)},
            {dataField: 'grid_email', caption: 'E-mail'},
            {dataField: 'grid_situacao', width: 100, caption: 'Situação'},
            {dataField: 'grid_ultimo_acesso', width: 150, caption: 'Último Acesso'}
        ]
    };

    $scope.novo = function () {
        $state.go("extranet.detalhes_usuarios", {id: null})
    }

    $scope.limpar_filtro = function () {
        $scope.palavraChave = "";
        $scope.filtroSituacao = [];
        $scope.init();
    }

    $scope.filtrar = function () {
        UsuariosService.filtrar($scope.palavraChave, $scope.filtroSituacao).then(function (ret) {
            var theGrid = jQuery('#gridUsuarios').dxDataGrid('instance');
            theGrid.option('dataSource', []);

            if (ret.success) {
                theGrid.option('dataSource', ret.retorno);
                theGrid.refresh();
                theGrid.repaint();
            }
        });
    }

    $scope.editar = function () {
        if ($scope.itensSelecionados.length == 1) {
            $state.go("extranet.detalhes_usuarios", {id: $scope.itensSelecionados[0].grid_id})
        }
    }

    $scope.excluir = function () {
        PromptFactory.info("Deseja realmente remover o(s) registro(s) selecionado(s)?", "").then(function (retPrompt) {
            if (retPrompt) {

                $timeout(function () {
                    UsuariosService.excluir($scope.itensSelecionados).then(function () {
                        AvisoFactory.success("Registro removido com sucesso.");

                        $rootScope.$broadcast('atualizar_intranet_total_usuarios', true);
                        $rootScope.$broadcast('atualizar_extranet_total_usuarios', true);
                        $scope.init();
                    });
                }, 500);

            }
        });
    }

    $scope.modalAlterarSenha = function () {

        var modalInstance = $uibModal.open({
            templateUrl: 'views/extranet/usuarios/modalAlterarSenha.html',
            controller: 'ModalAlterarSenhaUsuarios',
            resolve: {
                registroModal: function () {
                    return $scope.itensSelecionados[0];
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            AvisoFactory.success("Registro salvo com sucesso.");
        });
    }

    $scope.init();
});