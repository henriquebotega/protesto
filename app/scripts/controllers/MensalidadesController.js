'use strict';

app.controller('MensalidadesController', function ($scope, $rootScope, $state, MensalidadesService, AvisoFactory, StorageFactory, $uibModal) {

    $scope.itensSelecionados = [];
    $scope.loading = true;
    $scope.array_situacao = StorageFactory.get("listagemMensalidadesSituacao");

    $scope.init = function () {
        MensalidadesService.getAll().then(function (ret) {
            $scope.loading = false;
            var theGrid = jQuery('#gridMensalidades').dxDataGrid('instance');
            theGrid.option('dataSource', []);

            if (ret.success) {
                theGrid.option('dataSource', ret.retorno);
                theGrid.refresh();
                theGrid.repaint();
            }
        });
    }

    $scope.dataGridOptions = {
        dataSource: [],
        noDataText: 'Nenhum registro encontrado',
        rowAlternationEnabled: true,
        paging: {
            pageSize: configSistema.qtd_itens_listagem
        },
        export: {
            enabled: true,
            fileName: "tbl-Mensalidades",
            allowExportSelectedData: true
        },
        selection: {
            mode: 'multiple'
        },
        onSelectionChanged: function (data) {
            $scope.itensSelecionados = data.selectedRowsData;
        },
        columns: [
            {dataField: 'id', caption: '#', width: 60, alignment: 'center'},
            {dataField: 'grid_id_contrato', width: 80, alignment: 'center', caption: 'Contrato'},
            {dataField: 'grid_cliente', caption: 'Cliente'},
            {dataField: 'grid_situacao', width: 100, caption: 'Situação'},
            {dataField: 'grid_valor_vcto', width: 150, alignment: 'center', caption: 'Valor de Vencimento'},
            {dataField: 'grid_data_vcto', width: 150, alignment: 'center', caption: 'Data de Vencimento'},
            {dataField: 'grid_valor_pgto', width: 150, alignment: 'center', caption: 'Valor de Pagamento'},
            {dataField: 'grid_data_pgto', width: 150, alignment: 'center', caption: 'Data de Pagamento'}
        ]
    };

    $scope.limpar_filtro = function () {
        $scope.palavraChave = "";
        $scope.filtroSituacao = [];
        $scope.init();
    }

    $scope.filtrar = function () {
        MensalidadesService.filtrar($scope.palavraChave, $scope.filtroSituacao).then(function (ret) {
            var theGrid = jQuery('#gridMensalidades').dxDataGrid('instance');
            theGrid.option('dataSource', []);

            if (ret.success) {
                theGrid.option('dataSource', ret.retorno);
                theGrid.refresh();
                theGrid.repaint();
            }
        });
    }

    $scope.darBaixa = function () {
        if ($scope.itensSelecionados.length == 1) {
            var modalInstance = $uibModal.open({
                templateUrl: 'views/intranet/mensalidades/modalDarBaixa.html',
                controller: 'ModalDarBaixa',
                resolve: {
                    registroModal: function () {
                        return $scope.itensSelecionados[0];
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                AvisoFactory.success("Registro salvo com sucesso.");
                $scope.init();
            });
        }
    }

    $scope.gerarMensalidades = function () {
        MensalidadesService.gerar().then(function (ret) {
            if (ret.success) {
                if (ret.total == 1) {
                    AvisoFactory.success("Foi gerado " + ret.total + " registro.");
                    $scope.init();
                } else if (ret.total > 1) {
                    AvisoFactory.success("Foram gerados " + ret.total + " registros.");
                    $scope.init();
                } else {
                    AvisoFactory.warning("Nenhum registro gerado.");
                }

                $rootScope.$broadcast('atualizar_intranet_total_mensalidades', true);
            } else {
                AvisoFactory.warning("Nenhum registro gerado.");
            }
        })
    }

    $scope.taxas = function () {
        if ($scope.itensSelecionados.length == 1) {
            $state.go("intranet.taxas_mensalidades", {id_mensalidade: $scope.itensSelecionados[0].id})
        }
    }

    $scope.init();
});