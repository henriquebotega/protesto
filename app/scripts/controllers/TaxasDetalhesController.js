'use strict';

app.controller('TaxasDetalhesController', function ($scope, $rootScope, $stateParams, $state, MenuService, ServicosService, ClientesService, MensalidadesService, StorageFactory, TaxasService, AvisoFactory) {

    $scope.titulo = "Nova Taxa";
    $scope.loading = true;

    $scope.registroAtual = {
        data: "",
        historico: "",
        id_cliente: "0",
        id_servico: "0",
        valor: "0"
    }

    $scope.array_situacao = StorageFactory.get("listagemTaxasSituacao");
    $scope.array_servicos = [];
    $scope.array_clientes = [];

    ClientesService.getAll().then(function (ret) {
        ret.retorno.map(function (o, i, a) {

            var obj = o;

            if (obj.id_tipo == 1) {
                obj.cb_nome = o.nome;
            } else {
                obj.cb_nome = o.razao_social;
            }

            $scope.array_clientes.push(obj);
        });
    });

    ServicosService.getAll().then(function (ret) {
        $scope.array_servicos = ret.retorno;
    });

    if (isNotBlank($stateParams.id)) {
        $scope.titulo = "Editar Taxa";

        TaxasService.getByID($stateParams.id).then(function (ret) {
            if (ret.success) {
                
                if (ret.retorno[0].id_situacao == 1) {
                    $scope.loading = false;
                    $scope.registroAtual = ret.retorno[0];
                } else {
                    AvisoFactory.warning("Este registro não pode ser alterado!");
                    $state.go("intranet.taxas");
                }

            } else {
                AvisoFactory.warning("Este registro não existe!");
                $state.go("intranet.taxas")
            }
        });
    } else {
        $scope.loading = false;
    }

    $scope.inArray = function (item, arrayServicos) {

        for (var i = 0; i < arrayServicos.length; i++) {
            if (arrayServicos[i]['id'] == item) {
                return arrayServicos[i];
            }
        }

        return false;
    }

    $scope.definirServico = function () {
        var item = $scope.inArray($scope.registroAtual.id_servico, $scope.array_servicos);
        $scope.registroAtual.valor = item.valor;
    }

    $scope.salvar = function () {
        TaxasService.salvar($scope.registroAtual, $stateParams.id_mensalidade).then(function (ret) {
            AvisoFactory.success("Registro salvo com sucesso!");

            $rootScope.$broadcast('atualizar_intranet_total_taxas', true);
            $state.go("intranet.taxas");
        });
    }

    $scope.voltar = function () {
        $state.go("intranet.taxas");
    }
});