'use strict';

app.controller('InicioController', function ($scope, $state, StorageFactory, InicioService) {

    $scope.loading = true;

    $scope.redirecionarConfig = function () {
        $state.go("intranet.config");
    }

    InicioService.getTotalRegistros().then(function (ret) {
        var graficoTotalRegistros = jQuery('#graficoTotalRegistros').dxPieChart('instance');

        if (graficoTotalRegistros) {
            graficoTotalRegistros.showLoadingIndicator();
            graficoTotalRegistros.option('dataSource', []);

            if (ret.success) {
                graficoTotalRegistros.option('dataSource', ret.retorno);
            }
        }
    });

    InicioService.getTotalRemessasCartorio().then(function (ret) {
        var graficoTotalRemessasCartorio = jQuery('#graficoTotalRemessasCartorio').dxPieChart('instance');

        if (graficoTotalRemessasCartorio) {
            graficoTotalRemessasCartorio.showLoadingIndicator();
            graficoTotalRemessasCartorio.option('dataSource', []);

            if (ret.success) {
                graficoTotalRemessasCartorio.option('dataSource', ret.retorno);
            }
        }
    });

    InicioService.getTotalUsuariosPorCliente().then(function (ret) {
        var graficoTotalUsuariosPorCliente = jQuery('#graficoTotalUsuariosPorCliente').dxPieChart('instance');

        if (graficoTotalUsuariosPorCliente) {
            graficoTotalUsuariosPorCliente.showLoadingIndicator();
            graficoTotalUsuariosPorCliente.option('dataSource', []);

            if (ret.success) {
                graficoTotalUsuariosPorCliente.option('dataSource', ret.retorno);
            }
        }
    });

    InicioService.getTotalDevedoresPorCliente().then(function (ret) {
        var graficoTotalDevedoresPorCliente = jQuery('#graficoTotalDevedoresPorCliente').dxPieChart('instance');

        if (graficoTotalDevedoresPorCliente) {
            graficoTotalDevedoresPorCliente.showLoadingIndicator();
            graficoTotalDevedoresPorCliente.option('dataSource', []);

            if (ret.success) {
                graficoTotalDevedoresPorCliente.option('dataSource', ret.retorno);
            }
        }
    });

    InicioService.getConsumoMensalPorCliente().then(function (ret) {
        var graficoConsumoMensalPorCliente = jQuery('#graficoConsumoMensalPorCliente').dxChart('instance');

        if (graficoConsumoMensalPorCliente) {
            graficoConsumoMensalPorCliente.showLoadingIndicator();
            graficoConsumoMensalPorCliente.option('dataSource', []);

            if (ret.success) {
                graficoConsumoMensalPorCliente.option('dataSource', ret.retorno);
            }
        }
    });

    InicioService.getConsumoMensalPorWS().then(function (ret) {
        var graficoConsumoMensalPorWS = jQuery('#graficoConsumoMensalPorWS').dxChart('instance');

        if (graficoConsumoMensalPorWS) {
            graficoConsumoMensalPorWS.showLoadingIndicator();
            graficoConsumoMensalPorWS.option('dataSource', []);

            if (ret.success) {
                graficoConsumoMensalPorWS.option('dataSource', ret.retorno);
            }
        }
    });

    $scope.chart1Options = {
        dataSource: [],
        title: "Total de cadastros",
        legend: {
            orientation: "horizontal",
            itemTextPosition: "right",
            horizontalAlignment: "center",
            verticalAlignment: "bottom",
            columnCount: 4
        },
        series: [{
            type: "doughnut",
            innerRadius: 0.3,
            argumentField: "titulo",
            valueField: "valor",
            label: {
                visible: true,
                font: {
                    size: 16
                },
                connector: {
                    visible: true,
                    width: 1
                },
                position: "columns",
                customizeText: function (arg) {
                    return arg.valueText + " (" + arg.percentText + ")";
                }
            }
        }]
    };

    $scope.chart2Options = {
        dataSource: [],
        title: "Dividas em Cartório",
        legend: {
            orientation: "horizontal",
            itemTextPosition: "right",
            horizontalAlignment: "center",
            verticalAlignment: "bottom",
            columnCount: 4
        },
        series: [{
            type: "doughnut",
            innerRadius: 0.3,
            argumentField: "titulo",
            valueField: "total",
            label: {
                visible: true,
                font: {
                    size: 16
                },
                connector: {
                    visible: true,
                    width: 1
                },
                position: "columns",
                customizeText: function (arg) {
                    return arg.valueText + " (" + arg.percentText + ")";
                }
            }
        }]
    };

    $scope.chart3Options = {
        dataSource: [],
        title: 'Consumo mensal do WS por Cliente',
        commonSeriesSettings: {
            argumentField: "mes",
            valueField: "total",
            type: "spline"
        },
        seriesTemplate: {
            nameField: "cliente"
        },
        commonAxisSettings: {
            grid: {
                visible: true
            }
        },
        tooltip: {
            enabled: true,
            location: "edge",
            customizeTooltip: function (arg) {
                return {
                    text: '<b>Cliente:</b> ' + arg.seriesName + '<br />' +
                    '<b>Total:</b> ' + arg.valueText
                };
            }
        },
        legend: {
            verticalAlignment: "top",
            horizontalAlignment: "right"
        }
    };

    $scope.chart4Options = {
        dataSource: [],
        title: "Consumo mensal por tipo de serviço",
        equalBarWidth: false,
        commonSeriesSettings: {
            argumentField: "mes",
            type: "bar",
            label: {
                visible: true,
                format: {
                    type: "fixedPoint",
                    precision: 0
                }
            }
        },
        tooltip: {
            enabled: true,
            location: "edge",
            customizeTooltip: function (arg) {
                return {
                    text: '<b>WS:</b> ' + arg.seriesName + '<br />' +
                    '<b>Total:</b> ' + arg.valueText
                };
            }
        },
        series: [
            {valueField: "ws2", name: "Pessoa Cheque Plus"},
            {valueField: "ws3", name: "Pessoa Pefin Cheque"},
            {valueField: "ws4", name: "Pessoa Pefin Protesto"},
            {valueField: "ws5", name: "Pessoa Protesto Nacional"},
            {valueField: "ws6", name: "Empresa Cheque Plus"},
            {valueField: "ws7", name: "Empresa Pefin Cheque"},
            {valueField: "ws8", name: "Empresa Pefin Protesto"},
            {valueField: "ws9", name: "Empresa Concentre"},
            {valueField: "ws10", name: "Empresa Serasa"},
            {valueField: "ws11", name: "CPF CNPJ Completo"},
            {valueField: "ws12", name: "CPF CNPJ Nome"},
            {valueField: "ws13", name: "DDD Fone"},
            {valueField: "ws14", name: "Lista Telefonica"},
            {valueField: "ws15", name: "Quadro Societario"}
        ],
        legend: {
            verticalAlignment: "bottom",
            horizontalAlignment: "center"
        }
    };

    $scope.chart5Options = {
        dataSource: [],
        title: "Total de usuários por cliente",
        legend: {
            orientation: "horizontal",
            itemTextPosition: "right",
            horizontalAlignment: "center",
            verticalAlignment: "bottom",
            columnCount: 4
        },
        series: [{
            type: "doughnut",
            innerRadius: 0.1,
            argumentField: "cliente",
            valueField: "total",
            label: {
                visible: true,
                font: {
                    size: 16
                },
                connector: {
                    visible: true,
                    width: 1
                },
                position: "columns",
                customizeText: function (arg) {
                    return arg.valueText + " (" + arg.percentText + ")";
                }
            }
        }]
    };

    $scope.chart6Options = {
        dataSource: [],
        title: "Total de devedores por cliente",
        legend: {
            orientation: "horizontal",
            itemTextPosition: "right",
            horizontalAlignment: "center",
            verticalAlignment: "bottom",
            columnCount: 4
        },
        series: [{
            type: "doughnut",
            innerRadius: 0.1,
            argumentField: "cliente",
            valueField: "total",
            label: {
                visible: true,
                font: {
                    size: 16
                },
                connector: {
                    visible: true,
                    width: 1
                },
                position: "columns",
                customizeText: function (arg) {
                    return arg.valueText + " (" + arg.percentText + ")";
                }
            }
        }]
    };
});