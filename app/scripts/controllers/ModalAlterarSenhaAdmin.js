'use strict';

app.controller('ModalAlterarSenhaAdmin', function ($scope, $rootScope, $state, StorageFactory, UsuariosService, AdministradoresService, AvisoFactory, $uibModalInstance, registroModal) {

    $scope.registroAtual = {
        id: registroModal.id,
        id_tipo: '1',
        senha_atual: '',
        nova_senha: ''

    }
    $scope.ok = function () {
        AdministradoresService.salvarNovaSenha($scope.registroAtual).then(function (ret) {
            if (ret.success) {
                $uibModalInstance.close(true);
            } else {
                AvisoFactory.warning(ret.mensagem);
            }
        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});