'use strict';

app.controller('ModalBoleto', function ($scope, $rootScope, $state, $sce, StorageFactory, $uibModalInstance, $uibModal, registroModal) {

    $scope.urlBoleto = servidor + "boleto/gerar.php?id_divida=";

    registroModal.map(function (o, i, a) {
        $scope.urlBoleto += o.id + "-";
    });

    $scope.urlBoleto = $scope.urlBoleto.substr(0, ($scope.urlBoleto.length - 1));

    $scope.trustSrc = function(src) {
        return $sce.trustAsResourceUrl(src);
    }

    $scope.enviar = function () {
        $uibModal.open({
            size: 'lg',
            templateUrl: 'views/extranet/dividas/modalEnviarEmail.html',
            controller: 'ModalEnviarEmail',
            resolve: {
                registroModal: function () {
                    return registroModal;
                }
            }
        });
    }
    
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});