'use strict';

app.controller('ContatosController', function ($scope, $rootScope, $state, AvisoFactory, StorageFactory, ContatosService) {

    $scope.itensSelecionados = [];
    $scope.loading = true;

    $scope.init = function () {
        ContatosService.getAll().then(function (ret) {
            $scope.loading = false;

            var theGrid = jQuery('#gridContatos').dxDataGrid('instance');
            theGrid.option('dataSource', []);

            if (ret.success) {
                theGrid.option('dataSource', ret.retorno);
                theGrid.refresh();
                theGrid.repaint();
            }
        });
    }

    $scope.dataGridOptions = {
        dataSource: [],
        noDataText: 'Nenhum registro encontrado',
        rowAlternationEnabled: true,
        paging: {
            pageSize: configSistema.qtd_itens_listagem
        },
        export: {
            enabled: true,
            fileName: "tbl-Contatos",
            allowExportSelectedData: true
        },
        selection: {
            mode: 'multiple'
        },
        onSelectionChanged: function (data) {
            $scope.itensSelecionados = data.selectedRowsData;
        },
        columns: [
            {dataField: 'id', caption: '#', width: 60, alignment: 'center'},
            {dataField: 'grid_cliente', caption: 'Cliente', visible: ($rootScope.isAdmin)},
            {dataField: 'grid_titulo', caption: 'Titulo'},
            {dataField: 'grid_mensagem', caption: 'Mensagem'},
            {dataField: 'grid_data', width: 150, caption: 'Data'}
        ]
    };

    $scope.novo = function () {
        $state.go("extranet.detalhes_contatos", {id: null})
    }

    $scope.limpar_filtro = function () {
        $scope.palavraChave = "";
        $scope.init();
    }

    $scope.exibirInteracoes = function () {
        if ($scope.itensSelecionados.length == 1) {
            $state.go("extranet.interacoes_contatos", {id_contato: $scope.itensSelecionados[0].id})
        }
    }

    $scope.filtrar = function () {
        ContatosService.filtrar($scope.palavraChave).then(function (ret) {
            var theGrid = jQuery('#gridContatos').dxDataGrid('instance');
            theGrid.option('dataSource', []);

            if (ret.success) {
                theGrid.option('dataSource', ret.retorno);
                theGrid.refresh();
                theGrid.repaint();
            }
        });
    }

    $scope.editar = function () {
        if ($scope.itensSelecionados.length == 1) {
            $state.go("extranet.detalhes_contatos", {id: $scope.itensSelecionados[0].id})
        }
    }

    $scope.excluir = function () {
        if ($scope.itensSelecionados.length == 1) {

            ContatosService.permiteRemover($scope.itensSelecionados[0]).then(function (ret) {
                if (!ret.success) {
                    AvisoFactory.warning(ret.mensagem);
                } else {
                    PromptFactory.info("Deseja realmente remover o registro selecionado?", "").then(function (retPrompt) {
                        if (retPrompt) {
                            ContatosService.excluir($scope.itensSelecionados[0]).then(function () {
                                AvisoFactory.success("Registro removido com sucesso.");

                                $rootScope.$broadcast('atualizar_extranet_total_contatos', true);
                                $rootScope.$broadcast('atualizar_intranet_total_contatos', true);
                                $scope.init();
                            });
                        }
                    });
                }
            });

        }
    }
    $scope.init();
});