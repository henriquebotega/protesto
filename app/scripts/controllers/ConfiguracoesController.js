'use strict';

app.controller('ConfiguracoesController', function ($scope, StorageFactory, AvisoFactory, ConfiguracoesService) {

    $scope.registroAtual = {
        boleto_mensagem1: '',
        boleto_mensagem2: '',
        boleto_mensagem3: '',
        boleto_desconto: 0,
        boleto_abatimento: 0,
        boleto_mora: 0,
        boleto_multa: 0
    };

    ConfiguracoesService.getAll().then(function (ret) {
        $scope.registroAtual = ret.retorno[0];
    });

    $scope.salvar = function () {
        ConfiguracoesService.salvar($scope.registroAtual).then(function (ret) {
            AvisoFactory.success("Registro salvo com sucesso!");
        });
    }

});