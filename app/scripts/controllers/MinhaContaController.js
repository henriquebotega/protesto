'use strict';

app.controller('MinhaContaController', function ($scope, $rootScope, $stateParams, $state, MenuService, StorageFactory, ClientesService, AvisoFactory) {

    $scope.titulo = "Minha conta";
    $scope.loading = true;

    $scope.array_situacao = [
        {id: "0", titulo: "Desativado"},
        {id: "1", titulo: "Ativado"}
    ]

    $scope.cbTipoCadastroFisica = false;

    $scope.alterarTipoCadastro = function () {
        $scope.cbTipoCadastroFisica = !$scope.cbTipoCadastroFisica;
    }

    $scope.array_estados = [];
    $scope.array_cidades = [];

    $scope.registroAtual = {
        id_situacao: "0",
        razao_social: "",
        nome_fantasia: "",
        cnpj: "",
        ie: "",
        nome: "",
        data_nascimento: "",
        cpf: "",
        rg: "",
        cep: "",
        logradouro: "",
        numero: "",
        complemento: "",
        bairro: "",
        id_estado: "0",
        id_cidade: "0",
        tel1: "",
        contato1: "",
        email1: "",
        tel2: "",
        contato2: "",
        email2: "",
        tel3: "",
        contato3: "",
        email3: ""
    }

    $scope.array_estados = StorageFactory.get("listagemEstados");

    $scope.filtrarUF = function () {
        MenuService.getCidadesPorEstados($scope.registroAtual.id_estado).then(function (ret) {
            $scope.array_cidades = ret.retorno;
        });
    }

    ClientesService.getByID(StorageFactory.get("logado").usuario.id_cliente).then(function (ret) {
        if (ret.success) {
            $scope.loading = false;
            $scope.registroAtual = ret.retorno[0];
            $scope.cbTipoCadastroFisica = (ret.retorno[0].id_tipo == 1);

            $scope.filtrarUF();
        }
    });

    $scope.salvar = function () {

        if ($scope.cbTipoCadastroFisica) {
            $scope.registroAtual.id_tipo = "1";
            $scope.registroAtual.razao_social = "";
            $scope.registroAtual.nome_fantasia = "";
            $scope.registroAtual.cnpj = "";
            $scope.registroAtual.ie = "";
        } else {
            $scope.registroAtual.id_tipo = "0";
            $scope.registroAtual.nome = "";
            $scope.registroAtual.data_nascimento = "";
            $scope.registroAtual.cpf = "";
            $scope.registroAtual.rg = "";
        }

        ClientesService.salvar($scope.registroAtual).then(function (ret) {
            AvisoFactory.success("Registro salvo com sucesso!");
        });
    }

    $scope.localizar = function () {

        var cep = $scope.registroAtual.cep.replace(/\D/g, '');

        jQuery.getJSON("https://viacep.com.br/ws/" + cep + "/json", function (ret) {

            // Busca o municipio do CEP informado
            MenuService.getCidadePorCEP(ret.ibge).then(function (retCEP) {

                if (retCEP.retorno) {
                    // Define o UF
                    $scope.registroAtual.id_estado = retCEP.retorno[0].id_estado;

                    // Busca as cidades do UF definido
                    MenuService.getCidadesPorEstados($scope.registroAtual.id_estado).then(function (ret) {
                        $scope.array_cidades = ret.retorno;

                        // Seleciona a cidade
                        $scope.selectCidade(retCEP.retorno[0].titulo);
                    });
                }
            });

            // Atualiza os dados na tela
            $scope.$apply(function () {
                $scope.registroAtual.logradouro = ret.logradouro;
                $scope.registroAtual.bairro = ret.bairro;
            });

        });

    }

    $scope.selectCidade = function (cidadeCep) {
        var cidadeAtual = $scope.array_cidades.filter(function (o, i, a) {
            return (o.titulo == cidadeCep);
        });

        // Atualiza os dados na tela
        $scope.registroAtual.id_cidade = cidadeAtual[0].id;
    }

});