'use strict';

app.controller('DividasRemessaBancoController', function ($scope, $rootScope, $state, DividasRemessaBancoService, AvisoFactory, StorageFactory, $uibModal, FileSaver, DividasService) {

    $scope.loading = true;
    $scope.itensSelecionados = [];
    $scope.array_situacao = [{id: 0, titulo: 'Não'}, {id: 1, titulo: 'Sim'}];

    $scope.init = function () {
        DividasRemessaBancoService.getAll().then(function (ret) {
            $scope.loading = false;
            var theGrid = jQuery('#gridDividasRemessaBanco').dxDataGrid('instance');
            theGrid.option('dataSource', []);

            if (ret.success) {
                theGrid.option('dataSource', ret.retorno);
                theGrid.refresh();
                theGrid.repaint();
            }
        });
    }

    $scope.dataGridOptions = {
        dataSource: [],
        noDataText: 'Nenhum registro encontrado',
        rowAlternationEnabled: true,
        paging: {
            pageSize: configSistema.qtd_itens_listagem
        },
        export: {
            enabled: true,
            fileName: "tbl-DividasRemessaBanco",
            allowExportSelectedData: true
        },
        selection: {
            mode: 'multiple'
        },
        onSelectionChanged: function (data) {
            $scope.itensSelecionados = data.selectedRowsData;
        },
        columns: [
            {dataField: 'id', caption: '#', width: 60, alignment: 'center'},
            {dataField: 'grid_devedor', caption: 'Cliente'},
            {dataField: 'grid_divida', caption: 'Nº Divida', width: 100, alignment: 'center'},
            {dataField: 'grid_data', caption: 'Data de Entrada'},
            {dataField: 'grid_enviado_banco', caption: 'Enviado', width: 100, alignment: 'center'},
            {dataField: 'grid_boleto_numero_sequencial_remessa', caption: 'Nº Sequencial', width: 120, alignment: 'center'},
            {dataField: 'grid_data_saida_banco', caption: 'Data de Saída'},
            {dataField: 'grid_data_retorno_banco', caption: 'Data de Retorno'},
            {dataField: 'grid_cod_remessa', caption: 'Cod. Remessa', width: 120, alignment: 'center'},
            {dataField: 'grid_tipo_envio', caption: 'Tipo de Envio', width: 120, alignment: 'center'}
        ]
    };

    $scope.exibirDownloadRemessaBanco = function () {
        $uibModal.open({
            size: 'lg',
            templateUrl: 'views/intranet/dividas_remessa_banco/modalDownloadRemessaBanco.html',
            controller: 'ModalDownloadRemessaBanco'
        });
    }

    $scope.exportar = function () {
        DividasRemessaBancoService.exportar($scope.itensSelecionados).then(function (ret) {
            // var data = new Blob([ret.retorno], { type: 'text/plain;charset=utf-8' });
            // FileSaver.saveAs(data, 'remessa_banco.txt');

            $scope.exibirDownloadRemessaBanco();
            $scope.init();
        });
    }

    $scope.$watch('fileBanco', function (newFile) {
        if (newFile) {

            if (newFile.name.reverse().toUpperCase().substr(0, 3) == "TER" && newFile.size > 0) {

                // Envia o arquivo para o servidor
                DividasRemessaBancoService.uploadBanco(newFile).then(function (ret) {
                    if (ret.status == 200) {
                        AvisoFactory.success("Importação finalizada.");
                        $scope.init();
                    } else {
                        AvisoFactory.warning("Este arquivo não é válido!");
                    }
                });

            } else {
                AvisoFactory.warning("O arquivo não é válido!");
            }

        }
    });

    $scope.limpar_filtro = function () {
        $scope.palavraChave = "";
        $scope.filtroSituacao = [];
        $scope.init();
    }

    $scope.filtrar = function () {
        DividasRemessaBancoService.filtrar($scope.palavraChave, $scope.filtroSituacao).then(function (ret) {
            var theGrid = jQuery('#gridDividasRemessaBanco').dxDataGrid('instance');
            theGrid.option('dataSource', []);

            if (ret.success) {
                theGrid.option('dataSource', ret.retorno);
                theGrid.refresh();
                theGrid.repaint();
            }
        });
    }

    $scope.init();
});