'use strict';

app.controller('ModalConfigBoleto', function ($scope, $rootScope, DividasService, AvisoFactory, $uibModalInstance, registroModal) {

    $scope.registroAtual = {
        id: registroModal.id,
        boleto_mensagem1: registroModal.boleto_mensagem1,
        boleto_mensagem2: registroModal.boleto_mensagem2,
        boleto_mensagem3: registroModal.boleto_mensagem3,
        boleto_desconto: registroModal.boleto_desconto,
        boleto_abatimento: registroModal.boleto_abatimento,
        boleto_mora: registroModal.boleto_mora,
        boleto_multa: registroModal.boleto_multa
    }

    $scope.ok = function () {
        DividasService.salvarConfigBoleto($scope.registroAtual).then(function (ret) {
            if (ret.success) {
                $uibModalInstance.close(true);
            }
        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});