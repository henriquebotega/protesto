'use strict';

app.controller('AvisosDetalhesController', function ($scope, $rootScope, $state, $stateParams, AvisoFactory, AvisosService) {

    $scope.titulo = "Novo Aviso";
    $scope.loading = true;

    $scope.registroAtual = {
        titulo: "",
        conteudo: ""
    }

    if (isNotBlank($stateParams.id)) {
        $scope.titulo = "Editar Aviso";

        AvisosService.getByID($stateParams.id).then(function (ret) {
            if (ret.success) {
                $scope.loading = false;
                $scope.registroAtual = ret.retorno[0];

                AvisosService.definirComoLido($stateParams.id);
                $rootScope.$broadcast('atualizar_extranet_total_avisos', true);

            } else {
                AvisoFactory.warning("Este registro não existe!");
                $state.go("extranet.avisos");
            }
        });
    } else {
        $scope.loading = false;
    }

    $scope.voltar = function () {
        $state.go("extranet.avisos")
    }

});