'use strict';

app.controller('LayoutsController', function ($scope, $rootScope, $state, LayoutsService, AvisoFactory, StorageFactory, PromptFactory) {

    $scope.itensSelecionados = [];
    $scope.loading = true;
    $scope.habilitarBtn = ($rootScope.isAdmin);

    $scope.init = function () {
        LayoutsService.getAll().then(function (ret) {
            $scope.loading = false;

            var theGrid = jQuery('#gridLayouts').dxDataGrid('instance');
            theGrid.option('dataSource', []);

            if (ret.success) {
                theGrid.option('dataSource', ret.retorno);
                theGrid.refresh();
                theGrid.repaint();
            }
        });
    }

    $scope.dataGridOptions = {
        dataSource: [],
        noDataText: 'Nenhum registro encontrado',
        rowAlternationEnabled: true,
        paging: {
            pageSize: configSistema.qtd_itens_listagem
        },
        export: {
            enabled: true,
            fileName: "tbl-Layouts",
            allowExportSelectedData: true
        },
        selection: {
            mode: 'multiple'
        },
        onSelectionChanged: function (data) {
            $scope.itensSelecionados = data.selectedRowsData;
        },
        columns: [
            {dataField: 'id', caption: '#', width: 60, alignment: 'center'},
            {dataField: 'grid_titulo', caption: 'Titulo'}
        ]
    };

    $scope.downloadArquivo = function () {
        if ($scope.itensSelecionados.length == 1) {
            LayoutsService.download($scope.itensSelecionados[0]['arquivo']);
        }
    }

    $scope.novo = function () {
        $state.go("extranet.detalhes_layouts", {id: null})
    }

    $scope.limpar_filtro = function () {
        $scope.palavraChave = "";
        $scope.filtroSituacao = [];
        $scope.init();
    }

    $scope.filtrar = function () {
        LayoutsService.filtrar($scope.palavraChave).then(function (ret) {
            var theGrid = jQuery('#gridLayouts').dxDataGrid('instance');
            theGrid.option('dataSource', []);

            if (ret.success) {
                theGrid.option('dataSource', ret.retorno);
                theGrid.refresh();
                theGrid.repaint();
            }
        });
    }

    $scope.editar = function () {
        if ($scope.itensSelecionados.length == 1) {
            $state.go("extranet.detalhes_layouts", {id: $scope.itensSelecionados[0].id})
        }
    }

    $scope.excluir = function () {

        LayoutsService.excluir($scope.itensSelecionados).then(function () {
            AvisoFactory.success("Registro removido com sucesso.");

            $rootScope.$broadcast('atualizar_extranet_total_layouts', true);
            $scope.init();
        });

    }

    $scope.init();
});