'use strict';

app.controller('AdministradoresDetalhesController', function ($scope, $rootScope, $stateParams, $state, MenuService, StorageFactory, AdministradoresService, AvisoFactory) {

    $scope.titulo = "Novo Administrador";
    $scope.loading = true;

    $scope.registroAtual = {
        id_situacao: "0",
        email: ""
    }

    $scope.array_situacao = [
        {id: "0", titulo: "Desativado"},
        {id: "1", titulo: "Ativado"}
    ]

    if (isNotBlank($stateParams.id)) {
        $scope.titulo = "Editar Administrador";

        AdministradoresService.getByID($stateParams.id).then(function (ret) {
            if (ret.success) {
                $scope.loading = false;
                $scope.registroAtual = ret.retorno[0];
            } else {
                AvisoFactory.warning("Este registro não existe!");
                $state.go("intranet.administradores");
            }
        });
    } else {
        $scope.loading = false;
    }

    $scope.salvar = function () {
        AdministradoresService.salvar($scope.registroAtual).then(function (ret) {
            AvisoFactory.success("Registro salvo com sucesso!");

            $rootScope.$broadcast('atualizar_intranet_total_administradores', true);
            $state.go("intranet.administradores");
        });
    }

    $scope.voltar = function () {
        $state.go("intranet.administradores")
    }
});