'use strict';

app.controller('ContatosDetalhesController', function ($scope, $rootScope, $state, $stateParams, StorageFactory, AvisoFactory, ServicosService, ClientesService, ContatosInteracoesService, ContatosService) {

    $scope.titulo = "Novo Contato";
    $scope.loading = true;

    $scope.registroAtual = {
        id_cliente: "",
        titulo: "",
        mensagem: ""
    }

    $scope.array_clientes = [];

    ClientesService.getAll().then(function (ret) {
        ret.retorno.map(function (o, i, a) {
            var obj = o;

            if (obj.id_tipo == 1) {
                obj.cb_nome = o.nome;
            } else {
                obj.cb_nome = o.razao_social;
            }

            $scope.array_clientes.push(obj);
        });
    });

    if (isNotBlank($stateParams.id)) {
        $scope.titulo = "Editar Contato";

        ContatosService.getByID($stateParams.id).then(function (ret) {
            if (ret.success) {
                $scope.loading = false;
                $scope.registroAtual = ret.retorno[0];
            } else {
                AvisoFactory.warning("Este registro não existe!");
                $state.go("extranet.contatos");
            }
        });
    } else {
        $scope.loading = false;
    }

    $scope.salvar = function () {
        if (!$scope.validarCamposObrigatorios()) {
            $scope.salvarContato();
        } else {
            AvisoFactory.warning("Preencha os campos corretamente!");
        }
    }

    $scope.salvarContato = function () {
        ContatosService.salvar($scope.registroAtual).then(function (ret) {
            AvisoFactory.success("Registro salvo com sucesso!");

            $rootScope.$broadcast('atualizar_extranet_total_contatos', true);
            $rootScope.$broadcast('atualizar_intranet_total_contatos', true);
            $state.go("extranet.contatos");
        });
    }

    $scope.voltar = function () {
        $state.go("extranet.contatos")
    }

    $scope.validarCamposObrigatorios = function () {
        var erro = false;

        if ($rootScope.isAdmin) {
            if (isBlank($scope.registroAtual.id_cliente) || isBlank($scope.registroAtual.titulo) || isBlank($scope.registroAtual.mensagem)) {
                erro = true;
            }
        } else {
            if (isBlank($scope.registroAtual.titulo) || isBlank($scope.registroAtual.mensagem)) {
                erro = true;
            }
        }

        return erro;
    }

});