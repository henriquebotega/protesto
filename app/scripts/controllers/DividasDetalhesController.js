'use strict';

app.controller('DividasDetalhesController', function ($scope, $rootScope, $state, $stateParams, StorageFactory, AvisoFactory, ClientesService, DevedoresService, DividasService) {

    $scope.titulo = "Nova Divida";
    $scope.modoEdicao = false;
    $scope.loading = true;
    $scope.array_situacao = [StorageFactory.get("listagemDividasSituacao")[0]];
    $scope.array_pracas = StorageFactory.get("listagemPracas");
    $scope.array_clientes = [];
    $scope.array_devedores = [];

    $scope.registroAtual = {
        id_cliente: "0",
        id_situacao: "0",
        id_devedor: "0",
        id_praca: "0",
        nosso_numero: "",
        valor_vcto: "",
        data_vcto: "",
        deseja_parcelar: false,
        valor_da_parcela: "0",
        numero_parcela: "1"
    }

    if (!$rootScope.isAdmin) {
        DevedoresService.getAll().then(function (ret) {
            $scope.array_devedores = ret.retorno;
        });
    }

    $scope.filtrarDevedores = function () {
        $scope.array_devedores = [];

        DevedoresService.getAllByCliente($scope.registroAtual.id_cliente).then(function (ret) {
            $scope.array_devedores = ret.retorno;
        });
    }

    ClientesService.getAll().then(function (ret) {
        ret.retorno.map(function (o, i, a) {

            var obj = o;

            if (obj.id_tipo == 1) {
                obj.cb_nome = o.nome;
            } else {
                obj.cb_nome = o.razao_social;
            }

            $scope.array_clientes.push(obj);
        });
    });

    if (isNotBlank($stateParams.id)) {
        $scope.titulo = "Editar Divida";
        $scope.modoEdicao = true;
        $scope.array_situacao = StorageFactory.get("listagemDividasSituacao");

        DividasService.getByID($stateParams.id).then(function (ret) {
            if (ret.success) {
                if (ret.retorno[0].id_situacao == 1) {
                    $scope.loading = false;
                    $scope.registroAtual = ret.retorno[0];

                    if ($rootScope.isAdmin) {
                        DevedoresService.getAllByCliente($scope.registroAtual.id_cliente).then(function (retDev) {
                            $scope.array_devedores = retDev.retorno;
                        });
                    }

                } else {
                    AvisoFactory.warning("Este registro não pode ser alterado!");
                    $state.go("extranet.dividas");
                }
            } else {
                AvisoFactory.warning("Este registro não existe!");
                $state.go("extranet.dividas");
            }
        });
    } else {
        $scope.loading = false;
    }

    $scope.refazParcelamento = function () {

        var vlr = $scope.registroAtual.valor_vcto;

        if (vlr.indexOf(",") > 0) {
            if (vlr.substr(vlr.indexOf(",") + 1, vlr.length).length == 3) {
                var vlr2 = vlr.replace(".", "").replace(".", "").replace(".", "").replace(".", "").replace(",", "");
                vlr = vlr2 / 100;
            } else if (vlr.substr(vlr.indexOf(",") + 1, vlr.length).length == 1) {
                var vlr2 = vlr.replace(".", "").replace(".", "").replace(".", "").replace(".", "").replace(",", "");
                if (vlr2 > 99) {
                    vlr = vlr2 / 100;
                } else {
                    vlr = vlr2;
                }
            }
        } else {
            if (vlr > 99) {
                vlr = vlr / 100;
            }
        }

        if (vlr.toString().indexOf(",") > 0) {
            vlr = vlr.toString().replace(".", "").replace(".", "").replace(".", "").replace(".", "").replace(",", ".");
        }

        if (isNotBlank(vlr) && vlr > 0) {
            if (isNotBlank($scope.registroAtual.numero_parcela)) {
                $scope.registroAtual.valor_da_parcela = number_format((vlr / $scope.registroAtual.numero_parcela), 2, ",", ".");
            }
        } else {
            $scope.registroAtual.valor_da_parcela = vlr;
        }
    }

    $scope.salvar = function () {
        DividasService.salvar($scope.registroAtual).then(function (ret) {
            AvisoFactory.success("Registro salvo com sucesso!");

            $rootScope.$broadcast('atualizar_intranet_total_dividas', true);
            $rootScope.$broadcast('atualizar_extranet_total_dividas', true);
            $state.go("extranet.dividas");
        });
    }

    $scope.voltar = function () {
        $state.go("extranet.dividas")
    }
});