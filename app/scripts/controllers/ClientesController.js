'use strict';

app.controller('ClientesController', function ($scope, $rootScope, $state, ClientesService, PromptFactory, AvisoFactory) {

    $scope.itensSelecionados = [];
    $scope.loading = true;

    $scope.array_situacao = [
        {id: "0", titulo: "Desativado"},
        {id: "1", titulo: "Ativado"}
    ]

    $scope.init = function () {
        ClientesService.getAll().then(function (ret) {
            $scope.loading = false;
            var theGrid = jQuery('#gridClientes').dxDataGrid('instance');
            theGrid.option('dataSource', []);

            if (ret.success) {
                theGrid.option('dataSource', ret.retorno);
                theGrid.refresh();
                theGrid.repaint();
            }
        });
    }

    $scope.dataGridOptions = {
        dataSource: [],
        noDataText: 'Nenhum registro encontrado',
        rowAlternationEnabled: true,
        paging: {
            pageSize: configSistema.qtd_itens_listagem
        },
        export: {
            enabled: true,
            fileName: "tbl-Clientes",
            allowExportSelectedData: true
        },
        selection: {
            mode: 'multiple'
        },
        onSelectionChanged: function (data) {
            $scope.itensSelecionados = data.selectedRowsData;
        },
        columns: [
            {dataField: 'id', caption: '#', width: 60, alignment: 'center'},
            {dataField: 'grid_nome', caption: 'Nome'},
            {dataField: 'grid_cpf_cnpj', width: 150, caption: 'CPF/CNPJ'},
            {dataField: 'grid_situacao', width: 100, caption: 'Situação'}
        ]
    };

    $scope.novo = function () {
        $state.go("intranet.detalhes_clientes", {id: null})
    }

    $scope.limpar_filtro = function () {
        $scope.palavraChave = "";
        $scope.filtroSituacao = [];
        $scope.init();
    }

    $scope.filtrar = function () {
        ClientesService.filtrar($scope.palavraChave, $scope.filtroSituacao).then(function (ret) {
            var theGrid = jQuery('#gridClientes').dxDataGrid('instance');
            theGrid.option('dataSource', []);

            if (ret.success) {
                theGrid.option('dataSource', ret.retorno);
                theGrid.refresh();
                theGrid.repaint();
            }
        });
    }

    $scope.editar = function () {
        if ($scope.itensSelecionados.length == 1) {
            $state.go("intranet.detalhes_clientes", {id: $scope.itensSelecionados[0].id})
        }
    }

    $scope.excluir = function () {
        if ($scope.itensSelecionados.length == 1) {

            // Não permite excluir, se houver contratos vinculados ao cliente
            ClientesService.getContratoByCliente($scope.itensSelecionados[0]).then(function (ret) {

                if (!ret.success) {
                    AvisoFactory.warning(ret.mensagem);
                } else {
                    PromptFactory.info("Deseja realmente remover o registro selecionado?", "").then(function (retPrompt) {
                        if (retPrompt) {
                            ClientesService.excluir($scope.itensSelecionados[0]).then(function () {
                                AvisoFactory.success("Registro removido com sucesso.");

                                $rootScope.$broadcast('atualizar_intranet_total_clientes', true);
                                $scope.init();
                            });
                        }
                    });
                }

            });
           
        }
    }

    $scope.init();
});