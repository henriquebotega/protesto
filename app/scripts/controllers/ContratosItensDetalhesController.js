'use strict';

app.controller('ContratosItensDetalhesController', function ($scope, $rootScope, $stateParams, $state, MenuService, ServicosService, StorageFactory, ContratosService, ContratosItensService, AvisoFactory) {

    // Se o contrato não estiver como aberto, redireciona para tela anterior
    ContratosService.getByID($stateParams.id_contrato).then(function (ret) {
        if (ret.success) {
            if (ret.retorno[0].id_situacao != 1) {
                AvisoFactory.warning("Este registro não pode ser alterado!");
                $state.go("intranet.itens_contratos", {id_contrato: $stateParams.id_contrato});
            }
        }
    });

    $scope.titulo = "Novo Serviço Contratado";
    $scope.loading = true;

    $scope.registroAtual = {
        id_servico: "0",
        valor: "0",
        parcela: "0"
    }

    $scope.array_servicos = [];

    ServicosService.getAll().then(function (ret) {
        $scope.array_servicos = ret.retorno;
    });

    if (isNotBlank($stateParams.id_contrato) && isNotBlank($stateParams.id)) {
        $scope.titulo = "Editar Serviço Contratado";

        ContratosItensService.getByID($stateParams.id_contrato, $stateParams.id).then(function (ret) {

            if (ret.success) {
                if(ret.id_situacao == 1){
                    $scope.loading = false;
                    $scope.registroAtual = ret.retorno[0];
                } else {
                    AvisoFactory.warning("Este registro não pode ser alterado!");
                    $state.go("intranet.itens_contratos", {id_contrato: $stateParams.id_contrato});
                }

            } else {
                AvisoFactory.warning("Este registro não existe!");
                $state.go("intranet.itens_contratos", {id_contrato: $stateParams.id_contrato});
            }
        });
    } else {
        $scope.loading = false;
    }

    $scope.inArray = function (item, arrayServicos) {

        for (var i = 0; i < arrayServicos.length; i++) {
            if (arrayServicos[i]['id'] == item) {
                return arrayServicos[i];
            }
        }

        return false;
    }

    $scope.definirServico = function () {
        var item = $scope.inArray($scope.registroAtual.id_servico, $scope.array_servicos);
        $scope.registroAtual.valor = item.valor;
    }

    $scope.salvar = function () {
        ContratosItensService.salvar($scope.registroAtual, $stateParams.id_contrato).then(function (ret) {
            AvisoFactory.success("Registro salvo com sucesso!");
            $state.go("intranet.itens_contratos", {id_contrato: $stateParams.id_contrato});
        });
    }

    $scope.voltar = function () {
        $state.go("intranet.itens_contratos", {id_contrato: $stateParams.id_contrato})
    }
});