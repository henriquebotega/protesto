'use strict';

app.controller('ConfigController', function ($scope, StorageFactory, AvisoFactory, ConfigService, MenuService, BancosService, CarteirasService) {

    $scope.registroAtual = StorageFactory.get("configSistema");

    $scope.array_bancos = [];
    $scope.array_carteiras = [];
    $scope.array_estados = [];
    $scope.array_municipios = [];

    $scope.array_estados = StorageFactory.get("listagemEstados");

    $scope.filtrarUF = function () {
        MenuService.getMunicipiosPorEstados($scope.registroAtual.empresa_id_estado).then(function (ret) {
            $scope.array_municipios = ret.retorno;
        });
    }

    BancosService.getAll().then(function (ret) {
        $scope.array_bancos = ret.retorno;
    });

    CarteirasService.getAll().then(function (ret) {
        $scope.array_carteiras = ret.retorno;
    });

    $scope.salvar = function () {
        ConfigService.salvar($scope.registroAtual).then(function (ret) {
            StorageFactory.save("configSistema", $scope.registroAtual);
            AvisoFactory.success("Registro salvo com sucesso!");
        });
    }

    $scope.filtrarUF();
});