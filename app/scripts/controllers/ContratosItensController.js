'use strict';

app.controller('ContratosItensController', function ($scope, $rootScope, $state, $stateParams, ContratosItensService, AvisoFactory, PromptFactory, $uibModal) {

    $scope.itensSelecionados = [];
    $scope.loading = true;
    $scope.habilitarBtn = false;

    $scope.init = function () {
        if (isNotBlank($stateParams.id_contrato)) {
            ContratosItensService.getAll($stateParams.id_contrato).then(function (ret) {
                $scope.loading = false;

                if (ret.id_situacao == 1) {
                    $scope.habilitarBtn = true;
                }

                var theGrid = jQuery('#gridContratosItens').dxDataGrid('instance');
                theGrid.option('dataSource', []);

                if (ret.success) {
                    theGrid.option('dataSource', ret.retorno);
                    theGrid.refresh();
                    theGrid.repaint();
                }
            });
        } else {
            $state.go("intranet.contratos")
        }
    }

    $scope.dataGridOptions = {
        dataSource: [],
        noDataText: 'Nenhum registro encontrado',
        rowAlternationEnabled: true,
        paging: {
            pageSize: configSistema.qtd_itens_listagem
        },
        export: {
            enabled: true,
            fileName: "tbl-ContratosItens",
            allowExportSelectedData: true
        },
        selection: {
            mode: 'multiple'
        },
        onSelectionChanged: function (data) {
            $scope.itensSelecionados = data.selectedRowsData;
        },
        columns: [
            {dataField: 'id', caption: '#', width: 60, alignment: 'center'},
            {dataField: 'grid_servico', caption: 'Serviço'},
            {dataField: 'grid_valor', width: 150, caption: 'Valor'}
        ]
    };

    $scope.novo = function () {
        $state.go("intranet.detalhes_itens_contratos", {id_contrato: $stateParams.id_contrato, id: null})
    }

    $scope.limpar_filtro = function () {
        $scope.palavraChave = "";
        $scope.init();
    }

    $scope.filtrar = function () {
        ContratosItensService.filtrar($stateParams.id_contrato, $scope.palavraChave).then(function (ret) {
            var theGrid = jQuery('#gridContratosItens').dxDataGrid('instance');
            theGrid.option('dataSource', []);

            if (ret.success) {
                theGrid.option('dataSource', ret.retorno);
                theGrid.refresh();
                theGrid.repaint();
            }
        });
    }

    $scope.editar = function () {
        if ($scope.itensSelecionados.length == 1) {
            $state.go("intranet.detalhes_itens_contratos", {id_contrato: $stateParams.id_contrato, id: $scope.itensSelecionados[0].id})
        }
    }

    $scope.excluir = function () {
        PromptFactory.info("Deseja realmente remover o(s) registro(s) selecionado(s)?", "").then(function (retPrompt) {
            if (retPrompt) {
                ContratosItensService.excluir($scope.itensSelecionados).then(function () {
                    AvisoFactory.success("Registro removido com sucesso.");
                    $scope.init();
                });
            }
        });
    }

    $scope.voltar = function () {
        $state.go("intranet.contratos")
    }

    $scope.init();
});