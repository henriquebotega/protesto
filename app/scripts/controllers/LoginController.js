'use strict';

app.controller('LoginController', function ($scope, $rootScope, $state, $timeout, StorageFactory, ConfigService, AvisoFactory, LoginService, vcRecaptchaService) {

    $scope.init = function () {
        $scope.email = "admin@admin.com.br";
        $scope.senha = "admin";
        $scope.msg_success = "";
        $scope.msg_error = "";
    }

    // Se já esta logado, vai para a pagina principal
    if (StorageFactory.get("logado")) {
        if (StorageFactory.get("logado").usuario.id_tipo == 1) {
            $state.go('intranet.principal');
        } else {
            $state.go('extranet.principal');
        }
        return;
    } else {
        jQuery('#body').addClass('body-login');
        $scope.init();
    }

    $scope.isNotBlank = function (itemAtual) {
        return isNotBlank(itemAtual);
    }

    $scope.response = null;
    $scope.widgetId = null;

    $scope.setResponse = function (response) {
        $scope.response = response;
    };

    $scope.setWidgetId = function (widgetId) {
        $scope.widgetId = widgetId;
    };

    $scope.cbExpiration = function () {
        vcRecaptchaService.reload($scope.widgetId);
        $scope.response = null;
    };

    $scope.logar = function () {
        if (isNotBlank($scope.email) && isNotBlank($scope.senha) && validarEmail($scope.email)) {

            var debugTest = window.localStorage.getItem("debugTest");

            if (isBlank($scope.response) && isBlank(debugTest)) {
                AvisoFactory.warning("Você deve marcar a opção reCAPTCHA");

            } else {
                LoginService.logar($scope.email, $scope.senha, $scope.response, debugTest).then(function (ret) {

                    if (ret.success) {
                        var logado = {
                            id: ret.retorno[0]['id'],
                            token: ret.token,
                            usuario: ret.retorno[0]
                        }

                        ConfigService.getAll().then(function (ret) {
                            StorageFactory.save("configSistema", ret.retorno[0]);

                            $rootScope.$broadcast('logado', true);
                            StorageFactory.save("logado", logado);

                            if (StorageFactory.get("logado").usuario.id_tipo == 1) {
                                $state.go('intranet.principal');
                            } else {
                                $state.go('extranet.principal');
                            }
                        });

                    } else {
                        $rootScope.$broadcast('logado', false);
                        $scope.msg_error = ret.mensagem;
                        $scope.limpar_error();
                    }

                });
            }

        } else {
            $scope.msg_error = "Preencha o(s) campo(s) solicitado(s) corretamente!";
            $scope.limpar_error();
        }
    }

    $scope.recuperar_senha = function () {
        if (isNotBlank($scope.email) && validarEmail($scope.email)) {

            LoginService.recuperar_senha($scope.email).then(function (ret) {

                if (ret.success) {
                    $scope.senha = "";
                    $scope.msg_success = "Uma nova senha foi enviada para seu e-mail!"
                } else {
                    $scope.msg_error = ret.mensagem;
                }

                $scope.limpar_error();

            });
        } else {
            $scope.msg_error = "Preencha o campo e-mail corretamente!";
            $scope.limpar_error();
        }
    }

    $scope.limpar_error = function () {
        $timeout(function () {
            $scope.msg_success = "";
            $scope.msg_error = "";
        }, 5000);
    }
});