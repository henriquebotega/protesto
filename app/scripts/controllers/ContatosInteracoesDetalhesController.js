'use strict';

app.controller('ContatosInteracoesDetalhesController', function ($scope, $rootScope, $stateParams, $state, MenuService, ServicosService, StorageFactory, ContatosService, ContatosInteracoesService, AvisoFactory) {

    $scope.titulo = "Nova Interação";
    $scope.loading = true;

    $scope.registroAtual = {
        mensagem: ""
    }

    if (isNotBlank($stateParams.id_contato) && isNotBlank($stateParams.id)) {
        $scope.titulo = "Editar Interação";

        ContatosInteracoesService.getByID($stateParams.id_contato, $stateParams.id).then(function (ret) {

            if (ret.success) {
                $scope.loading = false;
                $scope.registroAtual = ret.retorno[0];

                // Marca como lido
                ContatosInteracoesService.lido($stateParams.id);
                $rootScope.$broadcast('atualizar_extranet_total_contatos', true);
                $rootScope.$broadcast('atualizar_intranet_total_contatos', true);

            } else {
                AvisoFactory.warning("Este registro não existe!");
                $state.go("extranet.interacoes_contatos", {id_contato: $stateParams.id_contato});
            }
        });
    } else {
        $scope.loading = false;
    }

    $scope.salvar = function () {
        ContatosInteracoesService.salvar($scope.registroAtual, $stateParams.id_contato).then(function (ret) {
            AvisoFactory.success("Registro salvo com sucesso!");

            $rootScope.$broadcast('atualizar_extranet_total_contatos', true);
            $rootScope.$broadcast('atualizar_intranet_total_contatos', true);
            $state.go("extranet.interacoes_contatos", {id_contato: $stateParams.id_contato});
        });
    }

    $scope.voltar = function () {
        $state.go("extranet.interacoes_contatos", {id_contato: $stateParams.id_contato})
    }
});