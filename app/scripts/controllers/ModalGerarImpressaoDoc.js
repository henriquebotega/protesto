'use strict';

app.controller('ModalGerarImpressaoDoc', function ($scope, $rootScope, $state, StorageFactory, $uibModalInstance, $uibModal, registroModal, tipoDoc) {

    $scope.urlDoc = servidor + "imprimir/tela.php?tipoDoc=" + tipoDoc + "&id_divida=";

    registroModal.map(function (o, i, a) {
        $scope.urlDoc += o.id + "-";
    });

    $scope.urlDoc = $scope.urlDoc.substr(0, ($scope.urlDoc.length - 1));

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});