'use strict';

app.controller('FaturasTaxasController', function ($scope, $rootScope, $state, $stateParams, FaturasTaxasService, AvisoFactory, $uibModal) {

    $scope.itensSelecionados = [];
    $scope.loading = true;
    $scope.exibirDetalhes = false;

    $scope.init = function () {
        if (isNotBlank($stateParams.id_mensalidade)) {
            FaturasTaxasService.getAll($stateParams.id_mensalidade).then(function (ret) {
                $scope.loading = false;

                var theGrid = jQuery('#gridFaturasTaxas').dxDataGrid('instance');
                theGrid.option('dataSource', []);

                if (ret.success) {
                    theGrid.option('dataSource', ret.retorno);
                    theGrid.refresh();
                    theGrid.repaint();
                } else {
                    AvisoFactory.warning("Este registro não existe!");
                    $state.go("extranet.faturas")
                }

            });
        } else {
            $state.go("extranet.faturas")
        }
    }

    $scope.dataGridOptions = {
        dataSource: [],
        noDataText: 'Nenhum registro encontrado',
        rowAlternationEnabled: true,
        paging: {
            pageSize: configSistema.qtd_itens_listagem
        },
        export: {
            enabled: true,
            fileName: "tbl-FaturasTaxas",
            allowExportSelectedData: true
        },
        selection: {
            mode: 'multiple'
        },
        onSelectionChanged: function (data) {
            $scope.itensSelecionados = data.selectedRowsData;
            $scope.detalhes();
        },
        columns: [
            {dataField: 'id', caption: '#', width: 60, alignment: 'center'},
            {dataField: 'grid_servico', caption: 'Serviço'},
            {dataField: 'grid_historico', caption: 'Histórico'},
            {dataField: 'grid_valor', width: 150, caption: 'Valor'},
            {dataField: 'grid_data', width: 150, caption: 'Data'}
        ]
    };

    $scope.detalhes = function () {
        $scope.exibirDetalhes = false;

        if ($scope.itensSelecionados.length == 1) {
            $scope.exibirDetalhes = true;
            $scope.registroAtual = $scope.itensSelecionados[0];
        }
    }

    $scope.voltar = function () {
        $state.go("extranet.faturas")
    }

    $scope.init();
});
