'use strict';

app.controller('ContratosController', function ($scope, $rootScope, $state, ContratosService, AvisoFactory, StorageFactory, PromptFactory, $uibModal) {

    $scope.itensSelecionados = [];
    $scope.loading = true;
    $scope.array_situacao = StorageFactory.get("listagemContratoSituacao");

    $scope.init = function () {
        ContratosService.getAll().then(function (ret) {
            $scope.loading = false;

            var theGrid = jQuery('#gridContratos').dxDataGrid('instance');
            theGrid.option('dataSource', []);

            if (ret.success) {
                theGrid.option('dataSource', ret.retorno);
                theGrid.refresh();
                theGrid.repaint();
            }
        });
    }

    $scope.dataGridOptions = {
        dataSource: [],
        noDataText: 'Nenhum registro encontrado',
        rowAlternationEnabled: true,
        paging: {
            pageSize: configSistema.qtd_itens_listagem
        },
        export: {
            enabled: true,
            fileName: "tbl-Contratos",
            allowExportSelectedData: true
        },
        selection: {
            mode: 'multiple'
        },
        onSelectionChanged: function (data) {
            $scope.itensSelecionados = data.selectedRowsData;
        },
        columns: [
            {dataField: 'id', caption: '#', width: 60, alignment: 'center'},
            {dataField: 'grid_cliente', caption: 'Cliente'},
            {dataField: 'grid_situacao', width: 100, caption: 'Situação'},
            {dataField: 'grid_dia_vcto', width: 150, caption: 'Dia de Vencimento'},
            {dataField: 'grid_data', width: 150, caption: 'Data'}
        ]
    };

    $scope.novo = function () {
        $state.go("intranet.detalhes_contratos", {id: null})
    }

    $scope.limpar_filtro = function () {
        $scope.palavraChave = "";
        $scope.filtroSituacao = [];
        $scope.init();
    }

    $scope.filtrar = function () {
        ContratosService.filtrar($scope.palavraChave, $scope.filtroSituacao).then(function (ret) {
            var theGrid = jQuery('#gridContratos').dxDataGrid('instance');
            theGrid.option('dataSource', []);

            if (ret.success) {
                theGrid.option('dataSource', ret.retorno);
                theGrid.refresh();
                theGrid.repaint();
            }
        });
    }

    $scope.servicosContratados = function () {
        if ($scope.itensSelecionados.length == 1) {
            $state.go("intranet.itens_contratos", {id_contrato: $scope.itensSelecionados[0].id})
        }
    }

    $scope.editar = function () {
        if ($scope.itensSelecionados.length == 1) {
            $state.go("intranet.detalhes_contratos", {id: $scope.itensSelecionados[0].id})
        }
    }

    $scope.excluir = function () {
        if ($scope.itensSelecionados.length == 1) {

            ContratosService.permiteRemover($scope.itensSelecionados[0]).then(function (ret) {
                if (!ret.success) {
                    AvisoFactory.warning(ret.mensagem);
                } else {
                    PromptFactory.info("Deseja realmente remover o registro selecionado?", "").then(function (retPrompt) {
                        if (retPrompt) {
                            ContratosService.excluir($scope.itensSelecionados[0]).then(function () {
                                AvisoFactory.success("Registro removido com sucesso.");

                                $rootScope.$broadcast('atualizar_intranet_total_contratos', true);
                                $scope.init();
                            });
                        }
                    });
                }
            });
            
        }
    }
    $scope.init();
});