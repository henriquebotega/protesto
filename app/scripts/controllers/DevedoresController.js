'use strict';

app.controller('DevedoresController', function ($scope, $rootScope, $state, DevedoresService, PromptFactory, AvisoFactory) {

    $scope.itensSelecionados = [];
    $scope.loading = true;

    $scope.init = function () {
        DevedoresService.getAll().then(function (ret) {
            $scope.loading = false;
            var theGrid = jQuery('#gridDevedores').dxDataGrid('instance');
            theGrid.option('dataSource', []);

            if (ret.success) {
                theGrid.option('dataSource', ret.retorno);
                theGrid.refresh();
                theGrid.repaint();
            }
        });
    }

    $scope.dataGridOptions = {
        dataSource: [],
        noDataText: 'Nenhum registro encontrado',
        rowAlternationEnabled: true,
        paging: {
            pageSize: configSistema.qtd_itens_listagem
        },
        export: {
            enabled: true,
            fileName: "tbl-Devedores",
            allowExportSelectedData: true
        },
        selection: {
            mode: 'multiple'
        },
        onSelectionChanged: function (data) {
            $scope.itensSelecionados = data.selectedRowsData;
        },
        columns: [
            {dataField: 'id', caption: '#', width: 60, alignment: 'center'},
            {dataField: 'grid_cliente', caption: 'Cliente', visible: ($rootScope.isAdmin)},
            {dataField: 'grid_nome', caption: 'Nome'},
            {dataField: 'grid_email', caption: 'E-mail'},
            {dataField: 'grid_cpf_cnpj', width: 150, caption: 'CPF/CNPJ'},
            {dataField: 'grid_rg_ie', width: 100, caption: 'RG/IE'},
            {dataField: 'grid_tel1', width: 150, caption: 'Telefone #1'}
        ]
    };

    $scope.novo = function () {
        $state.go("extranet.detalhes_devedores", {id: null})
    }

    $scope.limpar_filtro = function () {
        $scope.palavraChave = "";
        $scope.init();
    }

    $scope.filtrar = function () {
        DevedoresService.filtrar($scope.palavraChave).then(function (ret) {
            var theGrid = jQuery('#gridDevedores').dxDataGrid('instance');
            theGrid.option('dataSource', []);

            if (ret.success) {
                theGrid.option('dataSource', ret.retorno);
                theGrid.refresh();
                theGrid.repaint();
            }
        });
    }

    $scope.editar = function () {
        if ($scope.itensSelecionados.length == 1) {
            $state.go("extranet.detalhes_devedores", {id: $scope.itensSelecionados[0].id})
        }
    }

    $scope.excluir = function () {
        if ($scope.itensSelecionados.length == 1) {

            // Não permite excluir, se houver dividas vinculadas ao devedor
            DevedoresService.getDividasByDevedor($scope.itensSelecionados[0]).then(function (ret) {
                
                if (!ret.success) {
                    AvisoFactory.warning(ret.mensagem);
                } else {
                    PromptFactory.info("Deseja realmente remover o registro selecionado?", "").then(function (retPrompt) {
                        if (retPrompt) {
                            DevedoresService.excluir($scope.itensSelecionados[0]).then(function () {
                                AvisoFactory.success("Registro removido com sucesso.");

                                $rootScope.$broadcast('atualizar_intranet_total_devedores', true);
                                $rootScope.$broadcast('atualizar_extranet_total_devedores', true);
                                $scope.init();
                            });
                        }
                    });
                }

            });

        }
    }

    $scope.init();
});