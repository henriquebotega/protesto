'use strict';

app.controller('DevedoresDetalhesController', function ($scope, $rootScope, $stateParams, $state, MenuService, ClientesService, StorageFactory, DevedoresService, AvisoFactory) {

    $scope.titulo = "Novo Devedor";
    $scope.loading = true;

    $scope.cbTipoCadastroFisica = false;

    $scope.alterarTipoCadastro = function () {
        $scope.cbTipoCadastroFisica = !$scope.cbTipoCadastroFisica;
    }

    $scope.array_clientes = [];
    $scope.array_estados = [];
    $scope.array_cidades1 = [];
    $scope.array_cidades2 = [];
    $scope.array_cidades3 = [];

    $scope.registroAtual = {
        razao_social: "",
        nome_fantasia: "",
        cnpj: "",
        ie: "",
        nome: "",
        data_nascimento: "",
        cpf: "",
        rg: "",
        cep1: "",
        logradouro1: "",
        numero1: "",
        complemento1: "",
        bairro1: "",
        id_estado1: "0",
        id_cidade1: "0",
        cep2: "",
        logradouro2: "",
        numero2: "",
        complemento2: "",
        bairro2: "",
        id_estado2: "0",
        id_cidade2: "0",
        cep3: "",
        logradouro3: "",
        numero3: "",
        complemento3: "",
        bairro3: "",
        id_estado3: "0",
        id_cidade3: "0",
        tel1: "",
        contato1: "",
        email1: "",
        tel2: "",
        contato2: "",
        email2: "",
        tel3: "",
        contato3: "",
        email3: ""
    }

    $scope.array_estados = StorageFactory.get("listagemEstados");

    $scope.filtrarUF = function (pos) {

        var idUF = 0;

        if (pos === 1) {
            idUF = $scope.registroAtual.id_estado1;
            $scope.array_cidades1 = [];
        } else if (pos === 2) {
            idUF = $scope.registroAtual.id_estado2;
            $scope.array_cidades2 = [];
        } else {
            idUF = $scope.registroAtual.id_estado3;
            $scope.array_cidades3 = [];
        }

        MenuService.getCidadesPorEstados(idUF).then(function (ret) {
            if (pos === 1) {
                $scope.array_cidades1 = ret.retorno;
            } else if (pos === 2) {
                $scope.array_cidades2 = ret.retorno;
            } else {
                $scope.array_cidades3 = ret.retorno;
            }
        });
    }

    ClientesService.getAll().then(function (ret) {
        ret.retorno.map(function (o, i, a) {
            var obj = o;

            if (obj.id_tipo == 1) {
                obj.cb_nome = o.nome;
            } else {
                obj.cb_nome = o.razao_social;
            }

            $scope.array_clientes.push(obj);
        });
    });

    if (isNotBlank($stateParams.id)) {
        $scope.titulo = "Editar Devedor";

        DevedoresService.getByID($stateParams.id).then(function (ret) {
            if (ret.success) {
                $scope.loading = false;
                $scope.registroAtual = ret.retorno[0];

                $scope.filtrarUF(1);
                $scope.filtrarUF(2);
                $scope.filtrarUF(3);
            } else {
                AvisoFactory.warning("Este registro não existe!");
                $state.go("extranet.devedores");
            }
        });
    } else {
        $scope.loading = false;
    }

    $scope.salvar = function () {

        if ($scope.cbTipoCadastroFisica) {
            $scope.registroAtual.id_tipo = "1";
            $scope.registroAtual.razao_social = "";
            $scope.registroAtual.nome_fantasia = "";
            $scope.registroAtual.cnpj = "";
            $scope.registroAtual.ie = "";
        } else {
            $scope.registroAtual.id_tipo = "0";
            $scope.registroAtual.nome = "";
            $scope.registroAtual.data_nascimento = "";
            $scope.registroAtual.cpf = "";
            $scope.registroAtual.rg = "";
        }

        DevedoresService.salvar($scope.registroAtual).then(function (ret) {
            AvisoFactory.success("Registro salvo com sucesso!");

            $rootScope.$broadcast('atualizar_intranet_total_devedores', true);
            $rootScope.$broadcast('atualizar_extranet_total_devedores', true);
            $state.go("extranet.devedores");
        });
    }

    $scope.localizar = function (pos) {

        var cep = '';

        if (pos === 1) {
            cep = $scope.registroAtual.cep1.replace(/\D/g, '');
        } else if (pos === 2) {
            cep = $scope.registroAtual.cep2.replace(/\D/g, '');
        } else {
            cep = $scope.registroAtual.cep3.replace(/\D/g, '');
        }

        jQuery.getJSON("https://viacep.com.br/ws/" + cep + "/json", function (ret) {

            // Busca o municipio do CEP informado
            MenuService.getCidadePorCEP(ret.ibge).then(function (retCEP) {

                if (retCEP.retorno) {

                    var idUF = 0;

                    // Define o UF
                    if (pos === 1) {
                        $scope.registroAtual.id_estado1 = retCEP.retorno[0].id_estado;
                        idUF = retCEP.retorno[0].id_estado;
                    } else if (pos === 2) {
                        $scope.registroAtual.id_estado2 = retCEP.retorno[0].id_estado;
                        idUF = retCEP.retorno[0].id_estado;
                    } else {
                        $scope.registroAtual.id_estado3 = retCEP.retorno[0].id_estado;
                        idUF = retCEP.retorno[0].id_estado;
                    }

                    // Busca as cidades do UF definido
                    MenuService.getCidadesPorEstados(idUF).then(function (ret) {

                        if (pos === 1) {
                            $scope.array_cidades1 = ret.retorno;
                        } else if (pos === 2) {
                            $scope.array_cidades2 = ret.retorno;
                        } else {
                            $scope.array_cidades3 = ret.retorno;
                        }

                        // Seleciona a cidade
                        $scope.selectCidade(pos, retCEP.retorno[0].titulo);
                    });
                }
            });

            // Atualiza os dados na tela
            $scope.$apply(function () {

                if (pos === 1) {
                    $scope.registroAtual.logradouro1 = ret.logradouro;
                    $scope.registroAtual.bairro1 = ret.bairro;
                } else if (pos === 2) {
                    $scope.registroAtual.logradouro2 = ret.logradouro;
                    $scope.registroAtual.bairro2 = ret.bairro;
                } else {
                    $scope.registroAtual.logradouro3 = ret.logradouro;
                    $scope.registroAtual.bairro3 = ret.bairro;
                }

            });

        });

    }

    $scope.selectCidade = function (pos, cidadeCep) {

        var colCidades = [];

        if (pos === 1) {
            colCidades = $scope.array_cidades1;
        } else if (pos === 2) {
            colCidades = $scope.array_cidades2;
        } else {
            colCidades = $scope.array_cidades3;
        }

        var cidadeAtual = colCidades.filter(function (o, i, a) {
            return (o.titulo == cidadeCep);
        });

        // Atualiza os dados na tela
        if (pos === 1) {
            $scope.registroAtual.id_cidade1 = cidadeAtual[0].id;
        } else if (pos === 2) {
            $scope.registroAtual.id_cidade2 = cidadeAtual[0].id;
        } else {
            $scope.registroAtual.id_cidade3 = cidadeAtual[0].id;
        }
    }

    $scope.voltar = function () {
        $state.go("extranet.devedores")
    }
});