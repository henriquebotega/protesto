'use strict';

app.controller('LayoutsImportadosController', function ($scope, $rootScope, $state, LayoutsImportadosService, AvisoFactory, $uibModal, StorageFactory, PromptFactory) {

    $scope.itensSelecionados = [];
    $scope.loading = true;

    $scope.init = function () {
        LayoutsImportadosService.getAll().then(function (ret) {
            $scope.loading = false;

            var theGrid = jQuery('#gridLayoutsImportados').dxDataGrid('instance');
            theGrid.option('dataSource', []);

            if (ret.success) {
                theGrid.option('dataSource', ret.retorno);
                theGrid.refresh();
                theGrid.repaint();
            }
        });
    }

    $scope.dataGridOptions = {
        dataSource: [],
        noDataText: 'Nenhum registro encontrado',
        rowAlternationEnabled: true,
        paging: {
            pageSize: configSistema.qtd_itens_listagem
        },
        selection: {
            mode: 'multiple'
        },
        onSelectionChanged: function (data) {
            $scope.itensSelecionados = data.selectedRowsData;
        },
        columns: [
            {dataField: 'id', caption: '#', width: 60, alignment: 'center'},
            {dataField: 'grid_cliente', caption: 'Cliente', visible: ($rootScope.isAdmin)},
            {dataField: 'grid_layout', caption: 'Layout'},
            {dataField: 'grid_total_registros', caption: 'Total de Registros'},
            {dataField: 'grid_total_registros_sucesso', caption: 'Registros importados'},
            {dataField: 'grid_data', caption: 'Data'}
        ]
    };

    $scope.informacoes = function () {
        $uibModal.open({
            templateUrl: 'views/extranet/layouts_importados/modalInfo.html',
            controller: 'ModalInfo',
            resolve: {
                registroModal: function () {
                    return $scope.itensSelecionados[0];
                }
            }
        });
    }

    $scope.novo = function () {
        $state.go("extranet.detalhes_layouts_importados", {id: null})
    }

    $scope.excluir = function () {

        LayoutsImportadosService.excluir($scope.itensSelecionados).then(function () {
            AvisoFactory.success("Registro removido com sucesso.");

            $rootScope.$broadcast('atualizar_extranet_total_layouts_importados', true);
            $scope.init();
        });

    }

    $scope.init();
});