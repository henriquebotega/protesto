'use strict';

app.controller('ServicosController', function ($scope, $rootScope, $state, ServicosService, PromptFactory, AvisoFactory) {

    $scope.itensSelecionados = [];
    $scope.loading = true;
    $scope.exibeBtnExcluir = true;

    $scope.init = function () {
        ServicosService.getAll().then(function (ret) {
            $scope.loading = false;
            var theGrid = jQuery('#gridServicos').dxDataGrid('instance');
            theGrid.option('dataSource', []);

            if (ret.success) {
                theGrid.option('dataSource', ret.retorno);
                theGrid.refresh();
                theGrid.repaint();
            }
        });
    }

    $scope.dataGridOptions = {
        dataSource: [],
        noDataText: 'Nenhum registro encontrado',
        rowAlternationEnabled: true,
        paging: {
            pageSize: configSistema.qtd_itens_listagem
        },
        export: {
            enabled: true,
            fileName: "tbl-Servicos",
            allowExportSelectedData: true
        },
        selection: {
            mode: 'multiple'
        },
        onSelectionChanged: function (data) {
            $scope.itensSelecionados = data.selectedRowsData;
            $scope.habilitarBotoes();
        },
        columns: [
            {dataField: 'id', caption: '#', width: 60, alignment: 'center'},
            {dataField: 'titulo', caption: 'Titulo'},
            {dataField: 'grid_permite_remover', caption: 'Permite Remover', width: 150},
            {dataField: 'grid_obrigatorio', caption: 'Obrigatório', width: 150},
            {dataField: 'grid_valor', caption: 'Valor', width: 150}
        ]
    };

    $scope.habilitarBotoes = function () {
        $scope.exibeBtnExcluir = true;

        $scope.itensSelecionados.map(function (o, i, a) {
            if (o['permite_remover'] == 0) {
                $scope.exibeBtnExcluir = false;
            }
        });
    }

    $scope.novo = function () {
        $state.go("intranet.detalhes_servicos", {id: null})
    }

    $scope.limpar_filtro = function () {
        $scope.palavraChave = "";
        $scope.init();
    }

    $scope.filtrar = function () {
        ServicosService.filtrar($scope.palavraChave).then(function (ret) {
            var theGrid = jQuery('#gridServicos').dxDataGrid('instance');
            theGrid.option('dataSource', []);

            if (ret.success) {
                theGrid.option('dataSource', ret.retorno);
                theGrid.refresh();
                theGrid.repaint();
            }
        });
    }

    $scope.editar = function () {
        if ($scope.itensSelecionados.length == 1) {
            $state.go("intranet.detalhes_servicos", {id: $scope.itensSelecionados[0].id})
        }
    }

    $scope.excluir = function () {
        if ($scope.itensSelecionados.length == 1) {

            ServicosService.permiteRemover($scope.itensSelecionados[0]).then(function (ret) {
                if (!ret.success) {
                    AvisoFactory.warning(ret.mensagem);
                } else {
                    PromptFactory.info("Deseja realmente remover o registro selecionado?", "").then(function (retPrompt) {
                        if (retPrompt) {
                            ServicosService.excluir($scope.itensSelecionados[0]).then(function () {
                                AvisoFactory.success("Registro removido com sucesso.");

                                $rootScope.$broadcast('atualizar_intranet_total_servicos', true);
                                $scope.init();
                            });
                        }
                    });
                }
            });

        }
    }

    $scope.init();
});