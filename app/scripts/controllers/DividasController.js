'use strict';

app.controller('DividasController', function ($scope, $rootScope, $state, DividasService, AvisoFactory, StorageFactory, PromptFactory, $uibModal, DividasRemessaCartorioService) {

    $scope.itensSelecionados = [];
    $scope.loading = true;
    $scope.array_situacao = StorageFactory.get("listagemDividasSituacao");
    $scope.habilitarBtn = ($rootScope.isAdmin);

    $scope.init = function () {
        DividasService.getAll().then(function (ret) {
            $scope.loading = false;
            var theGrid = jQuery('#gridDividas').dxDataGrid('instance');
            theGrid.option('dataSource', []);

            if (ret.success) {
                theGrid.option('dataSource', ret.retorno);
                theGrid.refresh();
                theGrid.repaint();
            }
        });
    }

    $scope.dataGridOptions = {
        dataSource: [],
        noDataText: 'Nenhum registro encontrado',
        rowAlternationEnabled: true,
        paging: {
            pageSize: configSistema.qtd_itens_listagem
        },
        export: {
            enabled: true,
            fileName: "tbl-Dividas",
            allowExportSelectedData: true
        },
        selection: {
            mode: 'multiple'
        },
        onSelectionChanged: function (data) {
            $scope.itensSelecionados = data.selectedRowsData;
        },
        columns: [
            {dataField: 'id', caption: '#', width: 60, alignment: 'center'},
            {dataField: 'grid_cliente', caption: 'Cliente', visible: ($rootScope.isAdmin)},
            {dataField: 'grid_numero', width: 100, caption: 'Nosso Número', alignment: 'center'},
            {dataField: 'grid_devedor', caption: 'Devedor'},
            {dataField: 'grid_valor_vcto', width: 100, caption: 'Valor'},
            {dataField: 'grid_numero_parcela', width: 100, caption: 'Nº Parcela', alignment: 'center'},
            {dataField: 'grid_situacao', width: 150, caption: 'Situação'},
            {dataField: 'grid_remessa', width: 100, caption: 'Remessa', alignment: 'center'}
        ]
    };

    $scope.novo = function () {
        $state.go("extranet.detalhes_dividas", {id: null})
    }

    $scope.limpar_filtro = function () {
        $scope.palavraChave = "";
        $scope.filtroSituacao = [];
        $scope.init();
    }

    $scope.filtrar = function () {
        DividasService.filtrar($scope.palavraChave, $scope.filtroSituacao).then(function (ret) {
            var theGrid = jQuery('#gridDividas').dxDataGrid('instance');
            theGrid.option('dataSource', []);

            if (ret.success) {
                theGrid.option('dataSource', ret.retorno);
                theGrid.refresh();
                theGrid.repaint();
            }
        });
    }

    $scope.gerarNotaPromissoria = function () {
        $uibModal.open({
            size: 'lg',
            templateUrl: 'views/extranet/dividas/modalGerarImpressaoDoc.html',
            controller: 'ModalGerarImpressaoDoc',
            resolve: {
                registroModal: function () {
                    return $scope.itensSelecionados;
                },
                tipoDoc: function () {
                    return 'notaPromissoria'
                }
            }
        });
    }

    $scope.gerarDuplicataPrestacaoServico = function () {
        $uibModal.open({
            size: 'lg',
            templateUrl: 'views/extranet/dividas/modalGerarImpressaoDoc.html',
            controller: 'ModalGerarImpressaoDoc',
            resolve: {
                registroModal: function () {
                    return $scope.itensSelecionados;
                },
                tipoDoc: function () {
                    return 'duplicataPrestacaoServico'
                }
            }
        });
    }

    $scope.gerarDuplicataVendaMercantil = function () {
        $uibModal.open({
            size: 'lg',
            templateUrl: 'views/extranet/dividas/modalGerarImpressaoDoc.html',
            controller: 'ModalGerarImpressaoDoc',
            resolve: {
                registroModal: function () {
                    return $scope.itensSelecionados;
                },
                tipoDoc: function () {
                    return 'duplicataVendaMercantil'
                }
            }
        });
    }

    $scope.gerarTriplicataPrestacaoServico = function () {
        $uibModal.open({
            size: 'lg',
            templateUrl: 'views/extranet/dividas/modalGerarImpressaoDoc.html',
            controller: 'ModalGerarImpressaoDoc',
            resolve: {
                registroModal: function () {
                    return $scope.itensSelecionados;
                },
                tipoDoc: function () {
                    return 'triplicataPrestacaoServico'
                }
            }
        });
    }

    $scope.gerarTriplicataVendaMercantil = function () {
        $uibModal.open({
            size: 'lg',
            templateUrl: 'views/extranet/dividas/modalGerarImpressaoDoc.html',
            controller: 'ModalGerarImpressaoDoc',
            resolve: {
                registroModal: function () {
                    return $scope.itensSelecionados;
                },
                tipoDoc: function () {
                    return 'triplicataVendaMercantil'
                }
            }
        });
    }

    $scope.gerarBoleto = function () {
        var modalInstance = $uibModal.open({
            size: 'lg',
            templateUrl: 'views/extranet/dividas/modalBoleto.html',
            controller: 'ModalBoleto',
            resolve: {
                registroModal: function () {
                    return $scope.itensSelecionados;
                }
            }
        });

        modalInstance.result.then(function (retornoModal) {
            // Não faz nada
        }, function () {
            $rootScope.$broadcast('atualizar_intranet_total_dividas', true);
            $scope.init();
        });
    }

    $scope.editar = function () {
        if ($scope.itensSelecionados.length == 1) {
            $state.go("extranet.detalhes_dividas", {id: $scope.itensSelecionados[0].id})
        }
    }

    $scope.configBoleto = function () {
        $uibModal.open({
            size: 'lg',
            templateUrl: 'views/extranet/dividas/modalConfigBoleto.html',
            controller: 'ModalConfigBoleto',
            resolve: {
                registroModal: function () {
                    return $scope.itensSelecionados[0];
                }
            }
        });
    }

    $scope.cancelarBoleto = function () {

        PromptFactory.info("Você tem certeza disto?", "Será cobrado uma taxa referente ao serviço!").then(function (retPrompt) {
            if (retPrompt) {
                DividasService.cancelarBoleto($scope.itensSelecionados).then(function () {
                    AvisoFactory.success("Registro(s) cancelado(s) com sucesso.");

                    $rootScope.$broadcast('atualizar_intranet_total_dividas', true);
                    $rootScope.$broadcast('atualizar_extranet_total_dividas', true);

                    $scope.init();
                });
            }
        });

    }

    $scope.enviar_cartorio = function () {

        PromptFactory.info("Você tem certeza disto?", "Será cobrado uma taxa referente ao serviço!").then(function (retPrompt) {
            if (retPrompt) {
                DividasRemessaCartorioService.enviarCartorio($scope.itensSelecionados).then(function () {
                    AvisoFactory.success("Registro(s) enviado(s) com sucesso.");

                    $rootScope.$broadcast('atualizar_intranet_total_dividas', true);
                    $rootScope.$broadcast('atualizar_extranet_total_dividas', true);

                    $scope.init();
                });
            }
        });

    }

    $scope.init();
});