'use strict';

app.filter('trusted', ['$sce', function ($sce) {
    return $sce.trustAsResourceUrl;
}]);

app.controller('ModalDownloadRemessaBanco', function ($scope, $rootScope, $state, StorageFactory, $uibModalInstance, $uibModal) {
    $scope.iframeURLBanco = servidor + "controle/download_remessa_banco.php";

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});