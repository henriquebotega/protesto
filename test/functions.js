module.exports = {
    efetuarLogin: function (usuario, senha) {
        browser.driver.manage().window().maximize();
        browser.get('https://localhost:9000/');
        browser.waitForAngular();
        
        browser.executeScript('localStorage.setItem("debugTest", true);');
        browser.sleep(2000);

        if (this.isNotBlank(usuario) && this.isNotBlank(senha)) {
            element(by.css(".testTextboxEmail")).clear().sendKeys(usuario);
            element(by.css(".testTextboxSenha")).clear().sendKeys(senha);
        } else {
            element(by.css(".testTextboxEmail")).clear().sendKeys("admin@admin.com.br");
            element(by.css(".testTextboxSenha")).clear().sendKeys("admin");
        }

        element(by.css(".testBtnValidar")).click();
        browser.sleep(2000);
    },
    efetuarLogout: function (eh_intranet) {
        this.goToTopScreen();

        if(eh_intranet){
            element(by.css(".testBtnSairIntranet")).click().then(function () {
                browser.sleep(1000);
            })
        } else {
            element(by.css(".testBtnSairExtranet")).click().then(function () {
                browser.sleep(1000);
            })
        }
    },
    goToTopScreen: function () {
        browser.executeScript("window.scrollTo(0,0)");
    },
    isBlank: function (vlr) {
        if (vlr == undefined || vlr == null || vlr == "" || vlr == -1) {
            return true;
        }

        return false;
    },
    isNotBlank: function (vlr) {
        return !this.isBlank(vlr);
    }
};