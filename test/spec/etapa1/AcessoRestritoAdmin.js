var functionsClass = require('../../functions.js');

describe('Testar diferentes tipos de LOGIN - ', function () {

    // Login
    beforeEach(function () {
        functionsClass.efetuarLogin();
    });

    // Logout
    afterEach(function () {
        functionsClass.efetuarLogout(true);
    });

    it('Valida o acesso restrito: Administrador', function () {

        // Verifica se o menu "principal" está visivel
        expect(element(by.css(".testLinkIntranetPrincipal")).isDisplayed()).toBeTruthy();

    });

    it('Valida inclusao de novo Administrador', function () {

        // Clique no menu 'administradores'
        element(by.css(".testLinkIntranetAdministradores")).click();
        browser.sleep(2000);

        // Clica para incluir novo registro
        element(by.css(".testAdminNovo")).click();
        browser.sleep(1000);

        // Preenche o campo solicitado
        element(by.css(".testAdminFormEmail")).clear().sendKeys("teste@teste.com.br");

        // Clique em Salvar
        element(by.css(".testAdminFormBtnSalvar")).click();
        browser.sleep(2000);

        // Confirma o registro salvo
        expect(element(by.css(".sweet-alert")).isDisplayed()).toBeTruthy();
        expect(element(by.css(".sweet-alert h2")).getText()).toEqual("Registro salvo com sucesso!");
        element(by.css(".sweet-alert .confirm")).click();
        browser.sleep(2000);
    });

    it('Valida a edição do novo Administrador', function () {

        // Clique no menu 'administradores'
        element(by.css(".testLinkIntranetAdministradores")).click();
        browser.sleep(2000);

        // Seleciona a linha desejada
        element.all(by.css(".dx-datagrid .dx-datagrid-rowsview .dx-datagrid-content .dx-datagrid-table tbody tr")).get(2).click();
        browser.sleep(2000);

        // Clica em editar o registro
        element(by.css(".testAdminEditar")).click();
        browser.sleep(1000);

        // Preenche o campo solicitado
        element(by.css(".testAdminFormEmail")).clear().sendKeys("testando@testando.com.br");

        // Clique em Salvar
        element(by.css(".testAdminFormBtnSalvar")).click();
        browser.sleep(2000);

        // Confirma o registro salvo
        expect(element(by.css(".sweet-alert")).isDisplayed()).toBeTruthy();
        expect(element(by.css(".sweet-alert h2")).getText()).toEqual("Registro salvo com sucesso!");
        element(by.css(".sweet-alert .confirm")).click();
        browser.sleep(2000);

    });

    it('Valida a remoção do novo Administrador', function () {

        // Clique no menu 'administradores'
        element(by.css(".testLinkIntranetAdministradores")).click();
        browser.sleep(2000);

        // Seleciona a linha desejada
        element.all(by.css(".dx-datagrid .dx-datagrid-rowsview .dx-datagrid-content .dx-datagrid-table tbody tr")).get(2).click();
        browser.sleep(2000);

        // Clica em excluir o registro
        element(by.css(".testAdminExcluir")).click();
        browser.sleep(1000);

        // Confirma o registro removido
        expect(element(by.css(".sweet-alert")).isDisplayed()).toBeTruthy();
        expect(element(by.css(".sweet-alert h2")).getText()).toEqual("Deseja realmente remover o(s) registro(s) selecionado(s)?");
        element(by.css(".sweet-alert .confirm")).click();
        browser.sleep(5000);

        // Verifica mensagem de exclusão
        expect(element(by.css(".sweet-alert h2")).getText()).toEqual("Registro removido com sucesso.");
        element(by.css(".sweet-alert .confirm")).click();
        browser.sleep(2000);
    });

    it('Verifica tela de serviços', function () {

        // Clique no menu 'serviços'
        element(by.css(".testLinkIntranetServicos")).click();
        browser.sleep(2000);

        element.all(by.css(".dx-page")).get(1).click();
        browser.sleep(1000);

        element.all(by.css(".dx-page")).get(0).click();
        browser.sleep(1000);

    });

    it('Valida as configurações do sistema', function () {

        // Clique no menu 'config'
        element(by.css(".testLinkIntranetConfig")).click();
        browser.sleep(2000);

        element.all(by.css(".nav-tabs li")).get(1).click();
        browser.sleep(2000);

        element.all(by.css(".nav-tabs li")).get(2).click();
        browser.sleep(2000);

        element.all(by.css(".nav-tabs li")).get(3).click();
        browser.sleep(2000);

        element.all(by.css(".nav-tabs li")).get(4).click();
        browser.sleep(2000);

        element.all(by.css(".nav-tabs li")).get(5).click();
        browser.sleep(2000);
    });

});