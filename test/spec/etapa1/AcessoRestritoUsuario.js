var functionsClass = require('../../functions.js');

describe('Testar diferentes tipos de LOGIN - ', function () {

    // Login
    beforeEach(function () {
        functionsClass.efetuarLogin("anderson@anderson.com.br", "anderson");
    });

    // Logout
    afterEach(function () {
        functionsClass.efetuarLogout(false);
    });

    it('Valida o acesso restrito: Usuário', function () {

        // Verifica se o menu "administradores" está oculto
        expect(element(by.css('.testLinkIntranetAdministradores')).isPresent()).toBe(false);

    });

    it('Valida inclusao de novo Usuário', function () {

        // Clique no menu 'usuarios'
        element(by.css(".testLinkExtranetUsuarios")).click();
        browser.sleep(2000);

        // Clica para incluir novo registro
        element(by.css(".testUsuariosNovo")).click();
        browser.sleep(2000);

        element(by.css(".testUsuariosFormSituacao")).click().then(function () {
            browser.sleep(2000);

            element.all(by.css(".testUsuariosFormSituacao option")).get(1).click();
            browser.sleep(2000);

            // Preenche o campo solicitado
            element(by.css(".testUsuariosFormEmail")).clear().sendKeys("teste@teste.com.br");

            // Clique em Salvar
            element(by.css(".testUsuariosFormBtnSalvar")).click();
            browser.sleep(2000);

            // Confirma o registro salvo
            expect(element(by.css(".sweet-alert")).isDisplayed()).toBeTruthy();
            expect(element(by.css(".sweet-alert h2")).getText()).toEqual("Registro salvo com sucesso!");
            element(by.css(".sweet-alert .confirm")).click();
            browser.sleep(2000);
        });

    });

    it('Valida a edição do novo Usuário', function () {

        // Clique no menu 'usuarios'
        element(by.css(".testLinkExtranetUsuarios")).click();
        browser.sleep(2000);

        // Seleciona a linha desejada
        element.all(by.css(".dx-datagrid .dx-datagrid-rowsview .dx-datagrid-content .dx-datagrid-table tbody tr")).get(1).click();
        browser.sleep(2000);

        // Clica em editar o registro
        element(by.css(".testUsuariosEditar")).click();
        browser.sleep(1000);

        // Preenche o campo solicitado
        element(by.css(".testUsuariosFormEmail")).clear().sendKeys("testando@testando.com.br");

        // Clique em Salvar
        element(by.css(".testUsuariosFormBtnSalvar")).click();
        browser.sleep(2000);

        // Confirma o registro salvo
        expect(element(by.css(".sweet-alert")).isDisplayed()).toBeTruthy();
        expect(element(by.css(".sweet-alert h2")).getText()).toEqual("Registro salvo com sucesso!");
        element(by.css(".sweet-alert .confirm")).click();
        browser.sleep(2000);
    });

    it('Valida a remoção do novo Usuário', function () {

        // Clique no menu 'usuarios'
        element(by.css(".testLinkExtranetUsuarios")).click();
        browser.sleep(2000);

        // Seleciona a linha desejada
        element.all(by.css(".dx-datagrid .dx-datagrid-rowsview .dx-datagrid-content .dx-datagrid-table tbody tr")).get(1).click();
        browser.sleep(2000);

        // Clica em excluir o registro
        element(by.css(".testUsuariosExcluir")).click();
        browser.sleep(1000);

        // Confirma o registro removido
        expect(element(by.css(".sweet-alert")).isDisplayed()).toBeTruthy();
        expect(element(by.css(".sweet-alert h2")).getText()).toEqual("Deseja realmente remover o(s) registro(s) selecionado(s)?");
        element(by.css(".sweet-alert .confirm")).click();
        browser.sleep(5000);

        // Verifica mensagem de exclusão
        expect(element(by.css(".sweet-alert h2")).getText()).toEqual("Registro removido com sucesso.");
        element(by.css(".sweet-alert .confirm")).click();
        browser.sleep(2000);
    });

    it('Verifica tela de edição de devedores', function () {

        // Clique no menu 'serviços'
        element(by.css(".testLinkExtranetDevedores")).click();
        browser.sleep(2000);

        element(by.css(".testTxFiltrar")).clear().sendKeys("Jonas");
        element(by.css(".testBtnFiltrar")).click();
        browser.sleep(2000);

        element(by.css(".testTxFiltrar")).clear().sendKeys("Botega");
        element(by.css(".testBtnFiltrar")).click();
        browser.sleep(2000);
    });

});