var functionsClass = require('../../functions.js');

describe('Testar login com usuário bloqueado - ', function () {

    // Login
    beforeEach(function () {
        functionsClass.efetuarLogin("inexistente@dominio.com.br", "inexistente");
    });

    it('Valida se mensagem de erro existe', function () {

        expect(element(by.css('.testErroLogin')).isPresent()).toBe(true);
        expect(element(by.css('.testErroLogin')).getInnerHtml()).toContain('<i class="glyphicon glyphicon-alert"></i>');

    });

});