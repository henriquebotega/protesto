exports.config = {
    // plugins: [
    //     {
    //         package: 'protractor-console',
    //         logLevels: ['severe', 'debug', 'info', 'warning']
    //     }
    // ],

    allScriptsTimeout: 1000000,
    directConnect: true,

    // Atençao, verifique a versão do ChromeDrive 2.22 (webdriver-manager update)
    chromeDriver: '../node_modules/webdriver-manager/selenium/chromedriver_2.24.exe',

    capabilities: {'browserName': 'chrome'},
    jasmineNodeOpts: {
        defaultTimeoutInterval: 1000000,
        isVerbose: true,
        realtimeFailure: true,
        showColors: true,
        includeStackTrace: true
    },
    specs: [
        // Todos os arquivos (pasta/subpasta)
        'spec/{,*/}*.js'

        // Todos os arquivos dentro de uma unica pasta
        //'spec/etapa1/*.js'

        // Arquivo especifico
        // 'spec/etapa1/AcessoRestritoAdmin.js'
        // 'spec/etapa1/AcessoRestritoUsuario.js'
        // 'spec/etapa1/AcessoRestritoUsuarioBloqueado.js'
    ]
};
