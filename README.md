# Projeto E-Protesto

## Instalação

npm install

## Compilar Desenvolvimento

grunt

## Compilar Tests

grunt test

### Instalação do driver

npm install -g webdriver-manager
webdriver-manager update --versions.chrome 2.24

## Compilar Produção

grunt dist
