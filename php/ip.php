<?php
ini_set('safe_mode', '0');
ini_set('memory_limit', '-1');
set_time_limit(0);
setlocale(LC_ALL, 'pt_BR');

@date_default_timezone_set('America/Sao_Paulo');
@header('Content-Type: text/html; charset=UTF-8');

@header("Access-Control-Request-Origin: *");
@header("Access-Control-Allow-Origin: *");
@header("Access-Control-Max-Age: 86400");
@header('Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS');
@header('Access-Control-Allow-Headers: accept, access-control-allow-origin, authorization, content-type, www-authorization');
@header("Cache-Control: no-store, no-cache, must-revalidate");

@session_start();
$dataAtual = date('Y-m-d H:i:s', time());

define("URL_PHP_CONTROLE", $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/controle/");
define("URL_PHP_MODELO", $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/modelo/");
define("URL_PHP_CRUD", $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/modelo/crud/");

// Login
$host = "192.185.215.168";
$port = "3306";
$user = "alvoi435_protest";
$pass = "alvoi435_protesto";
$db = "alvoi435_protesto";

$ip_host = "www.alvoideal.com.br/uploads/protesto";
$ip_servidor = "https://" . $ip_host . "/";

include_once "modelo/crud/cUsuarios.php";
$dados_cUsuarios = new cUsuarios();

include_once "modelo/crud/cAdmin.php";
$dados_cAdmin = new cAdmin();

include_once "modelo/crud/cSessao.php";
$dados_cSessao = new cSessao();

include_once "modelo/crud/cConfig.php";
$dados_cConfig = new cConfig();
$ret_cConfig = $dados_cConfig->select();

$meses = array(1 => "Janeiro", 2 => "Fevereiro", 3 => "Março", 4 => "Abril", 5 => "Maio", 6 => "Junho", 7 => "Julho", 8 => "Agosto", 9 => "Setembro", 10 => "Outubro", 11 => "Novembro", 12 => "Dezembro");

function RetornaExtensao($nome_arquivo)
{
    $nome_arquivo_reverso = strrev($nome_arquivo);
    $extensao_reverso = substr($nome_arquivo_reverso, 0, strpos($nome_arquivo_reverso, "."));
    return strrev($extensao_reverso);
}

function completaComZeros($tamanhoCampo, $texto = "")
{
    if (strlen($texto) > $tamanhoCampo) {
        return substr($texto, 0, $tamanhoCampo);
    } else {
        return str_pad($texto, $tamanhoCampo, "0", STR_PAD_LEFT);
    }
}

function completaComEspacoBranco($tamanhoCampo, $texto = "")
{
    if (strlen($texto) > $tamanhoCampo) {
        return substr($texto, 0, $tamanhoCampo);
    } else {
        return str_pad($texto, $tamanhoCampo, " ", STR_PAD_RIGHT);
    }
}

function removeAcentos($string)
{
   return preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/","/(é|è|ê|ë)/","/(É|È|Ê|Ë)/","/(í|ì|î|ï)/","/(Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö)/","/(Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü)/","/(Ú|Ù|Û|Ü)/","/(ñ)/","/(Ñ)/"),explode(" ","a A e E i I o O u U n N"),$string);
}

function formatar_Dinheiro($vlr)
{
    $valor = $vlr;

    if (strlen($valor) > 6) {
        $valor = str_replace("R$", "", $valor);
        $valor = str_replace(" ", "", $valor);
        $valor = str_replace(".", "", $valor);
        $valor = str_replace(",", ".", $valor);
        $valor = floatval($valor);
    } else {
        $valor = str_replace(",", ".", $valor);
    }

    return $valor;
}

function exibir_grid_Valor($vlr)
{
    if (!empty($vlr)) {
        return "R$ " . number_format($vlr, 2, ',', '.');
    }
}

function exibir_Valor($vlr)
{
    if (!empty($vlr)) {
        return number_format($vlr, 2, ',', '.');
    }
}

function retornaMes($mes)
{
    $mesEscrito = "";
    switch ($mes) {
        case 1:
            $mesEscrito = "Janeiro";
            break;
        case 2:
            $mesEscrito = "Fevereiro";
            break;
        case 3:
            $mesEscrito = "Março";
            break;
        case 4:
            $mesEscrito = "Abril";
            break;
        case 5:
            $mesEscrito = "Maio";
            break;
        case 6:
            $mesEscrito = "Junho";
            break;
        case 7:
            $mesEscrito = "Julho";
            break;
        case 8:
            $mesEscrito = "Agosto";
            break;
        case 9:
            $mesEscrito = "Setembro";
            break;
        case 10:
            $mesEscrito = "Outubro";
            break;
        case 11:
            $mesEscrito = "Novembro";
            break;
        case 12:
            $mesEscrito = "Dezembro";
            break;

    }

    return $mesEscrito;
}

function formatar_DataHora($vlr)
{
    if (!empty($vlr)) {
        // Entrada: 20/08/1984
        // Saída: 1984-08-20
        // Saída: 1984-08-20 20:08:01

        if ($vlr[2] == "/") {

            if (strlen($vlr) == 10) {
                $dia = substr($vlr, 0, 2);
                $mes = substr($vlr, 3, 2);
                $ano = substr($vlr, 6, 4);

                return $ano . '-' . $mes . '-' . $dia;
            } else {
                $vlr = str_replace("T", " ", $vlr);
                $vlr = explode(" ", $vlr);

                $dia = substr($vlr[0], 0, 2);
                $mes = substr($vlr[0], 3, 2);
                $ano = substr($vlr[0], 6, 4);

                $horario = $vlr[1];
                return $ano . '-' . $mes . '-' . $dia . ' ' . $horario;
            }
        }

        // Entrada: 1984-08-20
        // Saída: 20/08/1984
        // Saída: 20/08/1984 20:08:01

        if ($vlr[4] == "-") {

            if (strlen($vlr) == 10) {
                $ano = substr($vlr, 0, 4);
                $mes = substr($vlr, 5, 2);
                $dia = substr($vlr, 8, 2);

                return $dia . '/' . $mes . '/' . $ano;
            } else {
                $vlr = str_replace("T", " ", $vlr);
                $vlr = explode(" ", $vlr);

                $ano = substr($vlr[0], 0, 4);
                $mes = substr($vlr[0], 5, 2);
                $dia = substr($vlr[0], 8, 2);

                $horario = $vlr[1];
                return $dia . '/' . $mes . '/' . $ano . ' ' . $horario;
            }
        }
    }
}

function calculaDigitoVerificadorBoleto($nossoNumero, $numCooperativa, $codClienteCooperativa)
{
    $sequencia = completaComZeros(4, $numCooperativa) . completaComZeros(10, $codClienteCooperativa) . completaComZeros(7, $nossoNumero);
    $constante = 3197;

    $cont = 0;
    $calculoDv = '';

    for ($num = 0; $num <= strlen($sequencia); $num++) {
        $cont++;
        if ($cont == 1) {
            $constante = 3;
        }
        if ($cont == 2) {
            $constante = 1;
        }
        if ($cont == 3) {
            $constante = 9;
        }
        if ($cont == 4) {
            $constante = 7;
            $cont = 0;
        }

        $calculoDv = $calculoDv + (substr($sequencia, $num, 1) * $constante);
    }

    $resto = $calculoDv % 11;

    if ($resto == 0 || $resto == 1) {
        $digitoNossoNumero = 0;
    } else {
        $digitoNossoNumero = 11 - $resto;
    }

    return $digitoNossoNumero;
}

function extenso($valor = 0, $maiusculas = false)
{
    if (!$maiusculas) {
        $singular = ["centavo", "real", "mil", "milhão", "bilhão", "trilhão", "quatrilhão"];
        $plural = ["centavos", "reais", "mil", "milhões", "bilhões", "trilhões", "quatrilhões"];
        $u = ["", "um", "dois", "três", "quatro", "cinco", "seis", "sete", "oito", "nove"];
    } else {
        $singular = ["CENTAVO", "REAL", "MIL", "MILHÃO", "BILHÃO", "TRILHÃO", "QUADRILHÃO"];
        $plural = ["CENTAVOS", "REAIS", "MIL", "MILHÕES", "BILHÕES", "TRILHÕES", "QUADRILHÕES"];
        $u = ["", "um", "dois", "TRÊS", "quatro", "cinco", "seis", "sete", "oito", "nove"];
    }

    $c = ["", "cem", "duzentos", "trezentos", "quatrocentos", "quinhentos", "seiscentos", "setecentos", "oitocentos", "novecentos"];
    $d = ["", "dez", "vinte", "trinta", "quarenta", "cinquenta", "sessenta", "setenta", "oitenta", "noventa"];
    $d10 = ["dez", "onze", "doze", "treze", "quatorze", "quinze", "dezesseis", "dezesete", "dezoito", "dezenove"];

    $z = 0;
    $rt = "";

    $valor = number_format($valor, 2, ".", ".");
    $inteiro = explode(".", $valor);
    for ($i = 0; $i < count($inteiro); $i++)
        for ($ii = strlen($inteiro[$i]); $ii < 3; $ii++)
            $inteiro[$i] = "0" . $inteiro[$i];

    $fim = count($inteiro) - ($inteiro[count($inteiro) - 1] > 0 ? 1 : 2);
    for ($i = 0; $i < count($inteiro); $i++) {
        $valor = $inteiro[$i];
        $rc = (($valor > 100) && ($valor < 200)) ? "cento" : $c[$valor[0]];
        $rd = ($valor[1] < 2) ? "" : $d[$valor[1]];
        $ru = ($valor > 0) ? (($valor[1] == 1) ? $d10[$valor[2]] : $u[$valor[2]]) : "";

        $r = $rc . (($rc && ($rd || $ru)) ? " e " : "") . $rd . (($rd &&
                $ru) ? " e " : "") . $ru;
        $t = count($inteiro) - 1 - $i;
        $r .= $r ? " " . ($valor > 1 ? $plural[$t] : $singular[$t]) : "";
        if ($valor == "000") $z++; elseif ($z > 0) $z--;
        if (($t == 1) && ($z > 0) && ($inteiro[0] > 0)) $r .= (($z > 1) ? " de " : "") . $plural[$t];
        if ($r) $rt = $rt . ((($i > 0) && ($i <= $fim) && ($inteiro[0] > 0) && ($z < 1)) ? (($i < $fim) ? ", " : " e ") : " ") . $r;
    }

    if (!$maiusculas) {
        $return = $rt ? $rt : "zero";
    } else {
        if ($rt) $rt = preg_replace("/ E /", " e ", ucwords($rt));
        $return = ($rt) ? ($rt) : "Zero";
    }

    if (!$maiusculas) {
        return preg_replace("/ E /", " e ", ucwords($return));
    } else {
        return strtoupper($return);
    }
}

function formatar_Data($vlr)
{
    if (!empty($vlr)) {
        // Entrada: 20/08/1984 20:08:01
        // Saída: 1984-08-20

        if ($vlr[2] == "/") {

            if (strlen($vlr) == 10) {
                $dia = substr($vlr, 0, 2);
                $mes = substr($vlr, 3, 2);
                $ano = substr($vlr, 6, 4);

                return $ano . '-' . $mes . '-' . $dia;
            } else {
                $vlr = str_replace("T", " ", $vlr);
                $vlr = explode(" ", $vlr);

                $dia = substr($vlr[0], 0, 2);
                $mes = substr($vlr[0], 3, 2);
                $ano = substr($vlr[0], 6, 4);

                return $ano . '-' . $mes . '-' . $dia;
            }
        }

        // Entrada: 1984-08-20 20:08:01
        // Saída: 20/08/1984

        if ($vlr[4] == "-") {

            if (strlen($vlr) == 10) {
                $ano = substr($vlr, 0, 4);
                $mes = substr($vlr, 5, 2);
                $dia = substr($vlr, 8, 2);

                return $dia . '/' . $mes . '/' . $ano;
            } else {
                $vlr = str_replace("T", " ", $vlr);
                $vlr = explode(" ", $vlr);

                $ano = substr($vlr[0], 0, 4);
                $mes = substr($vlr[0], 5, 2);
                $dia = substr($vlr[0], 8, 2);

                return $dia . '/' . $mes . '/' . $ano;
            }
        }
    }
}

function Redimensionar($nome, $pasta, $file, $largura, $altura = null)
{
    $img = null;

    if ($file['type'] == "image/jpeg" || $file['type'] == "image/pjpeg") {
        $img = imagecreatefromjpeg($file['tmp_name']);
    } else if ($file['type'] == "image/gif") {
        $img = imagecreatefromgif($file['tmp_name']);
    } else if ($file['type'] == "image/png" || $file['type'] == "image/x-png") {
        $img = imagecreatefrompng($file['tmp_name']);
    }

    $x = imagesx($img);
    $y = imagesy($img);

    if ($altura == null) {
        $altura = ($largura * $y) / $x;
    }

    if ($file['type'] == "image/png" || $file['type'] == "image/x-png") {
        imageistruecolor($img);
        $nova = imagecreatetruecolor($largura, $altura);
        imagealphablending($nova, false);
        imagesavealpha($nova, true);
        imagecopyresized($nova, $img, 0, 0, 0, 0, $largura, $altura, $x, $y);
    } else {
        $nova = imagecreatetruecolor($largura, $altura);
        imagecopyresampled($nova, $img, 0, 0, 0, 0, $largura, $altura, $x, $y);
    }

    $local = $pasta . $nome;

    if ($file['type'] == "image/jpeg" || $file['type'] == "image/pjpeg") {
        imagejpeg($nova, $local);
    } else if ($file['type'] == "image/gif") {
        imagegif($nova, $local);
    } else if ($file['type'] == "image/png" || $file['type'] == "image/x-png") {
        imagepng($nova, $local);
    }

    imagedestroy($img);
    imagedestroy($nova);

    return $local;
}

function validarSessao()
{
    global $dados_cSessao, $dados_cAdmin, $dados_cUsuarios, $dataAtual;

    // Valida se o codigo está ativo ainda
    $ret_cSessaoSec = $dados_cSessao->select(" (codigo = '" . @$_SERVER['HTTP_WWW_AUTHORIZATION'] . "' AND data_limite > '" . $dataAtual . "') ");
    if (sizeof($ret_cSessaoSec) > 0) {

        $ret_cAdmin = $dados_cAdmin->select(" (a.id = '" . $ret_cSessaoSec[0]['id_admin'] . "') ");

        if (sizeof($ret_cAdmin) > 0) {
            $ret_cUsuarios = null;

            if ($ret_cAdmin[0]['id_tipo'] == 0) {
                $ret_cUsuarios = $dados_cUsuarios->select(" (id_admin = '" . $ret_cSessaoSec[0]['id_admin'] . "') ");
            }

            return array("sessao" => $ret_cSessaoSec, "admin" => $ret_cAdmin, "usuario" => $ret_cUsuarios);
        } else {
            return null;
        }
    }

    return null;
}

function isBlank($vlr)
{
    if ($vlr == "undefined" || $vlr == null || $vlr == "" || $vlr == -1) {
        return true;
    }

    return false;
}

function isNotBlank($vlr)
{
    return !isBlank($vlr);
}

function retornaSomenteNumeros($vlr)
{
    return str_replace(array("(", ")", ".", ",", " ", "-", "/"), "", $vlr);
}

// Função para enviar e-mail
function EnviarEmail($destinatario, $assunto, $titulo, $mensagem, $enviarLocal = false, $files = null)
{
    global $ret_cConfig;
    include_once 'PHPMailer/class.phpmailer.php';

    $corpo = '
        <html>
            <head>
                <meta http-equiv="content-type" content="text/html; charset=ISO-8859-1">
                <style type="text/css">
                    HTML, BODY {
                        font-family: "Century Gothic", "Segoe UI", "Trebuchet MS", "Verdana";
                        margin:0;
                        padding:0;
                        font-size: 15px;
                    }

                    .mensagem p {
                        margin: 20px;
                        text-indent: 20px;
                    }

                    .mensagem TABLE TR TH
                    {
                        width: 150px;
                        height: 20px;
                        text-align: justify;
                        background-color: #EEEEEE;
                        color:#222222;
                    }

                    .mensagem TABLE TR TD
                    {
                        text-align: justify;
                        padding-left: 30px;
                        background-color: #F5F5F5;
                        color:#222222;
                    }
                </style>
            </head>
        <body>

        <center>

            <table border="0" width="100%">
                <tr>
                        <td bgColor="#242424">
                            <h1 style="font-size: 25px; color: #FFFFFF; text-align: center; margin:20px;">' . $titulo . '</h1>
                        </td>
                </tr>
                <tr>
                    <td class="mensagem">
                        ' . $mensagem . '
                    </td>
                </tr>
                <tr><td>&nbsp;</td></tr>
                <tr>
                    <td style="border-top:1px solid #d9d9d9">
                        <table border="0" cellspacing="5" width="100%">
                            <tr>
                                <td width="150">
                                    <a href="#" title="Visite nosso website">
                                        <img border="0" src="https://www.alvoideal.com.br/uploads/resources/images/logotipo/logotipo_email.png">
                                    </a>
                                </td>
                                <td>&nbsp;</td>
                                <td align="right" width="100">
                                    <a href="https://www.facebook.com/protesto" style="color:#227ecb; text-decoration:none; font-weight:bold; font-size:12px;" title="Conecte-se conosco em nosso Facebook">Facebook</a>
                                </td>
                                <td align="right" width="120">
                                    <a href="mailto:contato@protesto.com.br" style="color:#227ecb; text-decoration:none; font-weight:bold; font-size:12px;" title="Entre em contato e tire suas duvidas">Fale Conosco</a>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="5">
                                    <font size="1">
                                        ' . date("Y", time()) . ' PROTESTO - Todos os direitos reservados
                                    </font>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

        </center>
        </body>
        </html>';

    $mail = new PHPMailer();
    $mail->IsSMTP();
    $mail->Host = $ret_cConfig[0]["smtp_envio"];
    $mail->Username = $ret_cConfig[0]["email_envio"];
    $mail->Password = $ret_cConfig[0]["senha_envio"];
    $mail->Priority = 1;
    $mail->Port = 25;
    $mail->SMTPAuth = true;
    $mail->SMTPDebug = false;
    $mail->SetFrom($mail->Username, "nome do cliente");
    $mail->AddAddress($destinatario);
    $mail->Subject = $assunto;
    $mail->Body = $corpo;
    $mail->IsHTML(true);

    if ($files) {
        $mail->AddAttachment($files);
    }

    if ($enviarLocal) {
        return 'ok';
    } else {
        if (!$mail->Send()) {
            return $mail->ErrorInfo;
        } else {
            return 'ok';
        }
    }
}

?>