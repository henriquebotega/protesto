<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_CRUD . "cMunicipios.php";
$dados = new cMunicipios();

$dataPHP = json_decode(file_get_contents("php://input"));

// Valida sessão
if (isBlank(validarSessao())) {
    echo json_encode(array("success" => false, "nivel" => 3, "mensagem" => "A sessão expirou!"));
    exit();
}

if ($dataPHP->acao == "read") {

    $where = " (codigo = '" . $dataPHP->codigoIBGE . "') ";
    $order = " titulo ASC ";
    $more = "";

    executaQuery_cMunicipios($where, $order, $more);
}

if ($dataPHP->acao == "getMunicipiosPorEstados") {

    $where = " (id_estado = '" . $dataPHP->id_estado . "') ";
    $order = " titulo ASC ";
    $more = "";
    
    executaQuery_cMunicipios($where, $order, $more);
}

function executaQuery_cMunicipios($where, $order, $more)
{
    global $dados;

    $retorno = $dados->select($where, $order, $more);
    $total = sizeof((empty($more)) ? $retorno : $dados->select($where, $order));

    if (sizeof($retorno) > 0) {
        echo json_encode(array("success" => true, "nivel" => 1, "retorno" => $retorno, "total" => $total));
    } else {
        echo json_encode(array("success" => false, "nivel" => 2, "mensagem" => "Nenhum registro encontrado!"));
    }

    exit();
}

?>