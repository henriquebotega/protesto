<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_CRUD . "cRemessasBanco.php";
$dados = new cRemessasBanco();

$dataPHP = json_decode(file_get_contents("php://input"));

// Valida sessão
if (isBlank(validarSessao())) {
    echo json_encode(array("success" => false, "nivel" => 3, "mensagem" => "A sessão expirou!"));
    exit();
}

if ($dataPHP->acao == "read") {
    $where = "";
    $order = "";
    $more = "";

    executaQuery_cDividasRemessa($where, $order, $more);
}

if ($dataPHP->acao == "filtrar") {

    $where = "";

    if (isset($dataPHP->palavraChave) && isNotBlank($dataPHP->palavraChave)) {
        $where = " (rb.id_divida = '" . addslashes($dataPHP->palavraChave) . "') ";
        $where .= " OR (rb.boleto_numero_sequencial_remessa = '" . addslashes($dataPHP->palavraChave) . "') ";
        $where .= " OR (dev.nome LIKE '%" . addslashes($dataPHP->palavraChave) . "%') ";
    }

    if (isset($dataPHP->filtroSituacao) && sizeof($dataPHP->filtroSituacao) > 0) {

        $where .= " AND (rb.enviado_banco IN(";

        foreach ($dataPHP->filtroSituacao as $k => $v) {
            $where .= addslashes($v->id) . ",";
        }

        $where = substr($where, 0, -1);
        $where .= ")) ";
    }

    $order = "";
    $more = "";

    executaQuery_cDividasRemessa($where, $order, $more);
}

function executaQuery_cDividasRemessa($where, $order, $more)
{
    global $dados;

    $sessao = validarSessao();
    if (isset($sessao['usuario'])) {
        $where .= (empty($where) ? "" : " AND ");
        $where .= " (dev.id_cliente = '" . $sessao['usuario'][0]['id_cliente'] . "') ";
    }

    $retorno = $dados->select($where, $order, $more);

    $retornoArray = array();
    foreach ($retorno as $chave => $valor) {
        $retornoArray[$chave] = $valor;

        $retornoArray[$chave]['grid_devedor'] = $valor['grid_devedor'];
        $retornoArray[$chave]['grid_divida'] = $valor['id_divida'];
        $retornoArray[$chave]['grid_cod_remessa'] = $valor['cod_remessa'];
        $retornoArray[$chave]['grid_data'] = formatar_DataHora($valor['data']);
        $retornoArray[$chave]['grid_tipo_envio'] = ($valor['tipo_envio'] == 0 ? 'Registrar' : 'Cancelar');

        if (!empty($valor['data_saida_banco'])) {
            $retornoArray[$chave]['grid_data_saida_banco'] = formatar_DataHora($valor['data_saida_banco']);
        }

        if (!empty($valor['retorno_banco'])) {
            $retornoArray[$chave]['grid_data_retorno_banco'] = formatar_DataHora($valor['data_retorno_banco']);
        }

        $retornoArray[$chave]['grid_enviado_banco'] = ($valor['enviado_banco'] == 0) ? "Não" : "Sim";
        $retornoArray[$chave]['grid_boleto_numero_sequencial_remessa'] = $valor['boleto_numero_sequencial_remessa'];
    }

    if (sizeof($retornoArray) > 0) {
        echo json_encode(array("success" => true, "nivel" => 1, "retorno" => $retornoArray, "total" => sizeof($retornoArray)));
    } else {
        echo json_encode(array("success" => false, "nivel" => 2, "mensagem" => "Nenhum registro encontrado!"));
    }

    exit();
}

?>