<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_CRUD . "cDividas.php";
include_once URL_PHP_CRUD . "cRemessasCartorio.php";
include_once URL_PHP_CRUD . "cContratosItens.php";
include_once URL_PHP_CRUD . "cTaxas.php";
$dados = new cRemessasCartorio();
$dados_cDividas = new cDividas();
$dados_cContratosItens = new cContratosItens();
$dados_cTaxas = new cTaxas();

$dataPHP = json_decode(file_get_contents("php://input"));

// Valida sessão
if (isBlank(validarSessao())) {
    echo json_encode(array("success" => false, "nivel" => 3, "mensagem" => "A sessão expirou!"));
    exit();
}

if ($dataPHP->acao == "read") {
    $where = "";
    $order = "";
    $more = "";

    executaQuery_cRemessasCartorio($where, $order, $more);
}

if ($dataPHP->acao == "filtrar") {

    $where = "";

    if (isset($dataPHP->palavraChave) && isNotBlank($dataPHP->palavraChave)) {
        $where = " (dev.nome LIKE '%" . addslashes($dataPHP->palavraChave) . "%') ";
    }

    if (isset($dataPHP->filtroSituacao) && sizeof($dataPHP->filtroSituacao) > 0) {

        $where .= " AND (dc.id_situacao IN(";

        foreach ($dataPHP->filtroSituacao as $k => $v) {
            $where .= addslashes($v->id) . ",";
        }

        $where = substr($where, 0, -1);
        $where .= ")) ";
    }

    $order = "";
    $more = "";

    executaQuery_cRemessasCartorio($where, $order, $more);
}

if ($dataPHP->acao == "enviarCartorio") {

    foreach ($dataPHP->colRegistroAtual as $k => $v) {

        // Busca o cliente
        $ret = $dados_cDividas->select(" (d.id = '" . addslashes($v->id) . "') ");
        $idCliente = $ret[0]["id_cliente"];

        // Define a divida como enviada ao cartorio
        $dados_cDividas = new cDividas();
        $dados_cDividas->setEnviadoCartorio(1);
        $dados_cDividas->setId($v->id);
        $dados_cDividas->updateEnviadoCartorio();

        // Armazena a divida nos registros do cartorio
        $dados_cRemessasCartorio = new cRemessasCartorio();
        $dados_cRemessasCartorio->setIdDivida(addslashes($v->id));
        $dados_cRemessasCartorio->setNossoNumero($v->nosso_numero);
        $dados_cRemessasCartorio->salvar();

        // Busca o contrato finalizado
        $ret_cContratosItens = $dados_cContratosItens->select(" (c.id_situacao = 3 AND c.id_cliente = '" . $idCliente . "' AND ci.id_servico = 19) ");
        if (sizeof($ret_cContratosItens) > 0) {
            $valorEnvioDividaProtesto = exibir_Valor($ret_cContratosItens[0]["valor"]);

            // Cria taxa referente ao protesto
            $dados_cTaxas->setIdMensalidade(null);
            $dados_cTaxas->setIdServico(19);
            $dados_cTaxas->setHistorico("Divida #" . addslashes($v->id) . " enviada ao cartório");
            $dados_cTaxas->setValor($valorEnvioDividaProtesto);
            $dados_cTaxas->setData(date("d/m/Y H:i:s", time()));
            $dados_cTaxas->setIdCliente($idCliente);
            $dados_cTaxas->setIdSituacao(1);
            $dados_cTaxas->salvar();
        }
    }

    echo json_encode(array("success" => true, "nivel" => 1));
    exit();
}

function executaQuery_cRemessasCartorio($where, $order, $more)
{
    global $dados;

    $sessao = validarSessao();
    if (isset($sessao['usuario'])) {
        $where .= (empty($where) ? "" : " AND ");
        $where .= " (dev.id_cliente = '" . $sessao['usuario'][0]['id_cliente'] . "') ";
    }

    $retorno = $dados->select($where, $order, $more);

    $retornoArray = array();
    foreach ($retorno as $chave => $valor) {
        $retornoArray[$chave] = $valor;

        $retornoArray[$chave]['grid_devedor'] = $valor['grid_devedor'];
        $retornoArray[$chave]['grid_divida'] = $valor['id_divida'];
        $retornoArray[$chave]['grid_cod_remessa'] = $valor['cod_remessa'];
        $retornoArray[$chave]['grid_data'] = formatar_DataHora($valor['data']);

        if (!empty($valor['data_saida_cartorio'])) {
            $retornoArray[$chave]['grid_data_saida_cartorio'] = formatar_DataHora($valor['data_saida_cartorio']);
        }

        if (!empty($valor['retorno_cartorio'])) {
            $retornoArray[$chave]['grid_data_retorno_cartorio'] = formatar_DataHora($valor['data_retorno_cartorio']);
        }

        $retornoArray[$chave]['grid_enviado_cartorio'] = ($valor['enviado_cartorio'] == 0) ? "Não" : "Sim";
        $retornoArray[$chave]['grid_cartorio_numero_sequencial'] = $valor['cartorio_numero_sequencial'];
    }

    if (sizeof($retornoArray) > 0) {
        echo json_encode(array("success" => true, "nivel" => 1, "retorno" => $retornoArray, "total" => sizeof($retornoArray)));
    } else {
        echo json_encode(array("success" => false, "nivel" => 2, "mensagem" => "Nenhum registro encontrado!"));
    }

    exit();
}

?>