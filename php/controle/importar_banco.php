<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_CRUD . "cDividas.php";
include_once URL_PHP_CRUD . "cRemessasBanco.php";
$dados = new cDividas();
$dados_cRemessasBanco = new cRemessasBanco();

if (isset($_FILES['file']) && $_FILES['file']['error'] == 0 && $_FILES['file']['size'] > 0) {

    $extensao = RetornaExtensao($_FILES['file']['name']);
    $nomeArquivo = "retBanco-" . date('Y-m-d_H-i-s', time()) . "." . $extensao;
    $colTransacoes = array();

    if (move_uploaded_file($_FILES['file']['tmp_name'], "../uploads/importar/banco/" . $nomeArquivo)) {

        $ponteiro = fopen("../uploads/importar/banco/" . $nomeArquivo, "r");

        while (!feof($ponteiro)) {
            $linha = fgets($ponteiro);

            $linha = utf8_encode($linha);
            $linha = trim($linha);

            if (!empty($linha)) {
                array_push($colTransacoes, $linha);
            }
        }

        fclose($ponteiro);
        tratarLinha($colTransacoes);
    }
}

function tratarLinha($linhas)
{
    global $dados;

    if (count($linhas) >= 3) {

        $headerLinha = $linhas[0];

        $headerLinha_001a001 = substr($headerLinha, 0, 1);                   // Constante: "0"
        $headerLinha_002a002 = substr($headerLinha, 1, 1);                   // Tipo de Operação: “2”
        $headerLinha_003a009 = substr($headerLinha, 2, 7);                   // Identificação Tipo de Operação “RETORNO”
        $headerLinha_010a011 = substr($headerLinha, 9, 2);                   // Identificação do Tipo de Serviço: “01”
        $headerLinha_012a019 = utf8_encode(substr($headerLinha, 11, 8));     // Identificação por Extenso do Tipo de Serviço:“COBRANÇA”
        $headerLinha_020a026 = substr($headerLinha, 19, 7);                  // Complemento do Registro: o sistema retorna com as posições em branco
        $headerLinha_027a030 = substr($headerLinha, 26, 4);                  // Prefixo da Cooperativa: vide planilha "Capa" deste arquivo                                                          (4374)
        $headerLinha_031a031 = substr($headerLinha, 30, 1);                  // Dígito Verificador do Prefixo: Neste Campo Sempre será retornado o valor zero "0"
        $headerLinha_032a039 = substr($headerLinha, 31, 8);                  // Código do Cliente/Beneficiário: vide planilha "Capa" deste arquivo                                                  (00020205)
        $headerLinha_040a040 = substr($headerLinha, 39, 1);                  // Dígito Verificador do Código: vide planilha "Capa" deste arquivo                                                    (3)
        $headerLinha_041a046 = substr($headerLinha, 40, 6);                  // Número do convênio líder: o sistema retorna com as posições em branco                                               (072850)
        $headerLinha_047a076 = utf8_encode(substr($headerLinha, 46, 30));    // Nome do Beneficiário                                                                                                (J.ZANI PERON - COBRANÇAS  - ME)
        $headerLinha_077a094 = substr($headerLinha, 76, 18);                 // Identificação do Banco: "756 - BANCOOB S/A"                                                                         (756 - BANCOOB S/A )
        $headerLinha_095a100 = substr($headerLinha, 94, 6);                  // Data da Gravação do Retorno: formato ddmmaa                                                                         (150916)
        $headerLinha_101a107 = substr($headerLinha, 100, 7);                 // Seqüencial do Retorno: número seqüencial atribuído pelo Sicoob, acrescido de 1 a cada retorno. Inicia com "0000001" (0000004)
        $headerLinha_108a394 = substr($headerLinha, 107, 287);               // Complemento do Registro: o sistema retorna com as posições em branco
        $headerLinha_395a400 = substr($headerLinha, 394, 6);                 // Seqüencial do Registro:”000001”                                                                                      (000001)

        if ($headerLinha_003a009 == "RETORNO" && substr($headerLinha_012a019, 0, 6) == "COBRAN") {

            for ($i = 1; $i < (count($linhas) - 1); $i++) {

                $transacoesLinha_001a001 = substr($linhas[$i], 0, 1);       // Identificação do Registro Detalhe: "1"
                $transacoesLinha_002a003 = substr($linhas[$i], 1, 2);       // "Tipo de Inscrição do Beneficiário:                                          (02)     //""01"" = CPF        //""02"" = CNPJ  "
                $transacoesLinha_004a017 = substr($linhas[$i], 3, 14);      // Número do CPF/CNPJ do Beneficiário                                           (23039595000194)
                $transacoesLinha_018a021 = substr($linhas[$i], 17, 4);      // Prefixo da Cooperativa: vide planilha "Capa" deste arquivo                   (4374)
                $transacoesLinha_022a022 = substr($linhas[$i], 21, 1);      // Dígito Verificador do Prefixo: vide planilha "Capa" deste arquivo            (0)
                $transacoesLinha_023a030 = substr($linhas[$i], 22, 8);      // Conta Corrente: vide planilha "Capa" deste arquivo                           (00007285)
                $transacoesLinha_031a031 = substr($linhas[$i], 30, 1);      // Dígito Verificador da Conta: vide planilha "Capa" deste arquivo              (0)
                $transacoesLinha_032a037 = substr($linhas[$i], 31, 6);      // Número do Convênio de Cobrança do Beneficiário: "000000"
                $transacoesLinha_038a062 = substr($linhas[$i], 37, 25);     // Número de Controle do Participante: o sistema retorna com as posições em branco
                $transacoesLinha_063a073 = substr($linhas[$i], 62, 11);     // Nosso Número                                                                 (00000002103)
                $transacoesLinha_074a074 = substr($linhas[$i], 73, 1);      // DV Nosso Número                                                              (8)
                $transacoesLinha_075a076 = substr($linhas[$i], 74, 2);      // Número da Parcela                                                            (01)
                $transacoesLinha_077a080 = substr($linhas[$i], 76, 4);      // Grupo de Valor: "00"                                                         (0000)
                $transacoesLinha_081a082 = substr($linhas[$i], 80, 2);      // Código de Baixa/Recusa                                                       (00)
                $transacoesLinha_083a085 = substr($linhas[$i], 82, 3);      // "Prefixo do Título: Informa Espécie do Título                                (PC )       //DM = Duplicata Mercantil        //CH = Cheque        //DS = Duplicata de Serviço        //PC  = Parcela de Consórcio        //OU = Outros"
                $transacoesLinha_086a088 = substr($linhas[$i], 85, 3);      // Variação da Carteira: "000"
                $transacoesLinha_089a089 = substr($linhas[$i], 88, 1);      // Conta Caução: "0"
                $transacoesLinha_090a094 = substr($linhas[$i], 89, 5);      // Código de responsabilidade: "00000"
                $transacoesLinha_095a095 = substr($linhas[$i], 94, 1);      // DV do código de responsabilidade: "0"
                $transacoesLinha_096a100 = substr($linhas[$i], 95, 5);      // Taxa de desconto                                                             (00000)
                $transacoesLinha_101a105 = substr($linhas[$i], 100, 5);     // Taxa de IOF                                                                  (00000)
                $transacoesLinha_106a106 = substr($linhas[$i], 105, 1);     // Complemento do Registro: o sistema retorna com as posições em branco
                $transacoesLinha_107a108 = substr($linhas[$i], 106, 2);     // "Comando/Modalidade:                                                         (05)       //01 = Simples com registro          //03 = Garantia Caucionada
                $transacoesLinha_109a110 = substr($linhas[$i], 108, 2);     // "Comando/Movimento:                                                          (11)       //02 = Confirmação Entrada Título;        //05 = Liquidação Sem Registro: Identifica a liquidação de título da modalidade ""SEM REGISTRO"";        //06 = Liquidação Normal: Identificar a liquidação de título de modalidade ""REGISTRADA"", com exceção dos títulos que forem liquidados em cartório (Cód. de movimento 15=Liquidação em Cartório);        //09 = Baixa de Titulo: Identificar as baixas de títulos, com exceção da baixa realizada com o cód. de movimento 10 (Baixa - Pedido Beneficiário);        //10 = Baixa Solicitada (Baixa - Pedido Beneficiário): Identificar as baixas de títulos comandadas a pedido do Beneficiário;        //11 = Títulos em Ser: Identifica os títulos em carteira, que estiverem com a situação ""em abarto"" (vencidos e a vencer).        //14 = Alteração de Vencimento;        //15 = Liquidação em Cartório: Identifica as liquidações dos títulos ocorridas em cartórios de protesto;        //23 = Encaminhado a Protesto: Identifica o recebimento da instrução de protesto        //27 = Confirmação Alteração Dados.        //48 = Confirmação de instrução de transferência de carteira/modalidade de cobrança"
                $transacoesLinha_111a116 = substr($linhas[$i], 110, 6);     // Data da Entrada/Liquidação: formato ddmmaa                                   (291215)
                $transacoesLinha_117a126 = substr($linhas[$i], 116, 10);    // Seu Número/Número atribuído pela Empresa                                     (33333     )
                $transacoesLinha_127a146 = substr($linhas[$i], 126, 20);    // Complemento do Registro: o sistema retorna com as posições em branco         (                    )
                $transacoesLinha_147a152 = substr($linhas[$i], 146, 6);     // Data Vencimento Título: formato ddmmaa                                       (181116)
                $transacoesLinha_153a165 = substr($linhas[$i], 152, 13);    // Valor Titulo                                                                 (0000000006600)
                $transacoesLinha_166a168 = substr($linhas[$i], 165, 3);     // Código do banco recebedor: Informado o prefixo de agência do banco recebedor. Quando este prefixo não for informado, o campo vem preenchido com zeros (000)
                $transacoesLinha_169a172 = substr($linhas[$i], 168, 4);     // Prefixo da agência recebedora: Informado o prefixo de agência do banco recebedor. Quando este prefixo não for informado, o campo vem preenchido com zeros (0000)
                $transacoesLinha_173a173 = substr($linhas[$i], 172, 1);     // DV prefixo recebedora                                                        (0)
                $transacoesLinha_174a175 = substr($linhas[$i], 173, 2);     // "Espécie do Título :                                                         (99)     //01 = Duplicata Mercantil        //02 = Nota Promissória        //03 = Nota de Seguro        //05 = Recibo        //06 = Duplicata Rural        //08 = Letra de Câmbio        //09 = Warrant        //10 = Cheque        //12 = Duplicata de Serviço        //13 = Nota de Débito        //14 = Triplicata Mercantil        //15 = Triplicata de Serviço        //18 = Fatura        //20 = Apólice de Seguro        //21 = Mensalidade Escolar        //22 = Parcela de Consórcio        //99 = Outros"
                $transacoesLinha_176a181 = substr($linhas[$i], 175, 6);     // Data do crédito: formato ddmmaa                                              (000000)
                $transacoesLinha_182a188 = substr($linhas[$i], 181, 7);     // Valor da Tarifa                                                              (0000000)
                $transacoesLinha_189a201 = substr($linhas[$i], 188, 13);    // Outras despesas                                                              (0000000000000)
                $transacoesLinha_202a214 = substr($linhas[$i], 201, 13);    // Juros do desconto                                                            (0000000000000)
                $transacoesLinha_215a227 = substr($linhas[$i], 214, 13);    // IOF do desconto                                                              (0000000000000)
                $transacoesLinha_228a240 = substr($linhas[$i], 227, 13);    // Valor do abatimento                                                          (0000000000000)
                $transacoesLinha_241a253 = substr($linhas[$i], 240, 13);    // Desconto concedido (diferença entre valor do título e valor recebido)        (0000000000000)
                $transacoesLinha_254a266 = substr($linhas[$i], 253, 13);    // Valor recebido (valor recebido parcial)                                      (0000000000000)
                $transacoesLinha_267a279 = substr($linhas[$i], 266, 13);    // Juros de Mora                                                                (0000000000000)
                $transacoesLinha_280a292 = substr($linhas[$i], 279, 13);    // Outros recebimentos                                                          (0000000000000)
                $transacoesLinha_293a305 = substr($linhas[$i], 292, 13);    // Abatimento não aproveitado pelo Pagador                                      (0000000000000)
                $transacoesLinha_306a318 = substr($linhas[$i], 305, 13);    // Valor do Lançamento                                                          (0000000000000)
                $transacoesLinha_319a319 = substr($linhas[$i], 318, 1);     // Indicativo débito/crédito                                                    (1)
                $transacoesLinha_320a320 = substr($linhas[$i], 319, 1);     // Indicativo valor                                                             (0)
                $transacoesLinha_321a332 = substr($linhas[$i], 320, 12);    // Valor do Ajuste                                                              (000000000000)
                $transacoesLinha_333a342 = substr($linhas[$i], 332, 10);    // Complemento do Registro: o sistema retorna com as posições em branco
                $transacoesLinha_343a356 = substr($linhas[$i], 342, 14);    // CPF/CNPJ do Pagador                                                          (00004995743979)
                $transacoesLinha_357a394 = substr($linhas[$i], 356, 38);    // Complemento do Registro: o sistema retorna com as posições em branco
                $transacoesLinha_395a400 = substr($linhas[$i], 394, 6);     // Seqüencial do Registro: Incrementado em 1 a cada registro                    (003202)

                $nossoNumero = (int)$transacoesLinha_063a073;
                $dataEntrada = $transacoesLinha_111a116;
                $valor = $transacoesLinha_153a165;
                $codRemessa = (int)$headerLinha_101a107;

                $dados = new cDividas();
                $dados->importar($dataEntrada, $valor, $nossoNumero);

                $dados_cRemessasBanco = new cRemessasBanco();
                $dados_cRemessasBanco->setCodRemessa($codRemessa);
                $dados_cRemessasBanco->setNossoNumero($nossoNumero);
                $dados_cRemessasBanco->importar();
            }
        }

        $rodapeLinha = $linhas[sizeof($linhas) - 1];

        $rodapeLinha_001a001 = substr($rodapeLinha, 0, 1);                     // Identificação do Registro Trailer: “9”
        $rodapeLinha_002a003 = substr($rodapeLinha, 1, 2);                     // Identificação do Tipo de Serviço: “02”
        $rodapeLinha_004a006 = substr($rodapeLinha, 3, 3);                     // Número Banco: "756"
        $rodapeLinha_007a010 = substr($rodapeLinha, 6, 4);                     // Código da Cooperativa Remetente                                           (4374)
        $rodapeLinha_011a035 = utf8_encode(substr($rodapeLinha, 10, 25));      // Sigla da Cooperativa Remetente                                            (SICOOB ALIANÇA           )
        $rodapeLinha_036a085 = utf8_encode(substr($rodapeLinha, 35, 50));      // Endereço da Cooperativa Remetente                                         (AVENIDA PARANÁ                                    )
        $rodapeLinha_086a115 = utf8_encode(substr($rodapeLinha, 85, 30));      // Bairro da Cooperativa Remetente                                           (CENTRO                        )
        $rodapeLinha_116a123 = substr($rodapeLinha, 115, 8);                   // CEP da Cooperativa Remetente                                              (84261060)
        $rodapeLinha_124a153 = utf8_encode(substr($rodapeLinha, 123, 30));     // Cidade da Cooperativa Remetente                                           (Telêmaco Borba                )
        $rodapeLinha_154a155 = substr($rodapeLinha, 153, 2);                   // UF da Cooperativa Remetente                                               (PR)
        $rodapeLinha_156a163 = substr($rodapeLinha, 155, 8);                   // Data do movimento: formato ddmmaaaa                                       (15092016)
        $rodapeLinha_164a171 = substr($rodapeLinha, 163, 8);                   // Quantidade de registros no Detalhe                                        (00003201)
        $rodapeLinha_172a182 = substr($rodapeLinha, 171, 11);                  // Último Nosso Número Beneficiário                                          (00000008100)
        $rodapeLinha_183a394 = substr($rodapeLinha, 182, 212);                 // Complemento do Registro: o sistema retorna com as posições em branco
        $rodapeLinha_395a400 = substr($rodapeLinha, 394, 6);                   // Seqüencial do Registro: Incrementado em 1 a cada registro                 (003203)
    }
}

?>