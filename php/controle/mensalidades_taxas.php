<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_CRUD . "cMensalidades.php";
include_once URL_PHP_CRUD . "cContratos.php";
include_once URL_PHP_CRUD . "cContratosItens.php";
include_once URL_PHP_CRUD . "cClientes.php";
include_once URL_PHP_CRUD . "cTaxas.php";
$dados = new cTaxas();
$dados_cMensalidades = new cMensalidades();
$dados_cContratos = new cContratos();
$dados_cContratosItens = new cContratosItens();
$dados_cClientes = new cClientes();

$dataPHP = json_decode(file_get_contents("php://input"));

// Valida sessão
if (isBlank(validarSessao())) {
    echo json_encode(array("success" => false, "nivel" => 3, "mensagem" => "A sessão expirou!"));
    exit();
}

if ($dataPHP->acao == "read") {
    $retorno = $dados->select(" (t.id_mensalidade = '" . addslashes($dataPHP->id_mensalidade) . "') ");

    $retornoArray = array();
    $i = 0;
    foreach ($retorno as $chave => $valor) {
        $retornoArray[$i] = $valor;

        $ret_cClientes = $dados_cClientes->select(" (c.id = '" . $valor['id_cliente'] . "') ");
        $retornoArray[$i]['grid_cliente'] = $ret_cClientes[0]['grid_nome'];

        $retornoArray[$i]['grid_servico'] = $valor['servico'];
        $retornoArray[$i]['grid_historico'] = $valor['historico'];
        $retornoArray[$i]['grid_data'] = $valor['data'];
        $retornoArray[$i]['grid_valor'] = exibir_grid_Valor($valor['valor']);

        $retornoArray[$i]['valor'] = exibir_Valor($valor['valor']);
        $i++;
    }

    // Localiza a mensalidade
    $ret_cMensalidades = $dados_cMensalidades->select(" (m.id = '" . addslashes($dataPHP->id_mensalidade) . "') ");

    // Localiza o contrato
    $ret_cContratos = $dados_cContratos->select(" (c.id = '" . $ret_cMensalidades[0]['id_contrato'] . "') ");

    // Localiza os itens do contrato
    $ret_cContratosItens = $dados_cContratosItens->select(" (ci.id_contrato = '" . $ret_cContratos[0]['id'] . "') AND (ci.id_servico = 1) ");

    $ret_cClientes = $dados_cClientes->select(" (c.id = '" . $ret_cContratos[0]['id_cliente'] . "') ");
    $retornoArray[$i]['grid_cliente'] = $ret_cClientes[0]['grid_nome'];
    $retornoArray[$i]['grid_servico'] = "Mensalidade";
    $retornoArray[$i]['grid_historico'] = "Mensalidade";
    $retornoArray[$i]['grid_data'] = formatar_Data($ret_cMensalidades[0]['data_vigencia_inicial']);
    $retornoArray[$i]['grid_valor'] = exibir_grid_Valor($ret_cContratosItens[0]['valor']);
    $retornoArray[$i]['valor'] = exibir_Valor($ret_cContratosItens[0]['valor']);

    $retornoArray[$i]['id'] = "0";
    $retornoArray[$i]['data'] = formatar_Data($ret_cMensalidades[0]['data_vigencia_inicial']);
    $retornoArray[$i]['historico'] = "Mensalidade";
    $retornoArray[$i]['id_cliente'] = $ret_cContratos[0]['id_cliente'];
    $retornoArray[$i]['id_mensalidade'] = $ret_cMensalidades[0]['id'];
    $retornoArray[$i]['id_servico'] = $ret_cContratosItens[0]['id_servico'];
    $retornoArray[$i]['servico'] = $ret_cContratosItens[0]['servico'];
    $retornoArray[$i]['id_situacao'] = $ret_cMensalidades[0]['id_situacao'];
    $retornoArray[$i]['situacao'] = $ret_cMensalidades[0]['situacao'];

    if (sizeof($retornoArray) > 0) {
        echo json_encode(array("success" => true, "nivel" => 1, "retorno" => $retornoArray, "total" => sizeof($retornoArray)));
    } else {
        echo json_encode(array("success" => false, "nivel" => 2, "mensagem" => "Nenhum registro encontrado!"));
    }

    exit();
}

?>