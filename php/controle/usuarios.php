<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_CRUD . "cAdmin.php";
include_once URL_PHP_CRUD . "cUsuarios.php";
include_once URL_PHP_CRUD . "cClientes.php";
$dados = new cUsuarios();
$dados_cAdmin = new cAdmin();
$dados_cClientes = new cClientes();

$dataPHP = json_decode(file_get_contents("php://input"));

// Valida sessão
if (isBlank(validarSessao())) {
    echo json_encode(array("success" => false, "nivel" => 3, "mensagem" => "A sessão expirou!"));
    exit();
}

if ($dataPHP->acao == "read") {

    $where = " (a.id_tipo = 0) ";
    $order = "";
    $more = "";

    executaQuery_cUsuarios($where, $order, $more);
}

if ($dataPHP->acao == "salvar") {

    $sessao = validarSessao();
    if (isset($sessao['usuario'])) {
        $id_cliente = $sessao['usuario'][0]['id_cliente'];
    } else {
        $id_cliente = addslashes($dataPHP->registroAtual->id_cliente);
    }

    $dados_cAdmin->setEmail(addslashes($dataPHP->registroAtual->email));
    $dados_cAdmin->setIdSituacao(addslashes($dataPHP->registroAtual->id_situacao));
    $dados_cAdmin->setIdTipo(0);

    if (isset($dataPHP->registroAtual->id)) {
        $id_admin = addslashes($dataPHP->registroAtual->id);
        $dados_cAdmin->setId($id_admin);
    }

    $dados_cAdmin->salvar();

    if (!isset($dataPHP->registroAtual->id)) {
        $ret_cAdmin = $dados_cAdmin->ultimo_registro();
        $id_admin = $ret_cAdmin[0]['id'];
    }

    if (!isset($dataPHP->registroAtual->id)) {
        $dados->setIdAdmin($id_admin);
        $dados->setIdCliente($id_cliente);
        $dados->salvar();
    }

    echo json_encode(array("success" => true, "nivel" => 1));
}

if ($dataPHP->acao == "excluir") {

    foreach ($dataPHP->colRegistroAtual as $k => $v) {

        // Remove o usuário
        $dados->setIdAdmin(addslashes($v->id));
        $dados->delete();

        // Remove o admin deste usuário
        $dados_cAdmin->setId(addslashes($v->id));
        $dados_cAdmin->delete();
    }

    echo json_encode(array("success" => true, "nivel" => 1));
    exit();
}

if ($dataPHP->acao == "getByID") {

    $where = " (u.id = '" . addslashes($dataPHP->id) . "' AND a.id_tipo = 0) ";
    $order = "";
    $more = " LIMIT 1 ";

    executaQuery_cUsuarios($where, $order, $more);
}

if ($dataPHP->acao == "salvarNovaSenha") {

    if (empty($dataPHP->registroAtual->senha_atual)) {
        $where = " (a.id_tipo = '" . addslashes($dataPHP->registroAtual->id_tipo) . "' AND a.id = '" . addslashes($dataPHP->registroAtual->id) . "' AND a.senha = '') ";
        $retorno = $dados_cAdmin->select($where, "", "");
    } else {
        $where = " (a.id_tipo = '" . addslashes($dataPHP->registroAtual->id_tipo) . "' AND a.id = '" . addslashes($dataPHP->registroAtual->id) . "' AND a.senha = '" . md5(addslashes($dataPHP->registroAtual->senha_atual)) . "') ";
        $retorno = $dados_cAdmin->select($where, "", "");
    }

    if (sizeof($retorno) > 0) {
        $dados_cAdmin->setId(addslashes($dataPHP->registroAtual->id));
        $dados_cAdmin->setSenha(addslashes($dataPHP->registroAtual->nova_senha));
        $dados_cAdmin->upd_Senha();

        echo json_encode(array("success" => true, "nivel" => 1));
    } else {
        echo json_encode(array("success" => false, "nivel" => 2, "mensagem" => "A senha informada não confere!"));
    }

    exit();
}

if ($dataPHP->acao == "filtrar") {

    $where = " (a.id_tipo = 0) ";

    if (isset($dataPHP->palavraChave) && isNotBlank($dataPHP->palavraChave)) {
        $where .= " AND (a.email LIKE '%" . addslashes($dataPHP->palavraChave) . "%') ";
    }

    if (isset($dataPHP->filtroSituacao) && sizeof($dataPHP->filtroSituacao) > 0) {

        $where = $where . " AND (a.id_situacao IN(";

        foreach ($dataPHP->filtroSituacao as $k => $v) {
            $where .= addslashes($v->id) . ",";
        }

        $where = substr($where, 0, -1);
        $where .= ")) ";
    }

    $order = "";
    $more = "";

    executaQuery_cUsuarios($where, $order, $more);
}

function executaQuery_cUsuarios($where, $order, $more)
{
    global $dados, $dados_cAdmin, $dados_cClientes;

    $sessao = validarSessao();
    if (isset($sessao['usuario'])) {
        $where .= (empty($where) ? "" : " AND ");
        $where .= " (u.id_cliente = '" . $sessao['usuario'][0]['id_cliente'] . "') ";
    }

    $retorno = $dados_cAdmin->select($where, $order, $more);

    $i = 0;
    $retornoArray = array();
    foreach ($retorno as $chave => $valor) {

        $ret_cUsuarios = $dados->select(" (id_admin = '" . $valor['id'] . "') ", "", "");

        if (sizeof($ret_cUsuarios) > 0) {
            $retornoArray[$i] = $valor;
            $retornoArray[$i]['id_cliente'] = $ret_cUsuarios[0]['id_cliente'];
            $retornoArray[$i]['grid_id'] = $ret_cUsuarios[0]['id'];

            $ret_cClientes = $dados_cClientes->select(" (c.id = '" . $ret_cUsuarios[0]['id_cliente'] . "') ", "", "");
            $retornoArray[$i]['grid_cliente'] = $ret_cClientes[0]['grid_nome'];

            $retornoArray[$i]['grid_email'] = $valor['email'];
            $retornoArray[$i]['grid_situacao'] = ($valor['id_situacao'] == 0) ? "Desativado" : "Ativado";

            if (!empty($valor['data_ultimo_acesso'])) {
                $retornoArray[$i]['grid_ultimo_acesso'] = formatar_DataHora($valor['data_ultimo_acesso']);
            }
            $i++;
        }
    }

    if (sizeof($retorno) > 0) {
        echo json_encode(array("success" => true, "nivel" => 1, "retorno" => $retornoArray, "total" => sizeof($retornoArray)));
    } else {
        echo json_encode(array("success" => false, "nivel" => 2, "mensagem" => "Nenhum registro encontrado!"));
    }

    exit();
}

?>