<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_CRUD . "cRemessasCartorio.php";
$dados = new cRemessasCartorio();

//if (isset($_FILES['file']) && $_FILES['file']['error'] == 0 && $_FILES['file']['size'] > 0) {
//
//    $extensao = RetornaExtensao($_FILES['file']['name']);
//    $nomeArquivo = "retCartorio-" . date('Y-m-d_H-i-s', time()) . "." . $extensao;
//
//    if (move_uploaded_file($_FILES['file']['tmp_name'], "../uploads/" . $nomeArquivo)) {
//
//        $ponteiro = fopen("../uploads/" . $nomeArquivo, "r");
//
//        while (!feof($ponteiro)) {
//            $linha = fgets($ponteiro);
//
//            $linha = utf8_encode($linha);
//            $linha = trim($linha);
//
//            if (!empty($linha)) {
//                tratarLinha($linha);
//            }
//        }
//
//        fclose($ponteiro);
//    }
//}

vamosComecar();

function vamosComecar()
{
    $colTransacoes = array();
    $ponteiro = fopen("../uploads/importar/cartorio/RSDT1708.161", "r");

    while (!feof($ponteiro)) {
        $linha = fgets($ponteiro);

        if (!empty($linha)) {
            array_push($colTransacoes, $linha);
        }
    }

    fclose($ponteiro);

    tratarLinha($colTransacoes);
}

function tratarLinha($linhas)
{
    global $dados;

    if (count($linhas) >= 3) {

        $headerLinha = $linhas[0];

        $headerLinha_001a001 = substr($headerLinha, 0, 1);      // Identificar o registro header no arquivo                                                                                                                     (0)
        $headerLinha_002a004 = substr($headerLinha, 1, 3);      // Identificar o código do banco/portador                                                                                                                       (001)
        $headerLinha_005a044 = substr($headerLinha, 4, 40);     // Preencher com o nome do portador                                                                                                                             (OFICIO DO DISTRIBUIDOR E ANEXOS         )
        $headerLinha_045a052 = substr($headerLinha, 44, 8);     // Identificar a data de envio do arquivo retorno, no formato DDMMAAAA.                                                                                         (16082016)
        $headerLinha_053a055 = substr($headerLinha, 52, 3);     // Preencher com a sigla do remetente do arquivo: SDT – Serviço de Distribuição de Títulos.                                                                     (SDT)
        $headerLinha_056a058 = substr($headerLinha, 55, 3);     // Preencher com a sigla do destinatário do arquivo: BFO – Banco, Instituição Financeira e Outros.                                                              (BFO)
        $headerLinha_059a061 = substr($headerLinha, 58, 3);     // Preencher com a sigla de identificação da transação: RTP – Retorno da Remessa.                                                                               (RTP)
        $headerLinha_062a067 = substr($headerLinha, 61, 6);     // Controlar o seqüencial de remessas, que deverá ser contínuo.                                                                                                 (000005)
        $headerLinha_068a071 = substr($headerLinha, 67, 4);     // Preencher com o somatório da quantidade de registros constantes no registro de transação.                                                                    (0016)
        $headerLinha_072a075 = substr($headerLinha, 71, 4);     // Preencher com o zeros
        $headerLinha_076a079 = substr($headerLinha, 75, 4);     // Preencher com o zeros
        $headerLinha_080a083 = substr($headerLinha, 79, 4);     // Preencher com o zeros
        $headerLinha_084a089 = substr($headerLinha, 83, 6);     // Preencher com o número de identificação da Agência Centralizadora informado pelo portador, no Arquivo Remessa.                                               (00    )
        $headerLinha_090a092 = substr($headerLinha, 89, 3);     // Identificação da versão vigente do layout. Esta refere-se à 043                                                                                              (043)
        $headerLinha_093a099 = substr($headerLinha, 92, 7);     // Preencher 2 dígitos para o Código da Unidade da Federação e 5 para o Código do Município. Deve-se buscar esta informação na tabela IBGE (vide link página 2) (4127106)
        $headerLinha_100a596 = substr($headerLinha, 99, 497);   // Preencher com brancos.
        $headerLinha_597a600 = substr($headerLinha, 596, 4);    // Constante 0001.                                                                                                                                              (0001)

        if ($headerLinha_059a061 == "RTP" && $headerLinha_090a092 == "043") {

            for ($i = 1; $i < (count($linhas) - 1); $i++) {

                $transacoesLinha_001a001 = substr($linhas[$i], 0, 1);          // Identificar o Registro Transação no arquivo. Constante 1.                           (1)
                $transacoesLinha_002a004 = substr($linhas[$i], 1, 3);          // Identificar o código do portador                                                    (001)
                $transacoesLinha_005a019 = substr($linhas[$i], 4, 15);         // Identificar a Agência e Código do Cedente do Título/Cliente                         (001885293800023)
                $transacoesLinha_020a064 = substr($linhas[$i], 19, 45);        // Não utilizar.
                $transacoesLinha_065a109 = substr($linhas[$i], 64, 45);        // Não utilizar.
                $transacoesLinha_110a123 = substr($linhas[$i], 109, 14);       // Não utilizar.
                $transacoesLinha_124a168 = substr($linhas[$i], 123, 45);       // Não utilizar.
                $transacoesLinha_169a176 = substr($linhas[$i], 168, 8);        // Não utilizar.                                                                       (00000000)
                $transacoesLinha_177a196 = substr($linhas[$i], 176, 20);       // Não utilizar.
                $transacoesLinha_197a198 = substr($linhas[$i], 196, 2);        // Não utilizar.
                $transacoesLinha_199a213 = substr($linhas[$i], 198, 15);       // Identificar o título do cedente, conforme informado no Arquivo Remessa.             (651560010078116)
                $transacoesLinha_214a216 = substr($linhas[$i], 213, 3);        // Não utilizar.
                $transacoesLinha_217a227 = substr($linhas[$i], 216, 11);       // Não utilizar.
                $transacoesLinha_228a235 = substr($linhas[$i], 227, 8);        // Não utilizar.                                                                       (00000000)
                $transacoesLinha_236a243 = substr($linhas[$i], 235, 8);        // Não utilizar.                                                                       (00000000)
                $transacoesLinha_244a246 = substr($linhas[$i], 243, 3);        // Identificar o tipo de moeda corrente - 001 – Real                                   (001)
                $transacoesLinha_247a260 = substr($linhas[$i], 246, 14);       // Preencher com o valor do título.                                                    (00000000044600)
                $transacoesLinha_261a274 = substr($linhas[$i], 260, 14);       // Deverá conter o valor informado no arquivo remessa.                                 (00000000044600)
                $transacoesLinha_275a294 = substr($linhas[$i], 274, 20);       // Não utilizar.
                $transacoesLinha_295a295 = substr($linhas[$i], 294, 1);        // Não utilizar.
                $transacoesLinha_296a296 = substr($linhas[$i], 295, 1);        // Não utilizar.
                $transacoesLinha_297a297 = substr($linhas[$i], 296, 1);        // Não utilizar.                                                                       (0)
                $transacoesLinha_298a342 = substr($linhas[$i], 297, 45);       // Não utilizar.
                $transacoesLinha_343a345 = substr($linhas[$i], 342, 3);        // Não utilizar.                                                                       (000)
                $transacoesLinha_346a359 = substr($linhas[$i], 345, 14);       // Não utilizar.                                                                       (00000000000000)
                $transacoesLinha_360a370 = substr($linhas[$i], 359, 11);       // Não utilizar.
                $transacoesLinha_371a415 = substr($linhas[$i], 370, 45);       // Não utilizar.
                $transacoesLinha_416a423 = substr($linhas[$i], 415, 8);        // Não utilizar.                                                                       (00000000)
                $transacoesLinha_424a443 = substr($linhas[$i], 423, 20);       // Não utilizar.
                $transacoesLinha_444a445 = substr($linhas[$i], 443, 2);        // Não utilizar.
                $transacoesLinha_446a447 = substr($linhas[$i], 445, 2);        // Código de Identificação do Cartório de destino do título                            (01)
                $transacoesLinha_448a457 = substr($linhas[$i], 447, 10);       // Número do protocolo no Cartório de destino do título                                (0000007509)
                $transacoesLinha_458a458 = substr($linhas[$i], 457, 1);        // Preencher com o número de identificação do tipo de ocorrência. Anexo 2              (A)
                $transacoesLinha_459a466 = substr($linhas[$i], 458, 8);        // Preencher com a data do protocolo, no formato DDMMAAAA.                             (29072016)
                $transacoesLinha_467a476 = substr($linhas[$i], 466, 10);       // Preencher com o valor das custas do Cartório                                        (0000000000)
                $transacoesLinha_477a477 = substr($linhas[$i], 476, 1);        // Não utilizar.
                $transacoesLinha_478a485 = substr($linhas[$i], 477, 8);        // Identificar a data da ocorrência.                                                   (16082016)
                $transacoesLinha_486a487 = substr($linhas[$i], 485, 2);        // Preencher com o número de identificação do código da irregularidade. Anexo 4        (00)
                $transacoesLinha_488a507 = substr($linhas[$i], 487, 20);       // Não utilizar.
                $transacoesLinha_508a517 = substr($linhas[$i], 507, 10);       // Preencher com o valor das custas de Distribuição                                    (0000000000)
                $transacoesLinha_518a523 = substr($linhas[$i], 517, 6);        // Uso restrito do 7º Ofício do Rio de Janeiro
                $transacoesLinha_524a533 = substr($linhas[$i], 523, 10);       // Preencher com os valores de gravação eletrônica / Sedex                             (0000000000)
                $transacoesLinha_534a538 = substr($linhas[$i], 533, 5);        // Não utilizar.                                                                       (00000)
                $transacoesLinha_539a553 = substr($linhas[$i], 538, 15);       // Não utilizar.                                                                       (000000000000000)
                $transacoesLinha_554a556 = substr($linhas[$i], 553, 3);        // Não utilizar.                                                                       (000)
                $transacoesLinha_557a557 = substr($linhas[$i], 556, 1);        // Não utilizar.                                                                       (0)
                $transacoesLinha_558a565 = substr($linhas[$i], 557, 8);        // Preencher com o número de identificação do código da irregularidade                 (00000000)
                $transacoesLinha_566a566 = substr($linhas[$i], 565, 1);        // Não utilizar.
                $transacoesLinha_567a567 = substr($linhas[$i], 566, 1);        // Não utilizar.
                $transacoesLinha_568a577 = substr($linhas[$i], 567, 10);       // O Tabelionato de Protesto preencher com valor do Sedex rateado entre os títulos     (0000000000)
                $transacoesLinha_578a596 = substr($linhas[$i], 577, 19);       // Não utilizar.
                $transacoesLinha_597a600 = substr($linhas[$i], 596, 4);        // Número seqüencial do registro no arquivo.                                           (0013)

                $codCliente = $transacoesLinha_005a019;
                $identificacaoTitulo = $transacoesLinha_199a213;
                $valor = $transacoesLinha_247a260;
                $numeroProtocolo = $transacoesLinha_448a457;
                $anexo2_458a458 = $transacoesLinha_458a458;
                $anexo4_486a487 = $transacoesLinha_486a487;
                $dataProtocolo = $transacoesLinha_459a466;
                $dataOcorrencia = $transacoesLinha_478a485;

                //    $dados = new cRemessasCartorio();
                //    $dados->setDataRetorno(date("Y-m-d H:i:s", time()));
                //    $dados->setIdSituacao(addslashes($id_situacao));
                //    $dados->setId(addslashes($id));
                //    $dados->importar();
            }

        }

        $rodapeLinha = $linhas[sizeof($linhas) - 1];

        $rodapeLinha_001a001 = substr($rodapeLinha, 0, 1);          // Identificar o registro trailler no arquivo. Constante 9.         (9)
        $rodapeLinha_002a004 = substr($rodapeLinha, 1, 3);          // Identificar o código do banco/portador.                          (001)
        $rodapeLinha_005a044 = substr($rodapeLinha, 4, 40);         // Preencher com o nome do portador                                 (OFICIO DO DISTRIBUIDOR E ANEXOS         )
        $rodapeLinha_045a052 = substr($rodapeLinha, 44, 8);         // Informar a data do arquivo retorno, no formato DDMMAAAA          (16082016)
        $rodapeLinha_053a057 = substr($rodapeLinha, 52, 5);         // Preencher com a quantidade de registros.                         (00024)
        $rodapeLinha_058a075 = substr($rodapeLinha, 57, 18);        // Preencher com o somatório do Campo 18 do registro de Transação   (000000000002800919)
        $rodapeLinha_076a596 = substr($rodapeLinha, 75, 521);       // Preencher com brancos
        $rodapeLinha_597a600 = substr($rodapeLinha, 596, 4);        // Informar o número seqüencial do registro                         (0026)
    }
}

?>