<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_CRUD . "cTaxas.php";
include_once URL_PHP_CRUD . "cContratosItens.php";
include_once URL_PHP_CRUD . "cServicos.php";
$dados = new cServicos();
$dados_cTaxas = new cTaxas();
$dados_cContratosItens = new cContratosItens();

$dataPHP = json_decode(file_get_contents("php://input"));

// Valida sessão
if (isBlank(validarSessao())) {
    echo json_encode(array("success" => false, "nivel" => 3, "mensagem" => "A sessão expirou!"));
    exit();
}

if ($dataPHP->acao == "read") {
    $where = "";
    $order = "";
    $more = "";

    executaQuery_cServicos($where, $order, $more);
}

if ($dataPHP->acao == "readObrigatorios") {
    $where = " (obrigatorio = 1) ";
    $order = "";
    $more = "";

    executaQuery_cServicos($where, $order, $more);
}

if ($dataPHP->acao == "salvar") {
    $dados->setTitulo(addslashes($dataPHP->registroAtual->titulo));
    $dados->setDescricao(addslashes($dataPHP->registroAtual->descricao));
    $dados->setValor(addslashes($dataPHP->registroAtual->valor));

    if (isset($dataPHP->registroAtual->id)) {
        $dados->setId(addslashes($dataPHP->registroAtual->id));
    }

    $dados->salvar();
    echo json_encode(array("success" => true, "nivel" => 1));
}

if ($dataPHP->acao == "excluir") {

    if ($dataPHP->registroAtual->permite_remover == 1) {
        $dados->setId(addslashes($dataPHP->registroAtual->id));
        $dados->delete();
    }

    echo json_encode(array("success" => true, "nivel" => 1));
    exit();
}

if ($dataPHP->acao == "getByID") {

    $where = " (id = '" . addslashes($dataPHP->id) . "') ";
    $order = "";
    $more = " LIMIT 1 ";

    executaQuery_cServicos($where, $order, $more);
}

if ($dataPHP->acao == "permiteRemover") {

    $id_servico = $dataPHP->registroAtual->id;

    $ret_cTaxas = $dados_cTaxas->select(" (t.id_servico = '" . $id_servico . "') ");
    $ret_cContratosItens = $dados_cContratosItens->select(" (ci.id_servico = '" . $id_servico . "') ");

    if (sizeof($ret_cTaxas) > 0 || sizeof($ret_cContratosItens) > 0) {
        echo json_encode(array("success" => false, "nivel" => 2, "mensagem" => "Este serviço não pode ser removido, pois está vinculado a uma taxa!"));
        exit();
    }

    echo json_encode(array("success" => true, "nivel" => 1));
    exit();
}

if ($dataPHP->acao == "filtrar") {

    if (isset($dataPHP->palavraChave) && isNotBlank($dataPHP->palavraChave)) {
        $where = " (titulo LIKE '%" . addslashes($dataPHP->palavraChave) . "%' OR descricao LIKE '%" . addslashes($dataPHP->palavraChave) . "%') ";
    }

    $order = "";
    $more = "";

    executaQuery_cServicos($where, $order, $more);
}

function executaQuery_cServicos($where, $order, $more)
{
    global $dados;
    $retorno = $dados->select($where, $order, $more);

    $retornoArray = array();
    foreach ($retorno as $chave => $valor) {
        $retornoArray[$chave] = $valor;

        $retornoArray[$chave]['grid_obrigatorio'] = ($valor['obrigatorio'] == 1) ? "Sim" : "Não";
        $retornoArray[$chave]['grid_permite_remover'] = ($valor['permite_remover'] == 1) ? "Sim" : "Não";
        $retornoArray[$chave]['grid_valor'] = exibir_grid_Valor($valor['valor']);
        $retornoArray[$chave]['valor'] = exibir_Valor($valor['valor']);
    }

    if (sizeof($retornoArray) > 0) {
        echo json_encode(array("success" => true, "nivel" => 1, "retorno" => $retornoArray, "total" => sizeof($retornoArray)));
    } else {
        echo json_encode(array("success" => false, "nivel" => 2, "mensagem" => "Nenhum registro encontrado!"));
    }

    exit();
}

?>