<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_CRUD . "cConfig.php";
$dados = new cConfig();

$dataPHP = json_decode(file_get_contents("php://input"));

if ($dataPHP->acao == "read") {
    $retorno = $dados->select();

    $retornoArray = array();
    foreach ($retorno as $chave => $valor) {
        $retornoArray[$chave] = $valor;

        // Boolean
        $retornoArray[$chave]['ws_L0001'] = ((int)$valor['ws_L0001'] == 1 ? true : false);
        $retornoArray[$chave]['ws_L0002'] = ((int)$valor['ws_L0002'] == 1 ? true : false);
        $retornoArray[$chave]['ws_L0003'] = ((int)$valor['ws_L0003'] == 1 ? true : false);
        $retornoArray[$chave]['ws_L0004'] = ((int)$valor['ws_L0004'] == 1 ? true : false);
        $retornoArray[$chave]['ws_L0006'] = ((int)$valor['ws_L0006'] == 1 ? true : false);
        $retornoArray[$chave]['ws_L0007'] = ((int)$valor['ws_L0007'] == 1 ? true : false);
        $retornoArray[$chave]['ws_L0008'] = ((int)$valor['ws_L0008'] == 1 ? true : false);
        $retornoArray[$chave]['ws_L0011'] = ((int)$valor['ws_L0011'] == 1 ? true : false);
        $retornoArray[$chave]['ws_L0014'] = ((int)$valor['ws_L0014'] == 1 ? true : false);
        $retornoArray[$chave]['ws_L0015'] = ((int)$valor['ws_L0015'] == 1 ? true : false);
        $retornoArray[$chave]['ws_L0022'] = ((int)$valor['ws_L0022'] == 1 ? true : false);
        $retornoArray[$chave]['ws_L0024'] = ((int)$valor['ws_L0024'] == 1 ? true : false);
        $retornoArray[$chave]['ws_L0029'] = ((int)$valor['ws_L0029'] == 1 ? true : false);
        $retornoArray[$chave]['ws_L0030'] = ((int)$valor['ws_L0030'] == 1 ? true : false);
        $retornoArray[$chave]['ws_L0032'] = ((int)$valor['ws_L0032'] == 1 ? true : false);
        $retornoArray[$chave]['ws_L0033'] = ((int)$valor['ws_L0033'] == 1 ? true : false);
        $retornoArray[$chave]['ws_L0035'] = ((int)$valor['ws_L0035'] == 1 ? true : false);
        $retornoArray[$chave]['ws_L0038'] = ((int)$valor['ws_L0038'] == 1 ? true : false);
        $retornoArray[$chave]['ws_L0039'] = ((int)$valor['ws_L0039'] == 1 ? true : false);
        $retornoArray[$chave]['ws_L0041'] = ((int)$valor['ws_L0041'] == 1 ? true : false);
        $retornoArray[$chave]['ws_L0059'] = ((int)$valor['ws_L0059'] == 1 ? true : false);
        $retornoArray[$chave]['ws_L0064'] = ((int)$valor['ws_L0064'] == 1 ? true : false);
        $retornoArray[$chave]['ws_L0066'] = ((int)$valor['ws_L0066'] == 1 ? true : false);
        $retornoArray[$chave]['ws_L0067'] = ((int)$valor['ws_L0067'] == 1 ? true : false);
        $retornoArray[$chave]['ws_L0068'] = ((int)$valor['ws_L0068'] == 1 ? true : false);
        $retornoArray[$chave]['ws_R0001'] = ((int)$valor['ws_R0001'] == 1 ? true : false);
        $retornoArray[$chave]['ws_R0002'] = ((int)$valor['ws_R0002'] == 1 ? true : false);
        $retornoArray[$chave]['ws_R0014'] = ((int)$valor['ws_R0014'] == 1 ? true : false);
        $retornoArray[$chave]['ws_R0016'] = ((int)$valor['ws_R0016'] == 1 ? true : false);
        $retornoArray[$chave]['ws_R0019'] = ((int)$valor['ws_R0019'] == 1 ? true : false);
        $retornoArray[$chave]['exibir_total_registro_menu'] = ((int)$valor['exibir_total_registro_menu'] == 1 ? true : false);

        // Inteiro
        $retornoArray[$chave]['boleto_numero_sequencial_nosso_numero'] = (int)$valor['boleto_numero_sequencial_nosso_numero'];
        $retornoArray[$chave]['boleto_agencia'] = (int)$valor['boleto_agencia'];
        $retornoArray[$chave]['boleto_agencia_digito'] = (int)$valor['boleto_agencia_digito'];
        $retornoArray[$chave]['boleto_cc'] = (int)$valor['boleto_cc'];
        $retornoArray[$chave]['boleto_cc_digito'] = (int)$valor['boleto_cc_digito'];
        $retornoArray[$chave]['boleto_cedente'] = (int)$valor['boleto_cedente'];
        $retornoArray[$chave]['boleto_cedente_digito'] = (int)$valor['boleto_cedente_digito'];
        $retornoArray[$chave]['boleto_numero_sequencial_remessa'] = (int)$valor['boleto_numero_sequencial_remessa'];
        $retornoArray[$chave]['cartorio_agencia'] = (int)$valor['cartorio_agencia'];
        $retornoArray[$chave]['qtd_itens_listagem'] = (int)$valor['qtd_itens_listagem'];
        $retornoArray[$chave]['exibir_grafico_ultimos_meses'] = (int)$valor['exibir_grafico_ultimos_meses'];

        // Float
        $retornoArray[$chave]['boleto_desconto'] = exibir_Valor($valor['boleto_desconto']);
        $retornoArray[$chave]['boleto_abatimento'] = exibir_Valor($valor['boleto_abatimento']);
        $retornoArray[$chave]['boleto_mora'] = exibir_Valor($valor['boleto_mora']);
        $retornoArray[$chave]['boleto_multa'] = exibir_Valor($valor['boleto_multa']);
    }

    if (sizeof($retornoArray) > 0) {
        echo json_encode(array("success" => true, "nivel" => 1, "retorno" => $retornoArray, "total" => sizeof($retornoArray)));
    } else {
        echo json_encode(array("success" => false, "nivel" => 2, "mensagem" => "Nenhum registro encontrado!"));
    }

    exit();
}

if ($dataPHP->acao == "salvar") {

    $dados->setEmailEnvio($dataPHP->registroAtual->email_envio);
    $dados->setSenhaEnvio($dataPHP->registroAtual->senha_envio);
    $dados->setSmtpEnvio($dataPHP->registroAtual->smtp_envio);
    $dados->setWsUrl($dataPHP->registroAtual->ws_url);
    $dados->setWsUsuario($dataPHP->registroAtual->ws_usuario);
    $dados->setWsSenha($dataPHP->registroAtual->ws_senha);
    $dados->setQtdItensListagem($dataPHP->registroAtual->qtd_itens_listagem);
    $dados->setExibirTotalRegistroMenu($dataPHP->registroAtual->exibir_total_registro_menu);
    $dados->setExibirGraficoUltimosMeses($dataPHP->registroAtual->exibir_grafico_ultimos_meses);
    $dados->setWsL0001($dataPHP->registroAtual->ws_L0001);
    $dados->setWsL0002($dataPHP->registroAtual->ws_L0002);
    $dados->setWsL0003($dataPHP->registroAtual->ws_L0003);
    $dados->setWsL0004($dataPHP->registroAtual->ws_L0004);
    $dados->setWsL0006($dataPHP->registroAtual->ws_L0006);
    $dados->setWsL0007($dataPHP->registroAtual->ws_L0007);
    $dados->setWsL0008($dataPHP->registroAtual->ws_L0008);
    $dados->setWsL0011($dataPHP->registroAtual->ws_L0011);
    $dados->setWsL0014($dataPHP->registroAtual->ws_L0014);
    $dados->setWsL0015($dataPHP->registroAtual->ws_L0015);
    $dados->setWsL0022($dataPHP->registroAtual->ws_L0022);
    $dados->setWsL0024($dataPHP->registroAtual->ws_L0024);
    $dados->setWsL0029($dataPHP->registroAtual->ws_L0029);
    $dados->setWsL0030($dataPHP->registroAtual->ws_L0030);
    $dados->setWsL0032($dataPHP->registroAtual->ws_L0032);
    $dados->setWsL0033($dataPHP->registroAtual->ws_L0033);
    $dados->setWsL0035($dataPHP->registroAtual->ws_L0035);
    $dados->setWsL0038($dataPHP->registroAtual->ws_L0038);
    $dados->setWsL0039($dataPHP->registroAtual->ws_L0039);
    $dados->setWsL0041($dataPHP->registroAtual->ws_L0041);
    $dados->setWsL0059($dataPHP->registroAtual->ws_L0059);
    $dados->setWsL0064($dataPHP->registroAtual->ws_L0064);
    $dados->setWsL0066($dataPHP->registroAtual->ws_L0066);
    $dados->setWsL0067($dataPHP->registroAtual->ws_L0067);
    $dados->setWsL0068($dataPHP->registroAtual->ws_L0068);
    $dados->setWsR0001($dataPHP->registroAtual->ws_R0001);
    $dados->setWsR0002($dataPHP->registroAtual->ws_R0002);
    $dados->setWsR0014($dataPHP->registroAtual->ws_R0014);
    $dados->setWsR0016($dataPHP->registroAtual->ws_R0016);
    $dados->setWsR0019($dataPHP->registroAtual->ws_R0019);
    $dados->setBoletoMensagem1($dataPHP->registroAtual->boleto_mensagem1);
    $dados->setBoletoMensagem2($dataPHP->registroAtual->boleto_mensagem2);
    $dados->setBoletoMensagem3($dataPHP->registroAtual->boleto_mensagem3);
    $dados->setBoletoDesconto($dataPHP->registroAtual->boleto_desconto);
    $dados->setBoletoAbatimento($dataPHP->registroAtual->boleto_abatimento);
    $dados->setBoletoMora($dataPHP->registroAtual->boleto_mora);
    $dados->setBoletoMulta($dataPHP->registroAtual->boleto_multa);
    $dados->setBoletoIdBanco($dataPHP->registroAtual->boleto_id_banco);
    $dados->setBoletoAgencia($dataPHP->registroAtual->boleto_agencia);
    $dados->setBoletoAgenciaDigito($dataPHP->registroAtual->boleto_agencia_digito);
    $dados->setBoletoCc($dataPHP->registroAtual->boleto_cc);
    $dados->setBoletoCcDigito($dataPHP->registroAtual->boleto_cc_digito);
    $dados->setBoletoCedente($dataPHP->registroAtual->boleto_cedente);
    $dados->setBoletoCedenteDigito($dataPHP->registroAtual->boleto_cedente_digito);
    $dados->setBoletoIdCarteira($dataPHP->registroAtual->boleto_id_carteira);
    $dados->setEmpresaNome($dataPHP->registroAtual->empresa_nome);
    $dados->setEmpresaEndereco($dataPHP->registroAtual->empresa_endereco);
    $dados->setEmpresaCnpj($dataPHP->registroAtual->empresa_cnpj);
    $dados->setEmpresaIdMunicipio($dataPHP->registroAtual->empresa_id_municipio);
    $dados->setEmpresaIdEstado($dataPHP->registroAtual->empresa_id_estado);
    $dados->setCartorioAgencia($dataPHP->registroAtual->cartorio_agencia);
    $dados->setCartorioSiglaRemetente($dataPHP->registroAtual->cartorio_sigla_remetente);
    $dados->setCartorioSiglaDestinatario($dataPHP->registroAtual->cartorio_sigla_destinatario);
    $dados->setCartorioSiglaTransacao($dataPHP->registroAtual->cartorio_sigla_transacao);
    $dados->setCartorioCodPortador($dataPHP->registroAtual->cartorio_cod_portador);
    $dados->setCartorioPortador($dataPHP->registroAtual->cartorio_portador);
    $dados->setCartorioVersaoLayout($dataPHP->registroAtual->cartorio_versao_layout);
    $dados->setBoletoNumeroSequencialNossoNumero($dataPHP->registroAtual->boleto_numero_sequencial_nosso_numero);
    $dados->setBoletoNumeroSequencialRemessa($dataPHP->registroAtual->boleto_numero_sequencial_remessa);

    $dados->salvar();

    echo json_encode(array("success" => true));
    exit();
}

?>
