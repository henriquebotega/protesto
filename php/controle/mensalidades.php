<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_CRUD . "cTaxas.php";
include_once URL_PHP_CRUD . "cMensalidades.php";
include_once URL_PHP_CRUD . "cContratosItens.php";
include_once URL_PHP_CRUD . "cContratos.php";
$dados_cTaxas = new cTaxas();
$dados_cContratosItens = new cContratosItens();
$dados = new cMensalidades();
$dados_cContratos = new cContratos();

$dataPHP = json_decode(file_get_contents("php://input"));

// Valida sessão
if (isBlank(validarSessao())) {
    echo json_encode(array("success" => false, "nivel" => 3, "mensagem" => "A sessão expirou!"));
    exit();
}

if ($dataPHP->acao == "read") {
    $where = "";
    $order = "";
    $more = "";

    executaQuery_cMensalidades($where, $order, $more);
}

if ($dataPHP->acao == "getByID") {

    $where = " (m.id = '" . addslashes($dataPHP->id) . "') ";
    $order = "";
    $more = " LIMIT 1 ";

    executaQuery_cMensalidades($where, $order, $more);
}

if ($dataPHP->acao == "darBaixa") {
    $dados->setId(addslashes($dataPHP->registroAtual->id));
    $dados->setIdSituacao(addslashes($dataPHP->registroAtual->id_situacao));
    $dados->setValorPgto(addslashes($dataPHP->registroAtual->valor_pgto));
    $dados->setDataPgto(addslashes($dataPHP->registroAtual->data_pgto));

    $dados->darBaixa();
    echo json_encode(array("success" => true, "nivel" => 1));
    exit();
}

if ($dataPHP->acao == "gerar") {

    if (date("m") - 1 == 0) {
        $m = 12;
        $anoAnterior = date("Y") - 1;
    } else {
        $m = date("m") - 1;
        $anoAnterior = date("Y");
    }

    $mesAnterior = (($m < 10) ? "0" . $m : $m);
    $ultimo_dia = date("t", mktime(0, 0, 0, $mesAnterior, '01', $anoAnterior));

    $data_vigencia_inicial = $anoAnterior . '-' . $mesAnterior . '-01';
    $data_vigencia_final = $anoAnterior . '-' . $mesAnterior . '-' . $ultimo_dia;

    $mesSeguinte = (($mesAnterior + 1) == 13 ? '01' : ($mesAnterior + 1));
    $anoSeguinte = ($mesSeguinte == 1) ? $anoAnterior + 1 : $anoAnterior;
    $dataSeguinte = $anoSeguinte . '-' . (($mesSeguinte < 10) ? "0" . $mesSeguinte : $mesSeguinte);

    // Busca todos os contratos finalidados, que tenham sido finalizados antes da data de vigência
    $ret_cContratos = $dados_cContratos->select(" (c.id_situacao = 3) AND (c.data < '" . $data_vigencia_final . "') ");

    $total = 0;
    foreach ($ret_cContratos as $k_cContratos => $v_cContratos) {

        // Percorre as mensalidades deste contrato, e verifica se existe lançamento do mês anterior
        $ret_cMensalidades = $dados->select(" (m.id_contrato = '" . $v_cContratos['id'] . "') AND (m.data_vigencia_inicial >= '" . $data_vigencia_inicial . "' AND m.data_vigencia_final <= '" . $data_vigencia_final . "') ");

        if (sizeof($ret_cMensalidades) == 0) {

            $valorVcto = 0;

            // Deve haver obrigatoriamente uma mensalidade ligada ao contrato
            $ret_cContratosItens = $dados_cContratosItens->select(" (ci.id_contrato = '" . $v_cContratos['id'] . "' AND ci.id_servico = 1) ");
            
            if (sizeof($ret_cContratosItens) > 0) {

                $valorVcto += $ret_cContratosItens[0]['valor'];
                $total++;
                $numero = $dados->gerarCodigo();
                $arrayTaxas = [];

                // Percorre as taxas, que não estão vinculadas a nenhuma mensalidade, que estejam em aberto, que sejam do cliente atual, que estejam no período de vigência.
                $ret_cTaxas = $dados_cTaxas->select(" (t.id_mensalidade IS NULL) AND (t.id_situacao = 1) AND (t.id_cliente = '" . $v_cContratos['id_cliente'] . "') AND (t.data BETWEEN '" . $data_vigencia_inicial . "' AND '" . $data_vigencia_final . "') ");

                if (sizeof($ret_cTaxas) > 0) {

                    foreach ($ret_cTaxas as $k_cTaxas => $v_cTaxas) {
                        $id_servico = $v_cTaxas['id_servico'];
                        $valor = $v_cTaxas['valor'];

                        if (!array_key_exists($id_servico, $arrayTaxas)) {
                            $arrayTaxas[$id_servico] = $valor;
                        } else {
                            $arrayTaxas[$id_servico] += $valor;
                        }
                    }
                }

                if (sizeof($arrayTaxas) > 0) {
                    foreach ($arrayTaxas as $serv => $vlr) {
                        $valorVcto += $vlr;
                    }
                }

                $dia_vcto = $v_cContratos['dia_vcto'];

                $dados->setIdContrato($v_cContratos['id']);
                $dados->setIdSituacao(1);
                $dados->setValorVcto($valorVcto);
                $dados->setValorPgto(null);
                $dados->setDataVcto(formatar_Data($dataSeguinte . '-' . (($dia_vcto < 10) ? "0" . $dia_vcto : $dia_vcto)));
                $dados->setDataPgto(null);
                $dados->setNumero($numero);
                $dados->setDataVigenciaInicial(formatar_Data($data_vigencia_inicial));
                $dados->setDataVigenciaFinal(formatar_Data($data_vigencia_final));
                $dados->salvar();

                // Pega o ID gerado
                $ret_cMensal = $dados->ultimo_registro();
                $id_cMensal = $ret_cMensal[0]['id'];

                if (sizeof($ret_cTaxas) > 0) {
                    foreach ($ret_cTaxas as $k_cTaxas => $v_cTaxas) {
                        $dados_cTaxas->setIdMensalidade($id_cMensal);
                        $dados_cTaxas->setIdSituacao(2);
                        $dados_cTaxas->setId($v_cTaxas['id']);
                        $dados_cTaxas->updateMensalidade();
                    }
                }
            }
        }
    }

    echo json_encode(array("success" => true, "nivel" => 1, "total" => $total));
    exit();
}

if ($dataPHP->acao == "filtrar") {

    if (isset($dataPHP->palavraChave) && isNotBlank($dataPHP->palavraChave)) {
        $where = " (m.numero LIKE '%" . addslashes($dataPHP->palavraChave) . "%'
                    OR (pj.razao_social LIKE '%" . addslashes($dataPHP->palavraChave) . "%' 
                    OR pf.nome LIKE '%" . addslashes($dataPHP->palavraChave) . "%' 
                    OR pj.nome_fantasia LIKE '%" . addslashes($dataPHP->palavraChave) . "%')) ";
    }

    if (isset($dataPHP->filtroSituacao) && sizeof($dataPHP->filtroSituacao) > 0) {

        $where = isNotBlank(@$where) ? $where . " AND " : "";
        $where = $where . " (m.id_situacao IN(";

        foreach ($dataPHP->filtroSituacao as $k => $v) {
            $where .= addslashes($v->id) . ",";
        }

        $where = substr($where, 0, -1);
        $where .= ")) ";
    }

    $order = "";
    $more = "";

    executaQuery_cMensalidades($where, $order, $more);
}


function executaQuery_cMensalidades($where, $order, $more)
{
    global $dados;
    $retorno = $dados->select($where, $order, $more);

    $retornoArray = array();
    foreach ($retorno as $chave => $valor) {
        $retornoArray[$chave] = $valor;

        $retornoArray[$chave]['grid_cliente'] = $valor['grid_nome'];
        $retornoArray[$chave]['grid_id_contrato'] = $valor['id_contrato'];
        $retornoArray[$chave]['grid_situacao'] = $valor['situacao'];
        $retornoArray[$chave]['grid_valor_vcto'] = exibir_grid_Valor($valor['valor_vcto']);
        $retornoArray[$chave]['grid_data_vcto'] = formatar_DataHora($valor['data_vcto']);
        $retornoArray[$chave]['grid_valor_pgto'] = exibir_grid_Valor($valor['valor_pgto']);
        $retornoArray[$chave]['grid_data_pgto'] = formatar_DataHora($valor['data_pgto']);

        $retornoArray[$chave]['valor_vcto'] = exibir_Valor($valor['valor_vcto']);
        $retornoArray[$chave]['valor_pgto'] = exibir_Valor($valor['valor_pgto']);
    }

    if (sizeof($retornoArray) > 0) {
        echo json_encode(array("success" => true, "nivel" => 1, "retorno" => $retornoArray, "total" => sizeof($retornoArray)));
    } else {
        echo json_encode(array("success" => false, "nivel" => 2, "mensagem" => "Nenhum registro encontrado!"));
    }

    exit();
}

?>