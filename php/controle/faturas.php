<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_CRUD . "cMensalidades.php";
include_once URL_PHP_CRUD . "cContratos.php";
$dados = new cMensalidades();
$dados_cContratos = new cContratos();

$dataPHP = json_decode(file_get_contents("php://input"));

// Valida sessão
if (isBlank(validarSessao())) {
    echo json_encode(array("success" => false, "nivel" => 3, "mensagem" => "A sessão expirou!"));
    exit();
}

if ($dataPHP->acao == "read") {
    $where = "";
    $order = "";
    $more = "";

    executaQuery_cMensalidades($where, $order, $more);
}

if ($dataPHP->acao == "getAllEmAberto") {
    $where = " (m.id_situacao = 1) ";
    $order = "";
    $more = "";

    executaQuery_cMensalidades($where, $order, $more);
}

if ($dataPHP->acao == "getAllEmAtraso") {
    $where = " (m.id_situacao = 1) AND (m.data_vcto < '" . date('Y-m-d') . "') ";
    $order = "";
    $more = "";

    executaQuery_cMensalidades($where, $order, $more);
}

function executaQuery_cMensalidades($where, $order, $more)
{
    global $dados;

    $sessao = validarSessao();
    if (isset($sessao['usuario'])) {
        $where .= (empty($where) ? "" : " AND ");
        $where .= " (c.id_cliente = '" . $sessao['usuario'][0]['id_cliente'] . "') ";
    }

    $retorno = $dados->select($where, $order, $more);

    $retornoArray = array();
    foreach ($retorno as $chave => $valor) {
        $retornoArray[$chave] = $valor;

        $retornoArray[$chave]['grid_id_contrato'] = $valor['id_contrato'];
        $retornoArray[$chave]['grid_situacao'] = $valor['situacao'];
        $retornoArray[$chave]['grid_valor_vcto'] = exibir_grid_Valor($valor['valor_vcto']);
        $retornoArray[$chave]['grid_data_vcto'] = formatar_DataHora($valor['data_vcto']);
        $retornoArray[$chave]['grid_valor_pgto'] = exibir_grid_Valor($valor['valor_pgto']);
        $retornoArray[$chave]['grid_data_pgto'] = formatar_DataHora($valor['data_pgto']);

        $retornoArray[$chave]['valor_vcto'] = exibir_Valor($valor['valor_vcto']);
        $retornoArray[$chave]['valor_pgto'] = exibir_Valor($valor['valor_pgto']);
    }

    if (sizeof($retornoArray) > 0) {
        echo json_encode(array("success" => true, "nivel" => 1, "retorno" => $retornoArray, "total" => sizeof($retornoArray)));
    } else {
        echo json_encode(array("success" => false, "nivel" => 2, "mensagem" => "Nenhum registro encontrado!"));
    }

    exit();
}

?>