<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_CRUD . "cClientes.php";
include_once URL_PHP_CRUD . "cDevedores.php";
include_once URL_PHP_CRUD . "cDevedoresPF.php";
include_once URL_PHP_CRUD . "cDevedoresPJ.php";
include_once URL_PHP_CRUD . "cDividas.php";
include_once URL_PHP_CRUD . "cDevedoresContatos.php";
include_once URL_PHP_CRUD . "cDevedoresEnderecos.php";
$dados = new cDevedores();
$dados_cDevedoresPF = new cDevedoresPF();
$dados_cDevedoresPJ = new cDevedoresPJ();
$dados_cClientes = new cClientes();
$dados_cDividas = new cDividas();
$dados_cDevedoresContatos = new cDevedoresContatos();
$dados_cDevedoresEnderecos = new cDevedoresEnderecos();

$dataPHP = json_decode(file_get_contents("php://input"));

// Valida sessão
if (isBlank(validarSessao())) {
    echo json_encode(array("success" => false, "nivel" => 3, "mensagem" => "A sessão expirou!"));
    exit();
}

if ($dataPHP->acao == "read") {
    $where = "";
    $order = "";
    $more = "";

    executaQuery_cDevedores($where, $order, $more);
}

if ($dataPHP->acao == "salvar") {

    $sessao = validarSessao();
    if (isset($sessao['usuario'])) {
        $id_cliente = $sessao['usuario'][0]['id_cliente'];
    } else {
        $id_cliente = addslashes($dataPHP->registroAtual->id_cliente);
    }

    $dados->setObservacao(addslashes($dataPHP->registroAtual->observacao));
    $dados->setIdTipo(addslashes($dataPHP->registroAtual->id_tipo));
    $dados->setIdCliente($id_cliente);

    if (isset($dataPHP->registroAtual->id)) {
        $dados->setId(addslashes($dataPHP->registroAtual->id));
    }

    $dados->salvar();

    if (isset($dataPHP->registroAtual->id)) {
        $idDevedor = $dataPHP->registroAtual->id;
    } else {
        $retorno = $dados->ultimo_registro();
        $idDevedor = $retorno[0]["id"];
    }

    $ret_cDevedoresContatos = $dados_cDevedoresContatos->select(" (id_devedor = '" . $idDevedor . "') ", "", "");
    if (sizeof($ret_cDevedoresContatos) > 0) {
        $dados_cDevedoresContatos->setId($ret_cDevedoresContatos[0]['id']);
    }

    $dados_cDevedoresContatos->setTel1(addslashes($dataPHP->registroAtual->tel1));
    $dados_cDevedoresContatos->setContato1(addslashes($dataPHP->registroAtual->contato1));
    $dados_cDevedoresContatos->setEmail1(addslashes($dataPHP->registroAtual->email1));
    $dados_cDevedoresContatos->setTel2(addslashes($dataPHP->registroAtual->tel2));
    $dados_cDevedoresContatos->setContato2(addslashes($dataPHP->registroAtual->contato2));
    $dados_cDevedoresContatos->setEmail2(addslashes($dataPHP->registroAtual->email2));
    $dados_cDevedoresContatos->setTel3(addslashes($dataPHP->registroAtual->tel3));
    $dados_cDevedoresContatos->setContato3(addslashes($dataPHP->registroAtual->contato3));
    $dados_cDevedoresContatos->setEmail3(addslashes($dataPHP->registroAtual->email3));
    $dados_cDevedoresContatos->setIdDevedor($idDevedor);

    $ret_cDevedoresContatos = $dados_cDevedoresEnderecos->select(" (id_devedor = '" . $idDevedor . "') ", "", "");
    if (sizeof($ret_cDevedoresContatos) > 0) {
        $dados_cDevedoresEnderecos->setId($ret_cDevedoresContatos[0]['id']);
    }

    $dados_cDevedoresEnderecos->setCep1(addslashes($dataPHP->registroAtual->cep1));
    $dados_cDevedoresEnderecos->setLogradouro1(addslashes($dataPHP->registroAtual->logradouro1));
    $dados_cDevedoresEnderecos->setNumero1(addslashes($dataPHP->registroAtual->numero1));
    $dados_cDevedoresEnderecos->setComplemento1(addslashes($dataPHP->registroAtual->complemento1));
    $dados_cDevedoresEnderecos->setBairro1(addslashes($dataPHP->registroAtual->bairro1));
    $dados_cDevedoresEnderecos->setIdCidade1(addslashes($dataPHP->registroAtual->id_cidade1));
    $dados_cDevedoresEnderecos->setCep2(addslashes($dataPHP->registroAtual->cep2));
    $dados_cDevedoresEnderecos->setLogradouro2(addslashes($dataPHP->registroAtual->logradouro2));
    $dados_cDevedoresEnderecos->setNumero2(addslashes($dataPHP->registroAtual->numero2));
    $dados_cDevedoresEnderecos->setComplemento2(addslashes($dataPHP->registroAtual->complemento2));
    $dados_cDevedoresEnderecos->setBairro2(addslashes($dataPHP->registroAtual->bairro2));
    $dados_cDevedoresEnderecos->setIdCidade2(addslashes($dataPHP->registroAtual->id_cidade2));
    $dados_cDevedoresEnderecos->setCep3(addslashes($dataPHP->registroAtual->cep3));
    $dados_cDevedoresEnderecos->setLogradouro3(addslashes($dataPHP->registroAtual->logradouro3));
    $dados_cDevedoresEnderecos->setNumero3(addslashes($dataPHP->registroAtual->numero3));
    $dados_cDevedoresEnderecos->setComplemento3(addslashes($dataPHP->registroAtual->complemento3));
    $dados_cDevedoresEnderecos->setBairro3(addslashes($dataPHP->registroAtual->bairro3));
    $dados_cDevedoresEnderecos->setIdCidade3(addslashes($dataPHP->registroAtual->id_cidade3));
    $dados_cDevedoresEnderecos->setIdDevedor($idDevedor);
    $dados_cDevedoresEnderecos->salvar();

    if ($dataPHP->registroAtual->id_tipo == "1") {

        $ret_cClientesPF = $dados_cDevedoresPF->select(" (id_devedor = '" . $idDevedor . "') ", "", "");
        if (sizeof($ret_cClientesPF) > 0) {
            $dados_cDevedoresPF->setId($ret_cClientesPF[0]['id']);
        }

        $dados_cDevedoresPF->setNome(addslashes($dataPHP->registroAtual->nome));
        $dados_cDevedoresPF->setCpf(addslashes($dataPHP->registroAtual->cpf));
        $dados_cDevedoresPF->setRg(addslashes($dataPHP->registroAtual->rg));
        $dados_cDevedoresPF->setDataNascimento(addslashes($dataPHP->registroAtual->data_nascimento));
        $dados_cDevedoresPF->setIdDevedor($idDevedor);
        $dados_cDevedoresPF->salvar();

        // Deleta PJ
        $dados_cDevedoresPJ->setIdDevedor($idDevedor);
        $dados_cDevedoresPJ->delete();
    } else {

        $ret_cClientesPJ = $dados_cDevedoresPJ->select(" (id_devedor = '" . $idDevedor . "') ", "", "");
        if (sizeof($ret_cClientesPJ) > 0) {
            $dados_cDevedoresPJ->setId($ret_cClientesPJ[0]['id']);
        }

        $dados_cDevedoresPJ->setRazaoSocial(addslashes($dataPHP->registroAtual->razao_social));
        $dados_cDevedoresPJ->setNomeFantasia(addslashes($dataPHP->registroAtual->nome_fantasia));
        $dados_cDevedoresPJ->setCnpj(addslashes($dataPHP->registroAtual->cnpj));
        $dados_cDevedoresPJ->setIe(addslashes($dataPHP->registroAtual->ie));
        $dados_cDevedoresPJ->setIdDevedor($idDevedor);
        $dados_cDevedoresPJ->salvar();

        // Deleta PF
        $dados_cDevedoresPF->setIdDevedor($idDevedor);
        $dados_cDevedoresPF->delete();
    }

    echo json_encode(array("success" => true, "nivel" => 1));
}

if ($dataPHP->acao == "excluir") {

    $idDevedor = addslashes($dataPHP->registroAtual->id);

    // Excluir Devedor Contatos
    $dados_cDevedoresContatos->setIdDevedor($idDevedor);
    $dados_cDevedoresContatos->delete();

    // Excluir Devedor PF
    $dados_cDevedoresPF->setIdDevedor($idDevedor);
    $dados_cDevedoresPF->delete();

    // Excluir Devedor PJ
    $dados_cDevedoresPJ->setIdDevedor($idDevedor);
    $dados_cDevedoresPJ->delete();

    $dados->setId(addslashes($dataPHP->registroAtual->id));
    $dados->delete();

    echo json_encode(array("success" => true, "nivel" => 1));
    exit();
}

if ($dataPHP->acao == "getAllByCliente") {

    $where = " (d.id_cliente = '" . addslashes($dataPHP->id_cliente) . "') ";
    $order = "";
    $more = "";

    executaQuery_cDevedores($where, $order, $more);
}

if ($dataPHP->acao == "getDividasByDevedor") {

    $id_devedor = $dataPHP->registroAtual->id;
    $ret_cDividas = $dados_cDividas->select(" (d.id_devedor = '" . $id_devedor . "') ");

    if (sizeof($ret_cDividas) > 0) {
        echo json_encode(array("success" => false, "nivel" => 2, "mensagem" => "Este devedor não pode ser removido, pois existem dividas vinculadas!"));
        exit();
    }

    echo json_encode(array("success" => true, "nivel" => 1));
    exit();
}

if ($dataPHP->acao == "getByID") {

    $where = " (d.id = '" . addslashes($dataPHP->id) . "') ";
    $order = "";
    $more = " LIMIT 1 ";

    executaQuery_cDevedores($where, $order, $more);
}

if ($dataPHP->acao == "filtrar") {

    $where = "";

    if (isset($dataPHP->palavraChave) && isNotBlank($dataPHP->palavraChave)) {
        $where = " ((dj.razao_social LIKE '%" . addslashes($dataPHP->palavraChave) . "%' OR df.nome LIKE '%" . addslashes($dataPHP->palavraChave) . "%') ";
        $where .= " OR (dj.nome_fantasia LIKE '%" . addslashes($dataPHP->palavraChave) . "%') OR dc.email1 LIKE '%" . addslashes($dataPHP->palavraChave) . "%' ";
        $where .= " OR (dj.cnpj LIKE '%" . addslashes($dataPHP->palavraChave) . "%' OR df.cpf LIKE '%" . addslashes($dataPHP->palavraChave) . "%')) ";
    }

    $order = "";
    $more = "";

    executaQuery_cDevedores($where, $order, $more);
}

function executaQuery_cDevedores($where, $order, $more)
{
    global $dados, $dados_cClientes;

    $sessao = validarSessao();
    if (isset($sessao['usuario'])) {
        $where .= (empty($where) ? "" : " AND ");
        $where .= " (d.id_cliente = '" . $sessao['usuario'][0]['id_cliente'] . "') ";
    }

    $retorno = $dados->select($where, $order, $more);

    $retornoArray = array();
    foreach ($retorno as $chave => $valor) {
        $retornoArray[$chave] = $valor;

        $ret_cClientes = $dados_cClientes->select(" (c.id = '" . $valor['id_cliente'] . "') ", "", "");
        $retornoArray[$chave]['grid_cliente'] = $ret_cClientes[0]['grid_nome'];

        $retornoArray[$chave]['grid_nome'] = $valor['grid_nome'];
        $retornoArray[$chave]['grid_cpf_cnpj'] = $valor['grid_cpf_cnpj'];
        $retornoArray[$chave]['grid_rg_ie'] = $valor['grid_rg_ie'];
        $retornoArray[$chave]['grid_email'] = $valor['email1'];
        $retornoArray[$chave]['grid_tel1'] = $valor['tel1'];
    }

    if (sizeof($retornoArray) > 0) {
        echo json_encode(array("success" => true, "nivel" => 1, "retorno" => $retornoArray, "total" => sizeof($retornoArray)));
    } else {
        echo json_encode(array("success" => false, "nivel" => 2, "mensagem" => "Nenhum registro encontrado!"));
    }

    exit();
}

?>