<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_CRUD . "cClientes.php";
include_once URL_PHP_CRUD . "cTaxas.php";
$dados = new cTaxas();
$dados_cClientes = new cClientes();

$dataPHP = json_decode(file_get_contents("php://input"));

// Valida sessão
if (isBlank(validarSessao())) {
    echo json_encode(array("success" => false, "nivel" => 3, "mensagem" => "A sessão expirou!"));
    exit();
}

if ($dataPHP->acao == "read") {
    $where = "";
    $order = "";
    $more = "";

    executaQuery_cTaxas($where, $order, $more);
}

if ($dataPHP->acao == "salvar") {
    $dados->setIdMensalidade(null);
    $dados->setIdServico(addslashes($dataPHP->registroAtual->id_servico));
    $dados->setHistorico(addslashes($dataPHP->registroAtual->historico));
    $dados->setValor(addslashes($dataPHP->registroAtual->valor));
    $dados->setData(addslashes($dataPHP->registroAtual->data));
    $dados->setIdCliente($dataPHP->registroAtual->id_cliente);
    $dados->setIdSituacao($dataPHP->registroAtual->id_situacao);

    if (isset($dataPHP->registroAtual->id)) {
        $dados->setId(addslashes($dataPHP->registroAtual->id));
    }

    $dados->salvar();
    echo json_encode(array("success" => true, "nivel" => 1));
}

if ($dataPHP->acao == "getByID") {

    $where = " (t.id = '" . addslashes($dataPHP->id) . "') ";
    $order = "";
    $more = " LIMIT 1 ";

    executaQuery_cTaxas($where, $order, $more);
}

if ($dataPHP->acao == "filtrar") {

    $where = " (t.id_mensalidade = '" . addslashes($dataPHP->id_mensalidade) . "') ";

    if (isset($dataPHP->palavraChave) && isNotBlank($dataPHP->palavraChave)) {
        $where .= " AND (t.historico LIKE '%" . addslashes($dataPHP->palavraChave) . "%' OR s.titulo LIKE '%" . addslashes($dataPHP->palavraChave) . "%') ";
    }

    $order = "";
    $more = "";

    executaQuery_cTaxas($where, $order, $more);
}

if ($dataPHP->acao == "excluir") {

    foreach ($dataPHP->colRegistroAtual as $k => $v) {
        $dados->setId(addslashes($v->id));
        $dados->delete();
    }

    echo json_encode(array("success" => true, "nivel" => 1));
    exit();
}

function executaQuery_cTaxas($where, $order, $more)
{
    global $dados, $dados_cClientes;

    $retorno = $dados->select($where, $order, $more);

    $retornoArray = array();
    foreach ($retorno as $chave => $valor) {
        $retornoArray[$chave] = $valor;

        $ret_cClientes = $dados_cClientes->select(" (c.id = '" . $valor['id_cliente'] . "') ", "", "");
        $retornoArray[$chave]['grid_cliente'] = $ret_cClientes[0]['grid_nome'];

        $retornoArray[$chave]['grid_id_mensalidade'] = $valor['id_mensalidade'];
        $retornoArray[$chave]['grid_servico'] = $valor['servico'];
        $retornoArray[$chave]['grid_historico'] = $valor['historico'];
        $retornoArray[$chave]['grid_data'] = $valor['data'];
        $retornoArray[$chave]['grid_situacao'] = $valor['situacao'];
        $retornoArray[$chave]['grid_valor'] = exibir_grid_Valor($valor['valor']);

        $retornoArray[$chave]['valor'] = exibir_Valor($valor['valor']);
    }

    if (sizeof($retornoArray) > 0) {
        echo json_encode(array("success" => true, "nivel" => 1, "retorno" => $retornoArray, "total" => sizeof($retornoArray)));
    } else {
        echo json_encode(array("success" => false, "nivel" => 2, "mensagem" => "Nenhum registro encontrado!"));
    }

    exit();
}

?>