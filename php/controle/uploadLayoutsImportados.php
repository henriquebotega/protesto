<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_CRUD . "cLayoutsImportados.php";
include_once URL_PHP_CRUD . "cDividas.php";
include_once URL_PHP_CRUD . "cDevedores.php";
include_once URL_PHP_CRUD . "cDevedoresContatos.php";
include_once URL_PHP_CRUD . "cCidades.php";
$dados = new cLayoutsImportados();
$dados_cDividas = new cDividas();
$dados_cDevedores = new cDevedores();
$dados_cDevedoresContatos = new cDevedoresContatos();
$dados_cCidades = new cCidades();

$dataPHP = json_decode(file_get_contents("php://input"));

// Valida sessão
if (isBlank(validarSessao())) {
    echo json_encode(array("success" => false, "nivel" => 3, "mensagem" => "A sessão expirou!"));
    exit();
}

if (isset($_POST['extra']) && isset($_FILES['file']) && $_FILES['file']['error'] == 0 && $_FILES['file']['size'] > 0) {

    $extra = json_decode($_POST['extra']);

    $sessao = validarSessao();
    if (isset($sessao['usuario'])) {
        $id_cliente = $sessao['usuario'][0]['id_cliente'];
    } else {
        $id_cliente = $extra->id_cliente;
    }

    $extensao = RetornaExtensao($_FILES['file']['name']);
    $tipoLayout = 'outros';

    if ($extra->id_layout == 1) {
        $tipoLayout = 'devedores';
    }

    if ($extra->id_layout == 2) {
        $tipoLayout = 'dividas';
    }

    $nomeArquivo = $tipoLayout . "-" . date('Y-m-d_H-i-s', time()) . "." . $extensao;

    if (move_uploaded_file($_FILES['file']['tmp_name'], "../uploads/layouts_importados/" . $nomeArquivo)) {

        // Atualiza tabelas do banco
        $dados->setId($extra->id_layout_importado);
        $dados->setArquivo($nomeArquivo);
        $dados->updateArquivo();

        if ($extra->id_layout == 1) {
            // Inicia a importação de Devedores
            importarDevedores($extra->id_layout_importado, $nomeArquivo, $id_cliente);
        }

        if ($extra->id_layout == 2) {
            // Inicia a importação de Dividas
            importarDividas($extra->id_layout_importado, $nomeArquivo);
        }

        if ($extra->id_layout == 3) {
            // Inicia a importação de Dividas
            importarDividasComDevedor($extra->id_layout_importado, $nomeArquivo, $id_cliente);
        }
    }
}

function importarDevedores($id_layout_importado, $nomeArquivo, $id_cliente)
{
    global $dados, $dados_cCidades, $dados_cDevedores, $dados_cDevedoresContatos;

    $abraArq = fopen("../uploads/layouts_importados/" . $nomeArquivo, "r");
    $totalRegistros = 0;
    $totalRegistrosSucesso = 0;
    $erros = "";

    if ($abraArq) {
        $linha = 0;

        while ($valores = fgetcsv($abraArq, 2048, ";")) {

            if ($linha > 0 && !empty($valores[0])) {

                // 0 = cpf
                // 1 = nome
                // 2 = telefone
                // 3 = cep
                // 4 = logradouro
                // 5 = numero
                // 6 = bairro
                // 7 = cidade
                // 8 = email
                // 9 = rg
                // 10 = data_nascimento

                $erros .= (empty($erros) ? "" : chr(13) . chr(10));

                $dados_cDevedores = new cDevedores();
                $ret_cDevedores = $dados_cDevedores->select(" (d.cpf = '" . $valores[0] . "') ");

                if (sizeof($ret_cDevedores) == 0) {
                    $idCidade = 0;

                    $ret_cCidades = $dados_cCidades->select(" (LOWER(titulo) = '" . strtolower($valores[7]) . "') ", "", "");
                    if (sizeof($ret_cCidades) > 0) {
                        $idCidade = $ret_cCidades[0]['id'];
                    }

                    $dados_cDevedores = new cDevedores();
                    $dados_cDevedores->setIdCliente($id_cliente);
                    $dados_cDevedores->setIdCidade($idCidade);
                    $dados_cDevedores->setCep($valores[3]);
                    $dados_cDevedores->setLogradouro(addslashes(utf8_encode($valores[4])));
                    $dados_cDevedores->setNumero(addslashes(utf8_encode($valores[5])));
                    $dados_cDevedores->setBairro(addslashes(utf8_encode($valores[6])));
                    $dados_cDevedores->setNome(addslashes(utf8_encode($valores[1])));
                    $dados_cDevedores->setCpf($valores[0]);
                    $dados_cDevedores->setRg($valores[9]);
                    $dados_cDevedores->setDataNascimento(str_replace("-", "/", $valores[10]));
                    $dados_cDevedores->salvar();

                    $retorno = $dados_cDevedores->ultimo_registro();
                    $idDevedor = $retorno[0]["id"];

                    $dados_cDevedoresContatos = new cDevedoresContatos();
                    $dados_cDevedoresContatos->setIdDevedor($idDevedor);
                    $dados_cDevedoresContatos->setEmail1(addslashes(utf8_encode($valores[8])));
                    $dados_cDevedoresContatos->setTel1($valores[2]);
                    $dados_cDevedoresContatos->setContato1(addslashes(utf8_encode($valores[1])));
                    $dados_cDevedoresContatos->setEmail2("");
                    $dados_cDevedoresContatos->setTel2("");
                    $dados_cDevedoresContatos->setContato2("");
                    $dados_cDevedoresContatos->setEmail3("");
                    $dados_cDevedoresContatos->setTel3("");
                    $dados_cDevedoresContatos->setContato3("");
                    $dados_cDevedoresContatos->salvar();

                    $totalRegistrosSucesso++;
                    $erros .= "#" . $linha . " - Importado com sucesso";
                } else {
                    $totalRegistrosSucesso++;
                    $erros .= "#" . $linha . " - OK";
                }

                $totalRegistros++;
            }

            $linha++;
        }

        fclose($abraArq);
    }

    // Atualiza tabelas do banco
    $dados->setId($id_layout_importado);
    $dados->setTotalRegistros($totalRegistros);
    $dados->setTotalRegistrosSucesso($totalRegistrosSucesso);
    $dados->setErros($erros);
    $dados->updateImportacao();
}

function importarDividas($id_layout_importado, $nomeArquivo)
{
    global $dados, $dados_cDividas, $dados_cDevedores;

    $abraArq = fopen("../uploads/layouts_importados/" . $nomeArquivo, "r");
    $totalRegistros = 0;
    $totalRegistrosSucesso = 0;
    $erros = "";

    if ($abraArq) {
        $linha = 0;

        while ($valores = fgetcsv($abraArq, 2048, ";")) {

            if ($linha > 0 && !empty($valores[0])) {

                // 0 = numero fatura
                // 1 = numero documento
                // 2 = cpf devedor
                // 3 = data vencimento
                // 4 = data emissao
                // 5 = valor vcto
                // 6 = numero parcela
                // 7 = observacoes

                $erros .= (empty($erros) ? "" : chr(13) . chr(10));

                $dados_cDividas = new cDividas();
                $ret_cDividas = $dados_cDividas->select(" (d.numero_fatura = '" . $valores[0] . "') ");
                if (sizeof($ret_cDividas) == 0) {

                    $ret_cDevedores = $dados_cDevedores->select(" (d.cpf = '" . $valores[2] . "') ");
                    if (sizeof($ret_cDevedores) > 0) {

                        $dados_cDividas = new cDividas();
                        $dados_cDividas->setIdDevedor($ret_cDevedores[0]['id']);
                        $dados_cDividas->setValorVcto($valores[5]);
                        $dados_cDividas->setDataVcto(str_replace("-", "/", $valores[3]));
                        $dados_cDividas->setIdSituacao(1);
                        $dados_cDividas->setDataEmissao($valores[4]);
                        $dados_cDividas->setNumeroDocumento($valores[1]);
                        $dados_cDividas->setNumeroFatura($valores[0]);
                        $dados_cDividas->setObservacoes($valores[7]);
                        $dados_cDividas->setIdPraca(1);
                        $dados_cDividas->setNumeroParcela($valores[6]);
                        $dados_cDividas->salvar();

                        $totalRegistrosSucesso++;
                        $erros .= "#" . $linha . " - Importado com sucesso";
                    } else {
                        $erros .= "#" . $linha . " - Devedor não está cadastrado (cpf: " . $valores[2] . ")";
                    }
                } else {
                    $totalRegistrosSucesso++;
                    $erros .= "#" . $linha . " - OK";
                }

                $totalRegistros++;
            }

            $linha++;
        }

        fclose($abraArq);
    }

    // Atualiza tabelas do banco
    $dados->setId($id_layout_importado);
    $dados->setTotalRegistros($totalRegistros);
    $dados->setTotalRegistrosSucesso($totalRegistrosSucesso);
    $dados->setErros($erros);
    $dados->updateImportacao();
}

function importarDividasComDevedor($id_layout_importado, $nomeArquivo, $id_cliente)
{
    global $dados, $dados_cDividas, $dados_cDevedores, $dados_cCidades;

    $abraArq = fopen("../uploads/layouts_importados/" . $nomeArquivo, "r");
    $totalRegistros = 0;
    $totalRegistrosSucesso = 0;
    $erros = "";

    if ($abraArq) {
        $linha = 0;

        while ($valores = fgetcsv($abraArq, 2048, ";")) {

            if ($linha > 0 && !empty($valores[0])) {

                // 0 = numero fatura
                // 1 = numero documento
                // 2 = data vencimento
                // 3 = data emissao
                // 4 = valor vcto
                // 5 = numero parcela
                // 6 = observacoes
                // 7 = cpf
                // 8 = nome
                // 9 = telefone
                // 10 = cep
                // 11 = logradouro
                // 12 = numero
                // 13 = bairro
                // 14 = cidade
                // 15 = email
                // 16 = rg
                // 17 = data_nascimento

                $idDevedores = 0;
                $erros .= (empty($erros) ? "" : chr(13) . chr(10));

                $dados_cDevedores = new cDevedores();
                $ret_cDevedores = $dados_cDevedores->select(" (d.cpf = '" . $valores[7] . "') ");

                if (sizeof($ret_cDevedores) == 0) {
                    $idCidade = 0;

                    $ret_cCidades = $dados_cCidades->select(" (LOWER(titulo) = '" . strtolower($valores[14]) . "') ", "", "");
                    if (sizeof($ret_cCidades) > 0) {
                        $idCidade = $ret_cCidades[0]['id'];
                    }

                    $dados_cDevedores = new cDevedores();
                    $dados_cDevedores->setIdCliente($id_cliente);
                    $dados_cDevedores->setIdCidade($idCidade);
                    $dados_cDevedores->setCep($valores[10]);
                    $dados_cDevedores->setLogradouro(addslashes(utf8_encode($valores[11])));
                    $dados_cDevedores->setNumero(addslashes(utf8_encode($valores[12])));
                    $dados_cDevedores->setBairro(addslashes(utf8_encode($valores[13])));
                    $dados_cDevedores->setNome(addslashes(utf8_encode($valores[8])));
                    $dados_cDevedores->setCpf($valores[7]);
                    $dados_cDevedores->setRg($valores[16]);
                    $dados_cDevedores->setDataNascimento(str_replace("-", "/", $valores[17]));
                    $dados_cDevedores->salvar();

                    $retorno = $dados_cDevedores->ultimo_registro();
                    $idDevedor = $retorno[0]["id"];

                    $dados_cDevedoresContatos = new cDevedoresContatos();
                    $dados_cDevedoresContatos->setIdDevedor($idDevedor);
                    $dados_cDevedoresContatos->setEmail1(addslashes(utf8_encode($valores[15])));
                    $dados_cDevedoresContatos->setTel1($valores[9]);
                    $dados_cDevedoresContatos->setContato1(addslashes(utf8_encode($valores[8])));
                    $dados_cDevedoresContatos->setEmail2("");
                    $dados_cDevedoresContatos->setTel2("");
                    $dados_cDevedoresContatos->setContato2("");
                    $dados_cDevedoresContatos->setEmail3("");
                    $dados_cDevedoresContatos->setTel3("");
                    $dados_cDevedoresContatos->setContato3("");
                    $dados_cDevedoresContatos->salvar();
                } else {
                    $idDevedor = $ret_cDevedores[0]["id"];
                }

                $dados_cDividas = new cDividas();
                $ret_cDividas = $dados_cDividas->select(" (d.numero_fatura = '" . $valores[0] . "') ");

                if (sizeof($ret_cDividas) == 0) {
                    $dados_cDividas = new cDividas();
                    $dados_cDividas->setIdDevedor($idDevedores);
                    $dados_cDividas->setValorVcto($valores[4]);
                    $dados_cDividas->setDataVcto(str_replace("-", "/", $valores[2]));
                    $dados_cDividas->setIdSituacao(1);
                    $dados_cDividas->setDataEmissao($valores[3]);
                    $dados_cDividas->setNumeroDocumento($valores[1]);
                    $dados_cDividas->setNumeroFatura($valores[0]);
                    $dados_cDividas->setObservacoes($valores[6]);
                    $dados_cDividas->setIdPraca(1);
                    $dados_cDividas->setNumeroParcela($valores[5]);
                    $dados_cDividas->salvar();

                    $totalRegistrosSucesso++;
                    $erros .= "#" . $linha . " - Importado com sucesso";
                } else {
                    $totalRegistrosSucesso++;
                    $erros .= "#" . $linha . " - OK";
                }

                $totalRegistros++;
            }

            $linha++;
        }

        fclose($abraArq);
    }

    // Atualiza tabelas do banco
    $dados->setId($id_layout_importado);
    $dados->setTotalRegistros($totalRegistros);
    $dados->setTotalRegistrosSucesso($totalRegistrosSucesso);
    $dados->setErros($erros);
    $dados->updateImportacao();
}

?>