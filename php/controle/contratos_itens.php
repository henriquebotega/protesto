<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_CRUD . "cClientes.php";
include_once URL_PHP_CRUD . "cContratosItens.php";
include_once URL_PHP_CRUD . "cContratos.php";
$dados = new cContratosItens();
$dados_cContratos = new cContratos();
$dados_cClientes = new cClientes();

$dataPHP = json_decode(file_get_contents("php://input"));

// Valida sessão
if (isBlank(validarSessao())) {
    echo json_encode(array("success" => false, "nivel" => 3, "mensagem" => "A sessão expirou!"));
    exit();
}

if ($dataPHP->acao == "read") {
    $where = " (ci.id_contrato = '" . addslashes($dataPHP->id_contrato) . "') ";
    $order = "";
    $more = "";

    executaQuery_cContratosItens($where, $order, $more, addslashes($dataPHP->id_contrato));
}

if ($dataPHP->acao == "salvar") {
    $dados->setIdContrato(addslashes($dataPHP->id_contrato));
    $dados->setIdServico(addslashes($dataPHP->registroAtual->id_servico));
    $dados->setValor(addslashes($dataPHP->registroAtual->valor));

    if (isset($dataPHP->registroAtual->id)) {
        $dados->setId(addslashes($dataPHP->registroAtual->id));
    }

    $dados->salvar();
    echo json_encode(array("success" => true, "nivel" => 1));
}

if ($dataPHP->acao == "excluir") {

    foreach ($dataPHP->colRegistroAtual as $k => $v) {
        $dados->setId(addslashes($v->id));
        $dados->delete();
    }

    echo json_encode(array("success" => true, "nivel" => 1));
    exit();
}

if ($dataPHP->acao == "getByID") {
    $where = " (ci.id_contrato = '" . addslashes($dataPHP->id_contrato) . "' AND ci.id = '" . addslashes($dataPHP->id) . "') ";
    $order = "";
    $more = " LIMIT 1 ";

    executaQuery_cContratosItens($where, $order, $more, addslashes($dataPHP->id_contrato));
}

if ($dataPHP->acao == "getByCliente") {
    $where = " (cli.id = '" . addslashes($dataPHP->id_cliente) . "') AND (c.id_situacao = 3) AND (s.webservice = 1) ";
    $order = "";
    $more = "";

    $retorno = $dados->select($where, $order, $more);

    if (sizeof($retorno) > 0) {
        echo json_encode(array("success" => true, "nivel" => 1, "retorno" => $retorno, "total" => sizeof($retorno)));
    } else {
        echo json_encode(array("success" => false, "nivel" => 2, "mensagem" => "Nenhum registro encontrado!"));
    }

    exit();
}

if ($dataPHP->acao == "filtrar") {

    $where = " (ci.id_contrato = '" . addslashes($dataPHP->id_contrato) . "') ";

    if (isset($dataPHP->palavraChave) && isNotBlank($dataPHP->palavraChave)) {
        $where .= " AND (s.titulo LIKE '%" . addslashes($dataPHP->palavraChave) . "%') ";
    }

    $order = "";
    $more = "";

    executaQuery_cContratosItens($where, $order, $more, addslashes($dataPHP->id_contrato));
}

function executaQuery_cContratosItens($where, $order, $more, $id_contrato)
{
    global $dados, $dados_cContratos;
    $retorno = $dados->select($where, $order, $more);

    $ret_cContratos = $dados_cContratos->select(" (c.id = '" . $id_contrato . "') ", "", " LIMIT 1 ");

    $retornoArray = array();
    foreach ($retorno as $chave => $valor) {
        $retornoArray[$chave] = $valor;

        $retornoArray[$chave]['grid_servico'] = $valor['servico'];
        $retornoArray[$chave]['grid_valor'] = exibir_grid_Valor($valor['valor']);
        $retornoArray[$chave]['valor'] = exibir_Valor($valor['valor']);
    }

    if (sizeof($retornoArray) > 0) {
        echo json_encode(array("success" => true, "nivel" => 1, "id_situacao" => $ret_cContratos[0]['id_situacao'], "retorno" => $retornoArray, "total" => sizeof($retornoArray)));
    } else {
        echo json_encode(array("success" => false, "nivel" => 2, "id_situacao" => $ret_cContratos[0]['id_situacao'], "mensagem" => "Nenhum registro encontrado!"));
    }

    exit();
}

?>