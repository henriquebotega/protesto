<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_CRUD . "cMensalidades.php";
include_once URL_PHP_CRUD . "cContratosItens.php";
include_once URL_PHP_CRUD . "cContratos.php";
$dados = new cContratos();
$dados_cContratosItens = new cContratosItens();
$dados_cMensalidades = new cMensalidades();

$dataPHP = json_decode(file_get_contents("php://input"));

// Valida sessão
if (isBlank(validarSessao())) {
    echo json_encode(array("success" => false, "nivel" => 3, "mensagem" => "A sessão expirou!"));
    exit();
}

if ($dataPHP->acao == "read") {
    $where = "";
    $order = "";
    $more = "";

    executaQuery_cContratos($where, $order, $more);
}

if ($dataPHP->acao == "salvar") {
    $dados->setIdSituacao(addslashes($dataPHP->registroAtual->id_situacao));
    $dados->setDiaVcto(addslashes($dataPHP->registroAtual->dia_vcto));
    $dados->setIdCliente(addslashes($dataPHP->registroAtual->id_cliente));

    if (isset($dataPHP->registroAtual->id)) {
        $dados->setId(addslashes($dataPHP->registroAtual->id));
    }

    $dados->salvar();
    echo json_encode(array("success" => true, "nivel" => 1));
}

if ($dataPHP->acao == "excluir") {

    $idContrato = addslashes($dataPHP->registroAtual->id);

    // Excluir Contratos Itens
    $dados_cContratosItens->setIdContrato($idContrato);
    $dados_cContratosItens->deletePorContrato();

    $dados->setId($idContrato);
    $dados->delete();

    echo json_encode(array("success" => true, "nivel" => 1));
    exit();
}

if ($dataPHP->acao == "getByID") {

    $where = " (c.id = '" . addslashes($dataPHP->id) . "') ";
    $order = "";
    $more = " LIMIT 1 ";

    executaQuery_cContratos($where, $order, $more);
}

if ($dataPHP->acao == "getContratoFinalizado") {

    $where = " (c.id_cliente = '" . addslashes($dataPHP->registroAtual->id_cliente) . "' AND c.id_situacao = 3) ";
    $order = "";
    $more = " LIMIT 1 ";

    executaQuery_cContratos($where, $order, $more);
}

if ($dataPHP->acao == "permiteRemover") {

    $id_contrato = $dataPHP->registroAtual->id;

    $ret_cMensalidades = $dados_cMensalidades->select(" (m.id_contrato = '" . $id_contrato . "') ");

    if (sizeof($ret_cMensalidades) > 0) {
        echo json_encode(array("success" => false, "nivel" => 2, "mensagem" => "Este contrato não pode ser removido, pois está vinculado a uma mensalidade!"));
        exit();
    }

    echo json_encode(array("success" => true, "nivel" => 1));
    exit();
}

if ($dataPHP->acao == "filtrar") {

    if (isset($dataPHP->palavraChave) && isNotBlank($dataPHP->palavraChave)) {
        $where = " (pj.razao_social LIKE '%" . addslashes($dataPHP->palavraChave) . "%' 
                        OR pf.nome LIKE '%" . addslashes($dataPHP->palavraChave) . "%'
                        OR pj.nome_fantasia LIKE '%" . addslashes($dataPHP->palavraChave) . "%') ";
    }

    if (isset($dataPHP->filtroSituacao) && sizeof($dataPHP->filtroSituacao) > 0) {

        $where = isNotBlank(@$where) ? $where . " AND " : "";
        $where = $where . " (c.id_situacao IN(";

        foreach ($dataPHP->filtroSituacao as $k => $v) {
            $where .= addslashes($v->id) . ",";
        }

        $where = substr($where, 0, -1);
        $where .= ")) ";
    }

    $order = "";
    $more = "";

    executaQuery_cContratos($where, $order, $more);
}

function executaQuery_cContratos($where, $order, $more)
{
    global $dados;
    $retorno = $dados->select($where, $order, $more);

    $retornoArray = array();
    foreach ($retorno as $chave => $valor) {
        $retornoArray[$chave] = $valor;

        $retornoArray[$chave]['grid_cliente'] = $valor['grid_nome'];
        $retornoArray[$chave]['grid_situacao'] = $valor['situacao'];
        $retornoArray[$chave]['grid_dia_vcto'] = $valor['dia_vcto'];
        $retornoArray[$chave]['grid_data'] = formatar_DataHora($valor['data']);
    }

    if (sizeof($retornoArray) > 0) {
        echo json_encode(array("success" => true, "nivel" => 1, "retorno" => $retornoArray, "total" => sizeof($retornoArray)));
    } else {
        echo json_encode(array("success" => false, "nivel" => 2, "mensagem" => "Nenhum registro encontrado!"));
    }

    exit();
}

?>