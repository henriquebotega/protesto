<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_CRUD . "cClientesConfig.php";
$dados = new cClientesConfig();

$dataPHP = json_decode(file_get_contents("php://input"));

if ($dataPHP->acao == "read") {

    $where = "";

    $sessao = validarSessao();
    if (isset($sessao['usuario'])) {
        $where .= (empty($where) ? "" : " AND ");
        $where .= " (id_cliente = '" . $sessao['usuario'][0]['id_cliente'] . "') ";
    }

    $retorno = $dados->select($where);

    $retornoArray = array();
    foreach ($retorno as $chave => $valor) {
        $retornoArray[$chave] = $valor;

        // Float
        $retornoArray[$chave]['boleto_desconto'] = exibir_Valor($valor['boleto_desconto']);
        $retornoArray[$chave]['boleto_abatimento'] = exibir_Valor($valor['boleto_abatimento']);
        $retornoArray[$chave]['boleto_mora'] = exibir_Valor($valor['boleto_mora']);
        $retornoArray[$chave]['boleto_multa'] = exibir_Valor($valor['boleto_multa']);
    }

    if (sizeof($retornoArray) > 0) {
        echo json_encode(array("success" => true, "nivel" => 1, "retorno" => $retornoArray, "total" => sizeof($retornoArray)));
    } else {
        echo json_encode(array("success" => false, "nivel" => 2, "mensagem" => "Nenhum registro encontrado!"));
    }

    exit();
}

if ($dataPHP->acao == "salvar") {

    $dados->setBoletoMensagem1($dataPHP->registroAtual->boleto_mensagem1);
    $dados->setBoletoMensagem2($dataPHP->registroAtual->boleto_mensagem2);
    $dados->setBoletoMensagem3($dataPHP->registroAtual->boleto_mensagem3);
    $dados->setBoletoDesconto($dataPHP->registroAtual->boleto_desconto);
    $dados->setBoletoAbatimento($dataPHP->registroAtual->boleto_abatimento);
    $dados->setBoletoMora($dataPHP->registroAtual->boleto_mora);
    $dados->setBoletoMulta($dataPHP->registroAtual->boleto_multa);

    if (isset($dataPHP->registroAtual->id)) {
        $dados->setId(addslashes($dataPHP->registroAtual->id));
    } else {
        $sessao = validarSessao();

        if (isset($sessao['usuario'])) {
            $dados->setIdCliente($sessao['usuario'][0]['id_cliente']);
        }
    }

    $dados->salvar();

    echo json_encode(array("success" => true));
    exit();
}

?>
