<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_CRUD . "cConfig.php";
include_once URL_PHP_CRUD . "cContratos.php";
include_once URL_PHP_CRUD . "cClientes.php";
include_once URL_PHP_CRUD . "cClientesPF.php";
include_once URL_PHP_CRUD . "cClientesPJ.php";
include_once URL_PHP_CRUD . "cClientesContatos.php";
include_once URL_PHP_CRUD . "cClientesConfig.php";
include_once URL_PHP_CRUD . "cAvisos.php";
$dados = new cClientes();
$dados_cContratos = new cContratos();
$dados_cClientesPF = new cClientesPF();
$dados_cClientesPJ = new cClientesPJ();
$dados_cClientesContatos = new cClientesContatos();
$dados_cAvisos = new cAvisos();
$dados_cClientesConfig = new cClientesConfig();
$dados_cConfig = new cConfig();

$dataPHP = json_decode(file_get_contents("php://input"));

// Valida sessão
if (isBlank(validarSessao())) {
    echo json_encode(array("success" => false, "nivel" => 3, "mensagem" => "A sessão expirou!"));
    exit();
}

if ($dataPHP->acao == "read") {
    $where = "";
    $order = "";
    $more = "";

    executaQuery_cClientes($where, $order, $more);
}

if ($dataPHP->acao == "salvar") {

    $dados->setIdSituacao(addslashes($dataPHP->registroAtual->id_situacao));
    $dados->setCep(addslashes($dataPHP->registroAtual->cep));
    $dados->setLogradouro(addslashes($dataPHP->registroAtual->logradouro));
    $dados->setNumero(addslashes($dataPHP->registroAtual->numero));
    $dados->setComplemento(addslashes($dataPHP->registroAtual->complemento));
    $dados->setBairro(addslashes($dataPHP->registroAtual->bairro));
    $dados->setIdTipo(addslashes($dataPHP->registroAtual->id_tipo));
    $dados->setIdCidade(addslashes($dataPHP->registroAtual->id_cidade));

    if (isset($dataPHP->registroAtual->id)) {
        $dados->setId(addslashes($dataPHP->registroAtual->id));
    }

    $dados->salvar();

    if (isset($dataPHP->registroAtual->id)) {
        $idCliente = $dataPHP->registroAtual->id;
    } else {
        $retorno = $dados->ultimo_registro();
        $idCliente = $retorno[0]["id"];

        // Gera um aviso de 'boas vindas'
        $dados_cAvisos->setTipo(0);
        $dados_cAvisos->setTitulo('Seja bem vindo!');
        $dados_cAvisos->setConteudo("Conheça nossos recursos");
        $dados_cAvisos->setIdCliente($idCliente);
        $dados_cAvisos->salvar();

        // Salva as configurações padrões do boleto (config padrao => config cliente)
        $ret_cConfig = $dados_cConfig->select();

        $dados_cClientesConfig->setBoletoMensagem1($ret_cConfig[0]['boleto_mensagem1']);
        $dados_cClientesConfig->setBoletoMensagem2($ret_cConfig[0]['boleto_mensagem2']);
        $dados_cClientesConfig->setBoletoMensagem3($ret_cConfig[0]['boleto_mensagem3']);
        $dados_cClientesConfig->setBoletoDesconto($ret_cConfig[0]['boleto_desconto']);
        $dados_cClientesConfig->setBoletoAbatimento($ret_cConfig[0]['boleto_abatimento']);
        $dados_cClientesConfig->setBoletoMulta($ret_cConfig[0]['boleto_multa']);
        $dados_cClientesConfig->setBoletoMora($ret_cConfig[0]['boleto_mora']);
        $dados_cClientesConfig->setIdCliente($idCliente);
        $dados_cClientesConfig->salvar();
    }

    if ($dataPHP->registroAtual->id_tipo == "1") {

        $ret_cClientesPF = $dados_cClientesPF->select(" (id_cliente = '" . $idCliente . "') ", "", "");
        if (sizeof($ret_cClientesPF) > 0) {
            $dados_cClientesPF->setId($ret_cClientesPF[0]['id']);
        }

        $dados_cClientesPF->setNome(addslashes($dataPHP->registroAtual->nome));
        $dados_cClientesPF->setCpf(addslashes($dataPHP->registroAtual->cpf));
        $dados_cClientesPF->setRg(addslashes($dataPHP->registroAtual->rg));
        $dados_cClientesPF->setDataNascimento(addslashes($dataPHP->registroAtual->data_nascimento));
        $dados_cClientesPF->setIdCliente($idCliente);
        $dados_cClientesPF->salvar();

        // Deleta PJ
        $dados_cClientesPJ->setIdCliente($idCliente);
        $dados_cClientesPJ->delete();
    } else {

        $ret_cClientesPJ = $dados_cClientesPJ->select(" (id_cliente = '" . $idCliente . "') ", "", "");
        if (sizeof($ret_cClientesPJ) > 0) {
            $dados_cClientesPJ->setId($ret_cClientesPJ[0]['id']);
        }

        $dados_cClientesPJ->setRazaoSocial(addslashes($dataPHP->registroAtual->razao_social));
        $dados_cClientesPJ->setNomeFantasia(addslashes($dataPHP->registroAtual->nome_fantasia));
        $dados_cClientesPJ->setCnpj(addslashes($dataPHP->registroAtual->cnpj));
        $dados_cClientesPJ->setIe(addslashes($dataPHP->registroAtual->ie));
        $dados_cClientesPJ->setIdCliente($idCliente);
        $dados_cClientesPJ->salvar();

        // Deleta PF
        $dados_cClientesPF->setIdCliente($idCliente);
        $dados_cClientesPF->delete();
    }

    $ret_cClientesContatos = $dados_cClientesContatos->select(" (id_cliente = '" . $idCliente . "') ", "", "");
    if (sizeof($ret_cClientesContatos) > 0) {
        $dados_cClientesContatos->setId($ret_cClientesContatos[0]['id']);
    }

    $dados_cClientesContatos->setTel1(addslashes($dataPHP->registroAtual->tel1));
    $dados_cClientesContatos->setContato1(addslashes($dataPHP->registroAtual->contato1));
    $dados_cClientesContatos->setEmail1(addslashes($dataPHP->registroAtual->email1));
    $dados_cClientesContatos->setTel2(addslashes($dataPHP->registroAtual->tel2));
    $dados_cClientesContatos->setContato2(addslashes($dataPHP->registroAtual->contato2));
    $dados_cClientesContatos->setEmail2(addslashes($dataPHP->registroAtual->email2));
    $dados_cClientesContatos->setTel3(addslashes($dataPHP->registroAtual->tel3));
    $dados_cClientesContatos->setContato3(addslashes($dataPHP->registroAtual->contato3));
    $dados_cClientesContatos->setEmail3(addslashes($dataPHP->registroAtual->email3));
    $dados_cClientesContatos->setIdCliente($idCliente);
    $dados_cClientesContatos->salvar();

    echo json_encode(array("success" => true, "nivel" => 1));
}

if ($dataPHP->acao == "filtrar") {

    if (isset($dataPHP->palavraChave) && isNotBlank($dataPHP->palavraChave)) {
        $where = " ((pj.razao_social LIKE '%" . addslashes($dataPHP->palavraChave) . "%' OR pf.nome LIKE '%" . addslashes($dataPHP->palavraChave) . "%') ";
        $where .= " OR (pj.nome_fantasia LIKE '%" . addslashes($dataPHP->palavraChave) . "%') ";
        $where .= " OR (pj.cnpj LIKE '%" . addslashes($dataPHP->palavraChave) . "%' OR pf.cpf LIKE '%" . addslashes($dataPHP->palavraChave) . "%'))) ";
    }

    if (isset($dataPHP->filtroSituacao) && sizeof($dataPHP->filtroSituacao) > 0) {

        $where = isNotBlank(@$where) ? $where . " AND " : "";
        $where = $where . " (c.id_situacao IN(";

        foreach ($dataPHP->filtroSituacao as $k => $v) {
            $where .= addslashes($v->id) . ",";
        }

        $where = substr($where, 0, -1);
        $where .= ")) ";
    }

    $order = "";
    $more = "";

    executaQuery_cClientes($where, $order, $more);
}

if ($dataPHP->acao == "getContratoByCliente") {

    $id_cliente = $dataPHP->registroAtual->id;
    $ret_cContratos = $dados_cContratos->select(" (c.id_cliente = '" . $id_cliente . "') ");

    if (sizeof($ret_cContratos) > 0) {
        echo json_encode(array("success" => false, "nivel" => 2, "mensagem" => "Este cliente não pode ser removido, pois existem contratos vinculados!"));
        exit();
    }

    echo json_encode(array("success" => true, "nivel" => 1));
    exit();
}

if ($dataPHP->acao == "excluir") {

    $idCliente = addslashes($dataPHP->registroAtual->id);

    // Excluir Cliente Contatos
    $dados_cClientesContatos->setIdCliente($idCliente);
    $dados_cClientesContatos->delete();

    // Excluir Cliente PF
    $dados_cClientesPF->setIdCliente($idCliente);
    $dados_cClientesPF->delete();

    // Excluir Cliente PJ
    $dados_cClientesPJ->setIdCliente($idCliente);
    $dados_cClientesPJ->delete();

    $dados->setId(addslashes($dataPHP->registroAtual->id));
    $dados->delete();

    echo json_encode(array("success" => true, "nivel" => 1));
    exit();
}

if ($dataPHP->acao == "getByID") {

    $where = " (c.id = '" . addslashes($dataPHP->id) . "') ";
    $order = "";
    $more = " LIMIT 1 ";

    executaQuery_cClientes($where, $order, $more);
}

function executaQuery_cClientes($where, $order, $more)
{
    global $dados;

    $retorno = $dados->select($where, $order, $more);
    $total = sizeof((empty($more)) ? $retorno : $dados->select($where, $order));

    if (sizeof($retorno) > 0) {
        echo json_encode(array("success" => true, "nivel" => 1, "retorno" => $retorno, "total" => $total));
    } else {
        echo json_encode(array("success" => false, "nivel" => 2, "mensagem" => "Nenhum registro encontrado!"));
    }

    exit();
}

?>