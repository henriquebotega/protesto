<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_CRUD . "cConfig.php";
include_once URL_PHP_CRUD . "cDividas.php";
include_once URL_PHP_CRUD . "cClientes.php";
include_once URL_PHP_CRUD . "cRemessasBanco.php";
$dados_cConfig = new cConfig();
$dados_cDividas = new cDividas();
$dados_cClientes = new cClientes();
$dados_cRemessasBanco = new cRemessasBanco();

// Recupera as informações do sistema
$ret_cConfig = $dados_cConfig->select();

/****************/
/*** FUNCÇÕES ***/
/****************/
function modulo_11($num, $base = 9, $r = 0)
{
    $soma = 0;
    $fator = 2;
    for ($i = strlen($num); $i > 0; $i--) {
        $numeros[$i] = substr($num, $i - 1, 1);
        $parcial[$i] = $numeros[$i] * $fator;
        $soma += $parcial[$i];
        if ($fator == $base) {
            $fator = 1;
        }
        $fator++;
    }
    if ($r == 0) {
        $soma *= 10;
        $digito = $soma % 11;

        //corrigido
        if ($digito == 10) {
            $digito = "X";
        }

        if (strlen($num) == "43") {
            //então estamos checando a linha digitável
            if ($digito == "0" or $digito == "X" or $digito > 9) {
                $digito = 1;
            }
        }
        return $digito;
    } elseif ($r == 1) {
        $resto = $soma % 11;
        return $resto;
    }
}

function formata_numero($numero, $loop, $insert, $tipo = "geral")
{
    if ($tipo == "geral") {
        $numero = str_replace(",", "", $numero);
        while (strlen($numero) < $loop) {
            $numero = $insert . $numero;
        }
    }
    if ($tipo == "valor") {
        /*
        retira as virgulas
        formata o numero
        preenche com zeros
        */
        $numero = str_replace(",", "", $numero);
        while (strlen($numero) < $loop) {
            $numero = $insert . $numero;
        }
    }
    if ($tipo == "convenio") {
        while (strlen($numero) < $loop) {
            $numero = $numero . $insert;
        }
    }
    return $numero;
}

function digitoVerificador_nossonumero($numero)
{
    $resto2 = modulo_11($numero, 9, 1);

    $digito = 11 - $resto2;
    if ($digito > 9) {
        $dv = 0;
    } else {
        $dv = $digito;
    }
    return $dv;
}

/*
function calculaDigitoVerificador($nossoNumero, $numCooperativa, $codClienteCooperativa)
{
    $sequencia = completaComZeros(4, $numCooperativa) . completaComZeros(10, $codClienteCooperativa) . completaComZeros(7, $nossoNumero);
    $constante = 3197;

    $cont = 0;
    $calculoDv = '';

    for ($num = 0; $num <= strlen($sequencia); $num++) {
        $cont++;
        if ($cont == 1) {
            $constante = 3;
        }
        if ($cont == 2) {
            $constante = 1;
        }
        if ($cont == 3) {
            $constante = 9;
        }
        if ($cont == 4) {
            $constante = 7;
            $cont = 0;
        }

        $calculoDv = $calculoDv + (substr($sequencia, $num, 1) * $constante);
    }

    $resto = $calculoDv % 11;

    if ($resto == 0 || $resto == 1) {
        $digitoNossoNumero = 0;
    } else {
        $digitoNossoNumero = 11 - $resto;
    }

    return $digitoNossoNumero;
}
*/

/*****************/
/*** CABEÇALHO ***/
/*****************/
$transacoes = "";
$quebra_linha = chr(13) . chr(10);

$prefixoCooperativa = $ret_cConfig[0]['boleto_agencia'];
$codCedente = $ret_cConfig[0]['boleto_cedente'];
$codCliente = $ret_cConfig[0]['boleto_cc'];
$digitoVerificadorCodCedente = $ret_cConfig[0]['boleto_cedente_digito'];
$dataArquivo = date("Ymd", time());
$codBeneficiario = $ret_cConfig[0]['boleto_cc'];
$cnpjBeneficiario = retornaSomenteNumeros($ret_cConfig[0]['empresa_cnpj']);
$numSequecialRemessa = $ret_cConfig[0]['boleto_numero_sequencial_remessa'];
$numSequecial = 1;

$valorDesconto = retornaSomenteNumeros($ret_cConfig[0]['boleto_desconto']) * 100;
$valorAbatimento = retornaSomenteNumeros($ret_cConfig[0]['boleto_abatimento']) * 100;
$taxaMoraMes = retornaSomenteNumeros($ret_cConfig[0]['boleto_mora']) * 100;
$taxaMulta = retornaSomenteNumeros($ret_cConfig[0]['boleto_multa']) * 100;

$header_001a001 = "0";                                                              // (0)
$header_002a002 = "1";                                                              // (1)
$header_003a009 = "REMESSA";                                                        // (REMESSA)
$header_010a011 = "01";                                                             // (01)
$header_012a026 = completaComEspacoBranco(15, "COBRANCA");                          // (COBRANCA       )
$header_027a031 = completaComZeros(5, $codBeneficiario);                            // (78320)
$header_032a045 = completaComZeros(14, $cnpjBeneficiario);                          // (23039595000194)
$header_046a076 = completaComEspacoBranco(31);                                      // (                               )
$header_077a079 = '748';                                                            // (748)
$header_080a094 = completaComEspacoBranco(15, 'SICREDI');                           // (SICREDI        )
$header_095a102 = $dataArquivo;                                                     // (20180219)
$header_103a110 = completaComEspacoBranco(8);                                       // (        )
$header_111a117 = completaComZeros(7, ++$numSequecialRemessa);                      // (0000001)
$header_118a390 = completaComEspacoBranco(273);                                     // ( )
$header_391a394 = '2.00';                                                           // (2.00)
$header_395a400 = completaComZeros(6, $numSequecial);                               // (000001)

$header = $header_001a001 . $header_002a002 . $header_003a009 . $header_010a011 . $header_012a026 . $header_027a031 . $header_032a045 . $header_046a076 . $header_077a079 . $header_080a094 . $header_095a102 . $header_103a110 . $header_111a117 . $header_118a390 . $header_391a394 . $header_395a400 . $quebra_linha;

/******************/
/*** TRANSAÇÕES ***/
/******************/

$dataPHP = json_decode(file_get_contents("php://input"));

if($dataPHP) {

    $contador = 0;
    foreach ($dataPHP->colRegistroAtual as $k => $dadosDivida) {

        $ret_cDividas = $dados_cDividas->select(" (d.id = '" . $dadosDivida->id_divida . "') AND (d.enviado_cartorio = 0) ");

        if (sizeof($ret_cDividas) > 0) {
            $ret_cRemessasBanco = $dados_cRemessasBanco->select(" (rb.id_divida = '" . $ret_cDividas[0]['id'] . "') AND (rb.enviado_banco = 0) ");
        
                if (sizeof($ret_cRemessasBanco) > 0) {
                foreach ($ret_cRemessasBanco as $chave => $remessaAtual) {

                    $ret_cClientes = $dados_cClientes->select(" (c.id = '" . $ret_cDividas[0]['id_cliente'] . "') ");

                    // Data de vencimento (divida) deve ser maior que a data de emissão (hoje)
                    if (strtotime(formatar_Data($ret_cDividas[0]['data_vcto'])) > strtotime(date("Y-m-d", time()))) {

                        $totalParcela = $ret_cDividas[0]['total_parcela'];
                        $numeroParcela = $ret_cDividas[0]['numero_parcela'];
                        $nossoNumero = $ret_cDividas[0]['nosso_numero'];
                        $dtVencimentoDivida = explode('-', formatar_DataHora($ret_cDividas[0]['data_vcto']));
                        $dataVencimento = $dtVencimentoDivida[2] . $dtVencimentoDivida[1] . substr($dtVencimentoDivida[0], 2, 4);
                        $dataArquivoMenor = substr($dataArquivo, 6, 2) . substr($dataArquivo, 4, 2). substr($dataArquivo, 2, 2);
                        $valorTitulo = retornaSomenteNumeros($ret_cDividas[0]['valor_vcto']);
                        $seuNumero = $dadosDivida->id_divida;
                        $dataDesconto = "000000";
                        $tipoInscricaoPagador = ($ret_cClientes[0]['id_tipo'] == 1) ? 1 : 2; // 1 = CPF, 2 = CNPJ
                        $numeroDocumentoPagador = retornaSomenteNumeros(($ret_cClientes[0]['id_tipo'] == 1) ? $ret_cClientes[0]['cpf'] : $ret_cClientes[0]['cnpj']);
                        $nomePagador = $ret_cClientes[0]['grid_nome'];
                        $enderecoPagador = $ret_cClientes[0]['logradouro'] . ", " . $ret_cClientes[0]['numero'];
                        $cepPagador = retornaSomenteNumeros($ret_cClientes[0]['cep']);
                        $idPagador = $ret_cClientes[0]['id'];

                        //$digNossoNumero = calculaDigitoVerificador($nossoNumero, $prefixoCooperativa, $codCedente . $digitoVerificadorCodCedente);
                        //$nNumero = date('y') . '2' . $nossoNumero . $digNossoNumero;

                        $agencia = $ret_cConfig[0]['boleto_agencia'];
                        $conta = $ret_cConfig[0]['boleto_cc'];
                        $posto = '10'; // fixo
                        $byteidt = '2'; // fixo
                        $nossoNumero = $ret_cDividas[0]['nosso_numero'];
                        $digNossoNumero = calculaDigitoVerificadorBoleto($nossoNumero, $prefixoCooperativa, $codCedente . $digitoVerificadorCodCedente);
                        $nN = $nossoNumero . $digNossoNumero;
                        $nnum = date("y") . $byteidt . formata_numero($nN, 5, 0);
                        $dv_nosso_numero = digitoVerificador_nossonumero("$agencia$posto$conta$nnum");
                        
                        $nNumero = $nnum . $dv_nosso_numero;

                        $transacoes_001a001 = "1";                                                          // (1)
                        $transacoes_002a002 = "A";                                                          // (A)
                        $transacoes_003a003 = "A";                                                          // (A)
                        $transacoes_004a004 = "A";                                                          // (A)
                        $transacoes_005a016 = completaComEspacoBranco(12);                                  // ( )
                        $transacoes_017a017 = "A";                                                          // (A)
                        $transacoes_018a018 = "A";                                                          // (A)
                        $transacoes_019a019 = "A";                                                          // (A)
                        $transacoes_020a047 = completaComEspacoBranco(28);                                  // ( )
                        $transacoes_048a056 = completaComZeros(9, $nNumero);                                // (182000123)
                        $transacoes_057a062 = completaComEspacoBranco(6);                                   // (      )
                        $transacoes_063a070 = completaComZeros(8, $dataArquivo);                            // (00000000)
                        $transacoes_071a071 = ' ';                                                          // ( )
                        $transacoes_072a072 = 'N';                                                          // (N)
                        $transacoes_073a073 = ' ';                                                          // ( )
                        $transacoes_074a074 = 'B';                                                          // (B)
                        $transacoes_075a076 = "00";                                                         // (00)
                        $transacoes_077a078 = "00";                                                         // (00)
                        $transacoes_079a082 = completaComEspacoBranco(4);                                   // (    )
                        $transacoes_083a092 = completaComZeros(10);                                         // (0000000000)
                        $transacoes_093a096 = completaComZeros(4, $taxaMulta);                              // (0000)
                        $transacoes_097a108 = completaComEspacoBranco(12);                                  // (            )
                        $transacoes_109a110 = '01';                                                         // (01)
                        $transacoes_111a120 = completaComZeros(10, $seuNumero);                             // (0000000003)
                        $transacoes_121a126 = completaComZeros(6, $dataVencimento);                         // (280218)
                        $transacoes_127a139 = completaComZeros(13, $valorTitulo);                           // (0000000010000)
                        $transacoes_140a148 = completaComEspacoBranco(9);                                   // (         )
                        $transacoes_149a149 = 'A';                                                          // (A)
                        $transacoes_150a150 = '0';                                                          // (0)
                        $transacoes_151a156 = completaComZeros(6, $dataArquivoMenor);                       // (190218)
                        $transacoes_157a158 = '00';                                                         // (00)
                        $transacoes_159a160 = '00';                                                         // (00)
                        $transacoes_161a173 = completaComZeros(13, $taxaMoraMes);                           // (0000000000000)
                        $transacoes_174a179 = completaComZeros(6, $dataDesconto);                           // (000000)
                        $transacoes_180a192 = completaComZeros(13, $valorDesconto);                         // (0000000000000)
                        $transacoes_193a205 = completaComZeros(13);                                         // (0000000000000)
                        $transacoes_206a218 = completaComZeros(13, $valorAbatimento);                       // (0000000000000)
                        $transacoes_219a219 = $tipoInscricaoPagador;                                        // (1)
                        $transacoes_220a220 = '0';                                                          // (0)
                        $transacoes_221a234 = completaComZeros(14, $numeroDocumentoPagador);                // (00000874346959)
                        $transacoes_235a274 = completaComEspacoBranco(40, utf8_decode($nomePagador));       // (ANDERSON HENRIQUE BOTEGA                )
                        $transacoes_275a314 = completaComEspacoBranco(40, utf8_decode($enderecoPagador));   // (AVENIDA AMERICO BELAY, 1                )
                        $transacoes_315a319 = completaComZeros(5);                                          // (00000)
                        $transacoes_320a325 = completaComZeros(6);                                          // (000000)
                        $transacoes_326a326 = ' ';                                                          // ( )
                        $transacoes_327a334 = completaComZeros(8, $cepPagador);                             // (87023000)
                        $transacoes_335a339 = completaComZeros(5, $idPagador);                              // (00001)                                                          // ( )
                        $transacoes_340a353 = completaComZeros(14);                                         // (00000000000000)
                        $transacoes_354a394 = completaComEspacoBranco(41);                                  // ( )
                        $transacoes_395a400 = completaComZeros(6, ++$numSequecial);                         // (000003)

                        $transacoes .= $transacoes_001a001 . $transacoes_002a002 . $transacoes_003a003 . $transacoes_004a004 . $transacoes_005a016 . $transacoes_017a017 . $transacoes_018a018 . $transacoes_019a019 . $transacoes_020a047 . $transacoes_048a056 . $transacoes_057a062 . $transacoes_063a070 . $transacoes_071a071 . $transacoes_072a072 . $transacoes_073a073 . $transacoes_074a074 . $transacoes_075a076 . $transacoes_077a078 . $transacoes_079a082 . $transacoes_083a092 . $transacoes_093a096 . $transacoes_097a108 . $transacoes_109a110 . $transacoes_111a120 . $transacoes_121a126 . $transacoes_127a139 . $transacoes_140a148 . $transacoes_149a149 . $transacoes_150a150 . $transacoes_151a156 . $transacoes_157a158 . $transacoes_159a160 . $transacoes_161a173 . $transacoes_174a179 . $transacoes_180a192 . $transacoes_193a205 . $transacoes_206a218 . $transacoes_219a219 . $transacoes_220a220 . $transacoes_221a234 . $transacoes_235a274 . $transacoes_275a314 . $transacoes_315a319 . $transacoes_320a325 . $transacoes_326a326 . $transacoes_327a334 . $transacoes_335a339 . $transacoes_340a353 . $transacoes_354a394 . $transacoes_395a400 . $quebra_linha;
                        
                        $dados_cRemessasBanco = new cRemessasBanco();
                        $dados_cRemessasBanco->setBoletoNumeroSequencialRemessa($numSequecialRemessa);
                        $dados_cRemessasBanco->setIdDivida($dadosDivida->id_divida);
                        $dados_cRemessasBanco->salvar_enviado_banco();

                        $contador++;
                    }
                }

            }
        }
    }

    /***************/
    /*** RODAPÉ ***/
    /**************/

    // $msgBeneficiario1 = utf8_decode($ret_cConfig[0]['boleto_mensagem1']);
    // $msgBeneficiario2 = utf8_decode($ret_cConfig[0]['boleto_mensagem2']);
    // $msgBeneficiario3 = utf8_decode($ret_cConfig[0]['boleto_mensagem3']);
    // $msgBeneficiario4 = utf8_decode("");
    // $msgBeneficiario5 = utf8_decode("");

    $rodape_001a001 = "9";                                                              // (9)
    $rodape_002a002 = "1";                                                              // (1)
    $rodape_003a005 = "748";                                                            // (748)
    $rodape_006a010 = completaComZeros(5, $codBeneficiario);                            // (78320)
    $rodape_011a394 = completaComEspacoBranco(384);                                     // ( )
    $rodape_395a400 = completaComZeros(6, ++$numSequecial);                             // (000017)

    $rodape = $rodape_001a001 . $rodape_002a002 . $rodape_003a005 . $rodape_006a010 . $rodape_011a394 . $rodape_395a400 . $quebra_linha;

    /**********************************************/
    /*** Gera o arquivo de remessa para o Banco ***/
    /**********************************************/

    if ($contador > 0) {

        //$cCedente = $codCedente . $digitoVerificadorCodCedente;
        $codMes = 1;
        $codDia = date('d');

        switch(date('n')){
            // Janeiro
            case 1: 
                $codMes = 1;
                break;
            // Fevereiro
            case 2: 
                $codMes = 2;
                break;
            // Março
            case 3: 
                $codMes = 3;
                break;
            // Abril
            case 4: 
                $codMes = 4;
                break;
            // Maio
            case 5: 
                $codMes = 5;
                break;
            // Junho
            case 6: 
                $codMes = 6;
                break;
            // Julho
            case 7: 
                $codMes = 7;
                break;
            // Agosto
            case 8: 
                $codMes = 8;
                break;
            // Setembro
            case 9: 
                $codMes = 9;
                break;
            // Outtubro
            case 10: 
                $codMes = 'O';
                break;
            // Novembro
            case 11: 
                $codMes = 'N';
                break;
            // Dezembro
            case 12: 
                $codMes = 'D';
                break;
        }

        $arquivoRemessa = $codCliente . $codMes . $codDia. ".CRM";
        
        $handle = fopen("../uploads/exportar/banco/" . $arquivoRemessa, "w+");

        // Remove todos os acentos
        $header = removeAcentos(utf8_encode($header));
        $transacoes = removeAcentos(utf8_encode($transacoes));
        $rodape = removeAcentos(utf8_encode($rodape));

        fwrite($handle, strtoupper($header));
        fwrite($handle, strtoupper($transacoes));
        fwrite($handle, strtoupper($rodape));
        fclose($handle);

        // Atualiza o numero sequencial REMESSA para próxima remessa
        $dados_cConfig->setBoletoNumeroSequencialRemessa(++$numSequecialRemessa);
        $dados_cConfig->updateNumeroSequecialBoletoRemessa();
    }
}

?>