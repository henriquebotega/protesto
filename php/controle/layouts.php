<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_CRUD . "cLayouts.php";
$dados = new cLayouts();

$dataPHP = json_decode(file_get_contents("php://input"));

// Valida sessão
if (isBlank(validarSessao())) {
    echo json_encode(array("success" => false, "nivel" => 3, "mensagem" => "A sessão expirou!"));
    exit();
}

if ($dataPHP->acao == "read") {
    $where = "";
    $order = "";
    $more = "";

    executaQuery_cLayouts($where, $order, $more);
}

if ($dataPHP->acao == "getByID") {

    $where = " (id = '" . addslashes($dataPHP->id) . "') ";
    $order = "";
    $more = " LIMIT 1 ";

    executaQuery_cLayouts($where, $order, $more);
}

if ($dataPHP->acao == "salvar") {

    $dados->setTitulo(addslashes($dataPHP->registroAtual->titulo));

    if (isset($dataPHP->registroAtual->id)) {
        $dados->setId(addslashes($dataPHP->registroAtual->id));
        $dados->salvar();

        $id_layout = $dataPHP->registroAtual->id;
    } else {
        $dados->salvar();

        $ultimo_id = $dados->ultimo_registro();
        $id_layout = $ultimo_id[0]['id'];
    }

    echo json_encode(array("success" => true, "nivel" => 1, "id_layout" => $id_layout));
}

if ($dataPHP->acao == "excluir") {

    foreach ($dataPHP->colRegistroAtual as $k => $v) {
        removerLayout($v->id);

        $dados->setId(addslashes($v->id));
        $dados->delete();
    }

    echo json_encode(array("success" => true, "nivel" => 1));
    exit();
}

if ($dataPHP->acao == "filtrar") {

    if (isset($dataPHP->palavraChave) && isNotBlank($dataPHP->palavraChave)) {
        $where = " (titulo LIKE '%" . addslashes($dataPHP->palavraChave) . "%') ";
    }

    $order = "";
    $more = "";

    executaQuery_cLayouts($where, $order, $more);
}

function removerLayout($id)
{
    global $dados;
    $retorno = $dados->select(" (id = '" . $id . "') ");

    if (sizeof($retorno) > 0) {
        @unlink("../uploads/layouts/" . $retorno[0]['arquivo']);
    }
}

function executaQuery_cLayouts($where, $order, $more)
{
    global $dados;

    $retorno = $dados->select($where, $order, $more);

    $retornoArray = array();
    foreach ($retorno as $chave => $valor) {
        $retornoArray[$chave] = $valor;
        $retornoArray[$chave]['grid_titulo'] = $valor['titulo'];
    }

    if (sizeof($retornoArray) > 0) {
        echo json_encode(array("success" => true, "nivel" => 1, "retorno" => $retornoArray, "total" => sizeof($retornoArray)));
    } else {
        echo json_encode(array("success" => false, "nivel" => 2, "mensagem" => "Nenhum registro encontrado!"));
    }

    exit();
}

?>