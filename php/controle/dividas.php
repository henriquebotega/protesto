<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_CRUD . "cTaxas.php";
include_once URL_PHP_CRUD . "cDividas.php";
include_once URL_PHP_CRUD . "cServicos.php";
include_once URL_PHP_CRUD . "cClientes.php";
include_once URL_PHP_CRUD . "cRemessasCartorio.php";
include_once URL_PHP_CRUD . "cContratos.php";
include_once URL_PHP_CRUD . "cContratosItens.php";
include_once URL_PHP_CRUD . "cRemessasBanco.php";
include_once URL_PHP_CRUD . "cClientesConfig.php";
$dados = new cDividas();
$dados_cClientes = new cClientes();
$dados_cTaxas = new cTaxas();
$dados_cContratosItens = new cContratosItens();
$dados_cRemessasBanco = new cRemessasBanco();
$dados_cDividas = new cDividas();
$dados_cClientesConfig = new cClientesConfig();

$dataPHP = json_decode(file_get_contents("php://input"));

// Valida sessão
if (isBlank(validarSessao())) {
    echo json_encode(array("success" => false, "nivel" => 3, "mensagem" => "A sessão expirou!"));
    exit();
}

if ($dataPHP->acao == "read") {
    $where = " (d.enviado_cartorio = 0) ";
    $order = "";
    $more = "";

    executaQuery_cDividas($where, $order, $more);
}

if ($dataPHP->acao == "getByID") {

    $where = " (d.enviado_cartorio = 0 AND d.id = '" . addslashes($dataPHP->id) . "') ";
    $order = "";
    $more = " LIMIT 1 ";

    executaQuery_cDividas($where, $order, $more);
}

if ($dataPHP->acao == "salvar") {

    if (isset($dataPHP->registroAtual->id)) {

        // Editar
        $dados->setValorVcto(addslashes($dataPHP->registroAtual->valor_vcto));
        $dados->setDataVcto(addslashes($dataPHP->registroAtual->data_vcto));
        $dados->setIdSituacao(addslashes($dataPHP->registroAtual->id_situacao));
        $dados->setNumeroParcela(addslashes($dataPHP->registroAtual->numero_parcela));
        $dados->setIdDevedor(addslashes($dataPHP->registroAtual->id_devedor));
        $dados->setDataEmissao(addslashes($dataPHP->registroAtual->data_emissao));
        $dados->setNumeroDocumento(addslashes($dataPHP->registroAtual->numero_documento));
        $dados->setNumeroFatura(addslashes($dataPHP->registroAtual->numero_fatura));
        $dados->setObservacoes(addslashes($dataPHP->registroAtual->observacoes));
        $dados->setIdPraca(addslashes($dataPHP->registroAtual->id_praca));
        $dados->setId(addslashes($dataPHP->registroAtual->id));
        $dados->salvar();

    } else {

        // Novo
        $numParcelas = addslashes($dataPHP->registroAtual->numero_parcela);

        for ($i = 1; $i <= $numParcelas; $i++) {

            if ($numParcelas == 1) {
                $dados->setValorVcto(addslashes($dataPHP->registroAtual->valor_vcto));
            } else {
                $dados->setValorVcto(addslashes($dataPHP->registroAtual->valor_da_parcela));
            }

            $dados->setDataEmissao(addslashes($dataPHP->registroAtual->data_emissao));
            $dados->setNumeroDocumento(addslashes($dataPHP->registroAtual->numero_documento));
            $dados->setNumeroFatura(addslashes($dataPHP->registroAtual->numero_fatura));
            $dados->setObservacoes(addslashes($dataPHP->registroAtual->observacoes));
            $dados->setDataVcto(addslashes($dataPHP->registroAtual->data_vcto));
            $dados->setIdSituacao(addslashes($dataPHP->registroAtual->id_situacao));
            $dados->setIdPraca(addslashes($dataPHP->registroAtual->id_praca));
            $dados->setNumeroParcela($i);
            $dados->setIdDevedor(addslashes($dataPHP->registroAtual->id_devedor));

            // Salva as configurações padrões do boleto (config do cliente => divida)
            $sessao = validarSessao();
            if (isset($sessao['usuario'])) {
                $ret_cClientesConfig = $dados_cClientesConfig->select(" (id_cliente = '" . $sessao['usuario'][0]['id_cliente'] . "') ");
            } else {
                $ret_cClientesConfig = $dados_cClientesConfig->select(" (id_cliente = '" . $dataPHP->registroAtual->id_cliente . "') ");
            }

            $dados->setBoletoMensagem1($ret_cClientesConfig[0]['boleto_mensagem1']);
            $dados->setBoletoMensagem2($ret_cClientesConfig[0]['boleto_mensagem2']);
            $dados->setBoletoMensagem3($ret_cClientesConfig[0]['boleto_mensagem3']);
            $dados->setBoletoDesconto($ret_cClientesConfig[0]['boleto_desconto']);
            $dados->setBoletoAbatimento($ret_cClientesConfig[0]['boleto_abatimento']);
            $dados->setBoletoMora($ret_cClientesConfig[0]['boleto_mora']);
            $dados->setBoletoMulta($ret_cClientesConfig[0]['boleto_multa']);

            $dados->salvar();
        }
    }

    echo json_encode(array("success" => true, "nivel" => 1));
}

if ($dataPHP->acao == "filtrar") {

    $where = " (d.enviado_cartorio = 0) ";

    if (isset($dataPHP->palavraChave) && isNotBlank($dataPHP->palavraChave)) {
        $where .= " AND ";
        $where .= " (d.nosso_numero LIKE '%" . addslashes($dataPHP->palavraChave) . "%' 
                        OR pf.nome LIKE '%" . addslashes($dataPHP->palavraChave) . "%'
                        OR pj.nome_fantasia LIKE '%" . addslashes($dataPHP->palavraChave) . "%'
                    ) ";
    }

    if (isset($dataPHP->filtroSituacao) && sizeof($dataPHP->filtroSituacao) > 0) {

        $where .= " AND (d.id_situacao IN(";

        foreach ($dataPHP->filtroSituacao as $k => $v) {
            $where .= addslashes($v->id) . ",";
        }

        $where = substr($where, 0, -1);
        $where .= ")) ";
    }

    $order = "";
    $more = "";

    executaQuery_cDividas($where, $order, $more);
}

if ($dataPHP->acao == "salvarConfigBoleto") {
    $dados->setBoletoMensagem1($dataPHP->registroAtual->boleto_mensagem1);
    $dados->setBoletoMensagem2($dataPHP->registroAtual->boleto_mensagem2);
    $dados->setBoletoMensagem3($dataPHP->registroAtual->boleto_mensagem3);
    $dados->setBoletoDesconto($dataPHP->registroAtual->boleto_desconto);
    $dados->setBoletoAbatimento($dataPHP->registroAtual->boleto_abatimento);
    $dados->setBoletoMora($dataPHP->registroAtual->boleto_mora);
    $dados->setBoletoMulta($dataPHP->registroAtual->boleto_multa);
    $dados->setId($dataPHP->registroAtual->id);
    $dados->updateConfigBoleto();

    echo json_encode(array("success" => true, "nivel" => 1));
}

if ($dataPHP->acao == "boletoEnviarEmail") {

    $emailEnviado = false;
    $emailAtual = addslashes($dataPHP->emailAtual);
    $registroModal = $dataPHP->registroModal;

    $listEmail = explode(";", $emailAtual);

    // Para cada boleto
    foreach ($registroModal as $k => $v) {

        $idDivida = $v->id;
        $tituloDivida = rand(111111, 999999);

        // Localiza a divida e gera um PDF temporario, anexa no email, envia, depois apaga o arquivo salvo.
        $ret_cConfig = $dados_cConfig->select();
        $prefixoCooperativa = $ret_cConfig[0]['boleto_agencia'];
        $codCedente = $ret_cConfig[0]['boleto_cedente'];
        $digitoVerificadorCodCedente = $ret_cConfig[0]['boleto_cedente_digito'];

        // Verifica se a divida já foi enviada ao cartório
        $ret_cDividas = $dados_cDividas->select(" (d.id = '" . $idDivida . "') ");

        if (sizeof($ret_cDividas) > 0) {
            $ret_cClientes = $dados_cClientes->select(" (c.id = '" . $ret_cDividas[0]['id_cliente'] . "') ");

            $nossoNumero = $ret_cDividas[0]['nosso_numero'];
            $digNossoNumero = calculaDigitoVerificadorBoleto($nossoNumero, $prefixoCooperativa, $codCedente . $digitoVerificadorCodCedente);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $ip_servidor . "php/boleto/gerar.php?id_divida=" . $idDivida);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $conteudo = curl_exec($ch);
            curl_close($ch);

            include_once "../mPDF/mpdf.php";
            $mpdf = new mPDF('c');
            $mpdf->SetDisplayMode('fullpage');
            $mpdf->WriteHTML($conteudo);
            $mpdf->Output('../boleto/pdf/' . $tituloDivida . '.pdf', 'F');
        }

        // Para cada e-mail
        foreach ($listEmail as $chave => $valor) {
            $emailEnvio = trim($valor);

            if (!empty($emailEnvio)) {

                $emailEnviado = true;

                $assunto = 'Boleto #' . $tituloDivida . ' - E-Protesto';
                $titulo = 'Envio de boleto via E-Protesto';
                $mensagem = 'Segue em anexo boleto referente a Dívida #' . $idDivida;

                // Enviar o boleto para o e-mail
                EnviarEmail($emailEnvio, $assunto, $titulo, $mensagem, false, '../boleto/pdf/' . $tituloDivida . '.pdf');
            }
        }

        // Remove o arquivo
        @unlink('../boleto/pdf/' . $tituloDivida . '.pdf');
    }

    if ($emailEnviado) {
        echo json_encode(array("success" => true, "nivel" => 1));
    } else {
        echo json_encode(array("success" => false, "nivel" => 2, "mensagem" => "Ocorreu uma falha no envio!"));
    }
    exit();
}

if ($dataPHP->acao == "cancelarBoleto") {
    foreach ($dataPHP->colRegistroAtual as $k => $idDivida) {

        // Busca o cliente
        $ret = $dados->select(" (d.id = '" . $idDivida . "' AND d.nosso_numero > 0) ");

        if (sizeof($ret) > 0) {
            $idCliente = $ret[0]["id_cliente"];

            // Busca o contrato finalizado
            $ret_cContratosItens = $dados_cContratosItens->select(" (c.id_situacao = 3 AND c.id_cliente = '" . $idCliente . "' AND ci.id_servico = 18) ");
            if (sizeof($ret_cContratosItens) > 0) {
                $valorCancelamento = exibir_Valor($ret_cContratosItens[0]["valor"]);

                // Cria taxa referente ao cancelamento
                $dados_cTaxas->setIdMensalidade(null);
                $dados_cTaxas->setIdServico(18);
                $dados_cTaxas->setHistorico("Boleto cancelado! Divida #" . $idDivida);
                $dados_cTaxas->setValor($valorCancelamento);
                $dados_cTaxas->setData(date("d/m/Y H:i:s", time()));
                $dados_cTaxas->setIdCliente($idCliente);
                $dados_cTaxas->setIdSituacao(1);
                $dados_cTaxas->salvar();

                // Define o boleto como cancelado
                $dados_cDividas = new cDividas();
                $dados_cDividas->setIdSituacao(2);
                $dados_cDividas->setId($idDivida);
                $dados_cDividas->updateSituacao();

                // 2 - Gerar novo registro na tabela 'remessas', vinculada ao boleto atual
                $ret_cRemessasBanco = $dados_cRemessasBanco->select(" (rb.id_divida = '" . $idDivida . "' AND rb.tipo_envio = 1) ");
                if (sizeof($ret_cRemessasBanco) == 0) {
                    $dados_cRemessasBanco = new cRemessasBanco();
                    $dados_cRemessasBanco->setIdDivida($idDivida);
                    $dados_cRemessasBanco->setNossoNumero($ret[0]['nosso_numero']);
                    $dados_cRemessasBanco->setTipoEnvio(1);
                    $dados_cRemessasBanco->salvar();
                }
            }
        }
    }
}

function executaQuery_cDividas($where, $order, $more)
{
    global $dados, $dados_cClientes, $dados_cRemessasBanco;

    $sessao = validarSessao();
    if (isset($sessao['usuario'])) {
        $where .= (empty($where) ? "" : " AND ");
        $where .= " (dev.id_cliente = '" . $sessao['usuario'][0]['id_cliente'] . "') ";
    }

    $retorno = $dados->select($where, $order, $more);

    $retornoArray = array();
    foreach ($retorno as $chave => $valor) {
        $retornoArray[$chave] = $valor;

        $ret_cClientes = $dados_cClientes->select(" (c.id = '" . $valor['id_cliente'] . "') ", "", "");
        $retornoArray[$chave]['grid_cliente'] = $ret_cClientes[0]['grid_nome'];

        $retornoArray[$chave]['grid_numero'] = $valor['nosso_numero'];
        $retornoArray[$chave]['grid_devedor'] = $valor['grid_devedor'];
        $retornoArray[$chave]['grid_situacao'] = $valor['situacao'];
        $retornoArray[$chave]['grid_numero_parcela'] = $valor['numero_parcela'];
        $retornoArray[$chave]['grid_valor_vcto'] = exibir_grid_Valor($valor['valor_vcto']);

        $ret_cRemessasBanco = $dados_cRemessasBanco->select(" (id_divida = '" . $valor['id'] . "') ", "", "");
        if (sizeof($ret_cRemessasBanco) > 0) {
            $retornoArray[$chave]['grid_remessa'] = ((int)$ret_cRemessasBanco[0]['enviado_banco'] == 1 ? 'Sim' : 'Não');
        } else {
            $retornoArray[$chave]['grid_remessa'] = 'Não';
        }

        $retornoArray[$chave]['valor_vcto'] = exibir_Valor($valor['valor_vcto']);
    }

    if (sizeof($retornoArray) > 0) {
        echo json_encode(array("success" => true, "nivel" => 1, "retorno" => $retornoArray, "total" => sizeof($retornoArray)));
    } else {
        echo json_encode(array("success" => false, "nivel" => 2, "mensagem" => "Nenhum registro encontrado!"));
    }

    exit();
}

?>