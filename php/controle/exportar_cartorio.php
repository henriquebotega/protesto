<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_CRUD . "cRemessasCartorio.php";
include_once URL_PHP_CRUD . "cDividas.php";
include_once URL_PHP_CRUD . "cConfig.php";
include_once URL_PHP_CRUD . "cClientes.php";
include_once URL_PHP_CRUD . "cDevedores.php";

$dados = new cRemessasCartorio();
$dados_cDividas = new cDividas();
$dados_cConfig = new cConfig();
$dados_cClientes = new cClientes();
$dados_cDevedores = new cDevedores();

// Recupera as informações do sistema
$ret_cConfig = $dados_cConfig->select();

$dataPHP = json_decode(file_get_contents("php://input"));
$quebra_linha = chr(13) . chr(10);
$numSequecialRemessa = $ret_cConfig[0]['cartorio_numero_sequencial'];

if ($dataPHP->acao == "exportar") {

    if (sizeof($dataPHP->colRegistroAtual) > 0) {

        $codPortador = $ret_cConfig[0]['cartorio_cod_portador'];
        $nomePortador = $ret_cConfig[0]['cartorio_portador'];
        $dataMovimento = date("dmY", time());
        $identificacaoRemetente = $ret_cConfig[0]['cartorio_sigla_remetente'];
        $identificacaoDestinatario = $ret_cConfig[0]['cartorio_sigla_destinatario'];
        $identificacaoTransacao = $ret_cConfig[0]['cartorio_sigla_transacao'];
        $qtdRegistros = sizeof($dataPHP->colRegistroAtual);
        $qtdTitulos = sizeof($dataPHP->colRegistroAtual);
        $qtdIndicacoes = "0";
        $qtdOriginais = sizeof($dataPHP->colRegistroAtual);
        $identificacaoAgencia = $ret_cConfig[0]['cartorio_agencia'];
        $versaoLayout = $ret_cConfig[0]['cartorio_versao_layout'];
        $codMunicipio = $ret_cConfig[0]['empresa_id_municipio'];
        $numSequencial = 1;
        $valorRemessa = 0;

        $header_001a001 = "0";                                                      // 0
        $header_002a004 = completaComZeros(3, $codPortador);                        // 000
        $header_005a044 = completaComEspacoBranco(40, $nomePortador);               // (SITE PROTESTO NACIONAL                  )
        $header_045a052 = completaComZeros(8, $dataMovimento);                      // 13032015
        $header_053a055 = completaComEspacoBranco(3, $identificacaoRemetente);      // SIT
        $header_056a058 = completaComEspacoBranco(3, $identificacaoDestinatario);   // SDT
        $header_059a061 = completaComEspacoBranco(3, $identificacaoTransacao);      // TPR
        $header_062a067 = completaComEspacoBranco(6, $numSequecialRemessa);         // (     1)
        $header_068a071 = completaComZeros(4, $qtdRegistros);                       // 0008
        $header_072a075 = completaComZeros(4, $qtdTitulos);                         // 0006
        $header_076a079 = completaComZeros(4, $qtdIndicacoes);                      // 0000
        $header_080a083 = completaComZeros(4, $qtdOriginais);                       // 0006
        $header_084a089 = completaComEspacoBranco(6, $identificacaoAgencia);        // 000000
        $header_090a092 = completaComZeros(3, $versaoLayout);                       // 043
        $header_093a099 = completaComEspacoBranco(7, $codMunicipio);                // 4127106
        $header_100a596 = completaComEspacoBranco(497);
        $header_597a600 = completaComZeros(4, $numSequencial);                      // 0001

        $header = $header_001a001 . $header_002a004 . $header_005a044 . $header_045a052 . $header_053a055 . $header_056a058 . $header_059a061 . $header_062a067 . $header_068a071 . $header_072a075 . $header_076a079 . $header_080a083 . $header_084a089 . $header_090a092 . $header_093a099 . $header_100a596 . $header_597a600 . $quebra_linha;

        $contador = 0;
        foreach ($dataPHP->colRegistroAtual as $k => $dadosDivida) {

            // Pega os dados da Divida
            $ret_cDividas = $dados_cDividas->select(" (d.id = '" . $dadosDivida->id_divida . "' AND d.id_situacao = 1) ", "", "");

            if (sizeof($ret_cDividas) > 0) {

                // Pega os dados da Remessa
                $ret_cRemessasCartorio = $dados->select(" (rc.id_divida = '" . $dadosDivida->id_divida . "' AND (rc.enviado_cartorio = 0)) ", "", "");

                if (sizeof($ret_cRemessasCartorio) > 0) {

                    // Pega os dados do Cliente
                    $ret_cClientes = $dados_cClientes->select(" (c.id = '" . $ret_cDividas[0]['id_cliente'] . "') ", "", "");

                    // Pega os dados do Devedor
                    $ret_cDevedores = $dados_cDevedores->select(" (d.id = '" . $dadosDivida->id_devedor . "') ", "", "");

                    $nomeCedente = $ret_cClientes[0]['grid_nome'];
                    $nomeSacador = $ret_cClientes[0]['grid_nome'];
                    $docSacador = retornaSomenteNumeros(($ret_cClientes[0]['id_tipo'] == 1) ? $ret_cClientes[0]['cpf'] : $ret_cClientes[0]['cnpj']);
                    $enderecoSacador = $ret_cClientes[0]['logradouro'] . " " . $ret_cClientes[0]['numero'];
                    $cepSacador = retornaSomenteNumeros($ret_cClientes[0]['cep']);
                    $cidadeSacador = $ret_cClientes[0]['cidade'];

                    $nosso_numero = $ret_cDividas[0]['nosso_numero'];
                    $especieTitulo = "DMI";
                    $numeroTitulo = $ret_cDividas[0]['numero_documento'];

                    $dtEmissao = explode("-", formatar_Data($ret_cDividas[0]['data_emissao']));
                    $dataEmissao = $dtEmissao[2] . $dtEmissao[1] . substr($dtEmissao[0], 2, 4);
                    $dtVencimento = explode("-", formatar_Data($ret_cDividas[0]['data_vcto']));
                    $dataVencimento = $dtVencimento[2] . $dtVencimento[1] . substr($dtVencimento[0], 2, 4);

                    $tipoMoeda = "1";
                    $valorTitulo = $ret_cDividas[0]['valor_vcto'];
                    $saldoTitulo = $ret_cDividas[0]['valor_vcto'];
                    $pracaProtesto = "";

                    $nomeDevedor = $dadosDivida->grid_devedor;
                    $tipoIdentificacaoDevedor = 2;
                    $numeroIdentificacaoDevedor = retornaSomenteNumeros($ret_cDevedores[0]['cpf']);
                    $rgDevedor = retornaSomenteNumeros($ret_cDevedores[0]['rg']);
                    $enderecoDevedor = $ret_cDevedores[0]['logradouro'] . " " . $ret_cDevedores[0]['numero'];
                    $cepDevedor = retornaSomenteNumeros($ret_cDevedores[0]['cep']);
                    $cidadeDevedor = $ret_cDevedores[0]['cidade'];
                    $bairroDevedor = $ret_cDevedores[0]['bairro'];

                    $dataProtocolo = "0";
                    $valorCustaCartorio = "0";
                    $dataOcorrencia = "0";
                    $valorCustaDistribuidor = "0";
                    $registroDistribuicao = "0";
                    $valorGravacao = "0";
                    $numeroOperacao = "0";
                    $numeroContratoBanco = "0";
                    $numeroParcelaContrato = "0";
                    $codIrregularidade = "";
                    $valorDespesas = "0";

                    $valorRemessa += (float)$saldoTitulo;

                    $transacoes_001a001 = "1";                                                               // 1
                    $transacoes_002a004 = completaComZeros(3, $codPortador);                                 // 000
                    $transacoes_005a019 = completaComEspacoBranco(15);                                       // (               )
                    $transacoes_020a064 = completaComEspacoBranco(45, utf8_decode($nomeCedente));            // (DISMAR DIST MGA DE ELET LTDA LOJAS DUDONY    )
                    $transacoes_065a109 = completaComEspacoBranco(45, utf8_decode($nomeSacador));            // (DISMAR DIST MGA DE ELET LTDA LOJAS DUDONY    )
                    $transacoes_110a123 = completaComEspacoBranco(14, $docSacador);                          // 80816606000139
                    $transacoes_124a168 = completaComEspacoBranco(45, utf8_decode($enderecoSacador));        // (RODOVIA PR 317  N 4949 LJ   03               )
                    $transacoes_169a176 = completaComZeros(8, $cepSacador);                                  // 87001970
                    $transacoes_177a196 = completaComEspacoBranco(20, utf8_decode($cidadeSacador));          // (LONDRINA            )
                    $transacoes_197a198 = "PR";                                                              // PR
                    $transacoes_199a213 = completaComEspacoBranco(15, $nosso_numero);                        // (611335         )
                    $transacoes_214a216 = completaComEspacoBranco(3, $especieTitulo);                        // DMI
                    $transacoes_217a227 = completaComEspacoBranco(11, $numeroTitulo);                        // (20025031812)
                    $transacoes_228a235 = completaComZeros(8, $dataEmissao);                                 // 21022009
                    $transacoes_236a243 = completaComZeros(8, $dataVencimento);                              // 15032010
                    $transacoes_244a246 = completaComZeros(3, $tipoMoeda);                                   // 001
                    $transacoes_247a260 = completaComZeros(14, $valorTitulo);                                // 00000000005330
                    $transacoes_261a274 = completaComZeros(14, $saldoTitulo);                                // 00000000005330
                    $transacoes_275a294 = completaComEspacoBranco(20, utf8_decode($pracaProtesto));          // (TELEMACO BORBA      )
                    $transacoes_295a295 = "M";                                                               // M
                    $transacoes_296a296 = "A";                                                               // N
                    $transacoes_297a297 = "1";                                                               // 1
                    $transacoes_298a342 = completaComEspacoBranco(45, utf8_decode($nomeDevedor));            // (MARIA DA CONCEICAO DINIZ                     )
                    $transacoes_343a345 = completaComZeros(3, $tipoIdentificacaoDevedor);                    // 002 (001 = cnpj, 002 = cpf)
                    $transacoes_346a359 = completaComZeros(14, $numeroIdentificacaoDevedor);                 // 00097123676904
                    $transacoes_360a370 = completaComEspacoBranco(11, $rgDevedor);                           // (           )
                    $transacoes_371a415 = completaComEspacoBranco(45, utf8_decode($enderecoDevedor));        // (R ALAMEDA PASTEUR 600                        )
                    $transacoes_416a423 = completaComZeros(8, $cepDevedor);                                  // 86010903
                    $transacoes_424a443 = completaComEspacoBranco(20, utf8_decode($cidadeDevedor));          // (TELEMACO BORBA      )
                    $transacoes_444a445 = "PR";                                                              // PR
                    $transacoes_446a447 = "00";                                                              // 00
                    $transacoes_448a457 = completaComEspacoBranco(10);                                       // (          )
                    $transacoes_458a458 = " ";                                                               // ( )
                    $transacoes_459a466 = completaComZeros(8, $dataProtocolo);                               // 00000000
                    $transacoes_467a476 = completaComZeros(10, $valorCustaCartorio);                         // 0000000000
                    $transacoes_477a477 = " ";                                                               // ( )         anexo 3
                    $transacoes_478a485 = completaComZeros(8, $dataOcorrencia);                              // 00000000
                    $transacoes_486a487 = completaComEspacoBranco(2);                                        // (  )
                    $transacoes_488a507 = completaComEspacoBranco(20, $bairroDevedor);                       // (                    )
                    $transacoes_508a517 = completaComZeros(10, $valorCustaDistribuidor);                     // 0000000000
                    $transacoes_518a523 = completaComZeros(6, $registroDistribuicao);                        // 000000
                    $transacoes_524a533 = completaComZeros(10, $valorGravacao);                              // 0000000000
                    $transacoes_534a538 = completaComZeros(5, $numeroOperacao);                              // 00000
                    $transacoes_539a553 = completaComZeros(15, $numeroContratoBanco);                        // 000000000000000
                    $transacoes_554a556 = completaComZeros(3, $numeroParcelaContrato);                       // 000
                    $transacoes_557a557 = " ";                                                               // ( )    anexo 5
                    $transacoes_558a565 = completaComEspacoBranco(8, $codIrregularidade);                    // (        )
                    $transacoes_566a566 = " ";                                                               // ( )
                    $transacoes_567a567 = " ";                                                               // ( )
                    $transacoes_568a577 = completaComEspacoBranco(10, $valorDespesas);                       // 0000000000
                    $transacoes_578a596 = completaComEspacoBranco(19);                                       // (                   )
                    $transacoes_597a600 = completaComZeros(4, ++$numSequencial);                             // 0006

                    $transacoes = $transacoes_001a001 . $transacoes_002a004 . $transacoes_005a019 . $transacoes_020a064 . $transacoes_065a109 . $transacoes_110a123 . $transacoes_124a168 . $transacoes_169a176 . $transacoes_177a196 . $transacoes_197a198 . $transacoes_199a213 . $transacoes_214a216 . $transacoes_217a227 . $transacoes_228a235 . $transacoes_236a243 . $transacoes_244a246 . $transacoes_247a260 . $transacoes_261a274 . $transacoes_275a294 . $transacoes_295a295 . $transacoes_296a296 . $transacoes_297a297 . $transacoes_298a342 . $transacoes_343a345 . $transacoes_346a359 . $transacoes_360a370 . $transacoes_371a415 . $transacoes_416a423 . $transacoes_424a443 . $transacoes_444a445 . $transacoes_446a447 . $transacoes_448a457 . $transacoes_458a458 . $transacoes_459a466 . $transacoes_467a476 . $transacoes_477a477 . $transacoes_478a485 . $transacoes_486a487 . $transacoes_488a507 . $transacoes_508a517 . $transacoes_518a523 . $transacoes_524a533 . $transacoes_534a538 . $transacoes_539a553 . $transacoes_554a556 . $transacoes_557a557 . $transacoes_558a565 . $transacoes_566a566 . $transacoes_567a567 . $transacoes_568a577 . $transacoes_578a596 . $transacoes_597a600 . $quebra_linha;

                    $dados = new cRemessasCartorio();
                    $dados->setCartorioNumeroSequencial($numSequecialRemessa);
                    $dados->setIdDivida($dadosDivida->id);
                    $dados->salvar_enviado_cartorio();

                    $contador++;
                }
            }
        }

        $qtdRemessa = (int)$qtdRegistros + (int)$qtdTitulos + (int)$qtdIndicacoes + (int)$qtdOriginais;

        $rodape_001a001 = "9";                                                       // 9
        $rodape_002a004 = completaComZeros(3, $codPortador);                         // 000
        $rodape_005a044 = completaComEspacoBranco(40, utf8_decode($nomePortador));   // (SITE PROTESTO NACIONAL                  )
        $rodape_045a052 = completaComZeros(8, $dataMovimento);                       // 13032015
        $rodape_053a057 = completaComZeros(5, $qtdRemessa);                          // 00020
        $rodape_058a075 = completaComZeros(18, $valorRemessa);                       // 000000000001183.81
        $rodape_076a596 = completaComEspacoBranco(521);
        $rodape_597a600 = completaComZeros(4, ++$numSequencial);                     // 0008

        $rodape = $rodape_001a001 . $rodape_002a004 . $rodape_005a044 . $rodape_045a052 . $rodape_053a057 . $rodape_058a075 . $rodape_076a596 . $rodape_597a600;

        /*************************************************/
        /*** Gera o arquivo de remessa para o Cartório ***/
        /*************************************************/

        if ($contador > 0) {
            $arquivoRemessa = "remessa-" . date('Y-m-d_H-i-s', time()) . ".txt";
            $handle = fopen("../uploads/exportar/cartorio/" . $arquivoRemessa, "w+");

            fwrite($handle, strtoupper($header));
            fwrite($handle, strtoupper($transacoes));
            fwrite($handle, strtoupper($rodape));
            fclose($handle);

            // Atualiza o numero sequencial para próxima remessa
            $dados_cConfig->setCartorioNumeroSequencial(++$numSequecialRemessa);
            $dados_cConfig->updateNumeroSequecialCartorio();

            echo json_encode(array("success" => true, "nivel" => 1, "retorno" => $arquivoRemessa));
            exit();
        }
    }
}

?>