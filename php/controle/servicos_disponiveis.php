<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_CRUD . "cTaxas.php";
include_once URL_PHP_CRUD . "cContratosItens.php";
include_once URL_PHP_CRUD . "cServicos.php";
$dados = new cTaxas();
$dados_cContratosItens = new cContratosItens();
$dados_cServicos = new cServicos();

// Valida sessão
if (isBlank(validarSessao())) {
    echo json_encode(array("success" => false, "nivel" => 3, "mensagem" => "A sessão expirou!"));
    exit();
}

$dataPHP = json_decode(file_get_contents("php://input"));

if ($dataPHP->acao == "historico") {

    $sessao = validarSessao();
    $id_cliente = $sessao['usuario'][0]['id_cliente'];

    $retorno = $dados->select(" (t.id_cliente = '" . $id_cliente . "' AND t.id_servico = '" . $dataPHP->id_servico . "') ", " t.id DESC ");

    $retornoArray = array();
    foreach ($retorno as $chave => $valor) {
        $retornoArray[$chave] = $valor;

        $retornoArray[$chave]['grid_data'] = $valor['data'];
        $retornoArray[$chave]['grid_situacao'] = $valor['situacao'];
        $retornoArray[$chave]['grid_valor'] = exibir_grid_Valor($valor['valor']);
    }

    if (sizeof($retornoArray) > 0) {
        echo json_encode(array("success" => true, "nivel" => 1, "retorno" => $retornoArray, "total" => sizeof($retornoArray)));
    } else {
        echo json_encode(array("success" => false, "nivel" => 2, "mensagem" => "Nenhum registro encontrado!"));
    }

    exit();
}

if ($dataPHP->acao == "ws_foi_desligado") {

    // Percorre todos os serviços
    $ret_cServicos = $dados_cServicos->select("(id = '" . $dataPHP->id_servico . "')");

    // Pega a rota
    $rota = $ret_cServicos[0]['rota'];

    // Retorna a configuração deste serviço em questão
    if ($ret_cConfig[0]['ws_' . $rota] == 1) {
        echo json_encode(array("success" => true));
    } else {
        echo json_encode(array("success" => false));
    }

    exit();
}

if ($dataPHP->acao == "consultar") {

//    $retorno = json_decode('{"code":"000","message":"Pesquisa feita com sucesso","date":"2017-06-07","hour":"22:20:31","revision":"12265","server":"42","content":{"nome":{"existe_informacao":"SIM","conteudo":[{"documento":"00874346959","tipo_documento":"PF","nome":"ANDERSON HENRIQUE BOTEGA","outras_datas_nascimento":"SIM","data_nascimento":"20\/08\/1984","idade":"32","signo":"Le\u00e3o","obito":"NAO","sexo":"M","uf":"PR,","situacao_receita":"REGULAR","situacao_receita_data":"2012-05-16","situacao_receita_hora":"00:00:00"},{"documento":"00874346959","tipo_documento":"PF","nome":"ANDERSON BOTEGA","outras_datas_nascimento":"SIM","data_nascimento":"20\/08\/1984","idade":"32","signo":"Le\u00e3o","obito":"NAO","sexo":"M","uf":"PR,","situacao_receita":"REGULAR","situacao_receita_data":"2012-05-16","situacao_receita_hora":"00:00:00"}]},"dados_parentes":{"existe_informacao":"SIM","conteudo":[{"documento":"88467660910","nome":"DALVA HELLEBRAND BOTEGA","campo":"mae","obito":"NAO","tipo_beneficio":"Aposentadoria Por Idade","aposentado":"SIM","id":"35457726","pontuacao":"0"},{"documento":"69847320934","nome":"VERONICE BOTEGA DE SOUSA","campo":"irmao","obito":"NAO","tipo_beneficio":"","aposentado":"NAO","id":"35457723","pontuacao":"0"}]},"pessoas_contato":{"existe_informacao":"NAO"},"pesquisa_enderecos":{"existe_informacao":"SIM","conteudo":[{"id_endereco":"705721532","logradouro":"RUA","endereco":"DAS MANGUEIRAS","bairro":"JARDIM TROPICAL","cidade":"MARINGA","numero":"338","cep":"87080680","bloco":"","apto":"","casa":"","quadra":"","lote":"","complemento":"B","uf":"PR","pontuacao":"0"},{"id_endereco":"95160583","logradouro":"AVENIDA","endereco":"DOM A RASGULAEFF","bairro":"PARQUE RESIDENCIAL CIDADE NOVA","cidade":"MARINGA","numero":"1","cep":"87023060","bloco":"","apto":"","casa":"","quadra":"","lote":"","complemento":"582","uf":"PR","pontuacao":"0"}]},"trabalha_trabalhou":{"existe_informacao":"NAO"},"contato_preferencial":{"existe_informacao":"SIM","conteudo":{"telefone_fixo":{},"telefone_celular":{"ddd":"44","telefone":"999464574","operadora":"TIM - Celular","pontuacao":"2","id_endereco":"494063684"},"telefone_outros":{"ddd":"44","telefone":"30460384","operadora":"Vivo - Fixo","pontuacao":"2","id_endereco":"572603435"},"contatos":{},"parentes":{},"email":"","endereco":{}}},"residentes_mesmo_domicilio":{"existe_informacao":"SIM","conteudo":[{"nome":"ANTONIO BOTEGA","documento":"08799130963","id":"35457729","pontuacao":"0"},{"nome":"DALVA HELLEBRAND BOTEGA","documento":"88467660910","id":"35457730","pontuacao":"0"}]},"emails":{"existe_informacao":"SIM","conteudo":[{"email":"henriquebotega@gmail.com","pontuacao":"0"},{"email":"anderson.botega@sabium.com.br","pontuacao":"0"}]},"numero_beneficio":{"existe_informacao":"NAO"},"alerta_participacoes":{"existe_informacao":"NAO"},"pesquisa_telefones":{"existe_informacao":"SIM","conteudo":{"outros":[{"ddd":"44","telefone":"30460384","operadora":"Vivo - Fixo","pontuacao":"2","id_endereco":"572603435"},{"ddd":"44","telefone":"32281394","operadora":"OI - Fixo","pontuacao":"2","id_endereco":"572603435"},{"ddd":"44","telefone":"999529854","operadora":"TIM - Celular","pontuacao":"0","id_endereco":"95160583"}],"celular":[{"ddd":"44","telefone":"999464574","operadora":"TIM - Celular","pontuacao":"2","id_endereco":"494063684"}]}},"alerta_monitore":{"existe_informacao":"SIM","conteudo":{"data_ultima_ocorrencia":"2017\/06\/07 21:59:00"}},"outros_documentos":{"existe_informacao":"SIM","conteudo":[{"tipo":"titulo_de_eleitor","documento":"079149250612"},{"tipo":"pis","documento":"12784470492"},{"tipo":"rg","documento":"N\/D"},{"tipo":"ct","documento":"N\/D"},{"tipo":"passaporte","documento":"N\/D"}]},"provaveis_datas_nascimento":{"existe_informacao":"NAO"}}}');
//    echo json_encode(array("success" => true, "retorno" => $retorno));
//    exit();

    $sessao = validarSessao();
    $id_cliente = $sessao['usuario'][0]['id_cliente'];

    $ret_cContratosItens = $dados_cContratosItens->select(" (c.id_cliente = '" . $id_cliente . "' AND c.id_situacao = 3 AND ci.id_servico = '" . $dataPHP->id_servico . "') ");

    if (sizeof($ret_cContratosItens) > 0) {

        $ret_cServicos = $dados_cServicos->select("(id = '" . $dataPHP->id_servico . "')");
        $url_ws = $ret_cServicos[0]['url_ws'];

        $ws_url = $ret_cConfig[0]['ws_url'];
        $ws_usuario = $ret_cConfig[0]['ws_usuario'];
        $ws_senha = $ret_cConfig[0]['ws_senha'];
        $basic = base64_encode($ws_usuario . ":" . $ws_senha);
        $ret = json_decode($dataPHP->registroAtual);

        switch ($ret_cServicos[0]['rota']) {
            case "L0001":
                $doc = $ret->documento;
                $url_servico = $ws_url . str_replace("{documento}", $doc, $url_ws);
                break;
            case "L0002":
                $nome = $ret->nome;
                $url_servico = $ws_url . str_replace("{nome}", $nome, $url_ws);
                break;
            case "L0003":
                $ddd = $ret->ddd;
                $telefone = $ret->telefone;

                $url_servico = $ws_url . $url_ws;
                $url_servico = str_replace("{ddd}", $ddd, $url_servico);
                $url_servico = str_replace("{telefone}", $telefone, $url_servico);
                break;
            case "L0004":
                $url_servico = $ws_url . $url_ws;
                break;
            case "L0006":
                $doc = $ret->documento;
                $url_servico = $ws_url . str_replace("{documento}", $doc, $url_ws);
                break;
            case "L0007":
                $doc = $ret->documento;
                $url_servico = $ws_url . str_replace("{documento}", $doc, $url_ws);
                break;
            case "L0008":
                $doc = $ret->documento;
                $url_servico = $ws_url . str_replace("{documento}", $doc, $url_ws);
                break;
            case "L0011":
                $doc = $ret->documento;
                $url_servico = $ws_url . str_replace("{documento}", $doc, $url_ws);
                break;
            case "L0014":
                $doc = $ret->documento;
                $url_servico = $ws_url . str_replace("{documento}", $doc, $url_ws);
                break;
            case "L0015":
                $doc = $ret->documento;
                $url_servico = $ws_url . str_replace("{documento}", $doc, $url_ws);
                break;
            case "L0022":
                $doc = $ret->documento;
                $url_servico = $ws_url . str_replace("{documento}", $doc, $url_ws);
                break;
            case "L0024":
                $doc = $ret->documento;
                $url_servico = $ws_url . str_replace("{documento}", $doc, $url_ws);
                break;
            case "L0029":
                $cep = $ret->cep;
                $url_servico = $ws_url . str_replace("{cep}", $cep, $url_ws);
                break;
            case "L0030":
                $doc = $ret->documento;
                $url_servico = $ws_url . str_replace("{documento}", $doc, $url_ws);
                break;
            case "L0032":
                $doc = $ret->documento;
                $url_servico = $ws_url . str_replace("{documento}", $doc, $url_ws);
                break;
            case "L0033":
                $doc = $ret->documento;
                $url_servico = $ws_url . str_replace("{documento}", $doc, $url_ws);
                break;
            case "L0035":
                $email = $ret->email;
                $url_servico = $ws_url . str_replace("{email}", $email, $url_ws);
                break;
            case "L0038":
                $url_servico = $ws_url . $url_ws;
                break;
            case "L0039":
                $doc = $ret->documento;
                $url_servico = $ws_url . str_replace("{documento}", $doc, $url_ws);
                break;
            case "L0041":
                $doc = $ret->documento;
                $url_servico = $ws_url . str_replace("{documento}", $doc, $url_ws);
                break;
            case "L0059":
                $doc = $ret->documento;
                $url_servico = $ws_url . str_replace("{documento}", $doc, $url_ws);
                break;
            case "L0064":
                $url_servico = $ws_url . $url_ws;
                break;
            case "L0066":
                $level = $ret->level;
                $url_servico = $ws_url . str_replace("{level}", $level, $url_ws);
                break;
            case "L0067":
                $doc = $ret->documento;
                $url_servico = $ws_url . str_replace("{documento}", $doc, $url_ws);
                break;
            case "L0068":
                $doc = $ret->documento;
                $url_servico = $ws_url . str_replace("{documento}", $doc, $url_ws);
                break;
            case "R0001":
                $doc = $ret->documento;
                $url_servico = $ws_url . str_replace("{documento}", $doc, $url_ws);
                break;
            case "R0002":
                $doc = $ret->documento;
                $url_servico = $ws_url . str_replace("{documento}", $doc, $url_ws);
                break;
            case "R0014":
                $doc = $ret->documento;
                $url_servico = $ws_url . str_replace("{documento}", $doc, $url_ws);
                break;
            case "R0016":
                $doc = $ret->documento;
                $url_servico = $ws_url . str_replace("{documento}", $doc, $url_ws);
                break;
            case "R0019":
                $doc = $ret->documento;
                $url_servico = $ws_url . str_replace("{documento}", $doc, $url_ws);
                break;
            default:
                echo json_encode(array("success" => false, "nivel" => 2, "mensagem" => "Serviço inválido!"));
                exit();
        }

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url_servico,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Authorization: Basic $basic"
            )
        ));

        $retorno = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            $dados->setHistorico($err);
        } else {
            $dados->setHistorico($retorno);
        }

        $dados->setIdSituacao(1);
        $dados->setIdCliente($id_cliente);
        $dados->setIdServico($ret_cContratosItens[0]['id_servico']);
        $dados->setValor($ret_cContratosItens[0]['valor']);
        $dados->setData(date("d/m/Y H:i:s", time()));
        $dados->salvar();

        echo json_encode(array("success" => true, "retorno" => $retorno));
        exit();
    }

    echo json_encode(array("success" => false, "nivel" => 2, "mensagem" => "Nenhum registro encontrado!"));
    exit();
}


?>
