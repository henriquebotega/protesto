<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_CRUD . "cAvisos.php";
$dados = new cAvisos();

$dataPHP = json_decode(file_get_contents("php://input"));

// Valida sessão
if (isBlank(validarSessao())) {
    echo json_encode(array("success" => false, "nivel" => 3, "mensagem" => "A sessão expirou!"));
    exit();
}

if ($dataPHP->acao == "read") {
    $where = "";
    $order = "";
    $more = "";

    executaQuery_cAvisos($where, $order, $more);
}

if ($dataPHP->acao == "salvar") {

    $dados->setTipo(addslashes($dataPHP->registroAtual->tipo));
    $dados->setTitulo(addslashes($dataPHP->registroAtual->titulo));
    $dados->setConteudo(addslashes($dataPHP->registroAtual->conteudo));
    $dados->setIdCliente(addslashes($dataPHP->registroAtual->id_cliente));

    if (isset($dataPHP->registroAtual->id)) {
        $dados->setId(addslashes($dataPHP->registroAtual->id));
    }

    $dados->salvar();
    echo json_encode(array("success" => true, "nivel" => 1));
}

if ($dataPHP->acao == "getByID") {

    $where = " (a.id = '" . addslashes($dataPHP->id) . "') ";
    $order = "";
    $more = " LIMIT 1 ";

    executaQuery_cAvisos($where, $order, $more);
}

if ($dataPHP->acao == "excluir") {

    foreach ($dataPHP->colRegistroAtual as $k => $v) {
        $dados->setId(addslashes($v->id));
        $dados->delete();
    }

    echo json_encode(array("success" => true, "nivel" => 1));
    exit();
}

if ($dataPHP->acao == "definirComoLido") {

    $dados->setId(addslashes($dataPHP->id));
    $dados->updateLido();

    echo json_encode(array("success" => true, "nivel" => 1));
    exit();
}

if ($dataPHP->acao == "getAllEmAberto") {
    $where = " (a.lido = 0) ";
    $order = "";
    $more = "";

    executaQuery_cAvisos($where, $order, $more);
}

if ($dataPHP->acao == "filtrar") {

    if (isset($dataPHP->palavraChave) && isNotBlank($dataPHP->palavraChave)) {
        $where = " (a.titulo LIKE '%" . addslashes($dataPHP->palavraChave) . "%' 
                    OR a.conteudo LIKE '%" . addslashes($dataPHP->palavraChave) . "%') ";
    }

    if (isset($dataPHP->filtroTipo) && sizeof($dataPHP->filtroTipo) > 0) {

        $where = isNotBlank(@$where) ? $where . " AND " : "";
        $where = $where . " (a.tipo IN(";

        foreach ($dataPHP->filtroTipo as $k => $v) {
            $where .= addslashes($v->id) . ",";
        }

        $where = substr($where, 0, -1);
        $where .= ")) ";
    }

    $order = "";
    $more = "";

    executaQuery_cAvisos($where, $order, $more);
}

function executaQuery_cAvisos($where, $order, $more)
{
    global $dados;

    $sessao = validarSessao();
    if (isset($sessao['usuario'])) {
        $where .= (empty($where) ? "" : " AND ");
        $where .= " (a.id_cliente = '" . $sessao['usuario'][0]['id_cliente'] . "') ";
    }

    $retorno = $dados->select($where, $order, $more);

    $retornoArray = array();
    foreach ($retorno as $chave => $valor) {
        $retornoArray[$chave] = $valor;

        $retornoArray[$chave]['grid_titulo'] = $valor['titulo'];
        $retornoArray[$chave]['grid_nome'] = $valor['grid_nome'];
        $retornoArray[$chave]['grid_conteudo'] = $valor['conteudo'];
        $retornoArray[$chave]['grid_tipo'] = tipoAviso($valor['tipo']);
        $retornoArray[$chave]['grid_lido'] = lidoAviso($valor['lido']);
    }

    if (sizeof($retornoArray) > 0) {
        echo json_encode(array("success" => true, "nivel" => 1, "retorno" => $retornoArray, "total" => sizeof($retornoArray)));
    } else {
        echo json_encode(array("success" => false, "nivel" => 2, "mensagem" => "Nenhum registro encontrado!"));
    }

    exit();
}

function tipoAviso($tipo)
{
    return ($tipo == 0) ? "Informação" : (($tipo == 1) ? "Aviso" : "Atenção");
}

function lidoAviso($lido)
{
    return ($lido == 0) ? "Não" : "Sim";
}

?>