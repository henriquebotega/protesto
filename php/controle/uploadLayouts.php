<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_CRUD . "cLayouts.php";
$dados = new cLayouts();

$dataPHP = json_decode(file_get_contents("php://input"));

// Valida sessão
if (isBlank(validarSessao())) {
    echo json_encode(array("success" => false, "nivel" => 3, "mensagem" => "A sessão expirou!"));
    exit();
}

if (isset($_POST['extra']) && isset($_FILES['file']) && $_FILES['file']['error'] == 0 && $_FILES['file']['size'] > 0) {

    $extra = json_decode($_POST['extra']);

    $extensao = RetornaExtensao($_FILES['file']['name']);
    $nomeArquivo = "layout-" . date('Y-m-d_H-i-s', time()) . "." . $extensao;

    if (move_uploaded_file($_FILES['file']['tmp_name'], "../uploads/layouts/" . $nomeArquivo)) {

        // Atualiza tabelas do banco
        $dados->setId($extra->id_layout);
        $dados->setArquivo($nomeArquivo);
        $dados->updateArquivo();
    }

}


?>