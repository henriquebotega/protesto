<?php

$path = "../uploads/exportar/banco/";
$diretorio = dir($path);
$array_itens[] = null;

$i = 0;
while ($arquivo = $diretorio->read()) {
    if ($arquivo != "." && $arquivo != "..") {
        $array_itens[$i] = $arquivo;
        $i++;
    }
}

rsort($array_itens);

echo "<h3 style='background-color: #D5D5D5; padding:5px; font-size:11px'>Clique com o botão direito do mouse sobre o item desejado, e selecione Salvar.</h3>";

foreach ($array_itens as $chave => $arquivo) {
    echo "<a href='" . $path . $arquivo . "' target='_blank'>" . $arquivo . "</a><br />";
}

$diretorio->close();

?>