<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_CRUD . "cAdmin.php";
include_once URL_PHP_CRUD . "cTaxas.php";
include_once URL_PHP_CRUD . "cClientes.php";
include_once URL_PHP_CRUD . "cRemessasCartorio.php";
include_once URL_PHP_CRUD . "cUsuarios.php";
include_once URL_PHP_CRUD . "cDevedores.php";

$dados_cAdmin = new cAdmin();
$dados_cClientes = new cClientes();
$dados_cRemessasCartorio = new cRemessasCartorio();
$dados_cTaxas = new cTaxas();
$dados_cUsuarios = new cUsuarios();
$dados_cDevedores = new cDevedores();

$dataPHP = json_decode(file_get_contents("php://input"));

// Valida sessão
if (isBlank(validarSessao())) {
    echo json_encode(array("success" => false, "nivel" => 3, "mensagem" => "A sessão expirou!"));
    exit();
}

if ($dataPHP->acao == "getConsumoMensalPorCliente") {

    $retorno = $dados_cTaxas->selectConsumoMensalPorCliente();

    $arrayRetorno = array();
    for ($k = 0; $k < sizeof($retorno); $k++) {

        $mes = $retorno[$k]['mes'];
        $total = $retorno[$k]['total'];
        $id_cliente = $retorno[$k]['id_cliente'];

        $ret_cClientes = $dados_cClientes->select(" (c.id = '" . $id_cliente . "') ", "", "");
        $nome_cliente = $ret_cClientes[0]['grid_nome'];

        $arrayRetorno[$k]['mes'] = retornaMes($mes);
        $arrayRetorno[$k]['cliente'] = $nome_cliente;
        $arrayRetorno[$k]['total'] = (int)$total;
    }

    $colRetorno = array_values($arrayRetorno);

    $limite = $ret_cConfig[0]['exibir_grafico_ultimos_meses'];
    while (sizeof($colRetorno) > $limite) {
        array_shift($colRetorno);
    }

    if (sizeof($colRetorno) > 0) {
        echo json_encode(array("success" => true, "nivel" => 1, "retorno" => $colRetorno, "total" => sizeof($colRetorno)));
    } else {
        echo json_encode(array("success" => false, "nivel" => 2, "mensagem" => "Nenhum registro encontrado!"));
    }

    exit();
}

if ($dataPHP->acao == "getConsumoMensalPorWS") {

    $retorno = $dados_cTaxas->selectConsumoMensalPorWS();

    $arrayRetorno = array();
    for ($k = 0; $k < sizeof($retorno); $k++) {

        $mes = $retorno[$k]['mes'];
        $total = $retorno[$k]['total'];
        $id_servico = $retorno[$k]['id_servico'];

        if (!isset($arrayRetorno[$mes])) {
            $arrayRetorno[$mes]['mes'] = retornaMes($mes);
            $arrayRetorno[$mes]['ws' . $id_servico] = (int)$total;
        } else {
            $arrayRetorno[$mes]['ws' . $id_servico] = (int)$total;
        }
    }

    foreach ($arrayRetorno as $chave => $valor) {
        for ($j = 2; $j <= 15; $j++) {
            if (!isset($valor['ws' . $j])) {
                $arrayRetorno[$chave]['ws' . $j] = 0;
            }
        }
    }

    $colRetorno = array_values($arrayRetorno);

    $limite = $ret_cConfig[0]['exibir_grafico_ultimos_meses'];
    while (sizeof($colRetorno) > $limite) {
        array_shift($colRetorno);
    }

    if (sizeof($colRetorno) > 0) {
        echo json_encode(array("success" => true, "nivel" => 1, "retorno" => $colRetorno, "total" => sizeof($colRetorno)));
    } else {
        echo json_encode(array("success" => false, "nivel" => 2, "mensagem" => "Nenhum registro encontrado!"));
    }

    exit();
}

if ($dataPHP->acao == "totalRegistros") {
    $ret_cAdmin = $dados_cAdmin->totalRegistros();
    $ret_cClientesF = $dados_cClientes->totalRegistrosF();
    $ret_cClientesJ = $dados_cClientes->totalRegistrosJ();

    $retornoArray = array();

    if ($ret_cAdmin[0]['total'] > 0) {
        array_push($retornoArray, array("titulo" => "Administradores", "valor" => $ret_cAdmin[0]['total']));
    }

    if ($ret_cClientesF[0]['total'] > 0) {
        array_push($retornoArray, array("titulo" => "Clientes (Pessoa Física)", "valor" => $ret_cClientesF[0]['total']));
    }

    if ($ret_cClientesJ[0]['total'] > 0) {
        array_push($retornoArray, array("titulo" => "Clientes (Pessoa Jurídica)", "valor" => $ret_cClientesJ[0]['total']));
    }

    if (sizeof($retornoArray) > 0) {
        echo json_encode(array("success" => true, "nivel" => 1, "retorno" => $retornoArray, "total" => sizeof($retornoArray)));
    } else {
        echo json_encode(array("success" => false, "nivel" => 2, "mensagem" => "Nenhum registro encontrado!"));
    }

    exit();
}

if ($dataPHP->acao == "totalRemessasCartorio") {

    $retorno = $dados_cRemessasCartorio->totalRegistros();

    if (sizeof($retorno) > 0) {
        echo json_encode(array("success" => true, "nivel" => 1, "retorno" => $retorno, "total" => sizeof($retorno)));
    } else {
        echo json_encode(array("success" => false, "nivel" => 2, "mensagem" => "Nenhum registro encontrado!"));
    }

    exit();
}

if ($dataPHP->acao == "totalUsuariosPorCliente") {

    $retorno = $dados_cUsuarios->totalUsuariosPorCliente();

    $arrayRetorno = array();
    foreach ($retorno as $chave => $valor) {
        $arrayRetorno[$chave] = $valor;

        $ret_cClientes = $dados_cClientes->select(" (c.id = '" . $valor['id'] . "') ", "", "");
        $nome_cliente = $ret_cClientes[0]['grid_nome'];

        $arrayRetorno[$chave]['cliente'] = $nome_cliente;
    }

    if (sizeof($arrayRetorno) > 0) {
        echo json_encode(array("success" => true, "nivel" => 1, "retorno" => $arrayRetorno, "total" => sizeof($arrayRetorno)));
    } else {
        echo json_encode(array("success" => false, "nivel" => 2, "mensagem" => "Nenhum registro encontrado!"));
    }

    exit();
}

if ($dataPHP->acao == "totalDevedoresPorCliente") {

    $retorno = $dados_cDevedores->totalDevedoresPorCliente();

    $arrayRetorno = array();
    foreach ($retorno as $chave => $valor) {
        $arrayRetorno[$chave] = $valor;

        $ret_cClientes = $dados_cClientes->select(" (c.id = '" . $valor['id'] . "') ", "", "");
        $nome_cliente = $ret_cClientes[0]['grid_nome'];

        $arrayRetorno[$chave]['cliente'] = $nome_cliente;
    }

    if (sizeof($arrayRetorno) > 0) {
        echo json_encode(array("success" => true, "nivel" => 1, "retorno" => $arrayRetorno, "total" => sizeof($arrayRetorno)));
    } else {
        echo json_encode(array("success" => false, "nivel" => 2, "mensagem" => "Nenhum registro encontrado!"));
    }

    exit();
}

?>