<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_CRUD . "cLayoutsImportados.php";
include_once URL_PHP_CRUD . "cClientes.php";
$dados = new cLayoutsImportados();
$dados_cClientes = new cClientes();

$dataPHP = json_decode(file_get_contents("php://input"));

// Valida sessão
if (isBlank(validarSessao())) {
    echo json_encode(array("success" => false, "nivel" => 3, "mensagem" => "A sessão expirou!"));
    exit();
}

if ($dataPHP->acao == "read") {
    $where = "";
    $order = "";
    $more = "";

    executaQuery_cLayoutsImportados($where, $order, $more);
}

if ($dataPHP->acao == "salvar") {

    $sessao = validarSessao();
    if (isset($sessao['usuario'])) {
        $id_cliente = $sessao['usuario'][0]['id_cliente'];
    } else {
        $id_cliente = addslashes($dataPHP->registroAtual->id_cliente);
    }

    $dados->setIdCliente($id_cliente);
    $dados->setIdLayout(addslashes($dataPHP->registroAtual->id_layout));
    $dados->salvar();

    $ultimo_id = $dados->ultimo_registro();
    $id_layout_importado = $ultimo_id[0]['id'];

    echo json_encode(array("success" => true, "nivel" => 1, "id_layout_importado" => $id_layout_importado));
}

if ($dataPHP->acao == "excluir") {

    foreach ($dataPHP->colRegistroAtual as $k => $v) {
        removerLayoutImportado($v->id);

        $dados->setId(addslashes($v->id));
        $dados->delete();
    }

    echo json_encode(array("success" => true, "nivel" => 1));
    exit();
}

function removerLayoutImportado($id)
{
    global $dados;
    $retorno = $dados->select(" (li.id = '" . $id . "') ");

    if (sizeof($retorno) > 0) {
        @unlink("../uploads/layouts_importados/" . $retorno[0]['arquivo']);
    }
}

function executaQuery_cLayoutsImportados($where, $order, $more)
{
    global $dados, $dados_cClientes;

    $sessao = validarSessao();
    if (isset($sessao['usuario'])) {
        $where .= (empty($where) ? "" : " AND ");
        $where .= " (li.id_cliente = '" . $sessao['usuario'][0]['id_cliente'] . "') ";
    }

    $retorno = $dados->select($where, $order, $more);

    $retornoArray = array();
    foreach ($retorno as $chave => $valor) {
        $retornoArray[$chave] = $valor;

        $ret_cClientes = $dados_cClientes->select(" (c.id = '" . $valor['id_cliente'] . "') ", "", "");
        $retornoArray[$chave]['grid_cliente'] = $ret_cClientes[0]['grid_nome'];

        $retornoArray[$chave]['grid_layout'] = $valor['titulo'];
        $retornoArray[$chave]['grid_total_registros'] = $valor['total_registros'];
        $retornoArray[$chave]['grid_total_registros_sucesso'] = $valor['total_registros_sucesso'];
        $retornoArray[$chave]['grid_data'] = formatar_DataHora($valor['data']);
    }

    if (sizeof($retornoArray) > 0) {
        echo json_encode(array("success" => true, "nivel" => 1, "retorno" => $retornoArray, "total" => sizeof($retornoArray)));
    } else {
        echo json_encode(array("success" => false, "nivel" => 2, "mensagem" => "Nenhum registro encontrado!"));
    }

    exit();
}

?>