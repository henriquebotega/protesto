<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_CRUD . "cAdmin.php";
include_once URL_PHP_CRUD . "cSessao.php";
include_once URL_PHP_CRUD . "cUsuarios.php";
include_once URL_PHP_CRUD . "cClientes.php";
include_once URL_PHP_CRUD . "cContratos.php";

$dados = new cAdmin();
$dados_cSessao = new cSessao();
$dados_cUsuarios = new cUsuarios();
$dados_cClientes = new cClientes();
$dados_cContratos = new cContratos();

$dataPHP = json_decode(file_get_contents("php://input"));

if ($dataPHP->acao == "logar") {

    if (isset($dataPHP->debugTest) && !($dataPHP->debugTest)) {
        $postdata = http_build_query(
            array(
                'secret' => '6LcGCSYTAAAAAJwX7LWwD5hKrB8O4iG4dlYrKy7M',
                'response' => $dataPHP->tokenRecaptcha,
                'remoteip' => $_SERVER['REMOTE_ADDR']
            )
        );

        $opts = array('http' =>
            array(
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => $postdata
            )
        );

        $context = stream_context_create($opts);

        $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify", false, $context);
        $response = json_decode($response, true);
    } else {
        $response["success"] = true;
        $dataPHP->tokenRecaptcha = "";
    }

    if ($response["success"] === false) {
        echo json_encode(array("success" => false, "nivel" => 2, "mensagem" => "Token reCAPTCHA inválido!"));

    } else {

        $token = null;
        $retorno = $dados->logar(addslashes($dataPHP->email), addslashes($dataPHP->senha));

        if (sizeof($retorno) > 0) {

            if ($retorno[0]['id_situacao'] == 1) {

                if ($retorno[0]['id_tipo'] == 0) {

                    $ret_cUsuarios = $dados_cUsuarios->select(" (id_admin = '" . $retorno[0]['id'] . "') ");
                    $ret_cClientes = $dados_cClientes->select(" (c.id = '" . $ret_cUsuarios[0]['id_cliente'] . "') ");

                    if ($ret_cClientes[0]['id_situacao'] == 0) {
                        echo json_encode(array("success" => false, "nivel" => 2, "mensagem" => "Cliente bloqueado!"));
                    } else {

                        $ret_cContratos = $dados_cContratos->select(" (cli.id = '" . $ret_cClientes[0]['id'] . "' AND c.id_situacao = 3) ");

                        if (sizeof($ret_cContratos) > 0) {
                            $retorno[0]['id_cliente'] = $ret_cClientes[0]['id'];
                            loginOn($retorno);
                        } else {
                            echo json_encode(array("success" => false, "nivel" => 2, "mensagem" => "Contrato não está finalizado!"));
                        }
                    }

                } else {
                    loginOn($retorno);
                }

            } else {
                echo json_encode(array("success" => false, "nivel" => 2, "mensagem" => "Usuário bloqueado!"));
            }


        } else {
            echo json_encode(array("success" => false, "nivel" => 2, "mensagem" => "Os dados informados são inválidos!"));
        }
    }

    exit();
}

function loginOn($retorno)
{
    global $dados, $dados_cSessao;

    // Atualiza último acesso do Usuário
    $dados->setId($retorno[0]['id']);
    $dados->atualizar_ultimo_acesso();

    $token = $dados_cSessao->gerarCodigo();

    $dados_cSessao->setIdAdmin($retorno[0]['id']);
    $dados_cSessao->setCodigo($token);
    $dados_cSessao->salvar();

    echo json_encode(array("success" => true, "nivel" => 1, "retorno" => $retorno, "total" => sizeof($retorno), "token" => $token));
}

if ($dataPHP->acao == "read") {
    $where = "";
    $order = "";
    $more = "";

    $retorno = $dados->select($where, $order, $more);
    $total = sizeof((empty($more)) ? $retorno : $dados->select($where, $order));

    if (sizeof($retorno) > 0) {
        echo json_encode(array("success" => true, "nivel" => 1, "retorno" => $retorno, "total" => $total));
    } else {
        echo json_encode(array("success" => false, "nivel" => 2, "mensagem" => "Nenhum registro encontrado!"));
    }

    exit();
}

if ($dataPHP->acao == "recuperar_senha") {
    $where = " (email = '" . addslashes($dataPHP->email) . "') ";
    $order = "";
    $more = " LIMIT 1 ";

    $retorno = $dados->select($where, $order, $more);
    $total = sizeof((empty($more)) ? $retorno : $dados->select($where, $order));

    if (sizeof($retorno) > 0) {
        recuperar_senha($retorno[0]);

        echo json_encode(array("success" => true, "nivel" => 1));
    } else {
        echo json_encode(array("success" => false, "nivel" => 2, "mensagem" => "Os dados informados são inválidos!"));
    }

    exit();
}

if ($dataPHP->acao == "email_ja_existe") {
    $where = " (email = '" . addslashes($dataPHP->email) . "') ";
    $order = " id_situacao DESC ";
    $more = " LIMIT 1 ";

    $retorno = $dados->select($where, $order, $more);
    $total = (empty($more) ? sizeof($retorno) : sizeof($dados->select($where, $order)));

    if (sizeof($retorno) > 0) {
        echo json_encode(array("success" => true, "nivel" => 1, "retorno" => $retorno));
    } else {
        echo json_encode(array("success" => false, "nivel" => 2, "mensagem" => "Registro inexistente!"));
    }

    exit();
}

function recuperar_senha($retorno)
{
    global $dados, $dados_cSessao;

    $novaSenha = $dados_cSessao->gerarCodigo();
    $dados->setSenha($novaSenha);
    $dados->setId($retorno['id']);
    $dados->upd_Senha();

    $assunto = "Solicitação de recuperação de senha";
    $titulo = "Recebemos uma solicitação de recuperação de senha";
    $mensagem = "Sua nova senha é: <b>" . $novaSenha . "</b>";

    $email = $retorno['email'];
    //EnviarEmail($email, $assunto, $titulo, $mensagem, false);
}

?>