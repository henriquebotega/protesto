<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_CRUD . "cContatos.php";
include_once URL_PHP_CRUD . "cClientes.php";
$dados = new cContatos();
$dados_cClientes = new cClientes();

$dataPHP = json_decode(file_get_contents("php://input"));

// Valida sessão
if (isBlank(validarSessao())) {
    echo json_encode(array("success" => false, "nivel" => 3, "mensagem" => "A sessão expirou!"));
    exit();
}

if ($dataPHP->acao == "read") {
    $where = "";
    $order = "";
    $more = "";

    executaQuery_cContatos($where, $order, $more);
}

if ($dataPHP->acao == "getByID") {

    $where = " (id = '" . addslashes($dataPHP->id) . "') ";
    $order = "";
    $more = " LIMIT 1 ";

    executaQuery_cContatos($where, $order, $more);
}

if ($dataPHP->acao == "salvar") {

    $sessao = validarSessao();
    if (isset($sessao['usuario'])) {
        $id_cliente = $sessao['usuario'][0]['id_cliente'];
    } else {
        $id_cliente = addslashes($dataPHP->registroAtual->id_cliente);
    }

    $dados->setTitulo(addslashes($dataPHP->registroAtual->titulo));
    $dados->setMensagem(addslashes($dataPHP->registroAtual->mensagem));
    $dados->setIdCliente($id_cliente);

    if (isset($dataPHP->registroAtual->id)) {
        $dados->setId(addslashes($dataPHP->registroAtual->id));
    }

    $dados->salvar();
    echo json_encode(array("success" => true, "nivel" => 1));
}

if ($dataPHP->acao == "excluir") {

    foreach ($dataPHP->colRegistroAtual as $k => $v) {
        $dados->setId(addslashes($v->id));
        $dados->delete();
    }

    echo json_encode(array("success" => true, "nivel" => 1));
    exit();
}

if ($dataPHP->acao == "filtrar") {

    if (isset($dataPHP->palavraChave) && isNotBlank($dataPHP->palavraChave)) {
        $where = " (titulo LIKE '%" . addslashes($dataPHP->palavraChave) . "%' 
                    OR mensagem LIKE '%" . addslashes($dataPHP->palavraChave) . "%') ";
    }

    $order = "";
    $more = "";

    executaQuery_cContatos($where, $order, $more);
}

function executaQuery_cContatos($where, $order, $more)
{
    global $dados, $dados_cClientes;

    $sessao = validarSessao();
    if (isset($sessao['usuario'])) {
        $where .= (empty($where) ? "" : " AND ");
        $where .= " (id_cliente = '" . $sessao['usuario'][0]['id_cliente'] . "') ";
    }

    $retorno = $dados->select($where, $order, $more);

    $retornoArray = array();
    foreach ($retorno as $chave => $valor) {
        $retornoArray[$chave] = $valor;

        $ret_cClientes = $dados_cClientes->select(" (c.id = '" . $valor['id_cliente'] . "') ", "", "");
        $retornoArray[$chave]['grid_cliente'] = $ret_cClientes[0]['grid_nome'];
        $retornoArray[$chave]['grid_titulo'] = $valor['titulo'];
        $retornoArray[$chave]['grid_mensagem'] = $valor['mensagem'];
        $retornoArray[$chave]['grid_data'] = formatar_DataHora($valor['data']);
    }

    if (sizeof($retornoArray) > 0) {
        echo json_encode(array("success" => true, "nivel" => 1, "retorno" => $retornoArray, "total" => sizeof($retornoArray)));
    } else {
        echo json_encode(array("success" => false, "nivel" => 2, "mensagem" => "Nenhum registro encontrado!"));
    }

    exit();
}

?>