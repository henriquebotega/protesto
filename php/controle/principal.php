<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_CRUD . "cTaxas.php";
include_once URL_PHP_CRUD . "cDividas.php";
include_once URL_PHP_CRUD . "cRemessasCartorio.php";
$dados_cTaxas = new cTaxas();
$dados_cDividas = new cDividas();
$dados_cRemessasCartorio = new cRemessasCartorio();

$dataPHP = json_decode(file_get_contents("php://input"));

// Valida sessão
if (isBlank(validarSessao())) {
    echo json_encode(array("success" => false, "nivel" => 3, "mensagem" => "A sessão expirou!"));
    exit();
}

if ($dataPHP->acao == "getConsumoMensalWS") {

    $sessao = validarSessao();
    $id_cliente = $sessao['usuario'][0]['id_cliente'];

    $retorno = $dados_cTaxas->selectConsumoMensalWS($id_cliente);

    $arrayRetorno = array();
    for ($k = 0; $k < sizeof($retorno); $k++) {

        $mes = $retorno[$k]['mes'];
        $total = $retorno[$k]['total'];
        $id_servico = $retorno[$k]['id_servico'];

        if (!isset($arrayRetorno[$mes])) {
            $arrayRetorno[$mes]['mes'] = retornaMes($mes);
            $arrayRetorno[$mes]['ws' . $id_servico] = (int)$total;
        } else {
            $arrayRetorno[$mes]['ws' . $id_servico] = (int)$total;
        }
    }

    foreach ($arrayRetorno as $chave => $valor) {
        for ($j = 2; $j <= 15; $j++) {
            if (!isset($valor['ws' . $j])) {
                $arrayRetorno[$chave]['ws' . $j] = 0;
            }
        }
    }
    
    $colRetorno = array_values($arrayRetorno);

    $limite = $ret_cConfig[0]['exibir_grafico_ultimos_meses'];
    while (sizeof($colRetorno) > $limite) {
        array_shift($colRetorno);
    }

    if (sizeof($colRetorno) > 0) {
        echo json_encode(array("success" => true, "nivel" => 1, "retorno" => $colRetorno, "total" => sizeof($colRetorno)));
    } else {
        echo json_encode(array("success" => false, "nivel" => 2, "mensagem" => "Nenhum registro encontrado!"));
    }

    exit();
}

if ($dataPHP->acao == "totalDividas") {
    
    $sessao = validarSessao();
    $id_cliente = $sessao['usuario'][0]['id_cliente'];

    $retorno = $dados_cDividas->totalRegistrosPorCliente($id_cliente);

    if (sizeof($retorno) > 0) {
        echo json_encode(array("success" => true, "nivel" => 1, "retorno" => $retorno, "total" => sizeof($retorno)));
    } else {
        echo json_encode(array("success" => false, "nivel" => 2, "mensagem" => "Nenhum registro encontrado!"));
    }

    exit();
}

if ($dataPHP->acao == "totalRemessasCartorio") {

    $sessao = validarSessao();
    $id_cliente = $sessao['usuario'][0]['id_cliente'];

    $retorno = $dados_cRemessasCartorio->totalRegistrosPorCliente($id_cliente);

    if (sizeof($retorno) > 0) {
        echo json_encode(array("success" => true, "nivel" => 1, "retorno" => $retorno, "total" => sizeof($retorno)));
    } else {
        echo json_encode(array("success" => false, "nivel" => 2, "mensagem" => "Nenhum registro encontrado!"));
    }

    exit();
}

?>