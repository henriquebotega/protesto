<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_CRUD . "cContatosInteracoes.php";
$dados = new cContatosInteracoes();

$dataPHP = json_decode(file_get_contents("php://input"));

// Valida sessão
if (isBlank(validarSessao())) {
    echo json_encode(array("success" => false, "nivel" => 3, "mensagem" => "A sessão expirou!"));
    exit();
}

if ($dataPHP->acao == "read") {
    $where = " (ci.id_contato = '" . addslashes($dataPHP->id_contato) . "') ";
    $order = "";
    $more = "";

    executaQuery_cContatosInteracoes($where, $order, $more);
}

if ($dataPHP->acao == "salvar") {

    $sessao = validarSessao();

    if (isset($sessao['admin']) && $sessao['admin'][0]['id_tipo'] == 1) {
        $id_admin = $sessao['admin'][0]['id'];
    } else {
        $id_admin = 0;
    }

    $dados->setMensagem(addslashes($dataPHP->registroAtual->mensagem));
    $dados->setIdContato(addslashes($dataPHP->id_contato));
    $dados->setIdAdmin($id_admin);

    if (isset($dataPHP->registroAtual->id)) {
        $dados->setId(addslashes($dataPHP->registroAtual->id));
    }

    $dados->salvar();
    echo json_encode(array("success" => true, "nivel" => 1));
}

if ($dataPHP->acao == "excluir") {

    foreach ($dataPHP->colRegistroAtual as $k => $v) {
        $dados->setId(addslashes($v->id));
        $dados->delete();
    }

    echo json_encode(array("success" => true, "nivel" => 1));
    exit();
}

if ($dataPHP->acao == "getByID") {

    $where = " (ci.id_contato = '" . addslashes($dataPHP->id_contato) . "' AND ci.id = '" . addslashes($dataPHP->id) . "') ";
    $order = "";
    $more = " LIMIT 1 ";

    executaQuery_cContatosInteracoes($where, $order, $more);
}

if ($dataPHP->acao == "filtrar") {

    $where = " (ci.id_contato = '" . addslashes($dataPHP->id_contato) . "') ";

    if (isset($dataPHP->palavraChave) && isNotBlank($dataPHP->palavraChave)) {
        $where .= " AND (ci.mensagem LIKE '%" . addslashes($dataPHP->palavraChave) . "%') ";
    }

    $order = "";
    $more = "";

    executaQuery_cContatosInteracoes($where, $order, $more);
}

function executaQuery_cContatosInteracoes($where, $order, $more)
{
    global $dados;

    $sessao = validarSessao();
    if (isset($sessao['usuario'])) {
        $where .= (empty($where) ? "" : " AND ");
        $where .= " (c.id_cliente = '" . $sessao['usuario'][0]['id_cliente'] . "') ";
    }

    $retorno = $dados->select($where, $order, $more);

    $retornoArray = array();
    foreach ($retorno as $chave => $valor) {
        $retornoArray[$chave] = $valor;

        $retornoArray[$chave]['grid_mensagem'] = $valor['mensagem'];
        $retornoArray[$chave]['grid_resposta'] = ($valor['id_admin'] > 0)? 'Administrador' : 'Cliente';
        $retornoArray[$chave]['grid_data'] = formatar_DataHora($valor['data']);
    }

    if (sizeof($retornoArray) > 0) {
        echo json_encode(array("success" => true, "nivel" => 1, "retorno" => $retornoArray, "total" => sizeof($retornoArray)));
    } else {
        echo json_encode(array("success" => false, "nivel" => 2, "mensagem" => "Nenhum registro encontrado!"));
    }

    exit();
}

?>