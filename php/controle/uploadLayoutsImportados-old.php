<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_CRUD . "cLayoutsImportados.php";
include_once URL_PHP_CRUD . "cClientes.php";
include_once URL_PHP_CRUD . "cClientesPF.php";
include_once URL_PHP_CRUD . "cClientesPJ.php";
include_once URL_PHP_CRUD . "cClientesContatos.php";
include_once URL_PHP_CRUD . "cCidades.php";
$dados = new cLayoutsImportados();
$dados_cClientes = new cClientes();
$dados_cClientesPF = new cClientesPF();
$dados_cClientesPJ = new cClientesPJ();
$dados_cClientesContatos = new cClientesContatos();
$dados_cCidades = new cCidades();

$dataPHP = json_decode(file_get_contents("php://input"));

// Valida sessão
if (isBlank(validarSessao())) {
    echo json_encode(array("success" => false, "nivel" => 3, "mensagem" => "A sessão expirou!"));
    exit();
}

if (isset($_POST['extra']) && isset($_FILES['file']) && $_FILES['file']['error'] == 0 && $_FILES['file']['size'] > 0) {

    $extra = json_decode($_POST['extra']);

    $extensao = RetornaExtensao($_FILES['file']['name']);
    $nomeArquivo = "layout-" . date('Y-m-d_H-i-s', time()) . "." . $extensao;

    if (move_uploaded_file($_FILES['file']['tmp_name'], "../uploads/layouts_importados/" . $nomeArquivo)) {

        // Atualiza tabelas do banco
        $dados->setId($extra->id_layout_importado);
        $dados->setArquivo($nomeArquivo);
        $dados->updateArquivo();

        // Inicia a importação de dados
        importarClientes($extra->id_layout_importado, $nomeArquivo);

        /*
        // Modelo de cliente
        if ($extra->id_layout_importado == 1) {
            importarClientes("../uploads/layouts_importados/" . $nomeArquivo);
        }

        // Modelo de divida
        if ($extra->id_layout_importado == 2) {
            importarDividas("../uploads/layouts_importados/" . $nomeArquivo);
        }

        */
    }

}

function importarClientes($id_layout_importado, $nomeArquivo)
{
    global $dados, $dados_cCidades, $dados_cClientes, $dados_cClientesPF, $dados_cClientesPJ, $dados_cClientesContatos;

    $abraArq = fopen("../uploads/layouts_importados/" . $nomeArquivo, "r");
    $totalRegistros = 0;

    if ($abraArq) {
        $linha = 0;

        while ($valores = fgetcsv($abraArq, 2048, ";")) {

            if ($linha > 0 && !empty($valores[0])) {
                $jaExiste = false;

                if (strlen(str_replace(array(".", "-", "/"), "", $valores[0])) == 11) {
                    // CPF
                    $tipo = 1;

                    $ret_cClientesPF = $dados_cClientesPF->select(" (cpf = '" . $valores[0] . "') ", "", "");
                    if (sizeof($ret_cClientesPF) > 0) {
                        $jaExiste = true;
                    }
                } else {
                    // CNPJ
                    $tipo = 0;

                    $ret_cClientesPJ = $dados_cClientesPJ->select(" (cnpj = '" . $valores[0] . "') ", "", "");
                    if (sizeof($ret_cClientesPJ) > 0) {
                        $jaExiste = true;
                    }
                }

                if (!$jaExiste) {
                    $idCidade = 0;

                    $ret_cCidades = $dados_cCidades->select(" (LOWER(titulo) = '" . strtolower($valores[10]) . "') ", "", "");
                    if (sizeof($ret_cCidades) > 0) {
                        $idCidade = $ret_cCidades[0]['id'];
                    }

                    $dados_cClientes->setIdTipo($tipo);
                    $dados_cClientes->setIdSituacao(1);
                    $dados_cClientes->setCep(addslashes($valores[3]));
                    $dados_cClientes->setLogradouro(addslashes(utf8_encode($valores[4])));
                    $dados_cClientes->setNumero(addslashes($valores[5]));
                    $dados_cClientes->setComplemento("");
                    $dados_cClientes->setBairro(addslashes(utf8_encode($valores[6])));
                    $dados_cClientes->setIdCidade($idCidade);
                    $dados_cClientes->salvar();

                    $retorno = $dados_cClientes->ultimo_registro();
                    $idCliente = $retorno[0]["id"];

                    if (!empty($valores[2]) || !empty($valores[7])) {
                        $dados_cClientesContatos->setTel1($valores[2]);
                        $dados_cClientesContatos->setEmail1($valores[7]);
                        $dados_cClientesContatos->setContato1("");
                        $dados_cClientesContatos->setTel2("");
                        $dados_cClientesContatos->setEmail2("");
                        $dados_cClientesContatos->setContato2("");
                        $dados_cClientesContatos->setTel3("");
                        $dados_cClientesContatos->setEmail3("");
                        $dados_cClientesContatos->setContato3("");
                        $dados_cClientesContatos->setIdCliente($idCliente);
                        $dados_cClientesContatos->salvar();
                    }

                    if ($tipo == 1) {
                        $dados_cClientesPF->setNome(addslashes(utf8_encode($valores[1])));
                        $dados_cClientesPF->setCpf(addslashes($valores[0]));
                        $dados_cClientesPF->setRg(addslashes($valores[8]));
                        $dados_cClientesPF->setDataNascimento(addslashes(str_replace("-", "/", $valores[9])));
                        $dados_cClientesPF->setIdCliente($idCliente);
                        $dados_cClientesPF->salvar();
                    }

                    if ($tipo == 0) {
                        $dados_cClientesPJ->setRazaoSocial(addslashes(utf8_encode($valores[1])));
                        $dados_cClientesPJ->setNomeFantasia(addslashes(utf8_encode($valores[1])));
                        $dados_cClientesPJ->setCnpj(addslashes($valores[0]));
                        $dados_cClientesPJ->setIe(addslashes($valores[8]));
                        $dados_cClientesPJ->setIdCliente($idCliente);
                        $dados_cClientesPJ->salvar();
                    }

                    $totalRegistros++;
                }
            }

            $linha++;
        }

        fclose($abraArq);
    }

    // Atualiza tabelas do banco
    $dados->setId($id_layout_importado);
    $dados->setTotalRegistros($totalRegistros);
    $dados->updateImportacao();
}

function importarDividas($arquivo)
{

}

?>