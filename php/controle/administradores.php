<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_CRUD . "cAdmin.php";
$dados = new cAdmin();

$dataPHP = json_decode(file_get_contents("php://input"));

// Valida sessão
if (isBlank(validarSessao())) {
    echo json_encode(array("success" => false, "nivel" => 3, "mensagem" => "A sessão expirou!"));
    exit();
}

if ($dataPHP->acao == "read") {
    $where = " (a.id_tipo = 1) ";
    $order = "";
    $more = "";

    executaQuery_cAdmin($where, $order, $more);
}

if ($dataPHP->acao == "salvar") {

    $dados->setEmail(addslashes($dataPHP->registroAtual->email));
    $dados->setIdSituacao(addslashes($dataPHP->registroAtual->id_situacao));
    $dados->setIdTipo(1);

    if (isset($dataPHP->registroAtual->id)) {
        $dados->setId(addslashes($dataPHP->registroAtual->id));
    }

    $dados->salvar();
    echo json_encode(array("success" => true, "nivel" => 1));
}

if ($dataPHP->acao == "excluir") {

    foreach ($dataPHP->colRegistroAtual as $k => $v) {
        $dados->setId(addslashes($v->id));
        $dados->delete();
    }

    echo json_encode(array("success" => true, "nivel" => 1));
    exit();
}

if ($dataPHP->acao == "getByID") {

    $where = " (a.id_tipo = 1 AND a.id = '" . addslashes($dataPHP->id) . "') ";
    $order = "";
    $more = " LIMIT 1 ";

    executaQuery_cAdmin($where, $order, $more);
}

if ($dataPHP->acao == "salvarNovaSenha") {

    if(empty($dataPHP->registroAtual->senha_atual)){
        $where = " (a.id_tipo = '" . addslashes($dataPHP->registroAtual->id_tipo) . "' AND a.id = '" . addslashes($dataPHP->registroAtual->id) . "' AND a.senha = '') ";
        $retorno = $dados->select($where, "", "");
    } else {
        $where = " (a.id_tipo = '" . addslashes($dataPHP->registroAtual->id_tipo) . "' AND a.id = '" . addslashes($dataPHP->registroAtual->id) . "' AND a.senha = '" . md5(addslashes($dataPHP->registroAtual->senha_atual)) . "') ";
        $retorno = $dados->select($where, "", "");
    }

    if (sizeof($retorno) > 0) {
        $dados->setId(addslashes($dataPHP->registroAtual->id));
        $dados->setSenha(addslashes($dataPHP->registroAtual->nova_senha));
        $dados->upd_Senha();

        echo json_encode(array("success" => true, "nivel" => 1));
    } else {
        echo json_encode(array("success" => false, "nivel" => 2, "mensagem" => "A senha informada não confere!"));
    }

    exit();
}

if ($dataPHP->acao == "filtrar") {

    $where = " a.id_tipo = 1 ";

    if (isset($dataPHP->palavraChave) && isNotBlank($dataPHP->palavraChave)) {
        $where .= " AND a.email LIKE '%" . addslashes($dataPHP->palavraChave) . "%' ";
    }

    if (isset($dataPHP->filtroSituacao) && sizeof($dataPHP->filtroSituacao) > 0) {

        $where = $where . " AND (a.id_situacao IN(";

        foreach ($dataPHP->filtroSituacao as $k => $v) {
            $where .= addslashes($v->id) . ",";
        }

        $where = substr($where, 0, -1);
        $where .= ")) ";
    }

    $order = "";
    $more = "";

    executaQuery_cAdmin($where, $order, $more);
}

function executaQuery_cAdmin($where, $order, $more)
{
    global $dados;
    $retorno = $dados->select($where, $order, $more);

    $retornoArray = array();
    foreach ($retorno as $chave => $valor) {
        $retornoArray[$chave] = $valor;

        $retornoArray[$chave]['grid_email'] = $valor['email'];
        $retornoArray[$chave]['grid_situacao'] = ($valor['id_situacao'] == 0) ? "Desativado" : "Ativado";

        if (!empty($valor['data_ultimo_acesso'])) {
            $retornoArray[$chave]['grid_ultimo_acesso'] = formatar_DataHora($valor['data_ultimo_acesso']);
        }
    }

    if (sizeof($retorno) > 0) {
        echo json_encode(array("success" => true, "nivel" => 1, "retorno" => $retornoArray, "total" => sizeof($retornoArray)));
    } else {
        echo json_encode(array("success" => false, "nivel" => 2, "mensagem" => "Nenhum registro encontrado!"));
    }

    exit();
}
?>