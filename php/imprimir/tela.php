<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once "../mPDF/mpdf.php";
include_once URL_PHP_CRUD . "cClientes.php";
include_once URL_PHP_CRUD . "cDividas.php";
include_once URL_PHP_CRUD . "cDevedores.php";
$dados_cClientes = new cClientes();
$dados_cDividas = new cDividas();
$dados_cDevedores = new cDevedores();

$id_divida = $_REQUEST['id_divida'];
$tipoDoc = $_REQUEST['tipoDoc'];

$colID = explode("-", $id_divida);

$erroConteudo = "";
$conteudo = "<html><body>";

$conteudo .= "<style>
                    .letraG, .letraM, .letraP {
                        font-weight: bold; 
                        color: #222222;
                    }
                    .letraG {
                        font-size: 30px; 
                    }
                    .letraM {
                        font-size: 20px; 
                    }
                    .letraP {
                        font-size: 10px; 
                    }
                                
                    .tbl {
                        padding: 5px;
                        border: 1px solid #000;
                    }
                    .tbl tr th {
                        background-color: #D5D5D5;
                        padding: 5px;
                    }
                    .tbl tr td {
                        padding: 3px;
                    }
                    .aviso {
                        color: #FF0000;
                    }
              </style>";

foreach ($colID as $chave => $idDivida) {

    $ret_cDividas = $dados_cDividas->select(" (d.id = '" . $idDivida . "') ");

    if (sizeof($ret_cDividas) > 0) {
        $ret_cDevedores = $dados_cDevedores->select(" (d.id = '" . $ret_cDividas[0]['id_devedor'] . "') ");

        if (sizeof($ret_cDevedores) > 0) {
            $ret_cClientes = $dados_cClientes->select(" (c.id = '" . $ret_cDividas[0]['id_cliente'] . "') ");

            if (sizeof($ret_cClientes) > 0) {
                if ($tipoDoc == "notaPromissoria") {

                    $numero = $idDivida;
                    $praca_pgto = $ret_cDividas[0]['praca_pgto'];
                    $data_vcto = $ret_cDividas[0]['data_vcto'];
                    $dtVcto = explode("/", $data_vcto);
                    $dataVctoExtenso = $dtVcto[0] . " de " . $meses[(int)$dtVcto[1]] . " de " . $dtVcto[2];
                    $valor = exibir_Valor($ret_cDividas[0]['valor_vcto']);
                    $valorExtenso = extenso($ret_cDividas[0]['valor_vcto']);

                    $pago_ao = $ret_cDevedores[0]['grid_nome'];
                    $cpf_cnpj = $ret_cDevedores[0]['grid_cpf_cnpj'];
                    $endereco = $ret_cDevedores[0]['logradouro1'] . ', ' . $ret_cDevedores[0]['numero1'];
                    $bairro = $ret_cDevedores[0]['bairro1'];

                    $emitente_nome = $ret_cClientes[0]['grid_nome'];
                    $emitente_endereco = $ret_cClientes[0]['logradouro'] . ', ' . $ret_cClientes[0]['numero'];
                    $emitente_bairro = $ret_cClientes[0]['bairro'];
                    $emitente_cpf_cnpj = $ret_cClientes[0]['grid_cpf_cnpj'];
                    $diaAtual = date("d", time());
                    $mesAtual = $meses[date("n", time())];
                    $anoAtual = date("Y", time());

                    $conteudo .= "
        
            <style>
                .tbl {
                    margin-bottom: 150px;
                }
            </style>
            
            <table class='tbl' border='0' width='100%'>
                <tr>
                    <th>
                        <h2>NOTA PROMISSORIA</h2>
                    </th>
                </tr>
                <tr>
                    <td>

                        <table border='0' width='100%'>
                            <tr>
                                <td width='100px'>
                                    N. <b class='letraM'>" . $numero . "</b>
                                </td>
                                <td colspan='2'>
                                    Vencimento: <b class='letraM'>" . $dataVctoExtenso . "</b>
                                </td>
                                <td align='right'>
                                    Valor: R$ <b class='letraM'>" . $valor . "</b>
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>
                
                <tr>
                    <td>
                        <br>
                        Pagarei, por esta única via de NOTA PROMISSORIA, à <b class='letraM'>" . $emitente_nome . "</b>, portador do CPF <b class='letraM'>" . $emitente_cpf_cnpj . "</b>, ou a sua ordem,
                        a quantia de <b class='letraM'>" . $valorExtenso . "</b>.
                        <br>
                        <br>
                        <br>
                    </td>
                </tr>

                <tr>
                    <td>
                        Emitente: <b>" . $pago_ao . "</b><br>
                        Endereço: <b>" . $endereco . "</b><br>
                        Bairro: <b>" . $bairro . "</b><br>
                        CPF/CNPJ: <b>" . $cpf_cnpj . "</b><br>
                        Praça de pagamento: <b>" . $praca_pgto . "</b><br>
                    </td>
                </tr>

                <tr>
                    <td>
                        <br>
                        <br>
                        <table border='0' width='100%'>
                            <tr>
                                <td align='center'>
                                    " . $diaAtual . " de " . $mesAtual . " de " . $anoAtual . "<br>
                                    Data de hoje
                                </td>
                                <td align='center'>
                                    __________________________________________________<br>
                                    <b class='letraM'>" . $emitente_nome . "</b>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                
            </table>
        ";
                }

                if ($tipoDoc == "duplicataPrestacaoServico") {

                    $fatura = $idDivida;
                    $praca_pgto = $ret_cDividas[0]['praca_pgto'];
                    $valor = exibir_Valor($ret_cDividas[0]['valor_vcto']);
                    $valorExtenso = extenso($ret_cDividas[0]['valor_vcto']);
                    $nDuplicata = "123";
                    $data_vcto = $ret_cDividas[0]['data_vcto'];
                    $dtVcto = explode("/", $data_vcto);
                    $dataVctoExtenso = $dtVcto[0] . " de " . $meses[(int)$dtVcto[1]] . " de " . $dtVcto[2];
                    
                    $cedente_nome = $ret_cDevedores[0]['grid_nome'];
                    $cedente_cpf_cnpj = $ret_cDevedores[0]['grid_cpf_cnpj'];
                    $cedente_endereco = $ret_cDevedores[0]['logradouro1'] . ', ' . $ret_cDevedores[0]['numero1'];
                    $cedente_municipio = $ret_cDevedores[0]['cidade'];
                    $cedente_telefone = $ret_cDevedores[0]['tel1'];
                    $cedente_estado = $ret_cDevedores[0]['uf'];
                    $cedente_cep = $ret_cDevedores[0]['cep1'];
                    $cedente_ie = $ret_cDevedores[0]['grid_rg_ie'];
                    $sacado_nome = $ret_cClientes[0]['grid_nome'];
                    $sacado_cpf_cnpj = $ret_cClientes[0]['grid_cpf_cnpj'];
                    $diaAtual = date("d", time());
                    $mesAtual = $meses[date("n", time())];
                    $anoAtual = date("Y", time());

                    $conteudo .= "

            <style>
                .tbl {
                    margin-bottom: 100px;
                }
            </style>

            <table class='tbl' border='0' width='100%'>
                <tr>
                    <th>
                        <h2>DUPLICATA - PRESTAÇÃO DE SERVIÇO</h2>
                    </th>
                </tr>
                <tr>
                    <td>

                        <table border='0' width='100%' style='border: 1px solid gray'>
                            <tr>
                                <th>
                                    FATURA
                                </th>
                                <th>
                                    VALOR
                                </th>
                                <th>
                                    N. DUPLICATA
                                </th>
                                <th>
                                    VENCIMENTO
                                </th>
                            </tr>
                            <tr>
                                <td align='center'>
                                    " . $fatura . "
                                </td>
                                <td align='center'>
                                    " . $valor . "
                                </td>
                                <td align='center'>
                                    " . $nDuplicata . "
                                </td>
                                <td align='center'>
                                    " . $data_vcto . "
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>

                <tr>
                    <td>

                        <table border='0' width='100%'>
                            <tr>
                                <td>
                                    Nome: " . $cedente_nome . "<br>
                                    Endereço: " . $cedente_endereco . "<br>
                                    Municipio: " . $cedente_municipio . "<br>
                                    CPF/CNPJ: " . $cedente_cpf_cnpj . "<br>
                                    Praça de pagamento: " . $praca_pgto . "<br>
                                </td>
                                <td>
                                    Telefone: " . $cedente_telefone . "<br>
                                    Estado: " . $cedente_estado . "<br>
                                    CEP: " . $cedente_cep . "<br>
                                    RG/IE: " . $cedente_ie . "<br>
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>

                <tr>
                    <td>
                        <br>
                        Valor por extenso: <b class='letraM'>" . $valorExtenso . "</b>.
                        <br>
                    </td>
                </tr>

                <tr>
                    <td>
                        <br>
                        RECONHEÇO(EMOS) A EXATIDÃO DESTA DUPLICATA DE PRESTAÇÃO DE SERVIÇOS na importancia acima que pagarei(emos) à <b class='letraM'>" . $sacado_nome . "</b>, ou a sua ordem na praça e vencimento indicados.
                        <br>
                        <br><br><br>
                    </td>
                </tr>

                <tr>
                    <td>
                        <table border='0' width='100%'>
                            <tr>
                                <td align='center'>
                                    " . $diaAtual . " de " . $mesAtual . " de " . $anoAtual . "<br>
                                    Data de hoje
                                </td>
                                <td align='center'>
                                    __________________________________________________<br>
                                    <b class='letraM'>" . $sacado_nome . "</b>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

            </table>
        ";
                }

                if ($tipoDoc == "duplicataVendaMercantil") {

                    $fatura = $idDivida;
                    $praca_pgto = $ret_cDividas[0]['praca_pgto'];
                    $valor = exibir_Valor($ret_cDividas[0]['valor_vcto']);
                    $valorExtenso = extenso($ret_cDividas[0]['valor_vcto']);
                    $nDuplicata = "123";
                    $data_vcto = $ret_cDividas[0]['data_vcto'];
                    $dtVcto = explode("/", $data_vcto);
                    $dataVctoExtenso = $dtVcto[0] . " de " . $meses[(int)$dtVcto[1]] . " de " . $dtVcto[2];
                    $cedente_nome = $ret_cDevedores[0]['grid_nome'];
                    $cedente_cpf_cnpj = $ret_cDevedores[0]['grid_cpf_cnpj'];
                    $cedente_endereco = $ret_cDevedores[0]['logradouro1'] . ', ' . $ret_cDevedores[0]['numero1'];
                    $cedente_municipio = $ret_cDevedores[0]['cidade'];
                    $cedente_telefone = $ret_cDevedores[0]['tel1'];
                    $cedente_estado = $ret_cDevedores[0]['uf'];
                    $cedente_cep = $ret_cDevedores[0]['cep1'];
                    $cedente_ie = $ret_cDevedores[0]['grid_rg_ie'];
                    $sacado_nome = $ret_cClientes[0]['grid_nome'];
                    $sacado_cpf_cnpj = $ret_cClientes[0]['grid_cpf_cnpj'];
                    $diaAtual = date("d", time());
                    $mesAtual = $meses[date("n", time())];
                    $anoAtual = date("Y", time());

                    $conteudo .= "

            <style>
                .tbl {
                    margin-bottom: 100px;
                }
            </style>

            <table class='tbl' border='0' width='100%'>
                <tr>
                    <th>
                        <h2>DUPLICATA - VENDA MERCANTIL</h2>
                    </th>
                </tr>
                <tr>
                    <td>

                        <table border='0' width='100%' style='border: 1px solid gray'>
                            <tr>
                                <th>
                                    FATURA
                                </th>
                                <th>
                                    VALOR
                                </th>
                                <th>
                                    N. DUPLICATA
                                </th>
                                <th>
                                    VENCIMENTO
                                </th>
                            </tr>
                            <tr>
                                <td align='center'>
                                    " . $fatura . "
                                </td>
                                <td align='center'>
                                    " . $valor . "
                                </td>
                                <td align='center'>
                                    " . $nDuplicata . "
                                </td>
                                <td align='center'>
                                    " . $data_vcto . "
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>

                <tr>
                    <td>

                        <table border='0' width='100%'>
                            <tr>
                                <td>
                                    Nome: " . $cedente_nome . "<br>
                                    Endereço: " . $cedente_endereco . "<br>
                                    Municipio: " . $cedente_municipio . "<br>
                                    CPF/CNPJ: " . $cedente_cpf_cnpj . "<br>
                                    Praça de pagamento: " . $praca_pgto . "<br>
                                </td>
                                <td>
                                    Telefone: " . $cedente_telefone . "<br>
                                    Estado: " . $cedente_estado . "<br>
                                    CEP: " . $cedente_cep . "<br>
                                    RG/IE: " . $cedente_ie . "<br>
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>

                <tr>
                    <td>
                        <br>
                        Valor por extenso: <b class='letraM'>" . $valorExtenso . "</b>.
                        <br>
                    </td>
                </tr>

                <tr>
                    <td>
                        <br>
                        RECONHEÇO(EMOS) A EXATIDÃO DESTA DUPLICATA DE VENDA MERCANTIL na importancia acima que pagarei(emos) à <b class='letraM'>" . $sacado_nome . "</b>, ou a sua ordem na praça e vencimento indicados.
                        <br>
                        <br><br><br>
                    </td>
                </tr>

                <tr>
                    <td>
                        <table border='0' width='100%'>
                            <tr>
                                <td align='center'>
                                    " . $diaAtual . " de " . $mesAtual . " de " . $anoAtual . "<br>
                                    Data de hoje
                                </td>
                                <td align='center'>
                                    __________________________________________________<br>
                                    <b class='letraM'>" . $sacado_nome . "</b>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

            </table>
        ";
                }

                if ($tipoDoc == "triplicataPrestacaoServico") {

                    $fatura = $idDivida;
                    $praca_pgto = $ret_cDividas[0]['praca_pgto'];
                    $valor = exibir_Valor($ret_cDividas[0]['valor_vcto']);
                    $valorExtenso = extenso($ret_cDividas[0]['valor_vcto']);
                    $nDuplicata = "123";
                    $data_vcto = $ret_cDividas[0]['data_vcto'];
                    $dtVcto = explode("/", $data_vcto);
                    $dataVctoExtenso = $dtVcto[0] . " de " . $meses[(int)$dtVcto[1]] . " de " . $dtVcto[2];
                    $cedente_nome = $ret_cDevedores[0]['grid_nome'];
                    $cedente_cpf_cnpj = $ret_cDevedores[0]['grid_cpf_cnpj'];
                    $cedente_endereco = $ret_cDevedores[0]['logradouro1'] . ', ' . $ret_cDevedores[0]['numero1'];
                    $cedente_municipio = $ret_cDevedores[0]['cidade'];
                    $cedente_telefone = $ret_cDevedores[0]['tel1'];
                    $cedente_estado = $ret_cDevedores[0]['uf'];
                    $cedente_cep = $ret_cDevedores[0]['cep1'];
                    $cedente_ie = $ret_cDevedores[0]['grid_rg_ie'];
                    $sacado_nome = $ret_cClientes[0]['grid_nome'];
                    $sacado_cpf_cnpj = $ret_cClientes[0]['grid_cpf_cnpj'];
                    $diaAtual = date("d", time());
                    $mesAtual = $meses[date("n", time())];
                    $anoAtual = date("Y", time());

                    $conteudo .= "

            <style>
                .tbl {
                    margin-bottom: 100px;
                }
            </style>

            <table class='tbl' border='0' width='100%'>
                <tr>
                    <th>
                        <h2>TRIPLICATA - PRESTAÇÃO DE SERVIÇO</h2>
                    </th>
                </tr>
                <tr>
                    <td>

                        <table border='0' width='100%' style='border: 1px solid gray'>
                            <tr>
                                <th>
                                    FATURA
                                </th>
                                <th>
                                    VALOR
                                </th>
                                <th>
                                    N. TRIPLICATA
                                </th>
                                <th>
                                    VENCIMENTO
                                </th>
                            </tr>
                            <tr>
                                <td align='center'>
                                    " . $fatura . "
                                </td>
                                <td align='center'>
                                    " . $valor . "
                                </td>
                                <td align='center'>
                                    " . $nDuplicata . "
                                </td>
                                <td align='center'>
                                    " . $data_vcto . "
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>

                <tr>
                    <td>

                        <table border='0' width='100%'>
                            <tr>
                                <td>
                                    Nome: " . $cedente_nome . "<br>
                                    Endereço: " . $cedente_endereco . "<br>
                                    Municipio: " . $cedente_municipio . "<br>
                                    CPF/CNPJ: " . $cedente_cpf_cnpj . "<br>
                                    Praça de pagamento: " . $praca_pgto . "<br>
                                </td>
                                <td>
                                    Telefone: " . $cedente_telefone . "<br>
                                    Estado: " . $cedente_estado . "<br>
                                    CEP: " . $cedente_cep . "<br>
                                    RG/IE: " . $cedente_ie . "<br>
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>

                <tr>
                    <td>
                        <br>
                        Valor por extenso: <b class='letraM'>" . $valorExtenso . "</b>.
                        <br>
                    </td>
                </tr>

                <tr>
                    <td>
                        <br>
                        RECONHEÇO(EMOS) A EXATIDÃO DESTA TRIPLICATA DE PRESTAÇÃO DE SERVIÇOS na importancia acima que pagarei(emos) à <b class='letraM'>" . $sacado_nome . "</b>, ou a sua ordem na praça e vencimento indicados.
                        <br>
                        <br><br><br>
                    </td>
                </tr>

                <tr>
                    <td>
                        <table border='0' width='100%'>
                            <tr>
                                <td align='center'>
                                    " . $diaAtual . " de " . $mesAtual . " de " . $anoAtual . "<br>
                                    Data de hoje
                                </td>
                                <td align='center'>
                                    __________________________________________________<br>
                                    <b class='letraM'>" . $sacado_nome . "</b>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

            </table>
        ";
                }

                if ($tipoDoc == "triplicataVendaMercantil") {

                    $fatura = $idDivida;
                    $praca_pgto = $ret_cDividas[0]['praca_pgto'];
                    $valor = exibir_Valor($ret_cDividas[0]['valor_vcto']);
                    $valorExtenso = extenso($ret_cDividas[0]['valor_vcto']);
                    $nDuplicata = "123";
                    $data_vcto = $ret_cDividas[0]['data_vcto'];
                    $dtVcto = explode("/", $data_vcto);
                    $dataVctoExtenso = $dtVcto[0] . " de " . $meses[(int)$dtVcto[1]] . " de " . $dtVcto[2];
                    $cedente_nome = $ret_cDevedores[0]['grid_nome'];
                    $cedente_cpf_cnpj = $ret_cDevedores[0]['grid_cpf_cnpj'];
                    $cedente_endereco = $ret_cDevedores[0]['logradouro1'] . ', ' . $ret_cDevedores[0]['numero1'];
                    $cedente_municipio = $ret_cDevedores[0]['cidade'];
                    $cedente_telefone = $ret_cDevedores[0]['tel1'];
                    $cedente_estado = $ret_cDevedores[0]['uf'];
                    $cedente_cep = $ret_cDevedores[0]['cep1'];
                    $cedente_ie = $ret_cDevedores[0]['grid_rg_ie'];
                    $sacado_nome = $ret_cClientes[0]['grid_nome'];
                    $sacado_cpf_cnpj = $ret_cClientes[0]['grid_cpf_cnpj'];
                    $diaAtual = date("d", time());
                    $mesAtual = $meses[date("n", time())];
                    $anoAtual = date("Y", time());

                    $conteudo .= "

            <style>
                .tbl {
                    margin-bottom: 100px;
                }
            </style>

            <table class='tbl' border='0' width='100%'>
                <tr>
                    <th>
                        <h2>TRIPLICATA - VENDA MERCANTIL</h2>
                    </th>
                </tr>
                <tr>
                    <td>

                        <table border='0' width='100%' style='border: 1px solid gray'>
                            <tr>
                                <th>
                                    FATURA
                                </th>
                                <th>
                                    VALOR
                                </th>
                                <th>
                                    N. TRIPLICATA
                                </th>
                                <th>
                                    VENCIMENTO
                                </th>
                            </tr>
                            <tr>
                                <td align='center'>
                                    " . $fatura . "
                                </td>
                                <td align='center'>
                                    " . $valor . "
                                </td>
                                <td align='center'>
                                    " . $nDuplicata . "
                                </td>
                                <td align='center'>
                                    " . $data_vcto . "
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>

                <tr>
                    <td>

                        <table border='0' width='100%'>
                            <tr>
                                <td>
                                    Nome: " . $cedente_nome . "<br>
                                    Endereço: " . $cedente_endereco . "<br>
                                    Municipio: " . $cedente_municipio . "<br>
                                    CPF/CNPJ: " . $cedente_cpf_cnpj . "<br>
                                    Praça de pagamento: " . $praca_pgto . "<br>
                                </td>
                                <td>
                                    Telefone: " . $cedente_telefone . "<br>
                                    Estado: " . $cedente_estado . "<br>
                                    CEP: " . $cedente_cep . "<br>
                                    RG/IE: " . $cedente_ie . "<br>
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>

                <tr>
                    <td>
                        <br>
                        Valor por extenso: <b class='letraM'>" . $valorExtenso . "</b>.
                        <br>
                    </td>
                </tr>

                <tr>
                    <td>
                        <br>
                        RECONHEÇO(EMOS) A EXATIDÃO DESTA TRIPLICATA DE VENDA MERCANTIL na importancia acima que pagarei(emos) à <b class='letraM'>" . $sacado_nome . "</b>, ou a sua ordem na praça e vencimento indicados.
                        <br>
                        <br><br><br>
                    </td>
                </tr>

                <tr>
                    <td>
                        <table border='0' width='100%'>
                            <tr>
                                <td align='center'>
                                    " . $diaAtual . " de " . $mesAtual . " de " . $anoAtual . "<br>
                                    Data de hoje
                                </td>
                                <td align='center'>
                                    __________________________________________________<br>
                                    <b class='letraM'>" . $sacado_nome . "</b>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

            </table>
        ";
                }
            } else {
                $erroConteudo .= "<h3 class='aviso'>Cliente inexistente! #" . $ret_cDividas[0]['id_cliente'] . "</h3>";
            }
        } else {
            $erroConteudo .= "<h3 class='aviso'>Devedor inexistente! #" . $ret_cDividas[0]['id_devedor'] . "</h3>";
        }
    } else {
        $erroConteudo .= "<h3 class='aviso'>Dívida inexistente! #" . $idDivida . "</h3>";
    }
}

$conteudo .= $erroConteudo;
$conteudo .= "</body></html>";

$mpdf = new mPDF('c', 'A4', '', '', 10, 10, 10, 10, 0, 0);
$mpdf->SetDisplayMode('fullpage');
$mpdf->WriteHTML($conteudo);
$mpdf->Output();

exit();
?>