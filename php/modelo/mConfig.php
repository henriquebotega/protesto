<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_MODELO . "conexao.php";

class mConfig extends conexao
{
    private $email_envio;
    private $senha_envio;
    private $smtp_envio;
    private $ws_url;
    private $ws_usuario;
    private $ws_senha;
    private $qtd_itens_listagem;
    private $exibir_total_registro_menu;
    private $exibir_grafico_ultimos_meses;
    private $boleto_mensagem1;
    private $boleto_mensagem2;
    private $boleto_mensagem3;
    private $boleto_desconto;
    private $boleto_abatimento;
    private $boleto_mora;
    private $boleto_multa;
    private $boleto_id_banco;
    private $boleto_agencia;
    private $boleto_cc;
    private $boleto_id_carteira;
    private $boleto_cedente;
    private $boleto_cedente_digito;
    private $boleto_numero_sequencial_remessa;
    private $boleto_numero_sequencial_nosso_numero;
    private $boleto_agencia_digito;
    private $boleto_cc_digito;
    private $empresa_nome;
    private $empresa_endereco;
    private $empresa_cnpj;
    private $empresa_id_estado;
    private $empresa_id_municipio;
    private $cartorio_agencia;
    private $cartorio_versao_layout;
    private $cartorio_numero_sequencial;
    private $cartorio_sigla_remetente;
    private $cartorio_sigla_destinatario;
    private $cartorio_sigla_transacao;
    private $cartorio_cod_portador;
    private $cartorio_portador;
    private $ws_L0001;
    private $ws_L0002;
    private $ws_L0003;
    private $ws_L0004;
    private $ws_L0006;
    private $ws_L0007;
    private $ws_L0008;
    private $ws_L0011;
    private $ws_L0014;
    private $ws_L0015;
    private $ws_L0022;
    private $ws_L0024;
    private $ws_L0029;
    private $ws_L0030;
    private $ws_L0032;
    private $ws_L0033;
    private $ws_L0035;
    private $ws_L0038;
    private $ws_L0039;
    private $ws_L0041;
    private $ws_L0059;
    private $ws_L0064;
    private $ws_L0066;
    private $ws_L0067;
    private $ws_L0068;
    private $ws_R0002;
    private $ws_R0014;
    private $ws_R0016;
    private $ws_R0001;
    private $ws_R0019;

    public function getWsR0002()
    {
        return $this->ws_R0002;
    }

    public function setWsR0002($ws_R0002)
    {
        $this->ws_R0002 = $ws_R0002;
    }

    public function getWsR0014()
    {
        return $this->ws_R0014;
    }

    public function setWsR0014($ws_R0014)
    {
        $this->ws_R0014 = $ws_R0014;
    }

    public function getWsR0016()
    {
        return $this->ws_R0016;
    }

    public function setWsR0016($ws_R0016)
    {
        $this->ws_R0016 = $ws_R0016;
    }

    public function getWsR0001()
    {
        return $this->ws_R0001;
    }

    public function setWsR0001($ws_R0001)
    {
        $this->ws_R0001 = $ws_R0001;
    }

    public function getWsR0019()
    {
        return $this->ws_R0019;
    }

    public function setWsR0019($ws_R0019)
    {
        $this->ws_R0019 = $ws_R0019;
    }

    public function getEmailEnvio()
    {
        return $this->email_envio;
    }

    public function setEmailEnvio($email_envio)
    {
        $this->email_envio = $email_envio;
    }

    public function getSenhaEnvio()
    {
        return $this->senha_envio;
    }

    public function setSenhaEnvio($senha_envio)
    {
        $this->senha_envio = $senha_envio;
    }

    public function getSmtpEnvio()
    {
        return $this->smtp_envio;
    }

    public function setSmtpEnvio($smtp_envio)
    {
        $this->smtp_envio = $smtp_envio;
    }

    public function getWsUrl()
    {
        return $this->ws_url;
    }

    public function setWsUrl($ws_url)
    {
        $this->ws_url = $ws_url;
    }

    public function getWsUsuario()
    {
        return $this->ws_usuario;
    }

    public function setWsUsuario($ws_usuario)
    {
        $this->ws_usuario = $ws_usuario;
    }

    public function getWsSenha()
    {
        return $this->ws_senha;
    }

    public function setWsSenha($ws_senha)
    {
        $this->ws_senha = $ws_senha;
    }

    public function getQtdItensListagem()
    {
        return $this->qtd_itens_listagem;
    }

    public function setQtdItensListagem($qtd_itens_listagem)
    {
        $this->qtd_itens_listagem = $qtd_itens_listagem;
    }

    public function getExibirTotalRegistroMenu()
    {
        return $this->exibir_total_registro_menu;
    }

    public function setExibirTotalRegistroMenu($exibir_total_registro_menu)
    {
        $this->exibir_total_registro_menu = $exibir_total_registro_menu;
    }

    public function getExibirGraficoUltimosMeses()
    {
        return $this->exibir_grafico_ultimos_meses;
    }

    public function setExibirGraficoUltimosMeses($exibir_grafico_ultimos_meses)
    {
        $this->exibir_grafico_ultimos_meses = $exibir_grafico_ultimos_meses;
    }

    public function getBoletoMensagem1()
    {
        return $this->boleto_mensagem1;
    }

    public function setBoletoMensagem1($boleto_mensagem1)
    {
        $this->boleto_mensagem1 = $boleto_mensagem1;
    }

    public function getBoletoMensagem2()
    {
        return $this->boleto_mensagem2;
    }

    public function setBoletoMensagem2($boleto_mensagem2)
    {
        $this->boleto_mensagem2 = $boleto_mensagem2;
    }

    public function getBoletoMensagem3()
    {
        return $this->boleto_mensagem3;
    }

    public function setBoletoMensagem3($boleto_mensagem3)
    {
        $this->boleto_mensagem3 = $boleto_mensagem3;
    }

    public function getBoletoDesconto()
    {
        return $this->boleto_desconto;
    }

    public function setBoletoDesconto($boleto_desconto)
    {
        $this->boleto_desconto = $boleto_desconto;
    }

    public function getBoletoAbatimento()
    {
        return $this->boleto_abatimento;
    }

    public function setBoletoAbatimento($boleto_abatimento)
    {
        $this->boleto_abatimento = $boleto_abatimento;
    }

    public function getBoletoMora()
    {
        return $this->boleto_mora;
    }

    public function setBoletoMora($boleto_mora)
    {
        $this->boleto_mora = $boleto_mora;
    }

    public function getBoletoMulta()
    {
        return $this->boleto_multa;
    }

    public function setBoletoMulta($boleto_multa)
    {
        $this->boleto_multa = $boleto_multa;
    }

    public function getBoletoIdBanco()
    {
        return $this->boleto_id_banco;
    }

    public function setBoletoIdBanco($boleto_id_banco)
    {
        $this->boleto_id_banco = $boleto_id_banco;
    }

    public function getBoletoAgencia()
    {
        return $this->boleto_agencia;
    }

    public function setBoletoAgencia($boleto_agencia)
    {
        $this->boleto_agencia = $boleto_agencia;
    }

    public function getBoletoCc()
    {
        return $this->boleto_cc;
    }

    public function setBoletoCc($boleto_cc)
    {
        $this->boleto_cc = $boleto_cc;
    }

    public function getBoletoIdCarteira()
    {
        return $this->boleto_id_carteira;
    }

    public function setBoletoIdCarteira($boleto_id_carteira)
    {
        $this->boleto_id_carteira = $boleto_id_carteira;
    }

    public function getBoletoCedente()
    {
        return $this->boleto_cedente;
    }

    public function setBoletoCedente($boleto_cedente)
    {
        $this->boleto_cedente = $boleto_cedente;
    }

    public function getBoletoCedenteDigito()
    {
        return $this->boleto_cedente_digito;
    }

    public function setBoletoCedenteDigito($boleto_cedente_digito)
    {
        $this->boleto_cedente_digito = $boleto_cedente_digito;
    }

    public function getBoletoNumeroSequencialRemessa()
    {
        return $this->boleto_numero_sequencial_remessa;
    }

    public function setBoletoNumeroSequencialRemessa($boleto_numero_sequencial_remessa)
    {
        $this->boleto_numero_sequencial_remessa = $boleto_numero_sequencial_remessa;
    }

    public function getBoletoNumeroSequencialNossoNumero()
    {
        return $this->boleto_numero_sequencial_nosso_numero;
    }

    public function setBoletoNumeroSequencialNossoNumero($boleto_numero_sequencial_nosso_numero)
    {
        $this->boleto_numero_sequencial_nosso_numero = $boleto_numero_sequencial_nosso_numero;
    }

    public function getBoletoAgenciaDigito()
    {
        return $this->boleto_agencia_digito;
    }

    public function setBoletoAgenciaDigito($boleto_agencia_digito)
    {
        $this->boleto_agencia_digito = $boleto_agencia_digito;
    }

    public function getBoletoCcDigito()
    {
        return $this->boleto_cc_digito;
    }

    public function setBoletoCcDigito($boleto_cc_digito)
    {
        $this->boleto_cc_digito = $boleto_cc_digito;
    }

    public function getEmpresaNome()
    {
        return $this->empresa_nome;
    }

    public function setEmpresaNome($empresa_nome)
    {
        $this->empresa_nome = $empresa_nome;
    }

    public function getEmpresaEndereco()
    {
        return $this->empresa_endereco;
    }

    public function setEmpresaEndereco($empresa_endereco)
    {
        $this->empresa_endereco = $empresa_endereco;
    }

    public function getEmpresaCnpj()
    {
        return $this->empresa_cnpj;
    }

    public function setEmpresaCnpj($empresa_cnpj)
    {
        $this->empresa_cnpj = $empresa_cnpj;
    }

    public function getEmpresaIdEstado()
    {
        return $this->empresa_id_estado;
    }

    public function setEmpresaIdEstado($empresa_id_estado)
    {
        $this->empresa_id_estado = $empresa_id_estado;
    }

    public function getEmpresaIdMunicipio()
    {
        return $this->empresa_id_municipio;
    }

    public function setEmpresaIdMunicipio($empresa_id_municipio)
    {
        $this->empresa_id_municipio = $empresa_id_municipio;
    }

    public function getCartorioAgencia()
    {
        return $this->cartorio_agencia;
    }

    public function setCartorioAgencia($cartorio_agencia)
    {
        $this->cartorio_agencia = $cartorio_agencia;
    }

    public function getCartorioVersaoLayout()
    {
        return $this->cartorio_versao_layout;
    }

    public function setCartorioVersaoLayout($cartorio_versao_layout)
    {
        $this->cartorio_versao_layout = $cartorio_versao_layout;
    }

    public function getCartorioNumeroSequencial()
    {
        return $this->cartorio_numero_sequencial;
    }

    public function setCartorioNumeroSequencial($cartorio_numero_sequencial)
    {
        $this->cartorio_numero_sequencial = $cartorio_numero_sequencial;
    }

    public function getCartorioSiglaRemetente()
    {
        return $this->cartorio_sigla_remetente;
    }

    public function setCartorioSiglaRemetente($cartorio_sigla_remetente)
    {
        $this->cartorio_sigla_remetente = $cartorio_sigla_remetente;
    }

    public function getCartorioSiglaDestinatario()
    {
        return $this->cartorio_sigla_destinatario;
    }

    public function setCartorioSiglaDestinatario($cartorio_sigla_destinatario)
    {
        $this->cartorio_sigla_destinatario = $cartorio_sigla_destinatario;
    }

    public function getCartorioSiglaTransacao()
    {
        return $this->cartorio_sigla_transacao;
    }

    public function setCartorioSiglaTransacao($cartorio_sigla_transacao)
    {
        $this->cartorio_sigla_transacao = $cartorio_sigla_transacao;
    }

    public function getCartorioCodPortador()
    {
        return $this->cartorio_cod_portador;
    }

    public function setCartorioCodPortador($cartorio_cod_portador)
    {
        $this->cartorio_cod_portador = $cartorio_cod_portador;
    }

    public function getCartorioPortador()
    {
        return $this->cartorio_portador;
    }

    public function setCartorioPortador($cartorio_portador)
    {
        $this->cartorio_portador = $cartorio_portador;
    }

    public function getWsL0001()
    {
        return $this->ws_L0001;
    }

    public function setWsL0001($ws_L0001)
    {
        $this->ws_L0001 = $ws_L0001;
    }

    public function getWsL0002()
    {
        return $this->ws_L0002;
    }

    public function setWsL0002($ws_L0002)
    {
        $this->ws_L0002 = $ws_L0002;
    }

    public function getWsL0003()
    {
        return $this->ws_L0003;
    }

    public function setWsL0003($ws_L0003)
    {
        $this->ws_L0003 = $ws_L0003;
    }

    public function getWsL0004()
    {
        return $this->ws_L0004;
    }

    public function setWsL0004($ws_L0004)
    {
        $this->ws_L0004 = $ws_L0004;
    }

    public function getWsL0006()
    {
        return $this->ws_L0006;
    }

    public function setWsL0006($ws_L0006)
    {
        $this->ws_L0006 = $ws_L0006;
    }

    public function getWsL0007()
    {
        return $this->ws_L0007;
    }

    public function setWsL0007($ws_L0007)
    {
        $this->ws_L0007 = $ws_L0007;
    }

    public function getWsL0008()
    {
        return $this->ws_L0008;
    }

    public function setWsL0008($ws_L0008)
    {
        $this->ws_L0008 = $ws_L0008;
    }

    public function getWsL0011()
    {
        return $this->ws_L0011;
    }

    public function setWsL0011($ws_L0011)
    {
        $this->ws_L0011 = $ws_L0011;
    }

    public function getWsL0014()
    {
        return $this->ws_L0014;
    }

    public function setWsL0014($ws_L0014)
    {
        $this->ws_L0014 = $ws_L0014;
    }

    public function getWsL0015()
    {
        return $this->ws_L0015;
    }

    public function setWsL0015($ws_L0015)
    {
        $this->ws_L0015 = $ws_L0015;
    }

    public function getWsL0022()
    {
        return $this->ws_L0022;
    }

    public function setWsL0022($ws_L0022)
    {
        $this->ws_L0022 = $ws_L0022;
    }

    public function getWsL0024()
    {
        return $this->ws_L0024;
    }

    public function setWsL0024($ws_L0024)
    {
        $this->ws_L0024 = $ws_L0024;
    }

    public function getWsL0029()
    {
        return $this->ws_L0029;
    }

    public function setWsL0029($ws_L0029)
    {
        $this->ws_L0029 = $ws_L0029;
    }

    public function getWsL0030()
    {
        return $this->ws_L0030;
    }

    public function setWsL0030($ws_L0030)
    {
        $this->ws_L0030 = $ws_L0030;
    }

    public function getWsL0032()
    {
        return $this->ws_L0032;
    }

    public function setWsL0032($ws_L0032)
    {
        $this->ws_L0032 = $ws_L0032;
    }

    public function getWsL0033()
    {
        return $this->ws_L0033;
    }

    public function setWsL0033($ws_L0033)
    {
        $this->ws_L0033 = $ws_L0033;
    }

    public function getWsL0035()
    {
        return $this->ws_L0035;
    }

    public function setWsL0035($ws_L0035)
    {
        $this->ws_L0035 = $ws_L0035;
    }

    public function getWsL0038()
    {
        return $this->ws_L0038;
    }

    public function setWsL0038($ws_L0038)
    {
        $this->ws_L0038 = $ws_L0038;
    }

    public function getWsL0039()
    {
        return $this->ws_L0039;
    }

    public function setWsL0039($ws_L0039)
    {
        $this->ws_L0039 = $ws_L0039;
    }

    public function getWsL0041()
    {
        return $this->ws_L0041;
    }

    public function setWsL0041($ws_L0041)
    {
        $this->ws_L0041 = $ws_L0041;
    }

    public function getWsL0059()
    {
        return $this->ws_L0059;
    }

    public function setWsL0059($ws_L0059)
    {
        $this->ws_L0059 = $ws_L0059;
    }

    public function getWsL0064()
    {
        return $this->ws_L0064;
    }

    public function setWsL0064($ws_L0064)
    {
        $this->ws_L0064 = $ws_L0064;
    }

    public function getWsL0066()
    {
        return $this->ws_L0066;
    }

    public function setWsL0066($ws_L0066)
    {
        $this->ws_L0066 = $ws_L0066;
    }

    public function getWsL0067()
    {
        return $this->ws_L0067;
    }

    public function setWsL0067($ws_L0067)
    {
        $this->ws_L0067 = $ws_L0067;
    }

    public function getWsL0068()
    {
        return $this->ws_L0068;
    }

    public function setWsL0068($ws_L0068)
    {
        $this->ws_L0068 = $ws_L0068;
    }

}

?>