<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_MODELO . "mRemessasBanco.php";

class cRemessasBanco extends mRemessasBanco
{
    protected $sqlInsert = "INSERT INTO remessas_banco (id_divida, data, nosso_numero, enviado_banco, retorno_banco, tipo_envio) VALUES (:id_divida, :data, :nosso_numero, :enviado_banco, :retorno_banco, :tipo_envio)";
    protected $sqlUpdate = "UPDATE remessas_banco SET data_saida_banco = :data_saida_banco, enviado_banco = :enviado_banco, boleto_numero_sequencial_remessa = :boleto_numero_sequencial_remessa WHERE (id_divida = :id_divida)";
    protected $sqlSelect = "
      SELECT 
        rb.*, d.id_devedor,
        if(dev.id_tipo = 1, pf.nome, pj.nome_fantasia) as 'grid_devedor'
      
      FROM remessas_banco rb
        LEFT JOIN dividas d ON d.id = rb.id_divida
        LEFT JOIN devedores dev ON dev.id = d.id_devedor
        LEFT JOIN devedores_pf pf ON pf.id_devedor = dev.id
        LEFT JOIN devedores_pj pj ON pj.id_devedor = dev.id
      
      %s %s %s";

    protected $sqlUpdateImportar = "UPDATE remessas_banco SET cod_remessa = :cod_remessa, data_retorno_banco = :data_retorno_banco, retorno_banco = :retorno_banco WHERE (nosso_numero = :nosso_numero)";

    public function salvar()
    {
        $params = array(
            ':retorno_banco' => 0,
            ':tipo_envio' => $this->getTipoEnvio(),
            ':id_divida' => $this->getIdDivida(),
            ':nosso_numero' => $this->getNossoNumero(),
            ':data' => date('Y-m-d H:i:s', time()),
            ':enviado_banco' => 0
        );

        $this->Executar($this->sqlInsert, $params);
    }

    public function importar()
    {
        $params = array(
            ':data_retorno_banco' => date('Y-m-d H:i:s', time()),
            ':retorno_banco' => 1,
            ':cod_remessa' => $this->getCodRemessa(),
            ':nosso_numero' => $this->getNossoNumero()
        );

        $this->Executar($this->sqlUpdateImportar, $params);
    }

    public function salvar_enviado_banco()
    {
        $params = array(
            ':data_saida_banco' => date('Y-m-d H:i:s', time()),
            ':enviado_banco' => 1,
            ':boleto_numero_sequencial_remessa' => $this->getBoletoNumeroSequencialRemessa(),
            ':id_divida' => $this->getIdDivida()
        );

        $this->Executar($this->sqlUpdate, $params);
    }

    public function select($where = '', $order = '', $more = '')
    {
        $where = (empty($where) ? "" : " WHERE " . $where);
        $order = (empty($order) ? "" : " ORDER BY " . $order);

        $sql = sprintf($this->sqlSelect, $where, $order, $more);
        return $this->RunSelect($sql);
    }
}

?>