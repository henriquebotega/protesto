<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_MODELO . "mClientesPJ.php";

class cClientesPJ extends mClientesPJ
{
    protected $sqlInsert = "INSERT INTO clientes_pj (razao_social, nome_fantasia, cnpj, ie, id_cliente) VALUES (:razao_social, :nome_fantasia, :cnpj, :ie, :id_cliente)";
    protected $sqlUpdate = "UPDATE clientes_pj SET razao_social = :razao_social, nome_fantasia = :nome_fantasia, cnpj = :cnpj, ie = :ie WHERE (id = :id)";
    protected $sqlDelete = "DELETE FROM clientes_pj WHERE (id_cliente = :id_cliente)";
    protected $sqlSelect = "SELECT * FROM clientes_pj %s %s %s";

    public function salvar()
    {
        $params = array(
            ':razao_social' => ucwords($this->getRazaoSocial()),
            ':ie' => $this->getIe(),
            ':nome_fantasia' => ucwords($this->getNomeFantasia()),
            ':cnpj' => $this->getCnpj()
        );

        if ($this->getId()) {
            $params[':id'] = $this->getId();
            $this->Executar($this->sqlUpdate, $params);
        } else {
            $params[':id_cliente'] = $this->getIdCliente();
            $this->Executar($this->sqlInsert, $params);
        }
    }

    public function delete()
    {
        $params = array(
            ':id_cliente' => $this->getIdCliente()
        );

        $this->Executar($this->sqlDelete, $params);
    }

    public function select($where = '', $order = '', $more = '')
    {
        $where = (empty($where) ? "" : " WHERE " . $where);
        $order = (empty($order) ? "" : " ORDER BY " . $order);

        $sql = sprintf($this->sqlSelect, $where, $order, $more);
        return $this->RunSelect($sql);
    }
}

?>