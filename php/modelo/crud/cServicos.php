<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_MODELO . "mServicos.php";

class cServicos extends mServicos
{
    protected $sqlInsert = "INSERT INTO servicos (titulo, descricao, valor, permite_remover, obrigatorio, webservice, url_ws, rota) VALUES (:titulo, :descricao, :valor, :permite_remover, :obrigatorio, :webservice, :url_ws, :rota)";
    protected $sqlUpdate = "UPDATE servicos SET titulo = :titulo, descricao = :descricao, valor = :valor WHERE (id = :id)";
    protected $sqlDelete = "DELETE FROM servicos WHERE (id = :id)";
    protected $sqlSelect = "SELECT * FROM servicos %s %s %s";

    public function salvar()
    {
        $params = array(
            ':titulo' => ucwords($this->getTitulo()),
            ':descricao' => ucfirst($this->getDescricao()),
            ':valor' => formatar_Dinheiro($this->getValor())
        );

        if ($this->getId()) {
            $params[':id'] = $this->getId();
            $this->Executar($this->sqlUpdate, $params);
        } else {
            $params[':permite_remover'] = 1;
            $params[':obrigatorio'] = 0;
            $params[':webservice'] = 0;
            $params[':url_ws'] = "";
            $params[':rota'] = "";
            $this->Executar($this->sqlInsert, $params);
        }
    }

    public function delete()
    {
        $params = array(
            ':id' => $this->getId()
        );

        $this->Executar($this->sqlDelete, $params);
    }

    public function select($where = '', $order = '', $more = '')
    {
        $where = (empty($where) ? "" : " WHERE " . $where);
        $order = (empty($order) ? "" : " ORDER BY " . $order);

        $sql = sprintf($this->sqlSelect, $where, $order, $more);
        return $this->RunSelect($sql);
    }
}

?>