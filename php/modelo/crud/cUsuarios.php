<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_MODELO . "mUsuarios.php";

class cUsuarios extends mUsuarios
{
    protected $sqlInsert = "INSERT INTO usuarios (id_admin, id_cliente) VALUES (:id_admin, :id_cliente)";
    protected $sqlDelete = "DELETE FROM usuarios WHERE (id_admin = :id_admin)";
    protected $sqlSelect = "SELECT * FROM usuarios %s %s %s";

    protected $sqlTotalUsuariosPorCliente = "
        SELECT 
          COUNT(*) as 'total', c.id
    
        FROM usuarios u
          INNER JOIN clientes c ON c.id = u.id_cliente
                
        GROUP BY
          c.id
    ";

    public function salvar()
    {
        $params = array(
            ':id_admin' => $this->getIdAdmin(),
            ':id_cliente' => $this->getIdCliente()
        );

        $this->Executar($this->sqlInsert, $params);
    }

    public function delete()
    {
        $params = array(
            ':id_admin' => $this->getIdAdmin()
        );

        $this->Executar($this->sqlDelete, $params);
    }

    public function select($where = '', $order = '', $more = '')
    {
        $where = (empty($where) ? "" : " WHERE " . $where);
        $order = (empty($order) ? "" : " ORDER BY " . $order);

        $sql = sprintf($this->sqlSelect, $where, $order, $more);
        return $this->RunSelect($sql);
    }

    public function totalUsuariosPorCliente() {
        $sql = sprintf($this->sqlTotalUsuariosPorCliente);
        return $this->RunSelect($sql);
    }
}

?>