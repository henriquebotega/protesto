<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_MODELO . "mTaxas.php";

class cTaxas extends mTaxas
{
    protected $sqlInsert = "INSERT INTO taxas (id_mensalidade, historico, valor, id_servico, data, id_cliente, id_situacao) VALUES (:id_mensalidade, :historico, :valor, :id_servico, :data, :id_cliente, :id_situacao)";
    protected $sqlUpdate = "UPDATE taxas SET historico = :historico, id_servico = :id_servico, valor = :valor, data = :data, id_cliente = :id_cliente, id_situacao = :id_situacao WHERE (id = :id)";
    protected $sqlUpdateMensalidade = "UPDATE taxas SET id_mensalidade = :id_mensalidade, id_situacao = :id_situacao WHERE (id = :id)";
    protected $sqlDelete = "DELETE FROM taxas WHERE (id = :id)";
    protected $sqlSelect = "
      SELECT 
        t.*, DATE_FORMAT(t.data, '%s') as data, s.titulo as 'servico', ts.titulo as 'situacao'
      
      FROM taxas t
        INNER JOIN servicos s ON s.id = t.id_servico
        INNER JOIN taxas_situacao ts ON ts.id = t.id_situacao
      
      %s %s %s";

    protected $sqlSelectConsumoMensalPorCliente = "
        SELECT 
          COUNT(*) as 'total', MONTH(data) as 'mes', id_cliente 
        
        FROM taxas
        
        WHERE
          (id_servico > 1)
                
        GROUP BY 
          mes, id_cliente
          
        ORDER BY 
          mes
    ";

    protected $sqlSelectConsumoMensalPorWS = "
        SELECT 
          COUNT(*) as 'total', id_servico, MONTH(data) as 'mes'
    
        FROM taxas 

        WHERE
          (id_servico > 1)
                
        GROUP BY
          mes, id_servico
          
        ORDER BY 
          mes
    ";

    protected $sqlSelectConsumoMensalWS = "
        SELECT 
          COUNT(*) as 'total', id_servico, MONTH(data) as 'mes'
    
        FROM taxas 
        
        WHERE 
          (id_cliente = %s) AND (id_servico > 1)
        
        GROUP BY
          mes, id_servico
          
        ORDER BY 
          mes
    ";
    
    public function salvar()
    {
        $params = array(
            ':id_situacao' => $this->getIdSituacao(),
            ':historico' => $this->getHistorico(),
            ':id_cliente' => $this->getIdCliente(),
            ':id_servico' => $this->getIdServico(),
            ':valor' => formatar_Dinheiro($this->getValor()),
            ':data' => formatar_DataHora($this->getData())
        );

        if ($this->getId()) {
            $params[':id'] = $this->getId();
            $this->Executar($this->sqlUpdate, $params);
        } else {
            $params[':id_mensalidade'] = $this->getIdMensalidade();
            $this->Executar($this->sqlInsert, $params);
        }
    }

    public function delete()
    {
        $params = array(
            ':id' => $this->getId()
        );

        $this->Executar($this->sqlDelete, $params);
    }

    public function updateMensalidade()
    {
        $params = array(
            ':id_situacao' => $this->getIdSituacao(),
            ':id_mensalidade' => $this->getIdMensalidade(),
            ':id' => $this->getId()
        );

        $this->Executar($this->sqlUpdateMensalidade, $params);
    }

    public function select($where = '', $order = '', $more = '')
    {
        $where = (empty($where) ? "" : " WHERE " . $where);
        $order = (empty($order) ? "" : " ORDER BY " . $order);

        $sql = sprintf($this->sqlSelect, '%d/%m/%Y %H:%i:%s', $where, $order, $more);
        return $this->RunSelect($sql);
    }

    public function selectConsumoMensalWS($id_cliente) {
        $sql = sprintf($this->sqlSelectConsumoMensalWS, $id_cliente);
        return $this->RunSelect($sql);
    }

    public function selectConsumoMensalPorCliente() {
        $sql = sprintf($this->sqlSelectConsumoMensalPorCliente);
        return $this->RunSelect($sql);
    }

    public function selectConsumoMensalPorWS() {
        $sql = sprintf($this->sqlSelectConsumoMensalPorWS);
        return $this->RunSelect($sql);
    }
}

?>