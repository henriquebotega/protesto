<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_MODELO . "mClientesContatos.php";

class cClientesContatos extends mClientesContatos
{
    protected $sqlInsert = "INSERT INTO clientes_contatos (id_cliente, tel1, contato1, email1, tel2, contato2, email2, tel3, contato3, email3) VALUES (:id_cliente, :tel1, :contato1, :email1, :tel2, :contato2, :email2, :tel3, :contato3, :email3)";
    protected $sqlUpdate = "UPDATE clientes_contatos SET tel1 = :tel1, contato1 = :contato1, email1 = :email1, tel2 = :tel2, contato2 = :contato2, email2 = :email2, tel3 = :tel3, contato3 = :contato3, email3 = :email3 WHERE (id = :id)";
    protected $sqlSelect = "SELECT * FROM clientes_contatos %s %s %s";
    protected $sqlDelete = "DELETE FROM clientes_contatos WHERE (id_cliente = :id_cliente)";
    
    public function salvar()
    {
        $params = array(
            ':tel1' => $this->getTel1(),
            ':contato1' => ucwords($this->getContato1()),
            ':email1' => strtolower($this->getEmail1()),
            ':tel2' => $this->getTel2(),
            ':contato2' => ucwords($this->getContato2()),
            ':email2' => strtolower($this->getEmail2()),
            ':tel3' => $this->getTel3(),
            ':contato3' => ucwords($this->getContato3()),
            ':email3' => strtolower($this->getEmail3())
        );

        if ($this->getId()) {
            $params[':id'] = $this->getId();
            $this->Executar($this->sqlUpdate, $params);
        } else {
            $params[':id_cliente'] = $this->getIdCliente();
            $this->Executar($this->sqlInsert, $params);
        }
    }

    public function delete()
    {
        $params = array(
            ':id_cliente' => $this->getIdCliente()
        );

        $this->Executar($this->sqlDelete, $params);
    }
    
    public function select($where = '', $order = '', $more = '')
    {
        $where = (empty($where) ? "" : " WHERE " . $where);
        $order = (empty($order) ? "" : " ORDER BY " . $order);

        $sql = sprintf($this->sqlSelect, $where, $order, $more);
        return $this->RunSelect($sql);
    }
}

?>