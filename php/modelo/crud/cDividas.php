<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_MODELO . "mDividas.php";

class cDividas extends mDividas
{
    protected $sqlInsert = "INSERT INTO dividas (valor_vcto, data_vcto, id_situacao, id_devedor, nosso_numero, numero_parcela, data_emissao, numero_documento, numero_fatura, observacoes, enviado_cartorio, id_praca, boleto_mensagem1, boleto_mensagem2, boleto_mensagem3, boleto_desconto, boleto_abatimento, boleto_mora, boleto_multa) VALUES (:valor_vcto, :data_vcto, :id_situacao, :id_devedor, :nosso_numero, :numero_parcela, :data_emissao, :numero_documento, :numero_fatura, :observacoes, :enviado_cartorio, :id_praca, :boleto_mensagem1, :boleto_mensagem2, :boleto_mensagem3, :boleto_desconto, :boleto_abatimento, :boleto_mora, :boleto_multa)";
    protected $sqlUpdate = "UPDATE dividas SET valor_vcto = :valor_vcto, data_vcto = :data_vcto, id_situacao = :id_situacao, id_devedor = :id_devedor, data_emissao = :data_emissao, id_praca = :id_praca, numero_documento = :numero_documento, numero_fatura = :numero_fatura, observacoes = :observacoes WHERE (id = :id)";
    protected $sqlSelect = "
      SELECT 
        d.*, (SELECT count(dv.id) FROM dividas dv WHERE dv.numero_documento = d.numero_documento) as 'total_parcela',
        if(dev.id_tipo = 1, pf.nome, pj.nome_fantasia) as 'grid_devedor', 
        dev.id_cliente, ds.titulo as 'situacao', p.titulo as 'praca_pgto'
      
      FROM dividas d
        LEFT JOIN devedores dev ON dev.id = d.id_devedor
        LEFT JOIN devedores_pf pf ON pf.id_devedor = dev.id
        LEFT JOIN devedores_pj pj ON pj.id_devedor = dev.id
        LEFT JOIN dividas_situacao ds ON ds.id = d.id_situacao
        LEFT JOIN pracas p ON p.id = d.id_praca
      
      %s %s %s";

    protected $sqlTotalRegistrosPorCliente = "
        SELECT 
            count(d.id) as 'total', ds.titulo
          
          FROM dividas d
            INNER JOIN dividas_situacao ds ON ds.id = d.id_situacao 
            INNER JOIN devedores dev ON dev.id = d.id_devedor
          
          WHERE
            (dev.id_cliente = %s)
            
          GROUP BY
            dev.id_cliente, ds.id
    ";

    protected $sqlUppdateConfigBoleto = "UPDATE dividas SET boleto_mensagem1 = :boleto_mensagem1, boleto_mensagem2 = :boleto_mensagem2, boleto_mensagem3 = :boleto_mensagem3, boleto_desconto = :boleto_desconto, boleto_abatimento = :boleto_abatimento, boleto_mora = :boleto_mora, boleto_multa = :boleto_multa WHERE (id = :id)";
    protected $sqlUpdateEnviadoCartorio = "UPDATE dividas SET enviado_cartorio = :enviado_cartorio WHERE (id = :id)";
    protected $sqlUpdateImportar = "UPDATE dividas SET id_situacao = :id_situacao, valor_pgto = :valor_pgto, data_pgto = :data_pgto WHERE (nosso_numero = :nosso_numero)";
    protected $sqlUpdateSituacao = "UPDATE dividas SET id_situacao = :id_situacao WHERE (id = :id)";
    protected $sqlUpdateNossoNumero = "UPDATE dividas SET nosso_numero = :nosso_numero, id_situacao = :id_situacao WHERE (id = :id)";
    protected $sqlUltimoID = "SELECT * FROM dividas ORDER BY id DESC LIMIT 1";

    public function salvar()
    {
        $params = array(
            ':valor_vcto' => formatar_Dinheiro($this->getValorVcto()),
            ':data_vcto' => $this->getDataVcto(),
            ':id_situacao' => $this->getIdSituacao(),
            ':data_emissao' => $this->getDataEmissao(),
            ':numero_documento' => $this->getNumeroDocumento(),
            ':numero_fatura' => $this->getNumeroFatura(),
            ':observacoes' => $this->getObservacoes(),
            ':id_praca' => $this->getIdPraca(),
            ':id_devedor' => $this->getIdDevedor()
        );

        if ($this->getId()) {
            $params[':id'] = $this->getId();
            $this->Executar($this->sqlUpdate, $params);
        } else {
            $params[':numero_parcela'] = $this->getNumeroParcela();
            $params[':nosso_numero'] = 0;
            $params[':enviado_cartorio'] = 0;
            $params[':boleto_mensagem1'] = $this->getBoletoMensagem1();
            $params[':boleto_mensagem2'] = $this->getBoletoMensagem2();
            $params[':boleto_mensagem3'] = $this->getBoletoMensagem3();
            $params[':boleto_desconto'] = formatar_Dinheiro($this->getBoletoDesconto());
            $params[':boleto_abatimento'] = formatar_Dinheiro($this->getBoletoAbatimento());
            $params[':boleto_mora'] = formatar_Dinheiro($this->getBoletoMora());
            $params[':boleto_multa'] = formatar_Dinheiro($this->getBoletoMulta());
            $this->Executar($this->sqlInsert, $params);
        }
    }

    public function select($where = '', $order = '', $more = '')
    {
        $where = (empty($where) ? "" : " WHERE " . $where);
        $order = (empty($order) ? "" : " ORDER BY " . $order);

        $sql = sprintf($this->sqlSelect, $where, $order, $more);
        return $this->RunSelect($sql);
    }

    public function importar($dataEntrada, $valor, $nossoNumero)
    {
        $dtPgto = '20' . substr($dataEntrada, 4, 2) . '-' . substr($dataEntrada, 2, 2) . '-' . substr($dataEntrada, 0, 2);

        $params = array(
            ':id_situacao' => 3,
            ':nosso_numero' => $nossoNumero,
            ':valor_pgto' => number_format(((int)$valor / 100), 2, '.', ''),
            ':data_pgto' => $dtPgto
        );

        $this->Executar($this->sqlUpdateImportar, $params);
    }

    public function updateNossoNumero()
    {
        $params = array(
            ':id_situacao' => 3,
            ':nosso_numero' => $this->getNossoNumero(),
            ':id' => $this->getId()
        );

        $this->Executar($this->sqlUpdateNossoNumero, $params);
    }

    public function updateSituacao()
    {
        $params = array(
            ':id_situacao' => $this->getIdSituacao(),
            ':id' => $this->getId()
        );

        $this->Executar($this->sqlUpdateSituacao, $params);
    }

    public function ultimo_registro()
    {
        $sql = sprintf($this->sqlUltimoID);
        return $this->RunSelect($sql);
    }

    public function updateEnviadoCartorio()
    {
        $params = array(
            ':enviado_cartorio' => $this->getEnviadoCartorio(),
            ':id' => $this->getId()
        );

        $this->Executar($this->sqlUpdateEnviadoCartorio, $params);
    }

    public function updateConfigBoleto()
    {
        $params = array(
            ':boleto_mensagem1' => $this->getBoletoMensagem1(),
            ':boleto_mensagem2' => $this->getBoletoMensagem2(),
            ':boleto_mensagem3' => $this->getBoletoMensagem3(),
            ':boleto_desconto' => formatar_Dinheiro($this->getBoletoDesconto()),
            ':boleto_abatimento' => formatar_Dinheiro($this->getBoletoAbatimento()),
            ':boleto_mora' => formatar_Dinheiro($this->getBoletoMora()),
            ':boleto_multa' => formatar_Dinheiro($this->getBoletoMulta()),
            ':id' => $this->getId()
        );

        $this->Executar($this->sqlUppdateConfigBoleto, $params);
    }

    public function totalRegistrosPorCliente($id_cliente)
    {
        $sql = sprintf($this->sqlTotalRegistrosPorCliente, $id_cliente);
        return $this->RunSelect($sql);
    }
}

?>