<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_MODELO . "mDevedores.php";

class cDevedores extends mDevedores
{
    protected $sqlInsert = "INSERT INTO devedores (observacao, id_cliente, id_tipo) VALUES (:observacao, :id_cliente, :id_tipo)";
    protected $sqlUpdate = "UPDATE devedores SET observacao = :observacao, id_tipo = :id_tipo WHERE (id = :id)";
    protected $sqlDelete = "DELETE FROM devedores WHERE (id = :id)";

    protected $sqlSelect = "
      SELECT 
          d.*,
          cid1.id_estado, cid1.titulo as 'cidade', est1.sigla as 'uf',
          df.nome, dj.razao_social, dj.nome_fantasia, df.cpf, dj.cnpj, df.rg, dj.ie, df.data_nascimento,
          dc.tel1, dc.contato1, dc.email1, dc.tel2, dc.contato2, dc.email2, dc.tel3, dc.contato3, dc.email3, 
          if(d.id_tipo = 1, 'Fisica', 'Juridica') as 'tipo',
          if(d.id_tipo = 1, df.nome, dj.nome_fantasia) as 'grid_nome', 
          if(d.id_tipo = 1, df.cpf, dj.cnpj) as 'grid_cpf_cnpj',
          if(d.id_tipo = 1, df.rg, dj.ie) as 'grid_rg_ie',
          de.cep1, de.logradouro1, de.numero1, de.complemento1, de.bairro1, de.id_cidade1, cid1.id_estado as 'id_estado1',
          de.cep2, de.logradouro2, de.numero2, de.complemento2, de.bairro2, de.id_cidade2, cid2.id_estado as 'id_estado2',
          de.cep3, de.logradouro3, de.numero3, de.complemento3, de.bairro3, de.id_cidade3, cid3.id_estado as 'id_estado3'
      
      FROM devedores d
          LEFT JOIN devedores_pf df ON df.id_devedor = d.id
          LEFT JOIN devedores_pj dj ON dj.id_devedor = d.id
          LEFT JOIN devedores_contatos dc ON dc.id_devedor = d.id
          LEFT JOIN devedores_enderecos de ON de.id_devedor = d.id

          LEFT JOIN cidades cid1 ON cid1.id = de.id_cidade1
          LEFT JOIN estados est1 ON est1.id = cid1.id_estado
          LEFT JOIN cidades cid2 ON cid2.id = de.id_cidade2
          LEFT JOIN estados est2 ON est2.id = cid2.id_estado
          LEFT JOIN cidades cid3 ON cid3.id = de.id_cidade3
          LEFT JOIN estados est3 ON est3.id = cid3.id_estado
      
      %s %s %s";

    protected $sqlUltimoID = "SELECT * FROM devedores ORDER BY id DESC LIMIT 1";
    
    protected $sqlTotalDevedoresPorCliente = "
        SELECT 
          COUNT(*) as 'total', c.id
    
        FROM devedores d
          INNER JOIN clientes c ON c.id = d.id_cliente
                
        GROUP BY
          c.id
    ";

    protected $sqlTotalRegistrosF = "SELECT count(*) as 'total' FROM devedores WHERE (id_tipo = 1)";
    protected $sqlTotalRegistrosJ = "SELECT count(*) as 'total' FROM devedores WHERE (id_tipo = 0)";

    public function salvar()
    {
        $params = array(
            ':id_tipo' => $this->getIdTipo(),
            ':observacao' => $this->getObservacao()
        );

        if ($this->getId()) {
            $params[':id'] = $this->getId();
            $this->Executar($this->sqlUpdate, $params);
        } else {
            $params[':id_cliente'] = $this->getIdCliente();
            $this->Executar($this->sqlInsert, $params);
        }
    }

    public function delete()
    {
        $params = array(
            ':id' => $this->getId()
        );

        $this->Executar($this->sqlDelete, $params);
    }

    public function select($where = '', $order = '', $more = '')
    {
        $where = (empty($where) ? "" : " WHERE " . $where);
        $order = (empty($order) ? "" : " ORDER BY " . $order);

        $sql = sprintf($this->sqlSelect, $where, $order, $more);
        return $this->RunSelect($sql);
    }

    public function ultimo_registro()
    {
        $sql = sprintf($this->sqlUltimoID);
        return $this->RunSelect($sql);
    }

    public function totalDevedoresPorCliente() {
        $sql = sprintf($this->sqlTotalDevedoresPorCliente);
        return $this->RunSelect($sql);
    }

    public function totalRegistrosF()
    {
        $sql = sprintf($this->sqlTotalRegistrosF);
        return $this->RunSelect($sql);
    }

    public function totalRegistrosJ()
    {
        $sql = sprintf($this->sqlTotalRegistrosJ);
        return $this->RunSelect($sql);
    }
}

?>