<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_MODELO . "mConfig.php";

class cConfig extends mConfig
{
    protected $sqlUpdate = "UPDATE config SET email_envio = :email_envio, senha_envio = :senha_envio, smtp_envio = :smtp_envio, ws_url = :ws_url, ws_usuario = :ws_usuario, ws_senha = :ws_senha, 
      qtd_itens_listagem = :qtd_itens_listagem, exibir_total_registro_menu = :exibir_total_registro_menu, exibir_grafico_ultimos_meses = :exibir_grafico_ultimos_meses, boleto_mensagem1 = :boleto_mensagem1, 
      boleto_mensagem2 = :boleto_mensagem2, boleto_mensagem3 = :boleto_mensagem3, boleto_desconto = :boleto_desconto, boleto_abatimento = :boleto_abatimento, boleto_cedente_digito = :boleto_cedente_digito, boleto_cedente = :boleto_cedente, 
      boleto_mora = :boleto_mora, boleto_multa = :boleto_multa, boleto_id_banco = :boleto_id_banco, boleto_agencia = :boleto_agencia, boleto_cc = :boleto_cc, 
      boleto_id_carteira = :boleto_id_carteira, empresa_nome = :empresa_nome, empresa_endereco = :empresa_endereco, empresa_cnpj = :empresa_cnpj, empresa_id_estado = :empresa_id_estado, empresa_id_municipio = :empresa_id_municipio, 
      boleto_agencia_digito = :boleto_agencia_digito, boleto_cc_digito = :boleto_cc_digito, cartorio_agencia = :cartorio_agencia,  
      cartorio_versao_layout = :cartorio_versao_layout, cartorio_sigla_remetente = :cartorio_sigla_remetente, cartorio_sigla_destinatario = :cartorio_sigla_destinatario,
      cartorio_sigla_transacao = :cartorio_sigla_transacao, cartorio_cod_portador = :cartorio_cod_portador, cartorio_portador = :cartorio_portador,
      boleto_numero_sequencial_nosso_numero = :boleto_numero_sequencial_nosso_numero, boleto_numero_sequencial_remessa = :boleto_numero_sequencial_remessa,
      ws_L0001 = :ws_L0001, ws_L0002 = :ws_L0002, ws_L0003 = :ws_L0003, ws_L0004 = :ws_L0004, ws_L0006 = :ws_L0006, ws_L0007 = :ws_L0007, ws_L0008 = :ws_L0008, ws_L0011 = :ws_L0011, 
      ws_L0014 = :ws_L0014, ws_L0015 = :ws_L0015, ws_L0022 = :ws_L0022, ws_L0024 = :ws_L0024, ws_L0029 = :ws_L0029, ws_L0030 = :ws_L0030, ws_L0032 = :ws_L0032, ws_L0033 = :ws_L0033, 
      ws_L0035 = :ws_L0035, ws_L0038 = :ws_L0038, ws_L0039 = :ws_L0039, ws_L0041 = :ws_L0041, ws_L0059 = :ws_L0059, ws_L0064 = :ws_L0064, ws_L0066 = :ws_L0066, ws_L0067 = :ws_L0067, 
      ws_L0068 = :ws_L0068, ws_R0002 = :ws_R0002, ws_R0014 = :ws_R0014, ws_R0016 = :ws_R0016, ws_R0001 = :ws_R0001, ws_R0019 = :ws_R0019
    ";

    protected $sqlSelect = "
        SELECT 
          c.*, m.titulo as 'empresa_cidade', e.titulo as 'empresa_uf'
        
        FROM config c
            LEFT JOIN municipios m ON m.id = c.empresa_id_municipio
            LEFT JOIN estados e ON e.id = m.id_estado
        
        LIMIT 1";

    protected $sqlUpdateNumeroSequecialBoletoRemessa = "UPDATE config SET boleto_numero_sequencial_remessa = :boleto_numero_sequencial_remessa";

    protected $sqlUpdateNumeroSequecialBoletoNossoNumero = "UPDATE config SET boleto_numero_sequencial_nosso_numero = :boleto_numero_sequencial_nosso_numero";

    protected $sqlUpdateNumeroSequecialCartorio = "UPDATE config SET cartorio_numero_sequencial = :cartorio_numero_sequencial";
    
    public function salvar()
    {
        $params = array(
            ':email_envio' => strtolower($this->getEmailEnvio()),
            ':senha_envio' => $this->getSenhaEnvio(),
            ':smtp_envio' => $this->getSmtpEnvio(),
            ':ws_url' => $this->getWsUrl(),
            ':ws_usuario' => $this->getWsUsuario(),
            ':ws_senha' => $this->getWsSenha(),
            ':qtd_itens_listagem' => $this->getQtdItensListagem(),
            ':ws_L0001' => (($this->getWsL0001() == true) ? 1 : 0),
            ':ws_L0002' => (($this->getWsL0002() == true) ? 1 : 0),
            ':ws_L0003' => (($this->getWsL0003() == true) ? 1 : 0),
            ':ws_L0004' => (($this->getWsL0004() == true) ? 1 : 0),
            ':ws_L0006' => (($this->getWsL0006() == true) ? 1 : 0),
            ':ws_L0007' => (($this->getWsL0007() == true) ? 1 : 0),
            ':ws_L0008' => (($this->getWsL0008() == true) ? 1 : 0),
            ':ws_L0011' => (($this->getWsL0011() == true) ? 1 : 0),
            ':ws_L0014' => (($this->getWsL0014() == true) ? 1 : 0),
            ':ws_L0015' => (($this->getWsL0015() == true) ? 1 : 0),
            ':ws_L0022' => (($this->getWsL0022() == true) ? 1 : 0),
            ':ws_L0024' => (($this->getWsL0024() == true) ? 1 : 0),
            ':ws_L0029' => (($this->getWsL0029() == true) ? 1 : 0),
            ':ws_L0030' => (($this->getWsL0030() == true) ? 1 : 0),
            ':ws_L0032' => (($this->getWsL0032() == true) ? 1 : 0),
            ':ws_L0033' => (($this->getWsL0033() == true) ? 1 : 0),
            ':ws_L0035' => (($this->getWsL0035() == true) ? 1 : 0),
            ':ws_L0038' => (($this->getWsL0038() == true) ? 1 : 0),
            ':ws_L0039' => (($this->getWsL0039() == true) ? 1 : 0),
            ':ws_L0041' => (($this->getWsL0041() == true) ? 1 : 0),
            ':ws_L0059' => (($this->getWsL0059() == true) ? 1 : 0),
            ':ws_L0064' => (($this->getWsL0064() == true) ? 1 : 0),
            ':ws_L0066' => (($this->getWsL0066() == true) ? 1 : 0),
            ':ws_L0067' => (($this->getWsL0067() == true) ? 1 : 0),
            ':ws_L0068' => (($this->getWsL0068() == true) ? 1 : 0),
            ':ws_R0002' => (($this->getWsR0002() == true) ? 1 : 0),
            ':ws_R0014' => (($this->getWsR0014() == true) ? 1 : 0),
            ':ws_R0016' => (($this->getWsR0016() == true) ? 1 : 0),
            ':ws_R0001' => (($this->getWsR0001() == true) ? 1 : 0),
            ':ws_R0019' => (($this->getWsR0019() == true) ? 1 : 0),
            ':exibir_total_registro_menu' => (($this->getExibirTotalRegistroMenu() == true) ? 1 : 0),
            ':exibir_grafico_ultimos_meses' => $this->getExibirGraficoUltimosMeses(),
            ':boleto_mensagem1' => $this->getBoletoMensagem1(),
            ':boleto_mensagem2' => $this->getBoletoMensagem2(),
            ':boleto_mensagem3' => $this->getBoletoMensagem3(),
            ':boleto_desconto' => formatar_Dinheiro($this->getBoletoDesconto()),
            ':boleto_abatimento' => formatar_Dinheiro($this->getBoletoAbatimento()),
            ':boleto_mora' => formatar_Dinheiro($this->getBoletoMora()),
            ':boleto_multa' => formatar_Dinheiro($this->getBoletoMulta()),
            ':boleto_id_banco' => $this->getBoletoIdBanco(),
            ':boleto_agencia' => $this->getBoletoAgencia(),
            ':boleto_agencia_digito' => $this->getBoletoAgenciaDigito(),
            ':boleto_cc' => $this->getBoletoCc(),
            ':boleto_cc_digito' => $this->getBoletoCcDigito(),
            ':boleto_cedente' => $this->getBoletoCedente(),
            ':boleto_cedente_digito' => $this->getBoletoCedenteDigito(),
            ':boleto_id_carteira' => $this->getBoletoIdCarteira(),
            ':empresa_nome' => $this->getEmpresaNome(),
            ':empresa_endereco' => $this->getEmpresaEndereco(),
            ':empresa_cnpj' => $this->getEmpresaCnpj(),
            ':empresa_id_estado' => $this->getEmpresaIdEstado(),
            ':empresa_id_municipio' => $this->getEmpresaIdMunicipio(),
            ':cartorio_agencia' => $this->getCartorioAgencia(),
            ':cartorio_sigla_remetente' => $this->getCartorioSiglaRemetente(),
            ':cartorio_sigla_destinatario' => $this->getCartorioSiglaDestinatario(),
            ':cartorio_sigla_transacao' => $this->getCartorioSiglaTransacao(),
            ':cartorio_cod_portador' => $this->getCartorioCodPortador(),
            ':cartorio_portador' => $this->getCartorioPortador(),
            ':cartorio_versao_layout' => $this->getCartorioVersaoLayout(),
            ':boleto_numero_sequencial_nosso_numero' => $this->getBoletoNumeroSequencialNossoNumero(),
            ':boleto_numero_sequencial_remessa' => $this->getBoletoNumeroSequencialRemessa()
        );
        
        $this->Executar($this->sqlUpdate, $params);
    }

    public function updateNumeroSequecialBoletoRemessa()
    {
        $params = array(
            ':boleto_numero_sequencial_remessa' => $this->getBoletoNumeroSequencialRemessa()
        );

        $this->Executar($this->sqlUpdateNumeroSequecialBoletoRemessa, $params);
    }

    public function updateNumeroSequecialBoletoNossoNumero()
    {
        $params = array(
            ':boleto_numero_sequencial_nosso_numero' => $this->getBoletoNumeroSequencialNossoNumero()
        );

        $this->Executar($this->sqlUpdateNumeroSequecialBoletoNossoNumero, $params);
    }

    public function updateNumeroSequecialCartorio()
    {
        $params = array(
            ':cartorio_numero_sequencial' => $this->getCartorioNumeroSequencial()
        );

        $this->Executar($this->sqlUpdateNumeroSequecialCartorio, $params);
    }

    public function select()
    {
        $sql = sprintf($this->sqlSelect);
        return $this->RunSelect($sql);
    }
}

?>