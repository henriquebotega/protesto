<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_MODELO . "mAvisos.php";

class cAvisos extends mAvisos
{
    protected $sqlInsert = "INSERT INTO avisos (tipo, titulo, conteudo, lido, id_cliente) VALUES (:tipo, :titulo, :conteudo, :lido, :id_cliente)";
    protected $sqlDelete = "DELETE FROM avisos WHERE (id = :id)";
    protected $sqlSelect = "
      SELECT 
        a.*, if(c.id_tipo = 1, pf.nome, pj.nome_fantasia) as 'grid_nome'
      
      FROM avisos a
          INNER JOIN clientes c ON c.id = a.id_cliente
          LEFT JOIN clientes_pf pf ON pf.id_cliente = c.id
          LEFT JOIN clientes_pj pj ON pj.id_cliente = c.id
      
      %s %s %s";

    protected $sqlUpdateLido = "UPDATE avisos SET lido = :lido  WHERE (id = :id)";

    public function salvar()
    {
        $params = array(
            ':tipo' => $this->getTipo(),
            ':titulo' => $this->getTitulo(),
            ':conteudo' => $this->getConteudo(),
            ':id_cliente' => $this->getIdCliente(),
            ':lido' => 0
        );

        $this->Executar($this->sqlInsert, $params);
    }

    public function select($where = '', $order = '', $more = '')
    {
        $where = (empty($where) ? "" : " WHERE " . $where);
        $order = (empty($order) ? "" : " ORDER BY " . $order);

        $sql = sprintf($this->sqlSelect, $where, $order, $more);
        return $this->RunSelect($sql);
    }

    public function updateLido()
    {
        $params = array(
            ':id' => $this->getId(),
            ':lido' => 1
        );

        $this->Executar($this->sqlUpdateLido, $params);
    }

    public function delete()
    {
        $params = array(
            ':id' => $this->getId()
        );

        $this->Executar($this->sqlDelete, $params);
    }
}

?>