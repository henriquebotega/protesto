<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_MODELO . "mClientes.php";

class cClientes extends mClientes
{
    protected $sqlInsert = "INSERT INTO clientes (id_situacao, id_tipo, data_cadastro, cep, logradouro, numero, complemento, bairro, id_cidade) VALUES (:id_situacao, :id_tipo, :data_cadastro, :cep, :logradouro, :numero, :complemento, :bairro, :id_cidade)";
    protected $sqlUpdate = "UPDATE clientes SET id_situacao = :id_situacao, id_tipo = :id_tipo, cep = :cep, logradouro = :logradouro, numero = :numero, complemento = :complemento, bairro = :bairro, id_cidade = :id_cidade WHERE (id = :id)";
    protected $sqlDelete = "DELETE FROM clientes WHERE (id = :id)";
    protected $sqlSelect = "
      SELECT 
          c.*, cid.id_estado, cid.titulo as 'cidade', est.sigla as 'uf', 
          pf.nome, pj.razao_social, pj.nome_fantasia, pf.cpf, pj.cnpj, pf.rg, pj.ie, pf.data_nascimento,
          pc.tel1, pc.contato1, pc.email1, pc.tel2, pc.contato2, pc.email2, pc.tel3, pc.contato3, pc.email3, 
          if(c.id_tipo = 1, 'Fisica', 'Juridica') as 'tipo',
          if(c.id_tipo = 1, pf.nome, pj.nome_fantasia) as 'grid_nome', 
          if(c.id_tipo = 1, pf.cpf, pj.cnpj) as 'grid_cpf_cnpj',
          if(c.id_tipo = 1, pf.rg, pj.ie) as 'grid_rg_ie',
          if(c.id_situacao = 0, 'Desativado', 'Ativado') as 'grid_situacao'
      
      FROM clientes c
          LEFT JOIN clientes_pf pf ON pf.id_cliente = c.id
          LEFT JOIN clientes_pj pj ON pj.id_cliente = c.id
          LEFT JOIN clientes_contatos pc ON pc.id_cliente = c.id
          LEFT JOIN cidades cid ON cid.id = c.id_cidade
          LEFT JOIN estados est ON est.id = cid.id_estado
      
      %s %s %s";

    protected $sqlUltimoID = "SELECT * FROM clientes ORDER BY id DESC LIMIT 1";
    protected $sqlTotalRegistrosF = "SELECT count(*) as 'total' FROM clientes WHERE (id_tipo = 1)";
    protected $sqlTotalRegistrosJ = "SELECT count(*) as 'total' FROM clientes WHERE (id_tipo = 0)";

    public function salvar()
    {
        $params = array(
            ':id_situacao' => $this->getIdSituacao(),
            ':id_tipo' => $this->getIdTipo(),
            ':cep' => $this->getCep(),
            ':logradouro' => ucfirst($this->getLogradouro()),
            ':numero' => $this->getNumero(),
            ':complemento' => ucfirst($this->getComplemento()),
            ':bairro' => ucfirst($this->getBairro()),
            ':id_cidade' => $this->getIdCidade()
        );

        if ($this->getId()) {
            $params[':id'] = $this->getId();
            $this->Executar($this->sqlUpdate, $params);
        } else {
            $params[':data_cadastro'] = date("Y-m-d H:i:s", time());
            $this->Executar($this->sqlInsert, $params);
        }
    }

    public function delete()
    {
        $params = array(
            ':id' => $this->getId()
        );

        $this->Executar($this->sqlDelete, $params);
    }

    public function select($where = '', $order = '', $more = '')
    {
        $where = (empty($where) ? "" : " WHERE " . $where);
        $order = (empty($order) ? "" : " ORDER BY " . $order);

        $sql = sprintf($this->sqlSelect, $where, $order, $more);
        return $this->RunSelect($sql);
    }

    public function ultimo_registro()
    {
        $sql = sprintf($this->sqlUltimoID);
        return $this->RunSelect($sql);
    }

    public function totalRegistrosF()
    {
        $sql = sprintf($this->sqlTotalRegistrosF);
        return $this->RunSelect($sql);
    }

    public function totalRegistrosJ()
    {
        $sql = sprintf($this->sqlTotalRegistrosJ);
        return $this->RunSelect($sql);
    }
}

?>