<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_MODELO . "mSessao.php";

class cSessao extends mSessao
{
    protected $sqlInsert = "INSERT INTO sessao (codigo, data_entrada, data_limite, id_admin) VALUES (:codigo, :data_entrada, :data_limite, :id_admin)";
    protected $sqlDelete = "DELETE FROM sessao WHERE (id = :id)";
    protected $sqlSelect = "SELECT * FROM sessao %s %s %s";

    public function salvar()
    {
        $params = array(
            ':id_admin' => $this->getIdAdmin(),
            ':codigo' => $this->getCodigo(),
            ':data_entrada' => date("Y-m-d H:i:s", time()),
            ':data_limite' => date("Y-m-d H:i:s", time() + 3600)
        );

        $this->Executar($this->sqlInsert, $params);
    }

    public function delete()
    {
        $params = array(
            ':id' => $this->getId()
        );

        $this->Executar($this->sqlDelete, $params);
    }

    public function select($where = '', $order = '', $more = '')
    {
        $where = (empty($where) ? "" : " WHERE " . $where);
        $order = (empty($order) ? "" : " ORDER BY " . $order);

        $sql = sprintf($this->sqlSelect, $where, $order, $more);
        return $this->RunSelect($sql);
    }

    public function gerarCodigo()
    {
        $numero = 0;
        $sair = false;

        // Verifica se já está em uso
        while ($sair === false) {

            // Gera o numero randômico
            $numero = md5(rand(111111, 999999));

            $sql = sprintf($this->sqlSelect, " WHERE (codigo = '$numero') ", "", "");
            $res = $this->RunSelect($sql);

            if (sizeof($res) == 0) {
                $sair = true;
            }
        }

        // Retorna o numero
        return $numero;
    }
}

?>