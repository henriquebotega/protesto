<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_MODELO . "mDevedoresPF.php";

class cDevedoresPF extends mDevedoresPF
{
    protected $sqlInsert = "INSERT INTO devedores_pf (id_devedor, nome, cpf, rg, data_nascimento) VALUES (:id_devedor, :nome, :cpf, :rg, :data_nascimento)";
    protected $sqlUpdate = "UPDATE devedores_pf SET nome = :nome, cpf = :cpf, rg = :rg, data_nascimento = :data_nascimento WHERE (id = :id)";
    protected $sqlDelete = "DELETE FROM devedores_pf WHERE (id = :id)";
    protected $sqlSelect = "SELECT * FROM devedores_pf %s %s %s";

    protected $sqlUltimoID = "SELECT * FROM devedores_pf ORDER BY id DESC LIMIT 1";

    public function salvar()
    {
        $params = array(
            ':nome' => ucwords($this->getNome()),
            ':cpf' => $this->getCpf(),
            ':rg' => $this->getRg(),
            ':data_nascimento' => formatar_Data($this->getDataNascimento())
        );

        if ($this->getId()) {
            $params[':id'] = $this->getId();
            $this->Executar($this->sqlUpdate, $params);
        } else {
            $params[':id_devedor'] = $this->getIdDevedor();
            $this->Executar($this->sqlInsert, $params);
        }
    }

    public function delete()
    {
        $params = array(
            ':id' => $this->getId()
        );

        $this->Executar($this->sqlDelete, $params);
    }

    public function select($where = '', $order = '', $more = '')
    {
        $where = (empty($where) ? "" : " WHERE " . $where);
        $order = (empty($order) ? "" : " ORDER BY " . $order);

        $sql = sprintf($this->sqlSelect, $where, $order, $more);
        return $this->RunSelect($sql);
    }

    public function ultimo_registro()
    {
        $sql = sprintf($this->sqlUltimoID);
        return $this->RunSelect($sql);
    }
}

?>