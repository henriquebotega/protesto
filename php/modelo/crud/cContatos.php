<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_MODELO . "mContatos.php";

class cContatos extends mContatos
{
    protected $sqlInsert = "INSERT INTO contatos (titulo, mensagem, data, id_cliente) VALUES (:titulo, :mensagem, :data, :id_cliente)";
    protected $sqlUpdate = "UPDATE contatos SET titulo = :titulo, mensagem = :mensagem WHERE (id = :id)";
    protected $sqlDelete = "DELETE FROM contatos WHERE (id = :id)";
    protected $sqlSelect = "SELECT * FROM contatos %s %s %s";

    protected $sqlTotalRegistros = "SELECT count(*) as 'total' FROM contatos WHERE %s";

    public function salvar()
    {
        $params = array(
            ':titulo' => $this->getTitulo(),
            ':mensagem' => $this->getMensagem()
        );

        if ($this->getId()) {
            $params[':id'] = $this->getId();
            $this->Executar($this->sqlUpdate, $params);
        } else {
            $params[':data'] = date('Y-m-d H:i:s', time());
            $params[':id_cliente'] = $this->getIdCliente();
            $this->Executar($this->sqlInsert, $params);
        }
    }

    public function select($where = '', $order = '', $more = '')
    {
        $where = (empty($where) ? "" : " WHERE " . $where);
        $order = (empty($order) ? "" : " ORDER BY " . $order);

        $sql = sprintf($this->sqlSelect, $where, $order, $more);
        return $this->RunSelect($sql);
    }

    public function delete()
    {
        $params = array(
            ':id' => $this->getId()
        );

        $this->Executar($this->sqlDelete, $params);
    }

    public function totalRegistros($where)
    {
        $sql = sprintf($this->sqlTotalRegistros, $where);
        return $this->RunSelect($sql);
    }
}

?>