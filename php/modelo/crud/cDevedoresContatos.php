<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_MODELO . "mDevedoresContatos.php";

class cDevedoresContatos extends mDevedoresContatos
{
    protected $sqlInsert = "INSERT INTO devedores_contatos (id_devedor, tel1, contato1, email1, tel2, contato2, email2, tel3, contato3, email3) VALUES (:id_devedor, :tel1, :contato1, :email1, :tel2, :contato2, :email2, :tel3, :contato3, :email3)";
    protected $sqlUpdate = "UPDATE devedores_contatos SET tel1 = :tel1, contato1 = :contato1, email1 = :email1, tel2 = :tel2, contato2 = :contato2, email2 = :email2, tel3 = :tel3, contato3 = :contato3, email3 = :email3 WHERE (id = :id)";
    protected $sqlSelect = "SELECT * FROM devedores_contatos %s %s %s";
    protected $sqlDelete = "DELETE FROM devedores_contatos WHERE (id_devedor = :id_devedor)";
    
    public function salvar()
    {
        $params = array(
            ':tel1' => $this->getTel1(),
            ':contato1' => $this->getContato1(),
            ':email1' => $this->getEmail1(),
            ':tel2' => $this->getTel2(),
            ':contato2' => $this->getContato2(),
            ':email2' => $this->getEmail2(),
            ':tel3' => $this->getTel3(),
            ':contato3' => $this->getContato3(),
            ':email3' => $this->getEmail3()
        );

        if ($this->getId()) {
            $params[':id'] = $this->getId();
            $this->Executar($this->sqlUpdate, $params);
        } else {
            $params[':id_devedor'] = $this->getIdDevedor();
            $this->Executar($this->sqlInsert, $params);
        }
    }

    public function delete()
    {
        $params = array(
            ':id_devedor' => $this->getIdDevedor()
        );

        $this->Executar($this->sqlDelete, $params);
    }
    
    public function select($where = '', $order = '', $more = '')
    {
        $where = (empty($where) ? "" : " WHERE " . $where);
        $order = (empty($order) ? "" : " ORDER BY " . $order);

        $sql = sprintf($this->sqlSelect, $where, $order, $more);
        return $this->RunSelect($sql);
    }
}

?>