<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_MODELO . "mLayoutsImportados.php";

class cLayoutsImportados extends mLayoutsImportados
{
    protected $sqlInsert = "INSERT INTO layouts_importados (id_cliente, data, id_layout) VALUES (:id_cliente, :data, :id_layout)";
    protected $sqlDelete = "DELETE FROM layouts_importados WHERE (id = :id)";
    protected $sqlSelect = "
      SELECT 
          li.*, l.titulo
      
      FROM layouts_importados li
          INNER JOIN layouts l ON l.id = li.id_layout

      %s %s %s";

    protected $sqlUpdateArquivo = "UPDATE layouts_importados SET arquivo = :arquivo WHERE (id = :id)";
    protected $sqlUpdateImportacao = "UPDATE layouts_importados SET total_registros = :total_registros, total_registros_sucesso = :total_registros_sucesso, erros = :erros WHERE (id = :id)";
    protected $sqlUltimoID = "SELECT * FROM layouts_importados ORDER BY id DESC LIMIT 1";

    public function salvar()
    {
        $params = array(
            ':id_cliente' => $this->getIdCliente(),
            ':id_layout' => $this->getIdLayout(),
            ':data' => date('Y-m-d H:i:s', time())
        );

        $this->Executar($this->sqlInsert, $params);
    }

    public function select($where = '', $order = '', $more = '')
    {
        $where = (empty($where) ? "" : " WHERE " . $where);
        $order = (empty($order) ? "" : " ORDER BY " . $order);

        $sql = sprintf($this->sqlSelect, $where, $order, $more);
        return $this->RunSelect($sql);
    }

    public function delete()
    {
        $params = array(
            ':id' => $this->getId()
        );

        $this->Executar($this->sqlDelete, $params);
    }

    public function ultimo_registro()
    {
        $sql = sprintf($this->sqlUltimoID);
        return $this->RunSelect($sql);
    }

    public function updateArquivo()
    {
        $params = array(
            ':id' => $this->getId(),
            ':arquivo' => $this->getArquivo()
        );

        $this->Executar($this->sqlUpdateArquivo, $params);
    }

    public function updateImportacao()
    {
        $params = array(
            ':id' => $this->getId(),
            ':total_registros' => $this->getTotalRegistros(),
            ':total_registros_sucesso' => $this->getTotalRegistrosSucesso(),
            ':erros' => $this->getErros()
        );

        $this->Executar($this->sqlUpdateImportacao, $params);
    }
}

?>