<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_MODELO . "mDevedoresPJ.php";

class cDevedoresPJ extends mDevedoresPJ
{
    protected $sqlInsert = "INSERT INTO devedores_pj (id_devedor, razao_social, nome_fantasia, cnpj, ie) VALUES (:id_devedor, :razao_social, :nome_fantasia, :cnpj, :ie)";
    protected $sqlUpdate = "UPDATE devedores_pj SET razao_social = :razao_social, nome_fantasia = :nome_fantasia, cnpj = :cnpj, ie = :ie WHERE (id = :id)";
    protected $sqlDelete = "DELETE FROM devedores_pj WHERE (id = :id)";
    protected $sqlSelect = "SELECT * FROM devedores_pj %s %s %s";

    protected $sqlUltimoID = "SELECT * FROM devedores_pj ORDER BY id DESC LIMIT 1";
    
    public function salvar()
    {
        $params = array(
            ':razao_social' => ucwords($this->getRazaoSocial()),
            ':nome_fantasia' => ucwords($this->getNomeFantasia()),
            ':cnpj' => $this->getCnpj(),
            ':ie' => $this->getIe()
        );

        if ($this->getId()) {
            $params[':id'] = $this->getId();
            $this->Executar($this->sqlUpdate, $params);
        } else {
            $params[':id_devedor'] = $this->getIdDevedor();
            $this->Executar($this->sqlInsert, $params);
        }
    }

    public function delete()
    {
        $params = array(
            ':id' => $this->getId()
        );

        $this->Executar($this->sqlDelete, $params);
    }

    public function select($where = '', $order = '', $more = '')
    {
        $where = (empty($where) ? "" : " WHERE " . $where);
        $order = (empty($order) ? "" : " ORDER BY " . $order);

        $sql = sprintf($this->sqlSelect, $where, $order, $more);
        return $this->RunSelect($sql);
    }

    public function ultimo_registro()
    {
        $sql = sprintf($this->sqlUltimoID);
        return $this->RunSelect($sql);
    }
}

?>