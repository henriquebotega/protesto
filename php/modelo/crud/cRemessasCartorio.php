<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_MODELO . "mRemessasCartorio.php";

class cRemessasCartorio extends mRemessasCartorio
{
    protected $sqlInsert = "INSERT INTO remessas_cartorio (id_divida, data, nosso_numero, enviado_cartorio, retorno_cartorio) VALUES (:id_divida, :data, :nosso_numero, :enviado_cartorio, :retorno_cartorio)";
    protected $sqlUpdate = "UPDATE remessas_cartorio SET data_saida_cartorio = :data_saida_cartorio, enviado_cartorio = :enviado_cartorio, cartorio_numero_sequencial = :cartorio_numero_sequencial WHERE (id_divida = :id_divida)";
    protected $sqlSelect = "
      SELECT 
        rc.*, d.id_devedor,
        if(dev.id_tipo = 1, pf.nome, pj.nome_fantasia) as 'grid_devedor'

      FROM remessas_cartorio rc
        LEFT JOIN dividas d ON d.id = rc.id_divida
        LEFT JOIN devedores dev ON dev.id = d.id_devedor
        LEFT JOIN devedores_pf pf ON pf.id_devedor = dev.id
        LEFT JOIN devedores_pj pj ON pj.id_devedor = dev.id
      
      %s %s %s";

    protected $sqlTotalRegistrosPorCliente = "
        SELECT 
            count(d.id) as 'total',
            if(dev.id_tipo = 1, pf.nome, pj.nome_fantasia) as 'grid_devedor'
          
          FROM remessas_cartorio rc
            LEFT JOIN dividas d ON d.id = rc.id_divida
            LEFT JOIN devedores dev ON dev.id = d.id_devedor
            LEFT JOIN devedores_pf pf ON pf.id_devedor = dev.id
            LEFT JOIN devedores_pj pj ON pj.id_devedor = dev.id
          
          WHERE
            (dev.id_cliente = %s)
            
          GROUP BY
            dev.id_cliente
    ";

    protected $sqlTotalRegistros = "SELECT count(*) as 'total' FROM remessas_cartorio";

//    protected $sqlUpdateImportar = "UPDATE remessas_cartorio SET cod_remessa = :cod_remessa, data_retorno_cartorio = :data_retorno_cartorio, retorno_cartorio = :retorno_cartorio WHERE (nosso_numero = :nosso_numero)";

    public function salvar()
    {
        $params = array(
            ':retorno_cartorio' => false,
            ':id_divida' => $this->getIdDivida(),
            ':nosso_numero' => $this->getNossoNumero(),
            ':data' => date('Y-m-d H:i:s', time()),
            ':enviado_cartorio' => false
        );

        $this->Executar($this->sqlInsert, $params);
    }

    public function importar()
    {
//        $params = array(
//            ':data_retorno_cartorio' => date('Y-m-d H:i:s', time()),
//            ':retorno_cartorio' => true,
//            ':cod_remessa' => $this->getCodRemessa(),
//            ':nosso_numero' => $this->getNossoNumero()
//        );
//
//        $this->Executar($this->sqlUpdateImportar, $params);
    }

    public function salvar_enviado_cartorio()
    {
        $params = array(
            ':data_saida_cartorio' => date('Y-m-d H:i:s', time()),
            ':enviado_cartorio' => true,
            ':cartorio_numero_sequencial' => $this->getCartorioNumeroSequencial(),
            ':id_divida' => $this->getIdDivida()
        );

        $this->Executar($this->sqlUpdate, $params);
    }

    public function select($where = '', $order = '', $more = '')
    {
        $where = (empty($where) ? "" : " WHERE " . $where);
        $order = (empty($order) ? "" : " ORDER BY " . $order);

        $sql = sprintf($this->sqlSelect, $where, $order, $more);
        return $this->RunSelect($sql);
    }

    public function totalRegistros()
    {
        $sql = sprintf($this->sqlTotalRegistros);
        return $this->RunSelect($sql);
    }

    public function totalRegistrosPorCliente($id_cliente)
    {
        $sql = sprintf($this->sqlTotalRegistrosPorCliente, $id_cliente);
        return $this->RunSelect($sql);
    }
}

?>