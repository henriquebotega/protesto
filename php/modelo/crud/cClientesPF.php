<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_MODELO . "mClientesPF.php";

class cClientesPF extends mClientesPF
{
    protected $sqlInsert = "INSERT INTO clientes_pf (cpf, nome, rg, data_nascimento, id_cliente) VALUES (:cpf, :nome, :rg, :data_nascimento, :id_cliente)";
    protected $sqlUpdate = "UPDATE clientes_pf SET cpf = :cpf, nome = :nome, rg = :rg, data_nascimento = :data_nascimento WHERE (id = :id)";
    protected $sqlDelete = "DELETE FROM clientes_pf WHERE (id_cliente = :id_cliente)";
    protected $sqlSelect = "SELECT * FROM clientes_pf %s %s %s";

    public function salvar()
    {
        $params = array(
            ':cpf' => $this->getCpf(),
            ':nome' => ucwords($this->getNome()),
            ':rg' => $this->getRg(),
            ':data_nascimento' => formatar_Data($this->getDataNascimento())
        );

        if ($this->getId()) {
            $params[':id'] = $this->getId();
            $this->Executar($this->sqlUpdate, $params);
        } else {
            $params[':id_cliente'] = $this->getIdCliente();
            $this->Executar($this->sqlInsert, $params);
        }
    }

    public function delete()
    {
        $params = array(
            ':id_cliente' => $this->getIdCliente()
        );

        $this->Executar($this->sqlDelete, $params);
    }

    public function select($where = '', $order = '', $more = '')
    {
        $where = (empty($where) ? "" : " WHERE " . $where);
        $order = (empty($order) ? "" : " ORDER BY " . $order);

        $sql = sprintf($this->sqlSelect, $where, $order, $more);
        return $this->RunSelect($sql);
    }
}

?>