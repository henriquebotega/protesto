<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_MODELO . "mAdmin.php";

class cAdmin extends mAdmin
{
    protected $sqlInsert = "INSERT INTO admin (email, senha, id_tipo, id_situacao, data_ultimo_acesso) VALUES (:email, :senha, :id_tipo, :id_situacao, :data_ultimo_acesso)";
    protected $sqlUpdate = "UPDATE admin SET email = :email, id_tipo = :id_tipo, id_situacao = :id_situacao WHERE (id = :id)";
    protected $sqlDelete = "DELETE FROM admin WHERE (id = :id)";
    protected $sqlSelect = "
      SELECT 
          a.* 
      
      FROM admin a
          LEFT JOIN usuarios u ON u.id_admin = a.id
      
      %s %s %s";

    protected $upd_Senha = "UPDATE admin SET senha = :senha WHERE (id = :id)";
    protected $sqlAtualizaUltimoAcesso = "UPDATE admin SET data_ultimo_acesso = :data_ultimo_acesso  WHERE (id = :id)";
    protected $sqlUltimoID = "SELECT * FROM admin ORDER BY id DESC LIMIT 1";
    protected $sqlLogar = "SELECT * FROM admin WHERE (email = '%s' AND senha = '%s') LIMIT 1";
    protected $sqlTotalRegistros = "SELECT count(*) as 'total' FROM admin WHERE (id_tipo = 1)";

    public function salvar()
    {
        $params = array(
            ':email' => strtolower($this->getEmail()),
            ':id_situacao' => $this->getIdSituacao(),
            ':id_tipo' => $this->getIdTipo()
        );

        if ($this->getId()) {
            $params[':id'] = $this->getId();
            $this->Executar($this->sqlUpdate, $params);
        } else {
            $params[':data_ultimo_acesso'] = date('Y-m-d H:i:s', time());
            $params[':senha'] = "";
            $this->Executar($this->sqlInsert, $params);
        }
    }

    public function select($where = '', $order = '', $more = '')
    {
        $where = (empty($where) ? "" : " WHERE " . $where);
        $order = (empty($order) ? "" : " ORDER BY " . $order);

        $sql = sprintf($this->sqlSelect, $where, $order, $more);
        return $this->RunSelect($sql);
    }

    public function ultimo_registro()
    {
        $sql = sprintf($this->sqlUltimoID);
        return $this->RunSelect($sql);
    }

    public function logar($email, $senha)
    {
        $sql = sprintf($this->sqlLogar, $email, md5($senha));
        return $this->RunSelect($sql);
    }

    public function totalRegistros()
    {
        $sql = sprintf($this->sqlTotalRegistros);
        return $this->RunSelect($sql);
    }

    public function atualizar_ultimo_acesso()
    {
        $params = array(
            ':id' => $this->getId(),
            ':data_ultimo_acesso' => date('Y-m-d H:i:s', time())
        );

        $this->Executar($this->sqlAtualizaUltimoAcesso, $params);
    }

    public function delete()
    {
        $params = array(
            ':id' => $this->getId()
        );

        $this->Executar($this->sqlDelete, $params);
    }

    public function upd_Senha()
    {
        $params = array(
            ':senha' => md5($this->getSenha()),
            ':id' => $this->getId()
        );

        $this->Executar($this->upd_Senha, $params);
    }
}

?>