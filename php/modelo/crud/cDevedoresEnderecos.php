<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_MODELO . "mDevedoresEnderecos.php";

class cDevedoresEnderecos extends mDevedoresEnderecos
{
    protected $sqlInsert = "INSERT INTO devedores_enderecos (id_devedor, cep1, cep2, cep3, logradouro1, numero1, complemento1, bairro1, id_cidade1, logradouro2, numero2, complemento2, bairro2, id_cidade2, logradouro3, numero3, complemento3, bairro3, id_cidade3) VALUES (:id_devedor, :cep1, :cep2, :cep3, :logradouro1, :numero1, :complemento1, :bairro1, :id_cidade1, :logradouro2, :numero2, :complemento2, :bairro2, :id_cidade2, :logradouro3, :numero3, :complemento3, :bairro3, :id_cidade3)";
    protected $sqlUpdate = "UPDATE devedores_enderecos SET cep1 = :cep1, cep2 = :cep2, cep3 = :cep3, logradouro1 = :logradouro1, numero1 = :numero1, complemento1 = :complemento1, bairro1 = :bairro1, id_cidade1 = :id_cidade1, logradouro2 = :logradouro2, numero2 = :numero2, complemento2 = :complemento2, bairro2 = :bairro2, id_cidade2 = :id_cidade2, logradouro3 = :logradouro3, numero3 = :numero3, complemento3 = :complemento3, bairro3 = :bairro3, id_cidade3 = :id_cidade3 WHERE (id = :id)";
    protected $sqlSelect = "SELECT * FROM devedores_enderecos %s %s %s";
    protected $sqlDelete = "DELETE FROM devedores_enderecos WHERE (id_devedor = :id_devedor)";
    
    public function salvar()
    {
        $params = array(
            ':cep1' => $this->getCep1(),
            ':logradouro1' => $this->getLogradouro1(),
            ':numero1' => $this->getNumero1(),
            ':complemento1' => $this->getComplemento1(),
            ':bairro1' => $this->getBairro1(),
            ':id_cidade1' => $this->getIdCidade1(),
            ':cep2' => $this->getCep2(),
            ':logradouro2' => $this->getLogradouro2(),
            ':numero2' => $this->getNumero2(),
            ':complemento2' => $this->getComplemento2(),
            ':bairro2' => $this->getBairro2(),
            ':id_cidade2' => $this->getIdCidade2(),
            ':cep3' => $this->getCep3(),
            ':logradouro3' => $this->getLogradouro3(),
            ':numero3' => $this->getNumero3(),
            ':complemento3' => $this->getComplemento3(),
            ':bairro3' => $this->getBairro3(),
            ':id_cidade3' => $this->getIdCidade3()
        );

        if ($this->getId()) {
            $params[':id'] = $this->getId();
            $this->Executar($this->sqlUpdate, $params);
        } else {
            $params[':id_devedor'] = $this->getIdDevedor();
            $this->Executar($this->sqlInsert, $params);
        }
    }

    public function delete()
    {
        $params = array(
            ':id_devedor' => $this->getIdDevedor()
        );

        $this->Executar($this->sqlDelete, $params);
    }
    
    public function select($where = '', $order = '', $more = '')
    {
        $where = (empty($where) ? "" : " WHERE " . $where);
        $order = (empty($order) ? "" : " ORDER BY " . $order);

        $sql = sprintf($this->sqlSelect, $where, $order, $more);
        return $this->RunSelect($sql);
    }
}

?>