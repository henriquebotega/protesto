<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_MODELO . "mLayouts.php";

class cLayouts extends mLayouts
{
    protected $sqlInsert = "INSERT INTO layouts (titulo) VALUES (:titulo)";
    protected $sqlUpdate = "UPDATE layouts SET titulo = :titulo WHERE (id = :id)";
    protected $sqlDelete = "DELETE FROM layouts WHERE (id = :id)";
    protected $sqlSelect = "SELECT * FROM layouts %s %s %s";

    protected $sqlUpdateArquivo = "UPDATE layouts SET arquivo = :arquivo WHERE (id = :id)";
    protected $sqlUltimoID = "SELECT * FROM layouts ORDER BY id DESC LIMIT 1";

    public function salvar()
    {
        $params = array(
            ':titulo' => $this->getTitulo()
        );

        if ($this->getId()) {
            $params[':id'] = $this->getId();
            $this->Executar($this->sqlUpdate, $params);
        } else {
            $this->Executar($this->sqlInsert, $params);
        }
    }

    public function select($where = '', $order = '', $more = '')
    {
        $where = (empty($where) ? "" : " WHERE " . $where);
        $order = (empty($order) ? "" : " ORDER BY " . $order);

        $sql = sprintf($this->sqlSelect, $where, $order, $more);
        return $this->RunSelect($sql);
    }

    public function delete()
    {
        $params = array(
            ':id' => $this->getId()
        );

        $this->Executar($this->sqlDelete, $params);
    }

    public function ultimo_registro()
    {
        $sql = sprintf($this->sqlUltimoID);
        return $this->RunSelect($sql);
    }

    public function updateArquivo()
    {
        $params = array(
            ':id' => $this->getId(),
            ':arquivo' => $this->getArquivo()
        );

        $this->Executar($this->sqlUpdateArquivo, $params);
    }
}

?>