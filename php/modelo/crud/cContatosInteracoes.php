<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_MODELO . "mContatosInteracoes.php";

class cContatosInteracoes extends mContatosInteracoes
{
    protected $sqlInsert = "INSERT INTO contatos_interacoes (id_contato, mensagem, data, id_admin) VALUES (:id_contato, :mensagem, :data, :id_admin)";
    protected $sqlUpdate = "UPDATE contatos_interacoes SET mensagem = :mensagem WHERE (id = :id)";
    protected $sqlDelete = "DELETE FROM contatos_interacoes WHERE (id = :id)";
    protected $sqlSelect = "
        SELECT 
        ci.* 
        
        FROM contatos_interacoes ci
        INNER JOIN contatos c ON c.id = ci.id_contato
        
        %s %s %s";

    public function salvar()
    {
        $params = array(
            ':mensagem' => $this->getMensagem()
        );

        if ($this->getId()) {
            $params[':id'] = $this->getId();
            $this->Executar($this->sqlUpdate, $params);
        } else {
            $params[':data'] = date('Y-m-d H:i:s', time());
            $params[':id_contato'] = $this->getIdContato();
            $params[':id_admin'] = $this->getIdAdmin();
            $this->Executar($this->sqlInsert, $params);
        }
    }

    public function select($where = '', $order = '', $more = '')
    {
        $where = (empty($where) ? "" : " WHERE " . $where);
        $order = (empty($order) ? "" : " ORDER BY " . $order);

        $sql = sprintf($this->sqlSelect, $where, $order, $more);
        return $this->RunSelect($sql);
    }

    public function delete()
    {
        $params = array(
            ':id' => $this->getId()
        );

        $this->Executar($this->sqlDelete, $params);
    }
}

?>