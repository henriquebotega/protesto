<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_MODELO . "mMensalidades.php";

class cMensalidades extends mMensalidades
{
    protected $sqlInsert = "INSERT INTO mensalidades (id_contrato, numero, id_situacao, valor_vcto, valor_pgto, data_vcto, data_pgto, data_vigencia_inicial, data_vigencia_final) VALUES (:id_contrato, :numero, :id_situacao, :valor_vcto, :valor_pgto, :data_vcto, :data_pgto, :data_vigencia_inicial, :data_vigencia_final)";
    protected $sqlUpdate = "UPDATE mensalidades SET valor_vcto = :valor_vcto, data_vcto = :data_vcto WHERE (id = :id)";
    protected $sqlDelete = "DELETE FROM mensalidades WHERE (id = :id)";
    protected $sqlSelect = "
    SELECT 
      m.*, ms.titulo as 'situacao', c.id_cliente,
      if(cli.id_tipo = 1, pf.nome, pj.nome_fantasia) as 'grid_nome'
    
    FROM mensalidades m
      INNER JOIN mensalidades_situacao ms ON ms.id = m.id_situacao
      INNER JOIN contratos c ON c.id = m.id_contrato
      INNER JOIN clientes cli ON cli.id = c.id_cliente
      LEFT JOIN clientes_pf pf ON pf.id_cliente = cli.id
      LEFT JOIN clientes_pj pj ON pj.id_cliente = cli.id
      
    %s %s %s";

    protected $sqlUltimoID = "SELECT * FROM mensalidades ORDER BY id DESC LIMIT 1";
    protected $sqlUpdateBaixa = "UPDATE mensalidades SET id_situacao = :id_situacao, valor_pgto = :valor_pgto, data_pgto = :data_pgto WHERE (id = :id)";

    public function salvar()
    {
        $params = array(
            ':valor_vcto' => formatar_Dinheiro($this->getValorVcto()),
            ':data_vcto' => formatar_Data($this->getDataVcto())
        );

        if ($this->getId()) {
            $params[':id'] = $this->getId();
            $this->Executar($this->sqlUpdate, $params);
        } else {
            $params[':id_contrato'] = $this->getIdContrato();
            $params[':numero'] = $this->getNumero();
            $params[':id_situacao'] = $this->getIdSituacao();
            $params[':valor_pgto'] = $this->getValorPgto();
            $params[':data_pgto'] = formatar_Data($this->getDataPgto());
            $params[':data_vigencia_inicial'] = formatar_Data($this->getDataVigenciaInicial());
            $params[':data_vigencia_final'] = formatar_Data($this->getDataVigenciaFinal());
            $this->Executar($this->sqlInsert, $params);
        }
    }

    public function delete()
    {
        $params = array(
            ':id' => $this->getId()
        );

        $this->Executar($this->sqlDelete, $params);
    }

    public function darBaixa()
    {
        $params = array(
            ':id' => $this->getId(),
            ':id_situacao' => $this->getIdSituacao(),
            ':valor_pgto' => formatar_Dinheiro($this->getValorPgto()),
            ':data_pgto' => formatar_DataHora($this->getDataPgto())
        );

        $this->Executar($this->sqlUpdateBaixa, $params);
    }

    public function select($where = '', $order = '', $more = '')
    {
        $where = (empty($where) ? "" : " WHERE " . $where);
        $order = (empty($order) ? "" : " ORDER BY " . $order);

        $sql = sprintf($this->sqlSelect, $where, $order, $more);
        return $this->RunSelect($sql);
    }

    public function ultimo_registro()
    {
        $sql = sprintf($this->sqlUltimoID);
        return $this->RunSelect($sql);
    }
    
    public function gerarCodigo()
    {
        $numero = 0;
        $sair = false;

        // Verifica se já está em uso
        while ($sair === false) {

            // Gera o numero randômico
            $numero = md5(rand(111111111, 999999999));

            $sql = sprintf($this->sqlSelect, " WHERE (m.numero = '$numero') ", "", "");
            $res = $this->RunSelect($sql);

            if (sizeof($res) == 0) {
                $sair = true;
            }
        }

        // Retorna o numero
        return $numero;
    }
}

?>