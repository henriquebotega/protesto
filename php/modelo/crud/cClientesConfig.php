<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_MODELO . "mClientesConfig.php";

class cClientesConfig extends mClientesConfig
{
    protected $sqlInsert = "INSERT INTO clientes_config (id_cliente, boleto_mensagem1, boleto_mensagem2, boleto_mensagem3, boleto_desconto, boleto_abatimento, boleto_mora, boleto_multa) VALUES (:id_cliente, :boleto_mensagem1, :boleto_mensagem2, :boleto_mensagem3, :boleto_desconto, :boleto_abatimento, :boleto_mora, :boleto_multa)";
    protected $sqlUpdate = "UPDATE clientes_config SET boleto_mensagem1 = :boleto_mensagem1, boleto_mensagem2 = :boleto_mensagem2, boleto_mensagem3 = :boleto_mensagem3, boleto_desconto = :boleto_desconto, boleto_abatimento = :boleto_abatimento, boleto_mora = :boleto_mora, boleto_multa = :boleto_multa WHERE (id = :id)";
    protected $sqlDelete = "DELETE FROM clientes_config WHERE (id = :id)";
    protected $sqlSelect = "SELECT * FROM clientes_config %s %s %s";

    public function salvar()
    {
        $params = array(
            ':boleto_mensagem1' => $this->getBoletoMensagem1(),
            ':boleto_mensagem2' => $this->getBoletoMensagem2(),
            ':boleto_mensagem3' => $this->getBoletoMensagem3(),
            ':boleto_desconto' => formatar_Dinheiro($this->getBoletoDesconto()),
            ':boleto_abatimento' => formatar_Dinheiro($this->getBoletoAbatimento()),
            ':boleto_mora' => formatar_Dinheiro($this->getBoletoMora()),
            ':boleto_multa' => formatar_Dinheiro($this->getBoletoMulta())
        );

        if ($this->getId()) {
            $params[':id'] = $this->getId();
            $this->Executar($this->sqlUpdate, $params);
        } else {
            $params[':id_cliente'] = $this->getIdCliente();
            $this->Executar($this->sqlInsert, $params);
        }
    }

    public function delete()
    {
        $params = array(
            ':id' => $this->getId()
        );

        $this->Executar($this->sqlDelete, $params);
    }

    public function select($where = '', $order = '', $more = '')
    {
        $where = (empty($where) ? "" : " WHERE " . $where);
        $order = (empty($order) ? "" : " ORDER BY " . $order);

        $sql = sprintf($this->sqlSelect, $where, $order, $more);
        return $this->RunSelect($sql);
    }
}

?>