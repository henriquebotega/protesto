<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_MODELO . "mContratosItens.php";

class cContratosItens extends mContratosItens
{
    protected $sqlInsert = "INSERT INTO contratos_itens (id_servico, id_contrato, valor) VALUES (:id_servico, :id_contrato, :valor)";
    protected $sqlUpdate = "UPDATE contratos_itens SET id_servico = :id_servico, valor = :valor WHERE (id = :id)";
    protected $sqlDelete = "DELETE FROM contratos_itens WHERE (id = :id)";
    protected $sqlDeletePorContrato = "DELETE FROM contratos_itens WHERE (id_contrato = :id_contrato)";
    protected $sqlSelect = "
      SELECT 
        ci.*, s.titulo as 'servico', s.rota
      
      FROM contratos_itens ci
        INNER JOIN servicos s ON s.id = ci.id_servico
        INNER JOIN contratos c ON c.id = ci.id_contrato
        INNER JOIN clientes cli ON cli.id = c.id_cliente
      
      %s %s %s";

    public function salvar()
    {
        $params = array(
            ':id_servico' => $this->getIdServico(),
            ':valor' => formatar_Dinheiro($this->getValor())
        );

        if ($this->getId()) {
            $params[':id'] = $this->getId();
            $this->Executar($this->sqlUpdate, $params);
        } else {
            $params[':id_contrato'] = $this->getIdContrato();
            $this->Executar($this->sqlInsert, $params);
        }
    }

    public function delete()
    {
        $params = array(
            ':id' => $this->getId()
        );

        $this->Executar($this->sqlDelete, $params);
    }

    public function deletePorContrato()
    {
        $params = array(
            ':id_contrato' => $this->getIdContrato()
        );

        $this->Executar($this->sqlDeletePorContrato, $params);
    }

    public function select($where = '', $order = '', $more = '')
    {
        $where = (empty($where) ? "" : " WHERE " . $where);
        $order = (empty($order) ? "" : " ORDER BY " . $order);

        $sql = sprintf($this->sqlSelect, $where, $order, $more);
        return $this->RunSelect($sql);
    }
}

?>