<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_MODELO . "mContratos.php";

class cContratos extends mContratos
{
    protected $sqlInsert = "INSERT INTO contratos (id_cliente, id_situacao, data, dia_vcto) VALUES (:id_cliente, :id_situacao, :data, :dia_vcto)";
    protected $sqlUpdate = "UPDATE contratos SET id_cliente = :id_cliente, id_situacao = :id_situacao, dia_vcto = :dia_vcto WHERE (id = :id)";
    protected $sqlDelete = "DELETE FROM contratos WHERE (id = :id)";
    protected $sqlSelect = "
    SELECT 
      c.*, cs.titulo as 'situacao',
      if(cli.id_tipo = 1, pf.nome, pj.nome_fantasia) as 'grid_nome'
      
    FROM contratos c
      INNER JOIN contratos_situacao cs ON cs.id = c.id_situacao
      INNER JOIN clientes cli ON cli.id = c.id_cliente
      LEFT JOIN clientes_pf pf ON pf.id_cliente = cli.id
      LEFT JOIN clientes_pj pj ON pj.id_cliente = cli.id
      
    %s %s %s";

    public function salvar()
    {
        $params = array(
            ':id_cliente' => $this->getIdCliente(),
            ':dia_vcto' => $this->getDiaVcto(),
            ':id_situacao' => $this->getIdSituacao()
        );

        if ($this->getId()) {
            $params[':id'] = $this->getId();
            $this->Executar($this->sqlUpdate, $params);
        } else {
            $params[':data'] = date("Y-m-d H:i:s", time());
            $this->Executar($this->sqlInsert, $params);
        }
    }

    public function delete()
    {
        $params = array(
            ':id' => $this->getId()
        );

        $this->Executar($this->sqlDelete, $params);
    }

    public function select($where = '', $order = '', $more = '')
    {
        $where = (empty($where) ? "" : " WHERE " . $where);
        $order = (empty($order) ? "" : " ORDER BY " . $order);

        $sql = sprintf($this->sqlSelect, $where, $order, $more);
        return $this->RunSelect($sql);
    }
}

?>