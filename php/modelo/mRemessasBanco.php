<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_MODELO . "conexao.php";

class mRemessasBanco extends conexao
{
    private $id;
    private $id_divida;
    private $data;
    private $data_saida_banco;
    private $enviado_banco;
    private $boleto_numero_sequencial_remessa;
    private $cod_remessa;
    private $nosso_numero;
    private $retorno_banco;
    private $data_retorno_banco;
    private $tipo_envio;

    public function getTipoEnvio()
    {
        return $this->tipo_envio;
    }

    public function setTipoEnvio($tipo_envio)
    {
        $this->tipo_envio = $tipo_envio;
    }

    public function getRetornoBanco()
    {
        return $this->retorno_banco;
    }

    public function setRetornoBanco($retorno_banco)
    {
        $this->retorno_banco = $retorno_banco;
    }

    public function getDataRetornoBanco()
    {
        return $this->data_retorno_banco;
    }

    public function setDataRetornoBanco($data_retorno_banco)
    {
        $this->data_retorno_banco = $data_retorno_banco;
    }

    public function getNossoNumero()
    {
        return $this->nosso_numero;
    }

    public function setNossoNumero($nosso_numero)
    {
        $this->nosso_numero = $nosso_numero;
    }

    public function getCodRemessa()
    {
        return $this->cod_remessa;
    }

    public function setCodRemessa($cod_remessa)
    {
        $this->cod_remessa = $cod_remessa;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getIdDivida()
    {
        return $this->id_divida;
    }

    public function setIdDivida($id_divida)
    {
        $this->id_divida = $id_divida;
    }

    public function getData()
    {
        return $this->data;
    }

    public function setData($data)
    {
        $this->data = $data;
    }

    public function getDataSaidaBanco()
    {
        return $this->data_saida_banco;
    }

    public function setDataSaidaBanco($data_saida_banco)
    {
        $this->data_saida_banco = $data_saida_banco;
    }

    public function getEnviadoBanco()
    {
        return $this->enviado_banco;
    }

    public function setEnviadoBanco($enviado_banco)
    {
        $this->enviado_banco = $enviado_banco;
    }

    public function getBoletoNumeroSequencialRemessa()
    {
        return $this->boleto_numero_sequencial_remessa;
    }

    public function setBoletoNumeroSequencialRemessa($boleto_numero_sequencial_remessa)
    {
        $this->boleto_numero_sequencial_remessa = $boleto_numero_sequencial_remessa;
    }

}

?>