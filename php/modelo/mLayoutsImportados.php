<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_MODELO . "conexao.php";

class mLayoutsImportados extends conexao
{
    private $id;
    private $id_layout;
    private $id_cliente;
    private $arquivo;
    private $data;
    private $total_registros;
    private $total_registros_sucesso;
    private $erros;

    public function getTotalRegistrosSucesso()
    {
        return $this->total_registros_sucesso;
    }

    public function setTotalRegistrosSucesso($total_registros_sucesso)
    {
        $this->total_registros_sucesso = $total_registros_sucesso;
    }

    public function getErros()
    {
        return $this->erros;
    }

    public function setErros($erros)
    {
        $this->erros = $erros;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getIdLayout()
    {
        return $this->id_layout;
    }

    public function setIdLayout($id_layout)
    {
        $this->id_layout = $id_layout;
    }

    public function getIdCliente()
    {
        return $this->id_cliente;
    }

    public function setIdCliente($id_cliente)
    {
        $this->id_cliente = $id_cliente;
    }

    public function getArquivo()
    {
        return $this->arquivo;
    }

    public function setArquivo($arquivo)
    {
        $this->arquivo = $arquivo;
    }

    public function getData()
    {
        return $this->data;
    }

    public function setData($data)
    {
        $this->data = $data;
    }

    public function getTotalRegistros()
    {
        return $this->total_registros;
    }

    public function setTotalRegistros($total_registros)
    {
        $this->total_registros = $total_registros;
    }


}

?>