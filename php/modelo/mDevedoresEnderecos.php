<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_MODELO . "conexao.php";

class mDevedoresEnderecos extends conexao
{
    private $id;
    private $id_devedor;
    private $logradouro1;
    private $numero1;
    private $complemento1;
    private $bairro1;
    private $id_cidade1;
    private $logradouro2;
    private $numero2;
    private $complemento2;
    private $bairro2;
    private $id_cidade2;
    private $logradouro3;
    private $numero3;
    private $complemento3;
    private $bairro3;
    private $id_cidade3;
    private $cep1;
    private $cep2;
    private $cep3;

    public function getCep1()
    {
        return $this->cep1;
    }

    public function setCep1($cep1)
    {
        $this->cep1 = $cep1;
    }

    public function getCep2()
    {
        return $this->cep2;
    }

    public function setCep2($cep2)
    {
        $this->cep2 = $cep2;
    }

    public function getCep3()
    {
        return $this->cep3;
    }

    public function setCep3($cep3)
    {
        $this->cep3 = $cep3;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getIdDevedor()
    {
        return $this->id_devedor;
    }

    public function setIdDevedor($id_devedor)
    {
        $this->id_devedor = $id_devedor;
    }

    public function getLogradouro1()
    {
        return $this->logradouro1;
    }

    public function setLogradouro1($logradouro1)
    {
        $this->logradouro1 = $logradouro1;
    }

    public function getNumero1()
    {
        return $this->numero1;
    }

    public function setNumero1($numero1)
    {
        $this->numero1 = $numero1;
    }

    public function getComplemento1()
    {
        return $this->complemento1;
    }

    public function setComplemento1($complemento1)
    {
        $this->complemento1 = $complemento1;
    }

    public function getBairro1()
    {
        return $this->bairro1;
    }

    public function setBairro1($bairro1)
    {
        $this->bairro1 = $bairro1;
    }

    public function getIdCidade1()
    {
        return $this->id_cidade1;
    }

    public function setIdCidade1($id_cidade1)
    {
        $this->id_cidade1 = $id_cidade1;
    }

    public function getLogradouro2()
    {
        return $this->logradouro2;
    }

    public function setLogradouro2($logradouro2)
    {
        $this->logradouro2 = $logradouro2;
    }

    public function getNumero2()
    {
        return $this->numero2;
    }

    public function setNumero2($numero2)
    {
        $this->numero2 = $numero2;
    }

    public function getComplemento2()
    {
        return $this->complemento2;
    }

    public function setComplemento2($complemento2)
    {
        $this->complemento2 = $complemento2;
    }

    public function getBairro2()
    {
        return $this->bairro2;
    }

    public function setBairro2($bairro2)
    {
        $this->bairro2 = $bairro2;
    }

    public function getIdCidade2()
    {
        return $this->id_cidade2;
    }

    public function setIdCidade2($id_cidade2)
    {
        $this->id_cidade2 = $id_cidade2;
    }

    public function getLogradouro3()
    {
        return $this->logradouro3;
    }

    public function setLogradouro3($logradouro3)
    {
        $this->logradouro3 = $logradouro3;
    }

    public function getNumero3()
    {
        return $this->numero3;
    }

    public function setNumero3($numero3)
    {
        $this->numero3 = $numero3;
    }

    public function getComplemento3()
    {
        return $this->complemento3;
    }

    public function setComplemento3($complemento3)
    {
        $this->complemento3 = $complemento3;
    }

    public function getBairro3()
    {
        return $this->bairro3;
    }

    public function setBairro3($bairro3)
    {
        $this->bairro3 = $bairro3;
    }

    public function getIdCidade3()
    {
        return $this->id_cidade3;
    }

    public function setIdCidade3($id_cidade3)
    {
        $this->id_cidade3 = $id_cidade3;
    }
}

?>