<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_MODELO . "conexao.php";

class mServicos extends conexao
{
    private $id;
    private $titulo;
    private $descricao;
    private $valor;
    private $permite_remover;
    private $obrigatorio;
    private $webservice;
    private $url_ws;
    private $rota;

    public function getRota()
    {
        return $this->rota;
    }

    public function setRota($rota)
    {
        $this->rota = $rota;
    }

    public function getUrlWs()
    {
        return $this->url_ws;
    }

    public function setUrlWs($url_ws)
    {
        $this->url_ws = $url_ws;
    }

    public function getWebservice()
    {
        return $this->webservice;
    }

    public function setWebservice($webservice)
    {
        $this->webservice = $webservice;
    }

    public function getObrigatorio()
    {
        return $this->obrigatorio;
    }

    public function setObrigatorio($obrigatorio)
    {
        $this->obrigatorio = $obrigatorio;
    }

    public function getPermiteRemover()
    {
        return $this->permite_remover;
    }

    public function setPermiteRemover($permite_remover)
    {
        $this->permite_remover = $permite_remover;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getTitulo()
    {
        return $this->titulo;
    }

    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    }

    public function getDescricao()
    {
        return $this->descricao;
    }

    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }

    public function getValor()
    {
        return $this->valor;
    }

    public function setValor($valor)
    {
        $this->valor = $valor;
    }
}

?>