<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_MODELO . "conexao.php";

class mClientesConfig extends conexao
{
    private $id;
    private $id_cliente;
    private $boleto_mensagem1;
    private $boleto_mensagem2;
    private $boleto_mensagem3;
    private $boleto_desconto;
    private $boleto_abatimento;
    private $boleto_multa;
    private $boleto_mora;

    public function getIdCliente()
    {
        return $this->id_cliente;
    }

    public function setIdCliente($id_cliente)
    {
        $this->id_cliente = $id_cliente;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getBoletoMensagem1()
    {
        return $this->boleto_mensagem1;
    }

    public function setBoletoMensagem1($boleto_mensagem1)
    {
        $this->boleto_mensagem1 = $boleto_mensagem1;
    }

    public function getBoletoMensagem2()
    {
        return $this->boleto_mensagem2;
    }

    public function setBoletoMensagem2($boleto_mensagem2)
    {
        $this->boleto_mensagem2 = $boleto_mensagem2;
    }

    public function getBoletoMensagem3()
    {
        return $this->boleto_mensagem3;
    }

    public function setBoletoMensagem3($boleto_mensagem3)
    {
        $this->boleto_mensagem3 = $boleto_mensagem3;
    }

    public function getBoletoDesconto()
    {
        return $this->boleto_desconto;
    }

    public function setBoletoDesconto($boleto_desconto)
    {
        $this->boleto_desconto = $boleto_desconto;
    }

    public function getBoletoAbatimento()
    {
        return $this->boleto_abatimento;
    }

    public function setBoletoAbatimento($boleto_abatimento)
    {
        $this->boleto_abatimento = $boleto_abatimento;
    }

    public function getBoletoMulta()
    {
        return $this->boleto_multa;
    }

    public function setBoletoMulta($boleto_multa)
    {
        $this->boleto_multa = $boleto_multa;
    }

    public function getBoletoMora()
    {
        return $this->boleto_mora;
    }

    public function setBoletoMora($boleto_mora)
    {
        $this->boleto_mora = $boleto_mora;
    }

}

?>