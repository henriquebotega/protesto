<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_MODELO . "conexao.php";

class mTaxas extends conexao
{
    private $id;
    private $id_servico;
    private $id_cliente;
    private $id_mensalidade;
    private $historico;
    private $valor;
    private $data;
    private $id_situacao;

    public function getIdSituacao()
    {
        return $this->id_situacao;
    }

    public function setIdSituacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getIdServico()
    {
        return $this->id_servico;
    }

    public function setIdServico($id_servico)
    {
        $this->id_servico = $id_servico;
    }

    public function getIdCliente()
    {
        return $this->id_cliente;
    }

    public function setIdCliente($id_cliente)
    {
        $this->id_cliente = $id_cliente;
    }

    public function getIdMensalidade()
    {
        return $this->id_mensalidade;
    }

    public function setIdMensalidade($id_mensalidade)
    {
        $this->id_mensalidade = $id_mensalidade;
    }

    public function getHistorico()
    {
        return $this->historico;
    }

    public function setHistorico($historico)
    {
        $this->historico = $historico;
    }

    public function getValor()
    {
        return $this->valor;
    }

    public function setValor($valor)
    {
        $this->valor = $valor;
    }

    public function getData()
    {
        return $this->data;
    }

    public function setData($data)
    {
        $this->data = $data;
    }
}

?>