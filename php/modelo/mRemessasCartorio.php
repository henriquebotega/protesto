<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_MODELO . "conexao.php";

class mRemessasCartorio extends conexao
{
    private $id;
    private $id_divida;
    private $data;
    private $data_saida_cartorio;
    private $enviado_cartorio;
    private $cartorio_numero_sequencial;
    private $cod_remessa;
    private $nosso_numero;
    private $retorno_cartorio;
    private $data_retorno_cartorio;

    public function getRetornoCartorio()
    {
        return $this->retorno_cartorio;
    }

    public function setRetornoCartorio($retorno_cartorio)
    {
        $this->retorno_cartorio = $retorno_cartorio;
    }

    public function getDataRetornoCartorio()
    {
        return $this->data_retorno_cartorio;
    }

    public function setDataRetornoCartorio($data_retorno_cartorio)
    {
        $this->data_retorno_cartorio = $data_retorno_cartorio;
    }

    public function getNossoNumero()
    {
        return $this->nosso_numero;
    }

    public function setNossoNumero($nosso_numero)
    {
        $this->nosso_numero = $nosso_numero;
    }

    public function getCodRemessa()
    {
        return $this->cod_remessa;
    }

    public function setCodRemessa($cod_remessa)
    {
        $this->cod_remessa = $cod_remessa;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getIdDivida()
    {
        return $this->id_divida;
    }

    public function setIdDivida($id_divida)
    {
        $this->id_divida = $id_divida;
    }

    public function getData()
    {
        return $this->data;
    }

    public function setData($data)
    {
        $this->data = $data;
    }

    public function getDataSaidaCartorio()
    {
        return $this->data_saida_cartorio;
    }

    public function setDataSaidaCartorio($data_saida_cartorio)
    {
        $this->data_saida_cartorio = $data_saida_cartorio;
    }

    public function getEnviadoCartorio()
    {
        return $this->enviado_cartorio;
    }

    public function setEnviadoCartorio($enviado_cartorio)
    {
        $this->enviado_cartorio = $enviado_cartorio;
    }

    public function getCartorioNumeroSequencial()
    {
        return $this->cartorio_numero_sequencial;
    }

    public function setCartorioNumeroSequencial($cartorio_numero_sequencial)
    {
        $this->cartorio_numero_sequencial = $cartorio_numero_sequencial;
    }

}

?>