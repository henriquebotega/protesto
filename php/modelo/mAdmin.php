<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_MODELO . "conexao.php";

class mAdmin extends conexao
{
    private $id;
    private $email;
    private $senha;
    private $id_situacao;
    private $id_tipo;
    private $data_ultimo_acesso;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getSenha()
    {
        return $this->senha;
    }

    public function setSenha($senha)
    {
        $this->senha = $senha;
    }

    public function getIdSituacao()
    {
        return $this->id_situacao;
    }

    public function setIdSituacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
    }

    public function getIdTipo()
    {
        return $this->id_tipo;
    }

    public function setIdTipo($id_tipo)
    {
        $this->id_tipo = $id_tipo;
    }

    public function getDataUltimoAcesso()
    {
        return $this->data_ultimo_acesso;
    }

    public function setDataUltimoAcesso($data_ultimo_acesso)
    {
        $this->data_ultimo_acesso = $data_ultimo_acesso;
    }

}

?>