<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_MODELO . "conexao.php";

class mDevedoresContatos extends conexao
{
    private $id;
    private $id_devedor;
    private $tel1;
    private $contato1;
    private $email1;
    private $tel2;
    private $contato2;
    private $email2;
    private $tel3;
    private $contato3;
    private $email3;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getIdDevedor()
    {
        return $this->id_devedor;
    }

    public function setIdDevedor($id_devedor)
    {
        $this->id_devedor = $id_devedor;
    }

    public function getTel1()
    {
        return $this->tel1;
    }

    public function setTel1($tel1)
    {
        $this->tel1 = $tel1;
    }

    public function getContato1()
    {
        return $this->contato1;
    }

    public function setContato1($contato1)
    {
        $this->contato1 = $contato1;
    }

    public function getEmail1()
    {
        return $this->email1;
    }

    public function setEmail1($email1)
    {
        $this->email1 = $email1;
    }

    public function getTel2()
    {
        return $this->tel2;
    }

    public function setTel2($tel2)
    {
        $this->tel2 = $tel2;
    }

    public function getContato2()
    {
        return $this->contato2;
    }

    public function setContato2($contato2)
    {
        $this->contato2 = $contato2;
    }

    public function getEmail2()
    {
        return $this->email2;
    }

    public function setEmail2($email2)
    {
        $this->email2 = $email2;
    }

    public function getTel3()
    {
        return $this->tel3;
    }

    public function setTel3($tel3)
    {
        $this->tel3 = $tel3;
    }

    public function getContato3()
    {
        return $this->contato3;
    }

    public function setContato3($contato3)
    {
        $this->contato3 = $contato3;
    }

    public function getEmail3()
    {
        return $this->email3;
    }

    public function setEmail3($email3)
    {
        $this->email3 = $email3;
    }
}

?>