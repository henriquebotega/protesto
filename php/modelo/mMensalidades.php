<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_MODELO . "conexao.php";

class mMensalidades extends conexao
{
    private $id;
    private $id_contrato;
    private $numero;
    private $id_situacao;
    private $valor_vcto;
    private $valor_pgto;
    private $data_vcto;
    private $data_pgto;
    private $data_vigencia_inicial;
    private $data_vigencia_final;

    public function getDataVigenciaInicial()
    {
        return $this->data_vigencia_inicial;
    }

    public function setDataVigenciaInicial($data_vigencia_inicial)
    {
        $this->data_vigencia_inicial = $data_vigencia_inicial;
    }

    public function getDataVigenciaFinal()
    {
        return $this->data_vigencia_final;
    }

    public function setDataVigenciaFinal($data_vigencia_final)
    {
        $this->data_vigencia_final = $data_vigencia_final;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getIdContrato()
    {
        return $this->id_contrato;
    }

    public function setIdContrato($id_contrato)
    {
        $this->id_contrato = $id_contrato;
    }

    public function getNumero()
    {
        return $this->numero;
    }

    public function setNumero($numero)
    {
        $this->numero = $numero;
    }

    public function getIdSituacao()
    {
        return $this->id_situacao;
    }

    public function setIdSituacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
    }

    public function getValorVcto()
    {
        return $this->valor_vcto;
    }

    public function setValorVcto($valor_vcto)
    {
        $this->valor_vcto = $valor_vcto;
    }

    public function getValorPgto()
    {
        return $this->valor_pgto;
    }

    public function setValorPgto($valor_pgto)
    {
        $this->valor_pgto = $valor_pgto;
    }

    public function getDataVcto()
    {
        return $this->data_vcto;
    }

    public function setDataVcto($data_vcto)
    {
        $this->data_vcto = $data_vcto;
    }

    public function getDataPgto()
    {
        return $this->data_pgto;
    }

    public function setDataPgto($data_pgto)
    {
        $this->data_pgto = $data_pgto;
    }
}

?>