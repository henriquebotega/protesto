<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_MODELO . "conexao.php";

class mDividas extends conexao
{
    private $id;
    private $valor_vcto;
    private $valor_pgto;
    private $data_vcto;
    private $data_pgto;
    private $id_situacao;
    private $id_devedor;
    private $nosso_numero;
    private $numero_parcela;
    private $data_emissao;
    private $numero_documento;
    private $numero_fatura;
    private $observacoes;
    private $enviado_cartorio;
    private $id_praca;
    private $boleto_mensagem1;
    private $boleto_mensagem2;
    private $boleto_mensagem3;
    private $boleto_desconto;
    private $boleto_abatimento;
    private $boleto_mora;
    private $boleto_multa;

    public function getBoletoMensagem1()
    {
        return $this->boleto_mensagem1;
    }

    public function setBoletoMensagem1($boleto_mensagem1)
    {
        $this->boleto_mensagem1 = $boleto_mensagem1;
    }

    public function getBoletoMensagem2()
    {
        return $this->boleto_mensagem2;
    }

    public function setBoletoMensagem2($boleto_mensagem2)
    {
        $this->boleto_mensagem2 = $boleto_mensagem2;
    }

    public function getBoletoMensagem3()
    {
        return $this->boleto_mensagem3;
    }

    public function setBoletoMensagem3($boleto_mensagem3)
    {
        $this->boleto_mensagem3 = $boleto_mensagem3;
    }

    public function getBoletoDesconto()
    {
        return $this->boleto_desconto;
    }

    public function setBoletoDesconto($boleto_desconto)
    {
        $this->boleto_desconto = $boleto_desconto;
    }

    public function getBoletoAbatimento()
    {
        return $this->boleto_abatimento;
    }

    public function setBoletoAbatimento($boleto_abatimento)
    {
        $this->boleto_abatimento = $boleto_abatimento;
    }

    public function getBoletoMora()
    {
        return $this->boleto_mora;
    }

    public function setBoletoMora($boleto_mora)
    {
        $this->boleto_mora = $boleto_mora;
    }

    public function getBoletoMulta()
    {
        return $this->boleto_multa;
    }

    public function setBoletoMulta($boleto_multa)
    {
        $this->boleto_multa = $boleto_multa;
    }

    public function getIdPraca()
    {
        return $this->id_praca;
    }

    public function setIdPraca($id_praca)
    {
        $this->id_praca = $id_praca;
    }

    public function getEnviadoCartorio()
    {
        return $this->enviado_cartorio;
    }

    public function setEnviadoCartorio($enviado_cartorio)
    {
        $this->enviado_cartorio = $enviado_cartorio;
    }

    public function getValorPgto()
    {
        return $this->valor_pgto;
    }

    public function setValorPgto($valor_pgto)
    {
        $this->valor_pgto = $valor_pgto;
    }

    public function getDataPgto()
    {
        return $this->data_pgto;
    }

    public function setDataPgto($data_pgto)
    {
        $this->data_pgto = $data_pgto;
    }

    public function getDataEmissao()
    {
        return $this->data_emissao;
    }

    public function setDataEmissao($data_emissao)
    {
        $this->data_emissao = $data_emissao;
    }

    public function getNumeroDocumento()
    {
        return $this->numero_documento;
    }

    public function setNumeroDocumento($numero_documento)
    {
        $this->numero_documento = $numero_documento;
    }

    public function getNumeroFatura()
    {
        return $this->numero_fatura;
    }

    public function setNumeroFatura($numero_fatura)
    {
        $this->numero_fatura = $numero_fatura;
    }

    public function getObservacoes()
    {
        return $this->observacoes;
    }

    public function setObservacoes($observacoes)
    {
        $this->observacoes = $observacoes;
    }

    public function getNumeroParcela()
    {
        return $this->numero_parcela;
    }

    public function setNumeroParcela($numero_parcela)
    {
        $this->numero_parcela = $numero_parcela;
    }

    public function getNossoNumero()
    {
        return $this->nosso_numero;
    }

    public function setNossoNumero($nosso_numero)
    {
        $this->nosso_numero = $nosso_numero;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getValorVcto()
    {
        return $this->valor_vcto;
    }

    public function setValorVcto($valor_vcto)
    {
        $this->valor_vcto = $valor_vcto;
    }

    public function getDataVcto()
    {
        return $this->data_vcto;
    }

    public function setDataVcto($data_vcto)
    {
        $this->data_vcto = $data_vcto;
    }

    public function getIdSituacao()
    {
        return $this->id_situacao;
    }

    public function setIdSituacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
    }

    public function getIdDevedor()
    {
        return $this->id_devedor;
    }

    public function setIdDevedor($id_devedor)
    {
        $this->id_devedor = $id_devedor;
    }
}

?>