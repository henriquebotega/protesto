<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

class conexao
{
    private static $connMy = null;
    private static $_instance = null;

    public static function getInstance()
    {
        if (!isset(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function ConnectMy()
    {
        global $host, $port, $user, $pass, $db;

        if (!self::$connMy) {
            $connection = new PDO("mysql:host=$host; dbname=$db; port=$port", $user, $pass);
            $connection->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, 'SET NAMES UTF8');
            $connection->setAttribute(PDO::ATTR_PERSISTENT, true);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            self::$connMy = $connection;
        } else {
            $connection = self::$connMy;
        }

        return $connection;
    }

    public function Executar($sqlString, $params)
    {
        $pdo = $this->ConnectMy();
        $pdo->exec("SET NAMES 'UTF8'");
        $sql = $pdo->prepare($sqlString);

        try {
            $sql->execute($params);
        } catch (PDOException $e) {
            exit($e->getMessage() . "<hr>SQL: " . $sqlString);
        }
    }

    public function RunSelect($sql)
    {
        $pdo = $this->ConnectMy();
        $pdo->exec("SET NAMES 'UTF8'");
        $sql = $pdo->prepare($sql);
        $sql->execute();

        return $sql->fetchAll(PDO::FETCH_ASSOC);
    }
}

?>