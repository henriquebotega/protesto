<?php

// DADOS DO BOLETO PARA O SEU CLIENTE
$dias_de_prazo_para_pagamento = 0;
$taxa_boleto = $valor_taxa;
$data_venc = $con->ConvertData("EUA", "BRA", $ret_cMovimentos[0]['data_vcto']);  // Prazo de X dias OU informe data: "13/04/2006";
$valor_cobrado = $ret_cMovimentos[0]['valor_vcto']; // Valor - REGRA: Sem pontos na milhar e tanto faz com "." ou "," ou com 1 ou 2 ou sem casa decimal
$valor_cobrado = str_replace(",", ".", $valor_cobrado);
$valor_boleto = number_format($valor_cobrado + $taxa_boleto, 2, ',', '');

$dadosboleto["nosso_numero"] = $ret_cMovimentos[0]['nosso_numero'];  // Nosso numero - REGRA: Maximo de 8 caracteres!
$dadosboleto["numero_documento"] = $ret_cMovimentos[0]['num_contrato'];    // Num do pedido ou nosso numero
$dadosboleto["data_vencimento"] = $data_venc; // Data de Vencimento do Boleto - REGRA: Formato DD/MM/AAAA
$dadosboleto["data_documento"] = date("d/m/Y"); // Data de emissao do Boleto
$dadosboleto["data_processamento"] = date("d/m/Y"); // Data de processamento do boleto (opcional)
$dadosboleto["valor_boleto"] = $valor_boleto;    // Valor do Boleto - REGRA: Com virgula e sempre com duas casas depois da virgula

// DADOS DO SEU CLIENTE
$dadosboleto["sacado"] = strtoupper($ret_cMovimentos[0]['nome']);
$dadosboleto["endereco1"] = strtoupper($ret_cMovimentos[0]['endereco'] . ", " . $ret_cMovimentos[0]['end_num']);
$dadosboleto["endereco2"] = strtoupper($ret_cMovimentos[0]['titulo'] . " - " . $ret_cMovimentos[0]['sigla'] . " - CEP: " . $ret_cMovimentos[0]['cep']);

// INFORMACOES PARA O CLIENTE
$dadosboleto["demonstrativo1"] = utf8_encode(strtoupper($ret_cMovimentos[0]['linha_1']));
$dadosboleto["demonstrativo2"] = utf8_encode(strtoupper($ret_cMovimentos[0]['linha_2']));
$dadosboleto["demonstrativo3"] = utf8_encode(strtoupper($ret_cMovimentos[0]['linha_3']));

$dadosboleto["instrucoes1"] = strtoupper($ret_cMovimentos[0]['msg_cobrar_juros']);
$dadosboleto["instrucoes2"] = strtoupper($ret_cMovimentos[0]['msg_cobrar_multa']);
$dadosboleto["instrucoes3"] = "";
$dadosboleto["instrucoes4"] = "";

// DADOS OPCIONAIS DE ACORDO COM O BANCO OU CLIENTE
$dadosboleto["quantidade"] = "";
$dadosboleto["valor_unitario"] = "";
$dadosboleto["aceite"] = "N";
$dadosboleto["especie"] = "R$";
$dadosboleto["especie_doc"] = "";


// ---------------------- DADOS FIXOS DE CONFIGURACAO DO SEU BOLETO --------------- //


// DADOS DA SUA CONTA - ITAU
$dadosboleto["agencia"] = $ret_cConfigBoletos[0]["agencia"]; // Num da agencia, sem digito
$dadosboleto["conta"] = substr($ret_cConfigBoletos[0]["conta"], 0, -1); // Num da conta, sem digito
$dadosboleto["conta_dv"] = substr($ret_cConfigBoletos[0]["conta"], -1); // Digito do Num da conta

// DADOS PERSONALIZADOS - ITAU
$dadosboleto["carteira"] = $ret_cConfigBoletos[0]["num_carteira"];  // Codigo da Carteira: pode ser 175, 174, 104, 109, 178, ou 157

// SEUS DADOS
$dadosboleto["identificacao"] = "Sistema Sibras";
$dadosboleto["cpf_cnpj"] = @$ret_cConfigBoletos[0]["cpf_cnpj"];
$dadosboleto["endereco"] = @$ret_cConfigBoletos[0]["endereco"];
$dadosboleto["endereco_cedente"] = @$ret_cConfigBoletos[0]["endereco"];
$dadosboleto["cidade_uf"] = utf8_encode("Maringá / Paraná");
$dadosboleto["cedente"] = @$ret_cConfigBoletos[0]["empresa"];

// NAO ALTERAR!
include("include/funcoes_itau.php");
include("include/layout_itau.php");
?>
