<style type="text/css">
    /* Faz pular a folha na hora da impressão */
    p.quebra-aqui {
        page-break-after: always;
    }
</style>

<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

include_once URL_PHP_CRUD . "cConfig.php";
include_once URL_PHP_CRUD . "cDividas.php";
include_once URL_PHP_CRUD . "cClientes.php";
include_once URL_PHP_CRUD . "cRemessasBanco.php";
$dados_cConfig = new cConfig();
$dados_cDividas = new cDividas();
$dados_cClientes = new cClientes();
$dados_cRemessasBanco = new cRemessasBanco();

if (isset($_REQUEST['id_divida'])) {

    // Recupera as informações do sistema
    $ret_cConfig = $dados_cConfig->select();
    $boleto_numero_sequencial_nosso_numero = $ret_cConfig[0]['boleto_numero_sequencial_nosso_numero'];
    $nossoNumero = 0;

    $prefixoCooperativa = $ret_cConfig[0]['boleto_agencia'];
    $codCedente = $ret_cConfig[0]['boleto_cedente'];
    $digitoVerificadorCodCedente = $ret_cConfig[0]['boleto_cedente_digito'];

    $colID = explode("-", $_REQUEST['id_divida']);

    // Gera o campo 'nosso_numero'
    foreach ($colID as $chave => $idDivida) {

        // Verifica se a divida já foi enviada ao cartório
        $ret_cDividas = $dados_cDividas->select(" (d.id = '" . $idDivida . "') AND (d.enviado_cartorio = 0) AND (d.nosso_numero = 0) ");

        if (sizeof($ret_cDividas) > 0) {
            
            $ret_cClientes = $dados_cClientes->select(" (c.id = '" . $ret_cDividas[0]['id_cliente'] . "') ");

            // 1 - Gerar 'nosso_numero' para o boleto atual
            $prefixoCooperativa = $ret_cConfig[0]['boleto_agencia'];
            $codCedente = $ret_cConfig[0]['boleto_cedente'];
            $digitoVerificadorCodCedente = $ret_cConfig[0]['boleto_cedente_digito'];
            $nossoNumero = ++$boleto_numero_sequencial_nosso_numero;
            $digNossoNumero = calculaDigitoVerificadorBoleto($nossoNumero, $prefixoCooperativa, $codCedente . $digitoVerificadorCodCedente);

            $dados_cDividas = new cDividas();
            $dados_cDividas->setNossoNumero($nossoNumero);
            $dados_cDividas->setId($idDivida);
            $dados_cDividas->updateNossoNumero();

            // 2 - Gerar novo registro na tabela 'remessas', vinculada ao boleto atual
            $ret_cRemessasBanco = $dados_cRemessasBanco->select(" (rb.id_divida = '" . $idDivida . "' AND rb.tipo_envio = 0) ");
            if (sizeof($ret_cRemessasBanco) == 0) {
                $dados_cRemessasBanco = new cRemessasBanco();
                $dados_cRemessasBanco->setIdDivida($idDivida);
                $dados_cRemessasBanco->setNossoNumero($nossoNumero);
                $dados_cRemessasBanco->setTipoEnvio(0);
                $dados_cRemessasBanco->salvar();
            }
        }
    }

    if ($nossoNumero > 0) {
        $dados_cConfig->setBoletoNumeroSequencialNossoNumero(++$nossoNumero);
        $dados_cConfig->updateNumeroSequecialBoletoNossoNumero();
    }

    // Gera o PDF do boleto
    foreach ($colID as $chave => $idDivida) {

        // Verifica se a divida já foi enviada ao cartório
        $ret_cDividas = $dados_cDividas->select(" (d.id = '" . $idDivida . "') AND (d.enviado_cartorio = 0) AND (d.nosso_numero > 0) ");

        if (sizeof($ret_cDividas) > 0) {
            $ret_cClientes = $dados_cClientes->select(" (c.id = '" . $ret_cDividas[0]['id_cliente'] . "') ");

            $nossoNumero = $ret_cDividas[0]['nosso_numero'];
            $digNossoNumero = calculaDigitoVerificadorBoleto($nossoNumero, $prefixoCooperativa, $codCedente . $digitoVerificadorCodCedente);

            switch ($ret_cConfig[0]['boleto_id_banco']) {
                case 1:
                    $bc = "SICOOB";
                    // include("bancoob/boleto_bancoob.php");
                    // include("bancoob/funcoes_bancoob.php");
                    // include("bancoob/layout_bancoob.php");
                    exit("Esta implementação (SICOOB) esta desativada.");
                    break;
                case 2:
                    $bc = "ITAU";
                    //include("itau/boleto_itau.php");
                    //include("itau/funcoes_itau.php");
                    //include("itau/layout_itau.php");
                    exit("Esta implementação (ITAU) esta desativada.");
                    break;
                case 3:
                    $bc = "SICREDI";
                    include("sicredi/boleto_sicredi.php");
                    include("sicredi/funcoes_sicredi.php");
                    include("sicredi/layout_sicredi.php");
                    break;
            }

            echo "<p class='quebra-aqui'>&nbsp;</p>";
        }
    }

} else {
    echo "<h3>Esta requisição é inválida!</h3>";
    exit();
}

?>
