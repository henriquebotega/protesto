<?php
if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/uploads/protesto/php/ip.php";
    include_once $_SERVER['DOCUMENT_ROOT'] . "/protesto/php/ip.php";
} else if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php")) {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/php/protesto/php/ip.php";
} else {
    include_once $_SERVER['DOCUMENT_ROOT'] . "/ip.php";
}

if (!function_exists('formata_numero')) {
    function formata_numero($numero, $loop, $insert, $tipo = "geral")
    {
        if ($tipo == "geral") {
            $numero = str_replace(",", "", $numero);
            while (strlen($numero) < $loop) {
                $numero = $insert . $numero;
            }
        }
        if ($tipo == "valor") {
            /*
            retira as virgulas
            formata o numero
            preenche com zeros
            */
            $numero = str_replace(",", "", $numero);
            while (strlen($numero) < $loop) {
                $numero = $insert . $numero;
            }
        }
        if ($tipo == "convenio") {
            while (strlen($numero) < $loop) {
                $numero = $numero . $insert;
            }
        }
        return $numero;
    }
}

if (!function_exists('fbarcode')) {
    function fbarcode($valor)
    {

        global $ip_servidor;
        
        $fino = 1;
        $largo = 3;
        $altura = 50;

        $barcodes[0] = "00110";
        $barcodes[1] = "10001";
        $barcodes[2] = "01001";
        $barcodes[3] = "11000";
        $barcodes[4] = "00101";
        $barcodes[5] = "10100";
        $barcodes[6] = "01100";
        $barcodes[7] = "00011";
        $barcodes[8] = "10010";
        $barcodes[9] = "01010";
        for ($f1 = 9; $f1 >= 0; $f1--) {
            for ($f2 = 9; $f2 >= 0; $f2--) {
                $f = ($f1 * 10) + $f2;
                $texto = "";
                for ($i = 1; $i < 6; $i++) {
                    $texto .= substr($barcodes[$f1], ($i - 1), 1) . substr($barcodes[$f2], ($i - 1), 1);
                }
                $barcodes[$f] = $texto;
            }
        }


//Desenho da barra


//Guarda inicial
        ?><img src="<?php echo $ip_servidor; ?>php/boleto/imagens/p.png" width=<?php echo $fino ?> height=<?php echo $altura ?> border=0><img
        src="<?php echo $ip_servidor; ?>php/boleto/imagens/b.png" width=<?php echo $fino ?> height=<?php echo $altura ?> border=0><img
        src="<?php echo $ip_servidor; ?>php/boleto/imagens/p.png" width=<?php echo $fino ?> height=<?php echo $altura ?> border=0><img
        src="<?php echo $ip_servidor; ?>php/boleto/imagens/b.png" width=<?php echo $fino ?> height=<?php echo $altura ?> border=0><img
        <?php
        $texto = $valor;
        if ((strlen($texto) % 2) <> 0) {
            $texto = "0" . $texto;
        }

// Draw dos dados
        while (strlen($texto) > 0) {
            $i = round(esquerda($texto, 2));
            $texto = direita($texto, strlen($texto) - 2);
            $f = $barcodes[$i];
            for ($i = 1; $i < 11; $i += 2) {
                if (substr($f, ($i - 1), 1) == "0") {
                    $f1 = $fino;
                } else {
                    $f1 = $largo;
                }
                ?>
                src="<?php echo $ip_servidor; ?>php/boleto/imagens/p.png" width=<?php echo $f1 ?> height=<?php echo $altura ?> border=0><img
                    <?php
                    if (substr($f, $i, 1) == "0") {
                        $f2 = $fino;
                    } else {
                        $f2 = $largo;
                    }
                    ?>
                    src="<?php echo $ip_servidor; ?>php/boleto/imagens/b.png" width=<?php echo $f2 ?> height=<?php echo $altura ?> border=0><img
                <?php
            }
        }

// Draw guarda final
        ?>
        src="<?php echo $ip_servidor; ?>php/boleto/imagens/p.png" width=<?php echo $largo ?> height=<?php echo $altura ?> border=0><img
        src="<?php echo $ip_servidor; ?>php/boleto/imagens/b.png" width=<?php echo $fino ?> height=<?php echo $altura ?> border=0><img
        src="<?php echo $ip_servidor; ?>php/boleto/imagens/p.png" width=<?php echo 1 ?> height=<?php echo $altura ?> border=0>
        <?php
    } //Fim da função
}

if (!function_exists('esquerda')) {
    function esquerda($entra, $comp)
    {
        return substr($entra, 0, $comp);
    }
}

if (!function_exists('direita')) {
    function direita($entra, $comp)
    {
        return substr($entra, strlen($entra) - $comp, $comp);
    }
}

if (!function_exists('fator_vencimento')) {
    function fator_vencimento($data)
    {
        $data = explode("/", $data);
        $ano = $data[2];
        $mes = $data[1];
        $dia = $data[0];
        return (abs((_dateToDays("1997", "10", "07")) - (_dateToDays($ano, $mes, $dia))));
    }
}

if (!function_exists('_dateToDays')) {
    function _dateToDays($year, $month, $day)
    {
        $century = substr($year, 0, 2);
        $year = substr($year, 2, 2);
        if ($month > 2) {
            $month -= 3;
        } else {
            $month += 9;
            if ($year) {
                $year--;
            } else {
                $year = 99;
                $century--;
            }
        }
        return (floor((146097 * $century) / 4) +
            floor((1461 * $year) / 4) +
            floor((153 * $month + 2) / 5) +
            $day + 1721119);
    }
}

if (!function_exists('modulo_10')) {
    function modulo_10($num)
    {
        $numtotal10 = 0;
        $fator = 2;

        for ($i = strlen($num); $i > 0; $i--) {
            $numeros[$i] = substr($num, $i - 1, 1);
            $parcial10[$i] = $numeros[$i] * $fator;
            $numtotal10 .= $parcial10[$i];
            if ($fator == 2) {
                $fator = 1;
            } else {
                $fator = 2;
            }
        }

        $soma = 0;
        for ($i = strlen($numtotal10); $i > 0; $i--) {
            $numeros[$i] = substr($numtotal10, $i - 1, 1);
            $soma += $numeros[$i];
        }
        $resto = $soma % 10;
        $digito = 10 - $resto;
        if ($resto == 0) {
            $digito = 0;
        }

        return $digito;
    }
}

if (!function_exists('modulo_11')) {
    function modulo_11($num, $base = 9, $r = 0)
    {
        $soma = 0;
        $fator = 2;
        for ($i = strlen($num); $i > 0; $i--) {
            $numeros[$i] = substr($num, $i - 1, 1);
            $parcial[$i] = $numeros[$i] * $fator;
            $soma += $parcial[$i];
            if ($fator == $base) {
                $fator = 1;
            }
            $fator++;
        }
        if ($r == 0) {
            $soma *= 10;
            $digito = $soma % 11;

            //corrigido
            if ($digito == 10) {
                $digito = "X";
            }

            if (strlen($num) == "43") {
                //então estamos checando a linha digitável
                if ($digito == "0" or $digito == "X" or $digito > 9) {
                    $digito = 1;
                }
            }
            return $digito;
        } elseif ($r == 1) {
            $resto = $soma % 11;
            return $resto;
        }
    }
}

if (!function_exists('monta_linha_digitavel')) {
    function monta_linha_digitavel($linha)
    {
        // Posição 	Conteúdo
        // 1 a 3    Número do banco
        // 4        Código da Moeda - 9 para Real
        // 5        Digito verificador do Código de Barras
        // 6 a 19   Valor (12 inteiros e 2 decimais)
        // 20 a 44  Campo Livre definido por cada banco

        // 1. Campo - composto pelo código do banco, código da moéda, as cinco primeiras posições
        // do campo livre e DV (modulo10) deste campo
        $p1 = substr($linha, 0, 4);
        $p2 = substr($linha, 19, 5);
        $p3 = modulo_10("$p1$p2");
        $p4 = "$p1$p2$p3";
        $p5 = substr($p4, 0, 5);
        $p6 = substr($p4, 5);
        $campo1 = "$p5.$p6";

        // 2. Campo - composto pelas posiçoes 6 a 15 do campo livre
        // e livre e DV (modulo10) deste campo
        $p1 = substr($linha, 24, 10);
        $p2 = modulo_10($p1);
        $p3 = "$p1$p2";
        $p4 = substr($p3, 0, 5);
        $p5 = substr($p3, 5);
        $campo2 = "$p4.$p5";

        // 3. Campo composto pelas posicoes 16 a 25 do campo livre
        // e livre e DV (modulo10) deste campo
        $p1 = substr($linha, 34, 10);
        $p2 = modulo_10($p1);
        $p3 = "$p1$p2";
        $p4 = substr($p3, 0, 5);
        $p5 = substr($p3, 5);
        $campo3 = "$p4.$p5";

        // 4. Campo - digito verificador do codigo de barras
        $campo4 = substr($linha, 4, 1);

        // 5. Campo composto pelo valor nominal pelo valor nominal do documento, sem
        // indicacao de zeros a esquerda e sem edicao (sem ponto e virgula). Quando se
        // tratar de valor zerado, a representacao deve ser 000 (tres zeros).
        $campo5 = substr($linha, 5, 14);

        return "$campo1 $campo2 $campo3 $campo4 $campo5";
    }
}

if (!function_exists('geraCodigoBanco')) {
    function geraCodigoBanco($numero)
    {
        $parte1 = substr($numero, 0, 3);
        $parte2 = modulo_11($parte1);
        return $parte1 . "-" . $parte2;
    }
}

$codigobanco = "756";
$codigo_banco_com_dv = geraCodigoBanco($codigobanco);
$nummoeda = "9";
$fator_vencimento = fator_vencimento($dadosboleto["data_vencimento"]);

//valor tem 10 digitos, sem virgula
$valor = formata_numero($dadosboleto["valor_boleto"], 10, 0, "valor");
//agencia é sempre 4 digitos
$agencia = formata_numero($dadosboleto["agencia"], 4, 0);
//conta é sempre 8 digitos
$conta = formata_numero($dadosboleto["conta"], 8, 0);

$carteira = $dadosboleto["carteira"];

//Zeros: usado quando convenio de 7 digitos
$livre_zeros = '000000';
$modalidadecobranca = $dadosboleto["modalidade_cobranca"];
$numeroparcela = $dadosboleto["numero_parcela"];

$convenio = formata_numero($dadosboleto["convenio"], 7, 0);

//agencia e conta
$agencia_codigo = $agencia . " / " . $convenio;

// Nosso número de até 8 dígitos - 2 digitos para o ano e outros 6 numeros sequencias por ano
$nossonumero = formata_numero($dadosboleto["nosso_numero"], 8, 0);
$campolivre = "$modalidadecobranca$convenio$nossonumero$numeroparcela";

$dv = modulo_11("$codigobanco$nummoeda$fator_vencimento$valor$carteira$agencia$campolivre");
$linha = "$codigobanco$nummoeda$dv$fator_vencimento$valor$carteira$agencia$campolivre";

$dadosboleto["codigo_barras"] = $linha;
$dadosboleto["linha_digitavel"] = monta_linha_digitavel($linha);
$dadosboleto["agencia_codigo"] = $agencia_codigo;
$dadosboleto["nosso_numero"] = $nossonumero;
$dadosboleto["codigo_banco_com_dv"] = $codigo_banco_com_dv;

?>