<?php

//if (!function_exists('formata_numdoc')) {
//    function formata_numdoc($num, $tamanho)
//    {
//        while (strlen($num) < $tamanho) {
//            $num = "0" . $num;
//        }
//        return $num;
//    }
//}

// DADOS DO BOLETO PARA O SEU CLIENTE
$dias_de_prazo_para_pagamento = 7;
$taxa_boleto = 0;

$data_venc = $ret_cDividas[0]['data_vcto'];

$valor_cobrado = $ret_cDividas[0]['valor_vcto'];
$valor_cobrado = str_replace(",", ".", $valor_cobrado);
$valor_boleto = number_format($valor_cobrado + $taxa_boleto, 2, ',', '');

$agencia = $ret_cConfig[0]['boleto_agencia']; // Num da agencia, sem digito
$conta = $ret_cConfig[0]['boleto_cc']; // Num da conta, sem digito
$convenio = $ret_cConfig[0]['boleto_cedente'] . $ret_cConfig[0]['boleto_cedente_digito']; //Número do convênio indicado no frontend

$dadosboleto["nosso_numero"] = $nossoNumero . $digNossoNumero;

$dadosboleto["numero_documento"] = $ret_cDividas[0]['numero_documento'];    // Num do pedido ou do documento
$dadosboleto["data_vencimento"] = $data_venc; // Data de Vencimento do Boleto - REGRA: Formato DD/MM/AAAA
$dadosboleto["data_documento"] = date("d/m/Y"); // Data de emissão do Boleto
$dadosboleto["data_processamento"] = date("d/m/Y"); // Data de processamento do boleto (opcional)
$dadosboleto["valor_boleto"] = $valor_boleto;    // Valor do Boleto - REGRA: Com vírgula e sempre com duas casas depois da virgula

// DADOS DO SEU CLIENTE
$dadosboleto["sacado"] = $ret_cClientes[0]['grid_nome'];
$dadosboleto["endereco1"] = $ret_cClientes[0]['logradouro'] . ", " . $ret_cClientes[0]['numero'];
$dadosboleto["endereco2"] = $ret_cClientes[0]['cidade'] . " - " . $ret_cClientes[0]['uf'] . " -  CEP: " . $ret_cClientes[0]['cep'];

// INFORMACOES PARA O CLIENTE
$dadosboleto["demonstrativo1"] = "Cód. Dívida: " . $idDivida . " - Nº Parcela: " . $ret_cDividas[0]['numero_parcela'] . " - Nº Documento: " . $ret_cDividas[0]['numero_documento'];
$dadosboleto["demonstrativo2"] = "&nbsp;";
$dadosboleto["demonstrativo3"] = "";

// INSTRUÇÕES PARA O CAIXA
$dadosboleto["instrucoes1"] = $ret_cConfig[0]['boleto_mensagem1'];
$dadosboleto["instrucoes2"] = $ret_cConfig[0]['boleto_mensagem2'];
$dadosboleto["instrucoes3"] = $ret_cConfig[0]['boleto_mensagem3'];
$dadosboleto["instrucoes4"] = "";

// DADOS OPCIONAIS DE ACORDO COM O BANCO OU CLIENTE
$dadosboleto["quantidade"] = "1";
$dadosboleto["valor_unitario"] = $ret_cDividas[0]['valor_vcto'];
$dadosboleto["aceite"] = "N";
$dadosboleto["especie"] = "R$";
$dadosboleto["especie_doc"] = "DM";


// ---------------------- DADOS FIXOS DE CONFIGURAÇÃO DO SEU BOLETO --------------- //
// DADOS ESPECIFICOS DO SICOOB
$dadosboleto["modalidade_cobranca"] = "01";
$dadosboleto["numero_parcela"] = "001";
//$dadosboleto["numero_parcela"] = completaComZeros(3, $ret_cDividas[0]['numero_parcela']);

// DADOS DA SUA CONTA - BANCO SICOOB
$dadosboleto["agencia"] = $agencia; // Num da agencia, sem digito
$dadosboleto["conta"] = $conta; // Num da conta, sem digito

// DADOS PERSONALIZADOS - SICOOB
$dadosboleto["convenio"] = $convenio; // Num do convênio - REGRA: No máximo 7 dígitos
$dadosboleto["carteira"] = "1";

// SEUS DADOS
$dadosboleto["identificacao"] = $ret_cConfig[0]['empresa_nome'];
$dadosboleto["cpf_cnpj"] = $ret_cConfig[0]['empresa_cnpj'];
$dadosboleto["endereco"] = $ret_cConfig[0]['empresa_endereco'];
$dadosboleto["cidade_uf"] = $ret_cConfig[0]['empresa_cidade'] . " / " . $ret_cConfig[0]['empresa_uf'];
$dadosboleto["cedente"] = $ret_cConfig[0]['empresa_nome'];

?>